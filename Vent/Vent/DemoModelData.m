//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "DemoModelData.h"
#import "ChatSession.h"
#import "NSUserDefaults+DemoSettings.h"
#import "BaseRepository.h"
#import "ChatSessionRepository.h"
#import "AppUtility.h"
#import "ChatSessionViewController.h"

/**
 *  This is for demo/testing purposes only.
 *  This object sets up some fake model data.
 *  Do not actually do anything like this.
 */

@implementation DemoModelData

-(void)stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
//        if ([NSUserDefaults emptyMessagesSetting]) {
//            self.messages = [NSMutableArray new];
//        }
//        else {
//            [self loadFakeMessages];
//        }
        
        self.messages = [NSMutableArray new];

        /**
         *  Create avatar images once.
         *
         *  Be sure to create your avatars one time and reuse them for good performance.
         *
         *  If you are not using avatars, ignore this.
         */
        JSQMessagesAvatarImage *jsqImage = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"JSQ"
                                                                                      backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
                                                                                            textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
                                                                                                 font:[UIFont systemFontOfSize:14.0f]
                                                                                             diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        
        JSQMessagesAvatarImage *cookImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"demo_avatar_cook"]
                                                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        
        JSQMessagesAvatarImage *jobsImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"demo_avatar_jobs"]
                                                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        
        JSQMessagesAvatarImage *wozImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"demo_avatar_woz"]
                                                                                      diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        
        self.avatars = @{ kJSQDemoAvatarIdSquires : jsqImage,
                          kJSQDemoAvatarIdCook : cookImage,
                          kJSQDemoAvatarIdJobs : jobsImage,
                          kJSQDemoAvatarIdWoz : wozImage };
        
        
        self.users = @{ kJSQDemoAvatarIdJobs : kJSQDemoAvatarDisplayNameJobs,
                        kJSQDemoAvatarIdCook : kJSQDemoAvatarDisplayNameCook,
                        kJSQDemoAvatarIdWoz : kJSQDemoAvatarDisplayNameWoz,
                        kJSQDemoAvatarIdSquires : kJSQDemoAvatarDisplayNameSquires };
        

//        self.avatars = @{ kJSQDemoAvatarIdSquires : jsqImage,
//                          kJSQDemoAvatarIdCook : cookImage
//                          };
//        
//        
//        self.users = @{
//                        kJSQDemoAvatarIdCook : kJSQDemoAvatarDisplayNameCook,
//                        kJSQDemoAvatarIdSquires : kJSQDemoAvatarDisplayNameSquires };
        
        /**
         *  Create message bubble images objects.
         *
         *  Be sure to create your bubble images one time and reuse them for good performance.
         *
         */
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleBlueColor]];
    }
    
    return self;
}



//- (instancetype)initWithSender:(User*)sender Reciever:(Contacts*)reciever;
//{
//    self = [super init];
//    if (self) {
//        
//        //        if ([NSUserDefaults emptyMessagesSetting]) {
//        //            self.messages = [NSMutableArray new];
//        //        }
//        //        else {
//        //            [self loadFakeMessages];
//        //        }
//        
//        self.messages = [NSMutableArray new];
//        
//        
//        self.senderAvatar = sender;
//        self.recieverAvatar =reciever;
//        
//        /**
//         *  Create avatar images once.
//         *
//         *  Be sure to create your avatars one time and reuse them for good performance.
//         *
//         *  If you are not using avatars, ignore this.
//         */
//        JSQMessagesAvatarImage *jsqImage = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"ME"
//                                                                                      backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
//                                                                                            textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
//                                                                                                 font:[UIFont systemFontOfSize:14.0f]
//                                                                                             diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
//        
//        JSQMessagesAvatarImage *cookImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"demo_avatar_cook"]
//                                                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
//        
//       
//        if(self.recieverAvatar)
//        {
//        self.avatars = @{  self.senderAvatar.userPhoneNumber : jsqImage,
//                           self.recieverAvatar.contactID : cookImage};
//            
//            self.users = @{ self.senderAvatar.userPhoneNumber  : self.senderAvatar.userDisplayName,
//                            self.recieverAvatar.contactID : self.recieverAvatar.contactFullName };
//            
//        }
//        else
//        {
//            self.avatars = @{  self.senderAvatar.userPhoneNumber : jsqImage,
//                               @"G123456" : cookImage};
//            
//            self.users = @{ self.senderAvatar.userPhoneNumber  : self.senderAvatar.userDisplayName,
//                            @"G123456" : @"Group General" };
//            
//        }
//        
//
//        
//
//        /**
//         *  Create message bubble images objects.
//         *
//         *  Be sure to create your bubble images one time and reuse them for good performance.
//         *
//         */
//        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
//        
//        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
//        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleBlueColor]];
//    }
//    
//    return self;
//}


- (instancetype)initWithSenderAndReciever:(NSString*)senderId SenderName:(NSString*)senderName SenderImage:(NSData*)senderImage
                                 ReciverId:(NSString*)reciverId ReciverName:(NSString*)reciverName ReciverImage:(NSData*)reciverImage
{
    
    self = [super init];
    if (self) {
        
        
        self.locationManager = [[CLLocationManager alloc] init];
        
        self.locationManager.delegate = self;
        if([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
            NSUInteger code = [CLLocationManager authorizationStatus];
            if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
                // choose one request according to your business.
                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                    [self.locationManager requestAlwaysAuthorization];
                } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                    [self.locationManager  requestWhenInUseAuthorization];
                } else {
                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
                }
            }
        }
        [self.locationManager startUpdatingLocation];
        
        self.messages = [NSMutableArray new];

        self.senderAvatarID = senderId;
        self.senderAvatarName = senderName;
        
        if (reciverId == nil || [reciverId isEqualToString:@""])
        {
            BaseRepository *repositoryObj=[[BaseRepository alloc]init];
            reciverId = repositoryObj.uniqueUUID;
        }
            
        self.recieverAvatarID = reciverId;
        self.recieverAvatarName =reciverName;
        
        /**
         *  Create avatar images once.
         *
         *  Be sure to create your avatars one time and reuse them for good performance.
         *
         *  If you are not using avatars, ignore this.
         */
        JSQMessagesAvatarImage *senderAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:self.senderAvatarName
                                                                                      backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
                                                                                            textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
                                                                                                 font:[UIFont systemFontOfSize:14.0f]
                                                                                             diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        
        JSQMessagesAvatarImage *ReciverAvatarImage;
        
        if(reciverImage == nil)
        {
            ReciverAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"DefaultProfile"]
                                                                                           diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        }
        else
        {
            
        ReciverAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:reciverImage]
                                                                                       diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        }
        

            self.avatars = @{  self.senderAvatarID : senderAvatarImage,
                               self.recieverAvatarID : ReciverAvatarImage};
            
            self.users = @{ self.senderAvatarID : self.senderAvatarName,
                            self.recieverAvatarID : self.recieverAvatarName };


        
        /**
         *  Create message bubble images objects.
         *
         *  Be sure to create your bubble images one time and reuse them for good performance.
         *
         */
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleBlueColor]];
    }
    
    return self;

}


-(void)loadOldChatSessions:(NSArray*)oldData
{
    
    
   
    
    
     NSDateFormatter *formatter = [NSDateFormatter new];
      [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZ"];
    
    ChatSessionRepository *repo=[[ChatSessionRepository alloc]init];
    
     self.messages = [[NSMutableArray alloc]init];
    
    for (ChatSession* chatEntry in oldData) {
        
        NSError *error;
        
        chatEntry.chatSessionRead =[NSNumber numberWithBool:TRUE];
        [repo updateChatSession:chatEntry Error:&error];
       
      
        
        NSDate *date = [formatter dateFromString:chatEntry.chatSessionDeliveredDate];
        
        
        if ([chatEntry.chatSessionMyMessage isEqualToNumber:[NSNumber numberWithBool:TRUE]]) {
            
            // chat message send by me
            
            JSQMessage *reallyLongMessage;
            if ([chatEntry.chatSessionType isEqualToString:@"1"] ) {
                reallyLongMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                        displayName:self.senderAvatarName
                                                               date:date
                                                               text:chatEntry.chatSessionMessage
                                     ];
                
            }
            else  if ([chatEntry.chatSessionType isEqualToString:@"3"] ) {
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *fullpath;
                fullpath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", [[AppUtility sharedUtilityInstance]getMediaDirectory],chatEntry.chatSessionMessage]];
                //   fullpath = [fullpath stringByAppendingPathComponent:selectedSession.chatSessionMessage];
                //                NSURL* imageURL =[NSURL fileURLWithPath:fullpath];
                //    vedioURL =[NSURL fileURLWithPath:selectedSession.chatSessionMessage];
                
                UIImage *image=[UIImage imageWithContentsOfFile:fullpath];
                JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
                
                reallyLongMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                        displayName:self.senderAvatarName
                                                              media:photoItem
                                     ];
                
            }
            else  if ([chatEntry.chatSessionType isEqualToString:@"4"] ) {
                
                
                // don't have a real video, just pretending
                
                NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", chatEntry.chatSessionMessage]];
                
                JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
                reallyLongMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdSquires
                                                        displayName:kJSQDemoAvatarDisplayNameSquires
                                                              media:videoItem];
                
            }
            
            else  if ([chatEntry.chatSessionType isEqualToString:@"5"] ) {
                
                
                // don't have a real video, just pretending
                NSArray *locationArray=[ chatEntry.chatSessionMessage componentsSeparatedByString:@","];
                
                
                //                 CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]];
                
                CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:[locationArray[0] doubleValue] longitude:[locationArray[1] doubleValue]];
                
                //                 CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:29.988754480000001 longitude:31.282757799999999];
                
                JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
                //                [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{
                //
                //
                //                }];
                
                //  [locationItem setLocationSyn:ferryBuildingInSF];
                [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{
                    
                    ChatSessionViewController *chatVC=(ChatSessionViewController *)self.parentVC;
                    
                    
                    
                    
                    [chatVC.collectionView reloadData];
                }];
                
                
                
                reallyLongMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                        displayName:self.senderAvatarName
                                                              media:locationItem];
                
                
                
            }
            
            else if ([chatEntry.chatSessionType isEqualToString:@"6"] )
            {
                
                
                JSQVCardMediaItem *photoItem = [[JSQVCardMediaItem alloc] initWithImage:[UIImage imageNamed:@"DefaultProfile"] FullName:@"First Name" VCard:chatEntry.chatSessionMessage];
                
                
                photoItem.vcardString =chatEntry.chatSessionMessage;
                photoItem.fullName =@"First Name";
                
                reallyLongMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                        displayName:self.senderAvatarName
                                                              media:photoItem];
                
                
                
                
                
            }
            
            
            [self.messages addObject:reallyLongMessage];
        }
        else
        {
            
            // chat send by others
            JSQMessage *reallyLongMessage = [JSQMessage messageWithSenderId:self.recieverAvatarID
                                                                displayName:self.recieverAvatarName
                                                                       date:date
                                                                       text:chatEntry.chatSessionMessage];
            //TODO: Calling Mark as Read
            
            [self.messages addObject:reallyLongMessage];
            
        }
    }
    
    
    
    

    
}


- (void)addPhotoMediaMessage
{
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"Attachment"]];
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                   displayName:self.senderAvatarName
                                                         media:photoItem];
    

    
    [self.messages addObject:photoMessage];
}

- (void)addPhotoMediaMessage:(UIImage*)image
{
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                   displayName:self.senderAvatarName
                                                         media:photoItem];
    
    
    
    [self.messages addObject:photoMessage];
}


- (void)addVcardMediaMessage:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard
{
    JSQVCardMediaItem *photoItem = [[JSQVCardMediaItem alloc] initWithImage:[UIImage imageNamed:@"DefaultProfile"] FullName:fullName VCard:vcard];
    
    if (image != nil) {
        photoItem = [[JSQVCardMediaItem alloc] initWithImage:image FullName:fullName VCard:vcard];
    }

    
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                   displayName:self.senderAvatarName
                                                         media:photoItem];
    
    
    
    [self.messages addObject:photoMessage];
}


- (void)addLocationMediaMessageCompletion:(JSQLocationMediaItemCompletionBlock)completion
{
    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]];
    
    JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:completion];
    
    JSQMessage *locationMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
                                                      displayName:self.senderAvatarName
                                                            media:locationItem];
    [self.messages addObject:locationMessage];
}




- (void)addVideoMediaMessage
{
    // don't have a real video, just pretending
    NSURL *videoURL = [NSURL URLWithString:@"file://"];
    
    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
    JSQMessage *videoMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdSquires
                                                   displayName:kJSQDemoAvatarDisplayNameSquires
                                                         media:videoItem];
    [self.messages addObject:videoMessage];
}

- (void)addVideoMediaMessage:(NSString*)image
{
    
    // don't have a real video, just pretending
    NSURL *videoURL = [NSURL URLWithString:image];
    
    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
    JSQMessage *videoMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdSquires
                                                   displayName:kJSQDemoAvatarDisplayNameSquires
                                                         media:videoItem];
    [self.messages addObject:videoMessage];
}


-(NSString *)getFirstLastCharInName:(NSString *)fullname
{
    NSArray *array = [fullname componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *shortString =@"";
    
    if (array) {
        if (array.count >1)
        {
            shortString=[NSString stringWithFormat:@"%@%@",[[array[0] substringToIndex:1]uppercaseString],[[array[1] substringToIndex:1] uppercaseString]];
        }
        else
        {
            shortString=[NSString stringWithFormat:@"%@",[[array[0] substringToIndex:1]uppercaseString]];
        }
    }
    
    
    return shortString;
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
   // NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
       self.longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        self.latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        [self.locationManager stopUpdatingLocation];
        
        
//        CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude];
//        
//        JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
//        [locationItem setLocation:ferryBuildingInSF withCompletionHandler:nil];
//        
//        JSQMessage *locationMessage = [JSQMessage messageWithSenderId:self.senderAvatarID
//                                                          displayName:self.senderAvatarName
//                                                                media:locationItem];
//        [self.messages addObject:locationMessage];
//        
//        [self.locationManager stopUpdatingLocation];
        
    }
    
   
    
}

@end
