//
//  BaseContactsTableView.h
//  Vent
//
//  Created by Sameh Farouk on 3/10/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactListDelegate.h"

@class Contacts;
@class ContactsTableViewCell;

extern NSString *const kCellIdentifier;

@interface BaseContactsTableView : UITableViewController

@property (nonatomic, assign) id<ContactListDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *selectedContacts;

@end
