//
//  Favourite.m
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "Favourite.h"
#import "User.h"


@implementation Favourite

@dynamic favouriteID;
@dynamic favouriteType;
@dynamic favouriteChatListID;
@dynamic favouriteGroupListID;
@dynamic favouriteContactID;
@dynamic favouriteOwner;

@end
