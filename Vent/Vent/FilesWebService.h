//
//  FilesWebService.h
//  Vent
//
//  Created by Medhat Ali on 3/8/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "CommonWebService.h"
#import "FileGetDM.h"

@interface FilesWebService : CommonWebService

- (NSData *)downLoadFile:(NSString *)fileName Type:(StoreType) storeType Error:(NSError**)error;
- (NSDictionary *)uploadImage:(NSData *)image ImageName:(NSString *)imageName ImageExt:(NSString *)imageExt Error:(NSError**)error;
- (NSDictionary *)uploadVideo:(NSData *)video VideoName:(NSString *)videoName VideoExt:(NSString *)videoExt Error:(NSError**)error;
@end
