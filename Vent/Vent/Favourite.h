//
//  Favourite.h
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Favourite : NSManagedObject

@property (nonatomic, retain) NSString * favouriteID;
@property (nonatomic, retain) NSString * favouriteType;
@property (nonatomic, retain) NSString * favouriteChatListID;
@property (nonatomic, retain) NSString * favouriteGroupListID;
@property (nonatomic, retain) NSString * favouriteContactID;

@property (nonatomic, retain) User *favouriteOwner;

@end
