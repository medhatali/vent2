//
//  SearchResultsTableViewViewController.m
//  Vent
//
//  Created by Medhat Ali on 3/18/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "SearchResultsTableViewViewController.h"
#import "ChatListTableViewCell.h"

@interface SearchResultsTableViewViewController ()

@end

@implementation SearchResultsTableViewViewController

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self filteredResults] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     ChatListTableViewCell *cell = nil;
    
      cell = [tableView dequeueReusableCellWithIdentifier:@"ChatListCell" forIndexPath:indexPath];
    
    return cell;
}



@end
