//
//  MessageTimeDM.h
//  Vent
//
//  Created by Sameh Farouk on 5/26/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "DataModelObjectParent.h"

@interface MessageTimeDM : DataModelObjectParent

@property (strong,nonatomic) NSString *messageId;
@property (strong,nonatomic) NSDate *messageTime;



- (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary;

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields;

@end
