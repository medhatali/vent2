//
//  FavouriteListTableViewController.m
//  Vent
//
//  Created by Medhat Ali on 4/5/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "FavouriteListTableViewController.h"
#import "MainContactsTableView.h"
#import "SearchResultsTableViewViewController.h"
#import "ContactsRepository.h"
#import "ChatListRepository.h"
#import "ContactsBussinessService.h"

@interface FavouriteListTableViewController ()

@property (nonatomic,strong) NSMutableArray *searchResults;
@property (nonatomic,strong) NSString *sortOption;
@property (nonatomic,strong) ChatListRepository * chatListRepo;


@end

@implementation FavouriteListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self configNavigationBarItems:NO];
    
    self.topView.lblChatOwner.text = [NSString stringWithFormat:NSLocalizedString(@"Favourite List", nil)] ;
    
    [self configureVerticalMenu];
    self.verticalMenu.delegate = self;
    
}

-(void)reloadData
{
    // get chat lsit from DB
    NSError *error;
    self.chatListRepo=[[ChatListRepository alloc]init];
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    if (self.sortOption == Nil) {
        self.chatList =[ self.chatListRepo getUserFavouriteChatListSorted:currentUser SortOption:sortDefault Error:&error];
    }
    else if ([self.sortOption isEqualToString:sortDefault])
    {
        self.chatList =[ self.chatListRepo getUserFavouriteChatListSorted:currentUser SortOption:sortDefault Error:&error];
    }
    else if ([self.sortOption isEqualToString:sortAtoZ])
    {
        self.chatList =[ self.chatListRepo getUserFavouriteChatListSorted:currentUser SortOption:sortAtoZ Error:&error];
    }
    else if ([self.sortOption isEqualToString:sorttype])
    {
        self.chatList =[ self.chatListRepo getUserFavouriteChatListSorted:currentUser SortOption:sorttype Error:&error];
    }
    
    [self.tableView reloadData];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[[self navigationController]navigationBar] setHidden:FALSE];
    
     [self reloadData];
    
     [self getUnreadMessageForTabBar];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    //scroll to first row
    //    [self.tableView scrollRectToVisible:CGRectMake(0, 200, 210, 210) animated:NO];
    
    [self.tableView setContentOffset:CGPointMake(0, 1) animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // chat list count + header cell
    return (tableView == [self tableView]) ? self.chatList.count+1: self.searchResults.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        return 78;
    }
    
    static NSString *CellIdentifier = @"ChatListTVCell";
    
    ChatListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    return cell.frame.size.height;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ChatListTVCell";
    ChatListTableViewCell *cell = nil;
    
    if (tableView == [self tableView]) {
        
        if (indexPath.row == 0) {
            
            ChatListHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];
            // add search to header
            
            [cell addSubview:self.searchController.searchBar];
            
            return cell;
        }
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ChatListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        ChatList *selectModel=((ChatList*)[self.chatList objectAtIndex:indexPath.row -1]);
        [cell updateCellDataFromModel:selectModel];
        
        //        if ([selectModel.chatIsFavourite isEqualToNumber:[NSNumber numberWithBool:YES]]) {
        //            cell.rightUtilityButtons = [self rightSwipButtons:1];
        //        }
        //        else
        //        {
        //            cell.rightUtilityButtons = [self rightSwipButtons:2];
        //        }
        
        
        
    }else{
        
        
        static NSString *CellIdentifier = @"ChatListTVCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ChatListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
    }
    
    
    // Configure the cell...
    //add swip option
    if ([[AppUtility sharedUtilityInstance]getCurrentLanguage] == 1) {
        // RTL
        cell.leftUtilityButtons = [self rightSwipButtonsFavourite:2];
        
    }
    else
    {
       cell.rightUtilityButtons = [self rightSwipButtonsFavourite:2];
    }
    
    
    cell.delegate =self;
    
    return cell;
    
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    if (tableView == [self tableView]) {
        
        if (indexPath.row == 0) {
            return NO;
        }
        else
        {
            if (self.enableTableEdit) {
                return YES;
            }
        }
        
    }
    else
    {
        return YES;
    }
    return NO;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        // unfavourite the selected row
        ChatList* selectedList=(ChatList*)[self.chatList objectAtIndex:indexPath.row -1];
        selectedList.chatIsFavourite = [NSNumber numberWithBool:NO];
        NSError *error;
        [ self.chatListRepo updateChatList:selectedList Error:&error];
        
        [self reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    
    
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    if (tableView == [self tableView]) {
        
        if (indexPath.row == 0) {
            return NO;
        }
        else
        {
            return YES;
        }
        
    }
    else
    {
        return YES;
    }
    return NO;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatList *selectModel=((ChatList*)[self.chatList objectAtIndex:indexPath.row -1]);
    
    [self selectChatsession:selectModel];
    //[self selectChatsession:nil];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//#pragma mark - UISearchResultsUpdating
//
//- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
//    // update the filtered array based on the search text
//    NSString *searchText = searchController.searchBar.text;
////    NSMutableArray *searchResults = [NSMutableArray arrayWithCapacity:[[[self dataSource] contacts] count]];
////
////    /*
////     Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
////     */
////    for (NSArray *section in self.contatcs) {
////        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@ OR phone contains[c] %@",searchText,searchText];
////        [searchResults addObjectsFromArray: [section filteredArrayUsingPredicate:predicate]];
////    }
//
//    // hand over the filtered results to our search results table
//    SearchResultsTableViewViewController *tableController = (SearchResultsTableViewViewController *)self.searchController.searchResultsController;
//    //tableController.filteredContacts = searchResults;
//    [tableController.tableView reloadData];
//}






#pragma mark - FCVerticalMenu Delegate Methods

-(void)menuWillOpen:(FCVerticalMenu *)menu
{
    
    //NSLog(@"menuWillOpen hook");
}

-(void)menuDidOpen:(FCVerticalMenu *)menu
{
    //NSLog(@"menuDidOpen hook");
}

-(void)menuWillClose:(FCVerticalMenu *)menu
{
    // NSLog(@"menuWillClose hook");
    // NSLog(@"top menu click %@  and index = %i",self.selectedTopMenu,self.selectedTopMenuIndex);
    
    if (self.selectedTopMenuIndex == 1) {
        [self configNavigationBarItems:YES];
    }
    else if (self.selectedTopMenuIndex == 2) {
        [self.tableView setEditing:NO animated:YES];
        self.sortOption = sortDefault;
        
    }
    else if (self.selectedTopMenuIndex == 3) {
        [self.tableView setEditing:NO animated:YES];
        self.sortOption = sortAtoZ;
    }
    else if (self.selectedTopMenuIndex == 4) {
        [self.tableView setEditing:NO animated:YES];
        self.sortOption = sorttype;
    }
    
    self.selectedTopMenuIndex =0;
    [self reloadData];
    
}

-(void)menuDidClose:(FCVerticalMenu *)menu
{
    //NSLog(@"menuDidClose hook");
}


#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"check button was pressed");
            break;
        case 1:
            NSLog(@"clock button was pressed");
            break;
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
        {
            //NSLog(@"un favourite button was pressed");
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            ChatList* selectedList=(ChatList*)[self.chatList objectAtIndex:cellIndexPath.row -1];
            selectedList.chatIsFavourite = [NSNumber numberWithBool:NO];
            NSError *error;
            [ self.chatListRepo updateChatList:selectedList Error:&error];
            
             [self reloadData];
            break;
        }
        case 1:
        {
            // Delete button was pressed
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            
            //[self.tableView deleteRowsAtIndexPaths:@[cellIndexPath]
            //                      withRowAnimation:UITableViewRowAnimationAutomatic];
            
            ChatList *selectModel=((ChatList*)[self.chatList objectAtIndex:cellIndexPath.row -1]);
            NSError *error;
            [self.chatListRepo deleteChatList:selectModel Error:&error];
            [self reloadData];
            
            break;
        }
        default:
            break;
    }
}

#pragma mark - BaseSearchSelectionDelegate

- (SelectionType)selectionType{
    
    return kSingleSelect;
}
- (NSMutableArray*) searchResultsForSearchText:(NSString *)searchText{
    
    // update the filtered array based on the search text
    _searchResults = [NSMutableArray arrayWithCapacity:[[self chatList] count]];
    
    /*
     Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
     */
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@ ",searchText];
    [_searchResults addObjectsFromArray: [[self chatList] filteredArrayUsingPredicate:predicate]];
    
    return _searchResults;
}

- (UITableViewCell*) resultsCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"ChatListTVCell";
    
    ChatListTableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[ChatListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    return cell;
    
}



#pragma mark - Contact Selection


-(void)didSelectContacts:(Contacts *)contacts
{
    NSError *error;
    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
    
    NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:contacts Error:&error];
    
    NSLog(@"contact selection %@",ventphonenumber);
    
    
    ChatListRepository * chatlistRepo=[[ChatListRepository alloc]init];
    
    NSArray *phonelist=[[NSArray alloc]initWithObjects:ventphonenumber, nil];
    NSString *currentdate=[NSString stringWithFormat:@"%@" ,[NSDate date]];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    
    [chatlistRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Chat" chatLastUpdatedDate:currentdate  chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:NO] chatID:@"" chatName:contacts.contactFullName chatLastMessage:@"" chatUserImage:contacts.contactImage chatUserImageUrl:@"" chatFromPhone:currentUser.userPhoneNumber  chatToPhone:ventphonenumber  chatIsFavourite:[NSNumber numberWithBool:YES]  chatListOwner:currentUser chatListSession:Nil chatListUsers:phonelist chatIsSecure:[NSNumber numberWithBool:NO] Error:&error];
    
    [self reloadData];
}

-(void)didSelectContactsList:(NSArray *)contactsList
{
    NSLog(@"contacts list selection for conversation ");
}

@end
