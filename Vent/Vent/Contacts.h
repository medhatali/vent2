//
//  Contacts.h
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ContactPhones, User;

@interface Contacts : NSManagedObject

@property (nonatomic, retain) NSString * contactFirstName;
@property (nonatomic, retain) NSString * contactFullName;
@property (nonatomic, retain) NSString * contactID;
@property (nonatomic, retain) NSData * contactImage;
@property (nonatomic, retain) NSString * contactImageUrl;
@property (nonatomic, retain) NSNumber * contactIsVentUser;
@property (nonatomic, retain) NSString * contactLastName;
@property (nonatomic, retain) NSString * contactMiddleName;
@property (nonatomic, retain) NSNumber * contactIsOnline;
@property (nonatomic, retain) NSNumber * contactIsBlocked;
@property (nonatomic, retain) NSSet *contactPhones;
@property (nonatomic, retain) User *contactUser;
@end

@interface Contacts (CoreDataGeneratedAccessors)

- (void)addContactPhonesObject:(ContactPhones *)value;
- (void)removeContactPhonesObject:(ContactPhones *)value;
- (void)addContactPhones:(NSSet *)values;
- (void)removeContactPhones:(NSSet *)values;

@end
