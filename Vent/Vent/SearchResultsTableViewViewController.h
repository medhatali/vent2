//
//  SearchResultsTableViewViewController.h
//  Vent
//
//  Created by Medhat Ali on 3/18/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "MainMasterTableTVC.h"

@interface SearchResultsTableViewViewController : MainMasterTableTVC


@property (nonatomic, strong) NSMutableArray *filteredResults;

@end
