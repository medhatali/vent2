//
//  JSQMessagesOtherToolbarContentView.m
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "JSQMessagesOtherToolbarContentView.h"

@implementation JSQMessagesOtherToolbarContentView


+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([JSQMessagesOtherToolbarContentView class])
                          bundle:[NSBundle bundleForClass:[JSQMessagesOtherToolbarContentView class]]];
}

#pragma mark - Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(OrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self calculateButtonWidth];
  //  [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    
   // self.backgroundColor = [UIColor clearColor];
}



#pragma mark - Setters

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    //[super setBackgroundColor:backgroundColor];
  
}

#pragma mark - UIView overrides

- (void)setNeedsDisplay
{
   // [super setNeedsDisplay];
}

#pragma -mark orientation change


-(void)OrientationDidChange:(NSNotification*)notification
{
    UIDeviceOrientation Orientation=[[UIDevice currentDevice]orientation];
    
    if(Orientation==UIDeviceOrientationLandscapeLeft || Orientation==UIDeviceOrientationLandscapeRight)
    {
    }
    else if(Orientation==UIDeviceOrientationPortrait)
    {
    }
    
    [self calculateButtonWidth];
    
}

-(void)calculateButtonWidth
{
//    CGFloat width = [UIScreen mainScreen].bounds.size.width;
//    
//    for (NSLayoutConstraint *wConstraint in self.buttonWidthConstarint) {
//       wConstraint.constant = (width / 5 ) - 10;
//    }
    
}

- (IBAction)otherToolbarPressed:(id)sender {
    
    // content view 
    UIButton *selectedButoon=(UIButton*)sender;
    [self.otherToolBarDelegate toolBarButtonPressed:sender ButtonIndex:selectedButoon.tag ];
}


@end
