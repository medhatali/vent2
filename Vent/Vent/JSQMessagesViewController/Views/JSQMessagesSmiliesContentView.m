//
//  JSQMessagesSmiliesContentView.m
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "NSString+JSQMessages.h"
#import "JSQMessagesSmiliesContentView.h"
#import "SmilesCollectionViewDataSource.h"
#import "SmilyLabelCollectionViewCell.h"
#import "SmilyCellCollectionViewCell.h"
#import "SmilyIconViewCell.h"

@implementation JSQMessagesSmiliesContentView

+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([JSQMessagesSmiliesContentView class])
                          bundle:[NSBundle bundleForClass:[JSQMessagesSmiliesContentView class]]];
}

#pragma mark - Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UINib *cellNib = [UINib nibWithNibName:@"SmilyCellCollectionViewCell" bundle:nil];
    [self.smiliesCollection registerNib:cellNib forCellWithReuseIdentifier:@"smilCell"];
    
    UINib *cellNib2 = [UINib nibWithNibName:@"SmilyLabelCollectionViewCell" bundle:nil];
    [self.smiliesCategoriesLabel registerNib:cellNib2 forCellWithReuseIdentifier:@"SmilyLabel"];
    
    UINib *cellNib3 = [UINib nibWithNibName:@"SmilyIconViewCell" bundle:nil];
    [self.smiliesCategorieIcons registerNib:cellNib3 forCellWithReuseIdentifier:@"IconCell"];
    
    
    SmilesCollectionViewDataSource *smilesCollectionDataSource = [SmilesCollectionViewDataSource sharedInstance];
    [self setDataSource:[[smilesCollectionDataSource.smilyEmoji objectForKey:@"Types"] objectForKey:@"Type"]];
    
    
 
    
}






#pragma mark - Setters

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    //[super setBackgroundColor:backgroundColor];
    
}

#pragma mark - UIView overrides

- (void)setNeedsDisplay
{
     [super setNeedsDisplay];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

//    if (collectionView == self.smiliesCategoriesLabel) {
//        return 5;
//    }
//    else if (collectionView == self.smiliesCollection){
//        return 5;
//    }
//    else if (collectionView == self.smiliesCategorieIcons){
//        return 5;
//    }
    
    return [[self dataSource] count];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger sectionCount=[[[[self dataSource] objectAtIndex:section] objectForKey:@"Emoji"] count];

    
    if (collectionView == self.smiliesCategoriesLabel) {
        //return 1;
        return sectionCount;
    }
    else if (collectionView == self.smiliesCollection){
        return sectionCount;
    }
    else if (collectionView == self.smiliesCategorieIcons){
       return 1;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *mCell ;
    
   // mCell.backgroundColor = [UIColor lightGrayColor];
    

    
    if (collectionView == self.smiliesCategoriesLabel) {
        
        SmilyLabelCollectionViewCell *lCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SmilyLabel" forIndexPath:indexPath];
        
        [[lCell typeLable] setText:[[[self dataSource] objectAtIndex:indexPath.section] objectForKey:@"Name"]];
        
        mCell = (UICollectionViewCell *) lCell;
        
        
    }
    else if (collectionView == self.smiliesCollection){
        SmilyCellCollectionViewCell *sCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"smilCell" forIndexPath:indexPath];
        
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:[[[[[self dataSource]objectAtIndex:indexPath.section] objectForKey:@"Emoji"] objectAtIndex:indexPath.row] objectForKey:@"Name"],@".png"]];
        
        [[sCell imageView] setImage:image];
        
        sCell.tag = indexPath.section;
        
        mCell = (UICollectionViewCell *)sCell;

    }
    else if (collectionView == self.smiliesCategorieIcons){
        SmilyIconViewCell *iCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IconCell" forIndexPath:indexPath];

        mCell = iCell;
        
    }
    
    
    return mCell;
}



#pragma mark Collection view Layout things
// Layout set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
  //  NSLog(@"Settings Size for Item at index %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(40, 40);
    if (collectionView == self.smiliesCategoriesLabel) {
        mElementSize = CGSizeMake(700 + self.smiliesCategoriesLabel.frame.size.width, 20);
    }
    if (collectionView == self.smiliesCategorieIcons) {
        mElementSize = CGSizeMake(20, 20);
    }
    return mElementSize;
}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 2.0;
//}
//
//-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//    return 2.0;
//}
//
//// Layout: Set Edgees of whole screen - not mElementsize
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0, 0, 0, 0);
//}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView == self.smiliesCollection)
    {
//        CGFloat width = scrollView.frame.size.width;
//        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
       // int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;

        NSInteger maxSection=0;
       
    for (UICollectionViewCell *cell in self.smiliesCollection.visibleCells) {
        
        //TODO: Don’t use tags.
     // NSLog(@"scroll to page %ld , and number of cells %d in section %ld" ,(long)page , (int)[self.smiliesCollection.visibleCells count],(long)cell.tag);
        
       // maxSection =MAX(maxSection, cell.tag) ;
         maxSection = cell.tag ;
    }
        
        [self.smiliesCategoriesLabel scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:maxSection ] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        [self.smiliesCategorieIcons scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:maxSection] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        
       //  [self.smiliesCategorieIcons selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:maxSection] animated:NO scrollPosition:UICollectionViewScrollPositionRight];
        
        //[self.smiliesCategorieIcons  reloadData];
    }
    
//    if (scrollView == self.smiliesCategoriesLabel) {
//      //NSLog(@"scroll smiles label ");
//    }
//    else if (scrollView == self.smiliesCollection)
//    {
//       
//       // NSLog(@"scroll smiles");
//        CGFloat pageWidth = self.smiliesCollection.frame.size.width -40;
//    
//        NSInteger index =self.smiliesCollection.contentOffset.x / pageWidth;
//        
//        NSLog(@"smily collection offset= %f current page = %ld",self.smiliesCollection.contentOffset.x,(long)index);
//       
//         //self.pageControl.currentPage = [[[self.smiliesCollection indexPathsForVisibleItems] firstObject] row];
//        
//       // NSArray *visibleItems = [self.smiliesCollection indexPathsForVisibleItems];
////        NSIndexPath *indxpth = [[self.smiliesCategoriesLabel indexPathsForVisibleItems] firstObject];
//        
//        // NSIndexPath *indxpth=[[NSIndexPath alloc]initWithIndex:<#(NSUInteger)#>];
//
////        if (0 <= index < 5) {
//        [self.smiliesCategoriesLabel scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index ] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
////        [self.smiliesCategorieIcons scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
////        }
//
//        
//    }
//

    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.smiliesCollection)
    {
    NSLog(@"emoji selected");
    
    if([_delegate respondsToSelector:@selector(smilySelected:)])
        
//        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:[[[[[self dataSource]objectAtIndex:indexPath.section] objectForKey:@"Emoji"] objectAtIndex:indexPath.row] objectForKey:@"Name"],@".png"]];
    
        
        [[self delegate] smilySelected:[[[[self dataSource]objectAtIndex:indexPath.section] objectForKey:@"Emoji"] objectAtIndex:indexPath.row] ];
    }
    
    if (collectionView == self.smiliesCategorieIcons) {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor grayColor];
    }
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.smiliesCollection)
    {

    }

     if (collectionView == self.smiliesCategorieIcons) {
         UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
         cell.contentView.backgroundColor = [UIColor clearColor];
     }
}

- (IBAction)backspaceClicked:(id)sender {
    NSLog(@"JSQMessagesSmiliesContentView => backspace Clicked");
    if([_delegate respondsToSelector:@selector(removeLastEmoji)])
        [[self delegate] removeLastEmoji];
}


//- (BOOL)collectionView:(UICollectionView *)collectionView
//shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
//
//- (BOOL)collectionView:(UICollectionView *)collectionView
//shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
//{
//    return YES;
//}

//
//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
//    UICollectionReusableView *reusableview = nil;
//    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 30)];
//    lbl.text =@"medhat";
//    [reusableview addSubview:lbl];
//    return reusableview;
//}

//
//-(void)OrientationDidChange:(NSNotification*)notification
//{
//    UIDeviceOrientation Orientation=[[UIDevice currentDevice]orientation];
//    
//    if(Orientation==UIDeviceOrientationLandscapeLeft || Orientation==UIDeviceOrientationLandscapeRight)
//    {
//    }
//    else if(Orientation==UIDeviceOrientationPortrait)
//    {
//    }
//
//    
//    [self addConstraint:[NSLayoutConstraint constraintWithItem: self.mainView
//                                                     attribute:NSLayoutAttributeLeft
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeLeft
//                                                    multiplier:1.0
//                                                      constant:0.0]];
//    
//    [self addConstraint:[NSLayoutConstraint constraintWithItem: self.mainView
//                                                     attribute:NSLayoutAttributeRight
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeRight
//                                                    multiplier:1.0
//                                                      constant:0.0]];
//    
//    [self addConstraint:[NSLayoutConstraint constraintWithItem: self.mainView
//                                                     attribute:NSLayoutAttributeTop
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeTop
//                                                    multiplier:1.0
//                                                      constant:0.0]];
//    
//    [self addConstraint:[NSLayoutConstraint constraintWithItem: self.mainView
//                                                     attribute:NSLayoutAttributeBottom
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeBottom
//                                                    multiplier:1.0
//                                                      constant:0.0]];
//
//
//    
//}

@end
