//
//  JSQMessagesSmiliesContentView.h
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmilySelectionProtocol.h"

@interface JSQMessagesSmiliesContentView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *smiliesCollection;
@property (strong, nonatomic) IBOutlet UICollectionView *smiliesCategoriesLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *smiliesCategorieIcons;
@property (nonatomic, strong) id <SmilySelectionProtocol> delegate;
@property (nonatomic) id dataSource;
@property (strong, nonatomic) IBOutlet UIView *mainView;



@property (weak, nonatomic) IBOutlet UIButton *backspaceButton;



@property (weak, nonatomic) IBOutlet UICollectionViewCell *smilyCell;
+ (UINib *)nib;

@end
