//
//  UpdateChatCellDelgate.h
//  Vent
//
//  Created by Medhat Ali on 11/10/15.
//  Copyright © 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UpdateChatCellProtocol <NSObject>

@required
-(void)UpdateChatCellAtIndex:(NSIndexPath *)indexPath;


@end
