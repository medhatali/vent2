//
//  SmilyIconViewCell.h
//  Vent
//
//  Created by Sameh Farouk on 6/2/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmilyIconViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconImage;

@end
