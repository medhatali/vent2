//
//  ChatTopBarView.m
//  Vent
//
//  Created by Medhat Ali on 6/30/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatTopBarView.h"

@implementation ChatTopBarView

-(void)awakeFromNib
{
    [self.connectionIndicator setHidden:TRUE];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)selectChatTitle:(id)sender {
    
     [self.delegate selecTopChatHeader:sender];
}

@end
