//
//  JSQMessagesSmiliesControl.m
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "JSQMessagesSmiliesControl.h"

@implementation JSQMessagesSmiliesControl



- (void)awakeFromNib
{
    // [super awakeFromNib];
    // [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
   // self.preferredDefaultHeight = 44.0f;
    
       
    JSQMessagesSmiliesContentView *smilyContentView = [self loadSmilyContentView];
    
    smilyContentView.delegate =self;
   // self.delegate =smilyContentView.delegate;
   // smilyContentView.frame = self.frame;
    
    
    [self addSubview:smilyContentView];
    
    smilyContentView.translatesAutoresizingMaskIntoConstraints = NO;
    

    
    [self addConstraint:[NSLayoutConstraint constraintWithItem: smilyContentView
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem: smilyContentView
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeRight
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem: smilyContentView
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem: smilyContentView
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:0.0]];
    
   
   //  [self setNeedsUpdateConstraints];
    _contentView = smilyContentView;
    


    
}

- (JSQMessagesSmiliesContentView *)loadSmilyContentView
{
    NSArray *nibViews = [[NSBundle bundleForClass:[JSQMessagesSmiliesContentView class]] loadNibNamed:NSStringFromClass([JSQMessagesSmiliesContentView class])
                                                                                                     owner:nil
                                                                                                   options:nil];
    return nibViews.firstObject;

    
}


-(void)smilySelected:(NSDictionary*) emoji
{
    if([_delegate respondsToSelector:@selector(smilySelected:)])
    {
        
        [[self delegate] smilySelected:emoji];
    }

}

-(void)removeLastEmoji
{
    if([_delegate respondsToSelector:@selector(smilySelected:)])
    {
        
        [[self delegate] removeLastEmoji];
    }
}




@end
