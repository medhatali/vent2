//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "JSQMessagesComposerTextView.h"

#import <QuartzCore/QuartzCore.h>

#import "NSString+JSQMessages.h"
#import "SmilesCollectionViewDataSource.h"

@interface JSQMessagesComposerTextView ()


- (void)jsq_configureTextView;

- (void)jsq_addTextViewNotificationObservers;
- (void)jsq_removeTextViewNotificationObservers;
- (void)jsq_didReceiveTextViewNotification:(NSNotification *)notification;

- (NSDictionary *)jsq_placeholderTextAttributes;

@end



@implementation JSQMessagesComposerTextView

#pragma mark - Initialization

- (void)jsq_configureTextView
{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    CGFloat cornerRadius = 6.0f;
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.cornerRadius = cornerRadius;
    
    self.scrollIndicatorInsets = UIEdgeInsetsMake(cornerRadius, 0.0f, cornerRadius, 0.0f);
    
    self.textContainerInset = UIEdgeInsetsMake(4.0f, 2.0f, 4.0f, 2.0f);
    self.contentInset = UIEdgeInsetsMake(1.0f, 0.0f, 1.0f, 0.0f);
    
    self.scrollEnabled = YES;
    self.scrollsToTop = NO;
    self.userInteractionEnabled = YES;
    
    self.font = [UIFont systemFontOfSize:16.0f];
    self.textColor = [UIColor blackColor];
    self.textAlignment = NSTextAlignmentNatural;
    
    self.contentMode = UIViewContentModeRedraw;
    self.dataDetectorTypes = UIDataDetectorTypeNone;
    self.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.keyboardType = UIKeyboardTypeDefault;
    self.returnKeyType = UIReturnKeyDefault;
    
    self.text = nil;
    
    _placeHolder = nil;
    _placeHolderTextColor = [UIColor lightGrayColor];
    
    [self jsq_addTextViewNotificationObservers];
}

- (instancetype)initWithFrame:(CGRect)frame textContainer:(NSTextContainer *)textContainer
{
    self = [super initWithFrame:frame textContainer:textContainer];
    if (self) {
        [self jsq_configureTextView];
      
      
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self jsq_configureTextView];
}

- (void)dealloc
{
    [self jsq_removeTextViewNotificationObservers];
    _placeHolder = nil;
    _placeHolderTextColor = nil;
}

#pragma mark - Composer text view

- (BOOL)hasText
{
    //return ([[self.text jsq_stringByTrimingWhitespace] length] > 0);
    return ([self.detailText length] > 0)  ;

}

#pragma mark - Setters

- (void)setPlaceHolder:(NSString *)placeHolder
{
    if ([placeHolder isEqualToString:_placeHolder]) {
        return;
    }
    
    _placeHolder = [placeHolder copy];
    [self setNeedsDisplay];
}

- (void)setPlaceHolderTextColor:(UIColor *)placeHolderTextColor
{
    if ([placeHolderTextColor isEqual:_placeHolderTextColor]) {
        return;
    }
    
    _placeHolderTextColor = placeHolderTextColor;
    [self setNeedsDisplay];
}

#pragma mark - UITextView overrides

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self setNeedsDisplay];
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    [super setAttributedText:attributedText];
    [self setNeedsDisplay];
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    [self setNeedsDisplay];
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    [super setTextAlignment:textAlignment];
    [self setNeedsDisplay];
}

#pragma mark - Drawing

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if ([self.text length] == 0 && self.placeHolder) {
        [self.placeHolderTextColor set];
        
        [self.placeHolder drawInRect:CGRectInset(rect, 7.0f, 5.0f)
                      withAttributes:[self jsq_placeholderTextAttributes]];
    }
}

#pragma mark - Notifications

- (void)jsq_addTextViewNotificationObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(jsq_didReceiveTextViewNotification:)
                                                 name:UITextViewTextDidChangeNotification
                                               object:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(jsq_didReceiveTextViewNotification:)
                                                 name:UITextViewTextDidBeginEditingNotification
                                               object:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(jsq_didReceiveTextViewNotification:)
                                                 name:UITextViewTextDidEndEditingNotification
                                               object:self];
}

- (void)jsq_removeTextViewNotificationObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidChangeNotification
                                                  object:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidBeginEditingNotification
                                                  object:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidEndEditingNotification
                                                  object:self];
}

- (void)jsq_didReceiveTextViewNotification:(NSNotification *)notification
{
    [self setNeedsDisplay];
}

#pragma mark - Utilities

- (NSDictionary *)jsq_placeholderTextAttributes
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = self.textAlignment;
    
    return @{ NSFontAttributeName : self.font,
              NSForegroundColorAttributeName : self.placeHolderTextColor,
              NSParagraphStyleAttributeName : paragraphStyle };
}



#pragma mark nsattribute and string processing


-(NSAttributedString*)parseStringtoAttributeString:(NSString*)baseString
{
  SmilesCollectionViewDataSource *smilesCollectionDataSource = [SmilesCollectionViewDataSource sharedInstance];
    NSString *replaced;
    NSMutableString *formatedResponse = [NSMutableString string];
    NSMutableAttributedString *baseSmilyString=[[NSMutableAttributedString alloc]init];
    
    NSScanner *emotionScanner = [NSScanner scannerWithString:baseString];
    [emotionScanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@""]];
    while ([emotionScanner isAtEnd] == NO) {
        
        if([emotionScanner scanUpToString:@":0x" intoString:&replaced]) {
            [formatedResponse appendString:replaced];
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:replaced];
            [baseSmilyString appendAttributedString:attributedString];
            
        }
        if(![emotionScanner isAtEnd]) {
            [emotionScanner scanString:@":0x" intoString:nil];
            replaced = @"";
            [emotionScanner scanUpToString:@" " intoString:&replaced];
            //TODOif replace contains '[' then reset scanner location to this newer '[' and scan again.
            //            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", replaced]];
            //            if (img) {
            //                NSString *base64Image = [UIImagePNGRepresentation(img) base64String];
            //                [formatedResponse appendFormat:@"<img src='data:image/png;base64,%@' />", base64Image];
            NSString *unicode=[NSString stringWithFormat:@":0x%@",replaced];
            NSString *em = [smilesCollectionDataSource.smilyCodes valueForKey:unicode];
            if (em) {
                
                UIImage * pic= [UIImage imageNamed:em];
                
                UIGraphicsBeginImageContext(CGSizeMake(40, 40));
                [pic drawInRect:CGRectMake(0, 0, 40, 40)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                [attachment setImage:newImage];
                
                
                NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:attachment];
                
                 // NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formatedResponse];
                
                //[baseSmilyString setAttributedString: attributedString];
                // [baseSmilyString appendAttributedString:attributedString];
                [baseSmilyString appendAttributedString:attrStringWithImage];
                
               // return baseSmilyString;
               // [formatedResponse appendFormat:@"<img src='%@' />", em];
                
                
                
            }else {
                [formatedResponse appendFormat:@" %@ ", replaced];
            }
            
            [emotionScanner scanString:@" " intoString:nil];
        }
        
    }
    


   

   // NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formatedResponse];
    
    return baseSmilyString;
}

-(NSString*)parseAttributStringtoBaseString:(NSAttributedString*)attrString
{
    
    
    return nil;
}


@end
