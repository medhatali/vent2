//
//  JSQMessagesSmiliesControl.h
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSQMessagesSmiliesContentView.h"
#import "SmilySelectionProtocol.h"

@interface JSQMessagesSmiliesControl : UIView <SmilySelectionProtocol>

@property (strong, nonatomic) JSQMessagesSmiliesContentView *contentView;
@property (nonatomic, assign) id <SmilySelectionProtocol> delegate;
@property (assign, nonatomic) CGFloat preferredDefaultHeight;


- (JSQMessagesSmiliesContentView *)loadSmilyContentView;

@end
