//
//  ChatTopBarView.h
//  Vent
//
//  Created by Medhat Ali on 6/30/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopChatDelegate.h"

@interface ChatTopBarView : UIView

@property (strong, nonatomic) IBOutlet UILabel *lblChatOwner;
@property (strong, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnSelect;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *connectionIndicator;
@property (strong, nonatomic) id<TopChatDelegate> delegate;

@end
