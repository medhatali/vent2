//
//  SmilySelectionProtocol.h
//  Vent
//
//  Created by Medhat Ali on 7/28/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SmilySelectionProtocol <NSObject>

@required
-(void)smilySelected:(NSDictionary*) emoji;
-(void)removeLastEmoji;


@end
