//
//  SmilesCollectionViewDataSource.h
//  Vent
//
//  Created by Sameh Farouk on 6/28/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SmilesCollectionViewDataSource : NSObject

@property (strong,nonatomic) NSDictionary *smilyEmoji;
@property (strong,nonatomic) NSMutableDictionary *smilyCodes;

+ (SmilesCollectionViewDataSource *)sharedInstance;
- (NSDictionary *)smiles;

- (NSString*)getSmiliCodesByName:(NSString *) smilyName;
- (NSString*)getSmiliNmaeByCode:(NSString *) smilyCode;
-(NSAttributedString*)parseStringtoAttributeString:(NSString*)baseString;
-(NSAttributedString*)parseStringtoAttributeString:(NSString*)baseString FontSize:(float)fontsize;
@end
