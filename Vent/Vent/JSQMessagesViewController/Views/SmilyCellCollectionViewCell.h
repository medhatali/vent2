//
//  SmilyCellCollectionViewCell.h
//  Vent
//
//  Created by Medhat Ali on 5/22/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmilyCellCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
