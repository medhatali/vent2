//
//  OtherToolBarDelegate.h
//  Vent
//
//  Created by Medhat Ali on 5/17/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OtherToolBarDelegate <NSObject>

- (void)toolBarButtonPressed:(id)sender ButtonIndex:(NSInteger)buttonIndex;

@end
