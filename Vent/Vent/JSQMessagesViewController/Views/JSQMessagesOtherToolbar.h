//
//  JSQMessagesOtherToolbar.h
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "JSQMessagesOtherToolbarContentView.h"
#import "OtherToolBarDelegate.h"

@interface JSQMessagesOtherToolbar : UIToolbar <OtherToolBarDelegate>

/**
 *  Returns the content view of the toolbar. This view contains all subviews of the toolbar.
 */
@property (weak, nonatomic, readonly) JSQMessagesOtherToolbarContentView *contentView;

/**
 *  Specifies the default height for the toolbar. The default value is `44.0f`. This value must be positive.
 */
@property (assign, nonatomic) CGFloat preferredDefaultHeight;


@property (assign, nonatomic) id<OtherToolBarDelegate> otherToolBarDelegate;

/**
 *  Loads the content view for the toolbar.
 *
 *  @discussion Override this method to provide a custom content view for the toolbar.
 *
 *  @return An initialized `JSQMessagesToolbarContentView` if successful, otherwise `nil`.
 */
- (JSQMessagesOtherToolbarContentView *)loadToolbarContentView;


@end
