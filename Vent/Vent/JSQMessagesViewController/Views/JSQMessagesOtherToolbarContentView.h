//
//  JSQMessagesOtherToolbarContentView.h
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OtherToolBarDelegate.h"


@interface JSQMessagesOtherToolbarContentView : UIView

@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthConstarint;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *buttonWidthConstarint;
@property (assign, nonatomic) id<OtherToolBarDelegate> otherToolBarDelegate;


+ (UINib *)nib;

@end
