//
//  SmilyLabelCollectionViewCell.h
//  Vent
//
//  Created by Medhat Ali on 5/22/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmilyLabelCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *typeLable;
@end
