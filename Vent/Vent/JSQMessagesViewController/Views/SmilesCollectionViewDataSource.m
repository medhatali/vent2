//
//  SmilesCollectionViewDataSource.m
//  Vent
//
//  Created by Sameh Farouk on 6/28/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "SmilesCollectionViewDataSource.h"

@interface SmilesCollectionViewDataSource(){
    NSDictionary* smilesDic;
}

@end

@implementation SmilesCollectionViewDataSource


static SmilesCollectionViewDataSource *myInstane = nil;

+ (SmilesCollectionViewDataSource *)sharedInstance
{
    if (!myInstane) {
        myInstane = [[[self class]allocWithZone:nil]init];
        [myInstane parseJSON];
        [myInstane smiliCodesList];
    }
    return myInstane;
}


- (void)parseJSON {
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Emoji" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    smilesDic = parsedObject;
    self.smilyEmoji =[parsedObject objectForKey:@"Emojis"];
}

- (void)smiliCodesList {
    NSDictionary *smilisDictionary =[[self.smilyEmoji objectForKey:@"Types"] objectForKey:@"Type"];
    self.smilyCodes=[[NSMutableDictionary alloc]init];
    
    for (NSDictionary * category in smilisDictionary) {
        for (NSDictionary * emojiIcons in [category objectForKey:@"Emoji"]) {
            [self.smilyCodes setValue:[emojiIcons objectForKey:@"Name"] forKey:[emojiIcons objectForKey:@"Code"]];
        }
        
    }
    
}


- (NSString*)getSmiliCodesByName:(NSString *) smilyName {
  
    for (NSString *smilyNameKey in self.smilyCodes.allKeys) {
        if ([[self.smilyCodes valueForKey:smilyNameKey] isEqualToString:smilyName]) {
            return smilyNameKey;
        }
    }
    return @"";
    
}

- (NSString*)getSmiliNmaeByCode:(NSString *) smilyCode {
    
    
    return [self.smilyCodes valueForKey:smilyCode];
    
}


-(NSAttributedString*)parseStringtoAttributeString:(NSString*)baseString
{
    SmilesCollectionViewDataSource *smilesCollectionDataSource = [SmilesCollectionViewDataSource sharedInstance];
    NSString *replaced;
    NSMutableString *formatedResponse = [NSMutableString string];
    NSMutableAttributedString *baseSmilyString=[[NSMutableAttributedString alloc]init];
    
    NSScanner *emotionScanner = [NSScanner scannerWithString:baseString];
    [emotionScanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@""]];
    while ([emotionScanner isAtEnd] == NO) {
        
        if([emotionScanner scanUpToString:@":0x" intoString:&replaced]) {
            [formatedResponse appendString:replaced];
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:replaced];
            [baseSmilyString appendAttributedString:attributedString];
            
        }
        if(![emotionScanner isAtEnd]) {
            [emotionScanner scanString:@":0x" intoString:nil];
            replaced = @"";
            [emotionScanner scanUpToString:@" " intoString:&replaced];
            //TODOif replace contains '[' then reset scanner location to this newer '[' and scan again.
            //            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", replaced]];
            //            if (img) {
            //                NSString *base64Image = [UIImagePNGRepresentation(img) base64String];
            //                [formatedResponse appendFormat:@"<img src='data:image/png;base64,%@' />", base64Image];
            NSString *unicode=[NSString stringWithFormat:@":0x%@",replaced];
            NSString *em = [smilesCollectionDataSource.smilyCodes valueForKey:unicode];
            if (em) {
                
                UIImage * pic= [UIImage imageNamed:em];
                
                UIGraphicsBeginImageContext(CGSizeMake(20, 20));
                [pic drawInRect:CGRectMake(0, 0, 20, 20)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                [attachment setImage:newImage];
                
                
                NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:attachment];
                
                // NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formatedResponse];
                
                //[baseSmilyString setAttributedString: attributedString];
                // [baseSmilyString appendAttributedString:attributedString];
                [baseSmilyString appendAttributedString:attrStringWithImage];
                
                // return baseSmilyString;
                // [formatedResponse appendFormat:@"<img src='%@' />", em];
                
                
                
            }else {
                [formatedResponse appendFormat:@" %@ ", replaced];
            }
            
            [emotionScanner scanString:@" " intoString:nil];
        }
        
    }
    
    
    
    
    
    // NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formatedResponse];
    
    return baseSmilyString;
}

-(NSAttributedString*)parseStringtoAttributeString:(NSString*)baseString FontSize:(float)fontsize
{
    SmilesCollectionViewDataSource *smilesCollectionDataSource = [SmilesCollectionViewDataSource sharedInstance];
    NSString *replaced;
    NSMutableString *formatedResponse = [NSMutableString string];
    NSMutableAttributedString *baseSmilyString=[[NSMutableAttributedString alloc]init];
    
    NSScanner *emotionScanner = [NSScanner scannerWithString:baseString];
    [emotionScanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@""]];
    while ([emotionScanner isAtEnd] == NO) {
        
        if([emotionScanner scanUpToString:@":0x" intoString:&replaced]) {
            [formatedResponse appendString:replaced];
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:replaced];
            [baseSmilyString appendAttributedString:attributedString];
            
        }
        if(![emotionScanner isAtEnd]) {
            [emotionScanner scanString:@":0x" intoString:nil];
            replaced = @"";
            [emotionScanner scanUpToString:@" " intoString:&replaced];
            //TODOif replace contains '[' then reset scanner location to this newer '[' and scan again.
            //            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", replaced]];
            //            if (img) {
            //                NSString *base64Image = [UIImagePNGRepresentation(img) base64String];
            //                [formatedResponse appendFormat:@"<img src='data:image/png;base64,%@' />", base64Image];
            NSString *unicode=[NSString stringWithFormat:@":0x%@",replaced];
            NSString *em = [smilesCollectionDataSource.smilyCodes valueForKey:unicode];
            if (em) {
                
                UIImage * pic= [UIImage imageNamed:em];
                
                UIGraphicsBeginImageContext(CGSizeMake(fontsize, fontsize));
                [pic drawInRect:CGRectMake(0, 0, fontsize, fontsize)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                [attachment setImage:newImage];
                
                
                NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:attachment];
                
                // NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formatedResponse];
                
                //[baseSmilyString setAttributedString: attributedString];
                // [baseSmilyString appendAttributedString:attributedString];
                [baseSmilyString appendAttributedString:attrStringWithImage];
                
                // return baseSmilyString;
                // [formatedResponse appendFormat:@"<img src='%@' />", em];
                
                
                
            }else {
                [formatedResponse appendFormat:@" %@ ", replaced];
            }
            
            [emotionScanner scanString:@" " intoString:nil];
        }
        
    }
    
    
    
    
    
    // NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formatedResponse];
    
    return baseSmilyString;
}



- (NSDictionary *)smiles
{
    return smilesDic;
}
@end
