//
//  TopChatDelegate.h
//  Vent
//
//  Created by Medhat Ali on 8/3/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol TopChatDelegate <NSObject>

- (void)selecTopChatHeader:(id)sender;

@end
