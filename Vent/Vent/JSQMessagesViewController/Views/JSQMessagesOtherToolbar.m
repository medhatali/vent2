//
//  JSQMessagesOtherToolbar.m
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "JSQMessagesOtherToolbar.h"

@implementation JSQMessagesOtherToolbar

#pragma mark - Initialization

- (void)awakeFromNib
{
   // [super awakeFromNib];
   // [self setTranslatesAutoresizingMaskIntoConstraints:NO];

    
    self.preferredDefaultHeight = 44.0f;
    
    JSQMessagesOtherToolbarContentView *toolbarContentView = [self loadToolbarContentView];
    
    toolbarContentView.otherToolBarDelegate =self;
    
    //toolbarContentView.frame = self.frame;
    [self addSubview:toolbarContentView];

   // [self setNeedsUpdateConstraints];
    _contentView = toolbarContentView;
    
    

}

- (JSQMessagesOtherToolbarContentView *)loadToolbarContentView
{
    NSArray *nibViews = [[NSBundle bundleForClass:[JSQMessagesOtherToolbarContentView class]] loadNibNamed:NSStringFromClass([JSQMessagesOtherToolbarContentView class])
                                                                                          owner:nil
                                                                                        options:nil];
    return nibViews.firstObject;
}

- (void)dealloc
{
   // [self jsq_removeObservers];
    _contentView = nil;
}

#pragma mark - Setters

- (void)setPreferredDefaultHeight:(CGFloat)preferredDefaultHeight
{
    NSParameterAssert(preferredDefaultHeight > 0.0f);
    _preferredDefaultHeight = preferredDefaultHeight;
}

- (void)toolBarButtonPressed:(id)sender ButtonIndex:(NSInteger)buttonIndex
{
    // toolbar
    UIButton *selectedButoon=(UIButton*)sender;
    [self.otherToolBarDelegate toolBarButtonPressed:sender ButtonIndex:selectedButoon.tag ];
}

@end
