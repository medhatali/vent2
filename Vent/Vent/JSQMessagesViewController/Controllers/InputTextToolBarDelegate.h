//
//  InputTextToolBarDelegate.h
//  Vent
//
//  Created by Medhat Ali on 5/13/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InputTextToolBarDelegate <NSObject>

- (void)textInputToolBarViewDidBeginEditing:(UITextView *)textView;

- (void)textInputToolBarViewDidChange:(UITextView *)textView;

- (void)textInputToolBarViewDidEndEditing:(UITextView *)textView;

- (void)shouldChangeTextInRange:(NSString *)replacementText;

- (void)deleteTextInputToolBar:(UITextView *)textView;

@end