//
//  JSQVCardMediaItem.h
//  Vent
//
//  Created by Medhat Ali on 8/2/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "JSQMediaItem.h"

@interface JSQVCardMediaItem : JSQMediaItem <JSQMessageMediaData, NSCoding, NSCopying>

/**
 *  The image for the photo media item. The default value is `nil`.
 */
@property (copy, nonatomic) UIImage *image;


@property (nonatomic, strong) NSString *vcardString;
@property (nonatomic, strong) NSString *fullName;

/**
 *  Initializes and returns a photo media item object having the given image.
 *
 *  @param image The image for the photo media item. This value may be `nil`.
 *
 *  @return An initialized `JSQPhotoMediaItem` if successful, `nil` otherwise.
 *
 *  @discussion If the image must be dowloaded from the network,
 *  you may initialize a `JSQPhotoMediaItem` object with a `nil` image.
 *  Once the image has been retrieved, you can then set the image property.
 */
- (instancetype)initWithImage:(UIImage *)image FullName:(NSString*)fullName VCard:(NSString*)vcard;



@end
