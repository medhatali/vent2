//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "JSQVideoMediaItem.h"
#import <AVFoundation/AVFoundation.h>
#import "JSQMessagesMediaPlaceholderView.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIImage+JSQMessages.h"
#import "AppUtility.h"


@interface JSQVideoMediaItem ()

@property (strong, nonatomic) UIImageView *cachedVideoImageView;

@end


@implementation JSQVideoMediaItem

#pragma mark - Initialization

- (instancetype)initWithFileURL:(NSURL *)fileURL isReadyToPlay:(BOOL)isReadyToPlay
{
    self = [super init];
    if (self) {
        _fileURL = [fileURL copy];
        _isReadyToPlay = isReadyToPlay;
        _cachedVideoImageView = nil;
    }
    return self;
}

- (void)dealloc
{
    _fileURL = nil;
    _cachedVideoImageView = nil;
}

#pragma mark - Setters

- (void)setFileURL:(NSURL *)fileURL
{
    _fileURL = [fileURL copy];
    _cachedVideoImageView = nil;
}

- (void)setIsReadyToPlay:(BOOL)isReadyToPlay
{
    _isReadyToPlay = isReadyToPlay;
    _cachedVideoImageView = nil;
}

- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
{
    [super setAppliesMediaViewMaskAsOutgoing:appliesMediaViewMaskAsOutgoing];
    _cachedVideoImageView = nil;
}

#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    if (self.fileURL == nil || !self.isReadyToPlay) {
        return nil;
    }
    
    if (self.cachedVideoImageView == nil) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fullpath;
        fullpath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", [[AppUtility sharedUtilityInstance]getMediaDirectory],self.fileURL]];
        //   fullpath = [fullpath stringByAppendingPathComponent:selectedSession.chatSessionMessage];
        NSURL* vedioURL =[NSURL fileURLWithPath:fullpath];
        //    vedioURL =[NSURL fileURLWithPath:selectedSession.chatSessionMessage];
        
        NSLog(@"vurl %@",vedioURL);
        

        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:vedioURL options:nil];
        AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generator.appliesPreferredTrackTransform = YES;
        CMTime time = [asset duration];
        time.value = 0;
        
        NSError *error = nil;
        CMTime actualTime;
        
        CGImageRef image = [generator copyCGImageAtTime:time actualTime:&actualTime error:&error];
        UIImage *thumbnail = [[UIImage alloc] initWithCGImage:image];
        CGImageRelease(image);
        
        CGSize size = [self mediaViewDisplaySize];
        UIImage *playIcon = [[UIImage jsq_defaultPlayImage] jsq_imageMaskedWithColor:[UIColor lightGrayColor]];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:thumbnail];
        UIImageView *imageView2 = [[UIImageView alloc] initWithImage:playIcon];
        
        imageView.backgroundColor = [UIColor blackColor];
        imageView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
        imageView.contentMode = UIViewContentModeCenter;
        imageView.clipsToBounds = YES;
        
        
        
        
        
        
 
        
        imageView2.backgroundColor = [UIColor clearColor];
        imageView2.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
        imageView2.contentMode = UIViewContentModeCenter;
        imageView2.clipsToBounds = YES;
        

        
        [imageView addSubview:imageView2];
        
        UIButton * _imageButtonName =[[UIButton alloc]init];
        _imageButtonName.backgroundColor = [UIColor clearColor];
        _imageButtonName.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
        _imageButtonName.contentMode = UIViewContentModeCenter;
        _imageButtonName.clipsToBounds = YES;
        
        [_imageButtonName setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *newTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked)];
        
        
        [_imageButtonName addGestureRecognizer:newTap];
  
        [imageView addSubview:_imageButtonName];
        
        
        [JSQMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:imageView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
        
        
 
  
        self.cachedVideoImageView = imageView;
        
        [_imageButtonName addGestureRecognizer:newTap];
        
        [ self.cachedVideoImageView  addSubview:_imageButtonName];
        
        
    }
    
    return self.cachedVideoImageView;
}

-(void)imageViewClicked
{
    NSLog(@"display video player");
    
    //Play the movie now


    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL: [NSURL URLWithString:@"http://sepwww.stanford.edu/sep/jon/trash/GroundRoll.mp4"]];

  //  MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL: self.fileURL];
    [[player view] setFrame: [self.cachedVideoImageView bounds]];
    [self.cachedVideoImageView addSubview: [player view]];
    [player play];
}

- (NSUInteger)mediaHash
{
    return self.hash;
}

#pragma mark - NSObject

- (BOOL)isEqual:(id)object
{
    if (![super isEqual:object]) {
        return NO;
    }
    
    JSQVideoMediaItem *videoItem = (JSQVideoMediaItem *)object;
    
    return [self.fileURL isEqual:videoItem.fileURL]
            && self.isReadyToPlay == videoItem.isReadyToPlay;
}

- (NSUInteger)hash
{
    return super.hash ^ self.fileURL.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: fileURL=%@, isReadyToPlay=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.fileURL, @(self.isReadyToPlay), @(self.appliesMediaViewMaskAsOutgoing)];
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _fileURL = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(fileURL))];
        _isReadyToPlay = [aDecoder decodeBoolForKey:NSStringFromSelector(@selector(isReadyToPlay))];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.fileURL forKey:NSStringFromSelector(@selector(fileURL))];
    [aCoder encodeBool:self.isReadyToPlay forKey:NSStringFromSelector(@selector(isReadyToPlay))];
}

#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone
{
    JSQVideoMediaItem *copy = [[[self class] allocWithZone:zone] initWithFileURL:self.fileURL
                                                                   isReadyToPlay:self.isReadyToPlay];
    copy.appliesMediaViewMaskAsOutgoing = self.appliesMediaViewMaskAsOutgoing;
    return copy;
}

@end
