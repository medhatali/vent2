//
//  JSQVCardMediaItem.m
//  Vent
//
//  Created by Medhat Ali on 8/2/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "JSQVCardMediaItem.h"
#import "MHFacebookImageViewer.h"
#import "UIImageView+MHFacebookImageViewer.h"
#import "JSQMessagesMediaPlaceholderView.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"
#import "VcardImporter.h"
#import "ContactsBussinessService.h"

@interface JSQVCardMediaItem ()

@property (strong, nonatomic) UIImageView *cachedImageView;

@end



@implementation JSQVCardMediaItem

- (instancetype)initWithImage:(UIImage *)image FullName:(NSString*)fullName VCard:(NSString*)vcard
{
    self = [super init];
    if (self) {
        _image = [image copy];
        _cachedImageView = nil;
        _fullName =fullName;
        _vcardString = vcard;
    }
    return self;
}

- (void)dealloc
{
    _image = nil;
    _cachedImageView = nil;
}

#pragma mark - Setters

- (void)setImage:(UIImage *)image
{
    _image = [image copy];
    _cachedImageView = nil;
}

- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
{
    [super setAppliesMediaViewMaskAsOutgoing:appliesMediaViewMaskAsOutgoing];
    _cachedImageView = nil;
}

#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    if (self.image == nil) {
        return nil;
    }
    
    if (self.cachedImageView == nil) {
        
        
        CGSize size = [self mediaViewDisplaySize];
      //  UIImageView *imageView = [[UIImageView alloc] initWithImage:self.image];
//        UIButton * customButoon=[[UIButton alloc]initWithFrame:CGRectMake(0.0f, 0.0f, size.width, size.height)];
//        [customButoon setTitle:@"" forState:UIControlStateNormal];
//        [customButoon setBackgroundColor:[UIColor lightGrayColor]];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"EmptyBG"]];
        
        imageView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        //[imageView setupImageViewer];
        
        UIImageView *profileImageView = [[UIImageView alloc] initWithImage:self.image];
         profileImageView.frame = CGRectMake(15.0f, 20.0f, 50, 50);
        
        [[profileImageView layer] setCornerRadius: profileImageView.frame.size.width / 2 ];
        [[profileImageView layer] setBorderWidth: 3.0f];
        [[profileImageView layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [profileImageView setClipsToBounds: YES];
        
        
        [imageView addSubview:profileImageView];
        
        UILabel * fullNamelbl=[[UILabel alloc]initWithFrame:CGRectMake(75.0f, 20.0f, 120, 70)];
        fullNamelbl.numberOfLines =3;
        
        
        
        ContactsBussinessService *contBS=[[ContactsBussinessService alloc]init];
        if ([contBS isContatcsAccessGrantSilent]) {
            VcardImporter *vcardPaser=[[VcardImporter alloc]init];
            [vcardPaser parse:_vcardString];
            fullNamelbl.text = (__bridge NSString*)ABRecordCopyValue(vcardPaser.personRecord, kABPersonFirstNameProperty);
        }
        else
        {
//            VcardImporter *vcardPaser=[[VcardImporter alloc]init];
//            fullNamelbl.text =[[VcardImporter alloc] parseFullName:_vcardString];
            
            NSArray *lines = [_vcardString componentsSeparatedByString:@"\n"];
            
            for(NSString* line in lines) {
                if ([line hasPrefix:@"N:"]) {
                    
                    NSArray *upperComponents = [line componentsSeparatedByString:@":"];
                    NSArray *components = [[upperComponents objectAtIndex:1] componentsSeparatedByString:@";"];
                    
                    fullNamelbl.text = [components objectAtIndex:1];
                    
                }
            }
            
           //fullNamelbl.text =@"No Contact Permission";
        }
        

        [imageView addSubview:fullNamelbl];
        
        [JSQMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:imageView isOutgoing:self.appliesMediaViewMaskAsOutgoing];
        self.cachedImageView = imageView;
    }
    
    return self.cachedImageView;
}

- (NSUInteger)mediaHash
{
    return self.hash;
}

#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.image.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.image, @(self.appliesMediaViewMaskAsOutgoing)];
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _image = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(image))];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.image forKey:NSStringFromSelector(@selector(image))];
}

#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone
{
    JSQVCardMediaItem *copy = [[[self class] allocWithZone:zone] initWithImage:self.image];
    copy.appliesMediaViewMaskAsOutgoing = self.appliesMediaViewMaskAsOutgoing;
    return copy;
}

#pragma mark fbimageviewer
- (void) displayImage:(UIImageView*)imageView withImage:(UIImage*)image  {
    [imageView setImage:image];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [imageView setupImageViewer];
    imageView.clipsToBounds = YES;
}


@end
