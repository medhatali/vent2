//
//  ContactPhones.m
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ContactPhones.h"
#import "Contacts.h"


@implementation ContactPhones

@dynamic phoneIsVentNumber;
@dynamic phoneNumber;
@dynamic phoneContact;

@end
