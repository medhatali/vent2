//
//  BroadCastListTableViewController.h
//  Vent
//
//  Created by Medhat Ali on 6/11/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "MainMasterTableTVC.h"
#import <UIKit/UIKit.h>

@interface BroadCastListTableViewController : UITableViewController

@property (nonatomic, retain) NSArray *broadCastList;
@property (nonatomic,strong) BroadcastRepository * broadcastListRepo;
@property  (nonatomic,strong)NSMutableArray *selectedModelToUpdate;

@end
