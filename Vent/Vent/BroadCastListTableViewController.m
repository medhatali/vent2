//
//  BroadCastListTableViewController.m
//  Vent
//
//  Created by Medhat Ali on 6/11/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BroadCastListTableViewController.h"
#import "ActiveChatSessionViewController.h"
#import "BroadcastRepository.h"
#import "ContactsBussinessService.h"
#import "BroadcastList.h"
#import "NewBroadCastViewController.h"
#import "ContactsRepository.h"

@implementation BroadCastListTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIView *header=[[UIView alloc]initWithFrame:CGRectMake(0.0,0.0,self.view.frame.size.width,50.0)];
//    [header setBackgroundColor:[UIColor redColor]];
//    
//    UIButton *addNewButton=[[UIButton alloc]initWithFrame:CGRectMake(0.0,0.0,self.view.frame.size.width,50.0)];
//    [addNewButton setTitle:@"Add New" forState:UIControlStateNormal];
//    [header addSubview:addNewButton];
//    
//    self.tableView.tableHeaderView =header;
    
}

-(void)reloadData
{
    // get chat lsit from DB
    NSError *error;
    self.broadcastListRepo=[[BroadcastRepository alloc]init];
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    User* currentUser=[contactBWS getCurrentUserProfile];
    
   
    self.broadCastList =[ self.broadcastListRepo getUserBroadcastListSorted:currentUser SortOption:sortDefault Error:&error];
    
    [self.tableView reloadData];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[[self navigationController]navigationBar] setHidden:FALSE];
    
    [self reloadData];
    
     self.tabBarController.tabBar.hidden = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
     self.tabBarController.tabBar.hidden = NO;
}

//- (void)viewDidAppear:(BOOL)animated
//{
//    
//    [super viewDidAppear:animated];
//    
//
//}

//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //return 3;
    return self.broadCastList.count;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (indexPath.row == 0) {
//        
//        return 78;
//    }
//    
//    static NSString *CellIdentifier = @"ChatListTVCell";
//    
//    ChatListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    return cell.frame.size.height;
//    
//    
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"BCList";
    UITableViewCell *cell = nil;
    
      cell = [[ChatListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    BroadcastList *selectModel=((BroadcastList*)[self.broadCastList objectAtIndex:indexPath.row ]);

    
    cell.textLabel.text =selectModel.broadcastName;
   cell.detailTextLabel.text =selectModel.broadcastName;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeInfoDark];
    [button setTitle:@"detail" forState:UIControlStateNormal];
    
    // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
    [button addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = indexPath.row;
    
    cell.accessoryView = button;
    
//   cell.accessoryType=UITableViewCellAccessoryDetailDisclosureButton;
//    cell.accessoryView=nil;
    
       return cell;
    
}





-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    static NSString *CellIdentifier = @"Footer";
//    UITableViewCell *cell = nil;
//    
//    cell = [[ChatListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//    
//    UIButton *addNewButton=[[UIButton alloc]init];
//    [addNewButton setFrame:CGRectMake(0, 0, 320 ,50)];
//    [addNewButton setTitle:@"Add New" forState:UIControlStateNormal];
//    
//    [cell addSubview:addNewButton];
//    
//    return cell.contentView;
    
    UIView *header=[[UIView alloc]initWithFrame:CGRectMake(0.0,0.0,self.view.frame.size.width,50.0)];
    [header setBackgroundColor:[UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1]];
    
    UIButton *addNewButton=[[UIButton alloc]initWithFrame:CGRectMake(0.0,0.0,self.view.frame.size.width,50.0)];
    [addNewButton setTitle:@"New List" forState:UIControlStateNormal];
    [addNewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addNewButton addTarget:self action:@selector(addNewList:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:addNewButton];
    
    return header;
    
    
}


//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section:(NSInteger)section
//{
//    return 50;
//}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
    
}




// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
//        ChatList *selectModel=((ChatList*)[self.chatList objectAtIndex:indexPath.row -1]);
//        NSError *error;
//        [self.chatListRepo deleteChatList:selectModel Error:&error];
//        [self reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



//// Override to support rearranging the table view.
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
//    
//    
//    
//}



//// Override to support conditional rearranging of the table view.
//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return NO if you do not want the item to be re-orderable.
//    if (tableView == [self tableView]) {
//        
//        if (indexPath.row == 0) {
//            return NO;
//        }
//        else
//        {
//            return YES;
//        }
//        
//    }
//    else
//    {
//        return YES;
//    }
//    return NO;
//}

-(IBAction)checkButtonTapped:(id)sender
{
    UIButton *detailBtn=(UIButton *)sender;
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    User* currentUser=[contactBWS getCurrentUserProfile];
    NSError *error;
    
    self.selectedModelToUpdate = [[NSMutableArray alloc]init];
    
    BroadcastList *selectedBList=(BroadcastList *)[self.broadCastList objectAtIndex:detailBtn.tag];
    for(BroadcastListUserPhones *userPhone in selectedBList.broadcastListUsers)
    {
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    Contacts *userContact=     [contactRepo getUserContactByPhoneNumber:userPhone.broadcastUserPhone contactUser:currentUser Error:&error];
        
    [self.selectedModelToUpdate addObject:userContact] ;
    }
    
   [self performSegueWithIdentifier:@"NewBroadcast" sender:self];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    BroadcastList *selectModel = (BroadcastList *)[self.broadCastList objectAtIndex:indexPath.row];
    
    ActiveChatSessionViewController * chatsessionVC=[[ActiveChatSessionViewController alloc]initWithNibName:@"ChatSessionViewController" bundle:[NSBundle mainBundle]];
    
    
    chatsessionVC.chatTypeSession = BroacastChat ;
   
    
    chatsessionVC.selectedBroadcastList =selectModel;
    
    [self.navigationController pushViewController:chatsessionVC animated:YES];
}

-(IBAction)addNewList:(id)sender
{
    [self performSegueWithIdentifier:@"NewBroadcast" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"NewBroadcast"])
    {
        NewBroadCastViewController *dest=(NewBroadCastViewController *)segue.destinationViewController;
        dest.inUpdateMode = TRUE;
        dest.grouContactList =self.selectedModelToUpdate;
        
    }
    
}
@end
