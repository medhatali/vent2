

#import "LoadingView.h"
#import "LoadingViewDef.h"
#import "AppDelegate.h"


@interface LoadingView ()

@property (nonatomic, retain) UILabel *loadingViewLabel;
@property (nonatomic, retain) UIActivityIndicatorView *loadingViewActivityIndicator;

@property (nonatomic) BOOL loadingViewShown;


@end

@implementation LoadingView


static dispatch_once_t pred;
static LoadingView *sharedManager;

+ (id) sharedManager {
    
    dispatch_once(&pred, ^{
        sharedManager = [[LoadingView alloc] init];
    });
    
    return sharedManager;
    
}


#pragma mark - Lifecycle

- (id) initWithLabel:(NSString *)loadingViewLabelTitle
     withProgressBar:(BOOL)progressBarRequired
colorOFActivityIndicator:(UIColor *)activityIndicatorColor {
 
    self = [super init];
    
    if (self) {
        [self prepareDefaultFrame];
        [self prepareBackground];
        [self addLoadingViewLabelWithText:loadingViewLabelTitle];
        [self addActivityIndicatorWithColor:activityIndicatorColor];
        [self addSelfToWindow];

    }
    
    return self;
    
}

#pragma mark - Initialization

- (void) prepareDefaultFrame {
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        self.frame = CGRectMake((screenSize.height - Dimension_Loading_View_Default_Width) / 2,
//                                (screenSize.width - Dimension_Loading_View_Default_Height) / 2,
//                                Dimension_Loading_View_Default_Width,
//                                Dimension_Loading_View_Default_Height);
  
        // landscape only
        
        self.frame = CGRectMake((screenSize.width - Dimension_Loading_View_Default_Height) / 2,
                                (screenSize.height - Dimension_Loading_View_Default_Width) / 2,
                                 Dimension_Loading_View_Default_Height,
                                Dimension_Loading_View_Default_Width
                               );
           // self.transform = CGAffineTransformMakeRotation(90);
       //  self.transform=CGAffineTransformMakeRotation(-M_PI_2 );
            
    } else {
    self.frame = CGRectMake((screenSize.width - Dimension_Loading_View_Default_Width) / 2,
                            (screenSize.height - Dimension_Loading_View_Default_Height) / 2,
                            Dimension_Loading_View_Default_Width,
                            Dimension_Loading_View_Default_Height);
    }
    
}

- (void) prepareBackground {

    self.layer.cornerRadius = 5;
    self.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1];
//self.backgroundColor = [UIColor grayColor];
    
}

- (void) addLoadingViewLabelWithText:(NSString *)loadingViewLabelTitle {
 
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        self.loadingViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(Dimension_Loading_View_Label_Origin_X - 60,
                                                                          ((Dimension_Loading_View_Default_Height - Dimension_Loading_View_Label_Height) / 2 )+ 105,
                                                                          Dimension_Loading_View_Label_Width,
                                                                          Dimension_Loading_View_Label_Height)];
        self.loadingViewLabel.transform=CGAffineTransformMakeRotation(-M_PI_2 );
    }
    else
    {
//    self.loadingViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(Dimension_Loading_View_Label_Origin_X,
//                                                                      (Dimension_Loading_View_Default_Height - Dimension_Loading_View_Label_Height) / 2,
//                                                                      Dimension_Loading_View_Label_Width,
//                                                                      Dimension_Loading_View_Label_Height)];
        
        self.loadingViewLabel = [[UILabel alloc] initWithFrame:CGRectMake(Dimension_Loading_View_Label_Origin_X,
                                                                          60,
                                                                          self.frame.size.width - (Dimension_Loading_View_Label_Origin_X *2),
                                                                          Dimension_Loading_View_Label_Height)];
        
    }
    self.loadingViewLabel.text = loadingViewLabelTitle ? loadingViewLabelTitle:loadingViewLabelTitle;
    self.loadingViewLabel.backgroundColor = [UIColor clearColor];
    //self.loadingViewLabel.textColor = [UIColor whiteColor];
    self.loadingViewLabel.textColor = [UIColor blackColor];
    self.loadingViewLabel.font = [UIFont boldSystemFontOfSize:Dimension_Loading_View_Label_Font_Size];
    self.loadingViewLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.loadingViewLabel];
    
    UIFont *boldfont ;
    boldfont = [UIFont fontWithName:@"GESSTwoBold-Bold" size:15];
    [self.loadingViewLabel setFont:boldfont];
    
   // [self insertSubview:self.loadingViewLabel atIndex:999];
}

- (void) addActivityIndicatorWithColor:(UIColor *)activityIndicatorColor {
    
    self.loadingViewActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.loadingViewActivityIndicator.color = activityIndicatorColor ? activityIndicatorColor:[UIColor blackColor];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.loadingViewActivityIndicator.frame = CGRectMake((self.frame.size.width/2) - (self.loadingViewActivityIndicator.frame.size.width/2),
                                                             10,
                                                             self.loadingViewActivityIndicator.frame.size.width,
                                                             self.loadingViewActivityIndicator.frame.size.height);
    }
    else
    {
//    self.loadingViewActivityIndicator.frame = CGRectMake(self.loadingViewLabel.frame.size.width + self.loadingViewActivityIndicator.frame.origin.x + Dimension_Space_Label_Indicator,
//                                                         (self.frame.size.height - self.loadingViewActivityIndicator.frame.size.height) / 2,
//                                                         self.loadingViewActivityIndicator.frame.size.width,
//                                                          self.loadingViewActivityIndicator.frame.size.height);
        
        self.loadingViewActivityIndicator.frame = CGRectMake((self.frame.size.width/2) - (self.loadingViewActivityIndicator.frame.size.width/2),
                                                             10,
                                                             self.loadingViewActivityIndicator.frame.size.width,
                                                             self.loadingViewActivityIndicator.frame.size.height);
        
    }
    [self addSubview:self.loadingViewActivityIndicator];
        
}

- (void) addSelfToWindow {
    
    self.alpha = 0.8;
    
    
     [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
       // mainMenuViewController *mainSplitController = (mainMenuViewController *) ((AppDelegate *) [[UIApplication sharedApplication]delegate]).window.rootViewController;
       // [mainSplitController.view addSubview:self];
        
        //self.transform = CGAffineTransformMakeRotation(-90); //rotation in radians

        
         [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
        
    } else {
        [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
        
    }
    
    NSLayoutConstraint *heightConstraint =
    [NSLayoutConstraint constraintWithItem:self
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:1.0
                                  constant:100.0];
    [[[[UIApplication sharedApplication] delegate] window] addConstraint:heightConstraint];
    
    NSLayoutConstraint *widthConstraint =
    [NSLayoutConstraint constraintWithItem:self
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:1.0
                                  constant:230.0];
    [[[[UIApplication sharedApplication] delegate] window] addConstraint:widthConstraint];
    
    NSLayoutConstraint *centerYConstraint =
    [NSLayoutConstraint constraintWithItem:self
                                 attribute:NSLayoutAttributeCenterY
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:[[[UIApplication sharedApplication] delegate] window]
                                 attribute:NSLayoutAttributeCenterY
                                multiplier:1.0
                                  constant:0.0];
    [[[[UIApplication sharedApplication] delegate] window] addConstraint:centerYConstraint];
    
    // Center Horizontally
    NSLayoutConstraint *centerXConstraint =
    [NSLayoutConstraint constraintWithItem:self
                                 attribute:NSLayoutAttributeCenterX
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:[[[UIApplication sharedApplication] delegate] window]
                                 attribute:NSLayoutAttributeCenterX
                                multiplier:1.0
                                  constant:0.0];
    [[[[UIApplication sharedApplication] delegate] window] addConstraint:centerXConstraint];
    
    //[[[[UIApplication sharedApplication] delegate] window] addSubview:self];

}

#pragma mark - Show Loading View

- (void) showLoadingViewAnimated:(BOOL)animated {
    
    if (self.loadingViewShown) return;
    
    self.loadingViewShown = YES;
    [self.loadingViewActivityIndicator startAnimating];
    [UIApplication sharedApplication].delegate.window.userInteractionEnabled = NO;
    
    if (animated) {
        [self animateLoadingView_Show];
        
    } else {
        self.hidden = NO;
        
    }
    
}

- (void) animateLoadingView_Show {
    
    [UIView animateWithDuration:Duration_Show_Animation_First
                     animations:^{
                         self.alpha = 0.3;
                         self.transform = CGAffineTransformMakeScale(1.1, 1.1);
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:Duration_Show_Animation_Second
                                          animations:^{
                                              self.alpha = 0.7;
                                              self.transform = CGAffineTransformMakeScale(0.9, 0.9);
                                          }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:Duration_Show_Animation_Final
                                                               animations:^{
                                                                   self.alpha = 0.95;
                                                                   self.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                                               }
                                                               completion:^(BOOL finished) {
                                                                   
                                                               }];
                                          }];
                     }];
}


#pragma mark - Hide Loading View

- (void) hideLoadingViewAnimated:(BOOL)animated {
    
    if (!self.loadingViewShown) return;
    
    self.loadingViewShown = NO;
    [self.loadingViewActivityIndicator stopAnimating];
    [UIApplication sharedApplication].delegate.window.userInteractionEnabled = YES;
    
    if (animated) {
        [self animateLoadingView_Hide];
        
    } else {
        self.hidden = YES;
        [self removeFromSuperview];
        
    }
    
}

- (void) animateLoadingView_Hide {
    
    [UIView animateWithDuration:Duration_Hide_Animation_First
                     animations:^{
                         self.alpha = 0.9;
                         self.transform = CGAffineTransformMakeScale(1.1, 1.1);
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:Duration_Hide_Animation_Final
                                          animations:^{
                                              self.alpha = 0;
                                              self.transform = CGAffineTransformMakeScale(0.01, 0.01);
                                          }
                                          completion:^(BOOL finished) {
                                              [self removeFromSuperview];
                                          }];
                     }];
    
}

#pragma mark - Setting Progress

- (void) setProgress:(float)progressToSet {
    
}

@end
