//
//  SignalRContctivtyProtocol.h
//  Vent
//
//  Created by Sameh Farouk on 6/30/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignalRContctivtyProtocol <NSObject>
@required
-(void)started;
-(void)received:(NSDictionary *) data;
-(void)error:(NSError *)error;
-(void)closed;
-(void)reconnecting;
-(void)reconnected;
-(void)stateChanged:(connectionState) connectionState;
-(void)connectionSlow;

@end
