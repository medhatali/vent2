//
//  StatusListTableViewController.m
//  Vent
//
//  Created by Medhat Ali on 7/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "StatusListTableViewController.h"
#import "ContactsBussinessService.h"
#import "TPKeyboardAvoidingTableView.h"
#import "UserRepository.h"

@interface StatusListTableViewController ()

@property (strong,nonatomic) NSMutableArray *statusList;
@property (strong,nonatomic)  User* currentUser;
@property (strong, nonatomic) IBOutlet UITextField *txtStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentStatus;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;

@end

@implementation StatusListTableViewController

-(void)viewWillDisappear:(BOOL)animated
{
        [self.view endEditing:YES];
    [self saveStatus:self.txtStatus.text];

    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    self.currentUser=[contactBWS getCurrentUserProfile];
    
    self.txtStatus.text =self.currentUser.userStatus;
    
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"StatusList" ofType:@"plist"];
   self.statusList=[[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return self.statusList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StatusCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = self.statusList[indexPath.row];
     cell.accessoryType=UITableViewCellAccessoryNone;
    if ([cell.textLabel.text isEqualToString:self.currentUser.userStatus]) {
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self saveStatus:self.statusList[indexPath.row]];
    [self.tableView reloadData];
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)saveStatus:(NSString*)status
{
    self.currentUser.userStatus = status;
    self.txtStatus.text = status;
    
    UserRepository *repo=[[UserRepository alloc]init];
    NSError *error;
    [repo updateUser:self.currentUser Error:&error];
    
    
}


- (IBAction)dismisskeyboard:(id)sender {
    [self.view endEditing:YES];
}

@end
