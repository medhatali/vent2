//
//  GroupProfileViewControler.h
//  Vent
//
//  Created by Medhat Ali on 6/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "LoadingView.h"
#import "Reachability.h"

@interface GroupProfileViewControler : UITableViewController<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) User* currentUser;
@property (strong, nonatomic) IBOutlet UILabel *lblGroupName;

@property (strong, nonatomic) IBOutlet UIButton *profileImageBtn;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt;
@property (strong, nonatomic) IBOutlet UITextField *lblDisplayName;

@property (strong, nonatomic) IBOutlet UITextField *lblFirstName;
@property (strong, nonatomic) IBOutlet UITextField *lblLastName;
@property (strong, nonatomic) IBOutlet UITextField *lblEmail;

@property (strong, nonatomic) IBOutlet UILabel *lblEmailVerified;

@property (strong, nonatomic) IBOutlet UILabel *lblStatus;

@property (strong, nonatomic) LoadingView *loadingView;

@property (retain,nonatomic) Reachability *reachbilityInstance;

@end
