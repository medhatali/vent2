//
//  ContactUISearchBar.h
//  Vent
//
//  Created by Sameh Farouk on 4/5/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUISearchBar : UISearchBar
-(instancetype)initWithSearchBar:(UISearchBar *) aSuper;
@end
