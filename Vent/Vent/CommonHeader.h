//
//  CommonHeader.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#ifndef Vent_CommonHeader_h
#define Vent_CommonHeader_h

#pragma -mark web service URL


#define kRequestURL @"URL"
#define kRequestParams @"Params"
#define kRequestHeaders @"Headers"
#define kRequestBody @"RequestBody"

#define kResponseMessage @"ResponseMessage"
#define kResponseCode @"ResponseCode"
#define kResponse @"Response"



#pragma -mark first run

#define kFirstRun @"VentFirstRun"
#define kLastLoggedUser @"VentLastLoggedUser"
#define kLastLoggedUserPass @"VentLastLoggedUserPassword"
#define kUserProfile @"VentUserProfile"
#define kLastRecivedRequest @"VentLastRequestRecieved"
#define KVentDeviceToken @"VentDeviceToken"
#define KVentSecureTimeLimit @"VentSecureTimeLimit"
#define SPLASH_SHOW_DELAY    1.5f

#define kScreenPushNotification @"VentScreenPushNotification"

#import "Reachability.h"
#import "AppDelegate.h"


typedef enum  {DirectOrder=0 , DirectOrderDone=1, YearlyProjects=2} MaintenanceType;




#pragma -mark data model
#import "LoadingView.h"
#import "UserDM.h"
#import "NSUserDefaultsExtensions.h"



#endif
