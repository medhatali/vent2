//
//  ContactsBussinessService.m
//  Vent
//
//  Created by Medhat Ali on 3/10/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "ContactsBussinessService.h"
#import "NBPhoneNumberUtil.h"
#import <AddressBookUI/AddressBookUI.h>
#import "ContactsRepository.h"
#import "UserRepository.h"
#import "ContactsWebService.h"


#define firstNameString @"firstName"
#define middleNameString @"middleName"
#define lastNameString @"lastName"

@implementation ContactsBussinessService 

-(BOOL) isContatcsAccessGrant
{
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
//        UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
//        [cantLoadContactsAlert show];
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");

        return TRUE;
        
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        __block BOOL isgranted=FALSE;
        
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            dispatch_async(dispatch_get_main_queue(),^{
                if (!granted){
                    //4
                    NSLog(@"Just denied");
                    UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]   message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]   delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                    [cantLoadContactsAlert show];
                    isgranted = FALSE;
                    return ;
                }
                //5
                NSLog(@"Just authorized");
                isgranted = TRUE;

                
            });
        });
        
        if (isgranted)
        {
            
            
            
            
            
            return TRUE;
        }
        
    }
    
    
    return FALSE;
    
}

-(BOOL) isContatcsAccessGrantSilent
{
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
//        UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:nil cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles: nil];
//        [cantLoadContactsAlert show];
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        
        return TRUE;
        
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        __block BOOL isgranted=FALSE;
        
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            dispatch_async(dispatch_get_main_queue(),^{
                if (!granted){
                    //4
                    NSLog(@"Just denied");
//                    UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]   message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]   delegate:nil cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles: nil];
//                    [cantLoadContactsAlert show];
                    isgranted = FALSE;
                    return ;
                }
                //5
                NSLog(@"Just authorized");
                isgranted = TRUE;
                
                
            });
        });
        
        if (isgranted)
        {
            
            
            
            
            
            return TRUE;
        }
        
    }
    
    
    return FALSE;
    
}


-(BOOL) isContatcsAccessGrantInDispath
{
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        

        
//        dispatch_async(dispatch_get_main_queue(),
//                       ^{
//                           
//                           
//                           UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
//                           
//                           [cantLoadContactsAlert show];
//                           
//                           
//                           
//                       });
        
        
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        
        return TRUE;
        
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        __block BOOL isgranted=FALSE;
        
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            dispatch_async(dispatch_get_main_queue(),^{
                if (!granted){
                    //4
                    NSLog(@"Just denied");
//                    dispatch_async(dispatch_get_main_queue(),
//                                   ^{
//                                       
//                    UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]   message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]   delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
//                    [cantLoadContactsAlert show];
//                                        });
                    
                    isgranted = FALSE;
                    return ;
                }
                //5
                NSLog(@"Just authorized");
                isgranted = TRUE;
                
                
            });
        });
        
        if (isgranted)
        {
            
            
            
            
            
            return TRUE;
        }
        
    }
    
    
    return FALSE;
    
}



-(BOOL) loadContacts:(NSString*)defualtCountryCode ParentView:(UIViewController*)parentView
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
        [cantLoadContactsAlert show];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        

        
        [self loadPhoneContactWithOutLoading:defualtCountryCode];
        //[self loadPhoneContacts:defualtCountryCode];
        [parentView.navigationController popToRootViewControllerAnimated:YES];
        return TRUE;
        
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        __block BOOL isgranted=FALSE;
        
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            dispatch_async(dispatch_get_main_queue(),^{
                if (!granted){
                    //4
                    NSLog(@"Just denied");
                    UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]   message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]   delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                    [cantLoadContactsAlert show];
                    isgranted = FALSE;
                     return ;
                }
                //5
                NSLog(@"Just authorized");
                isgranted = TRUE;
            
                
            
                
                [self loadPhoneContactWithLoading:defualtCountryCode];
                
                [parentView.navigationController popToRootViewControllerAnimated:YES];
                
            });
        });
        
        if (isgranted)
        {
            
            
  
            
            
            return TRUE;
        }
        
    }
    
    
    return FALSE;
}

-(void)loadPhoneContacts:(NSString*)defualtCountryCode
{
    
 
    
   
 
    
//    NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];
//    NSError *anError = nil;
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
    
   NBPhoneNumberUtil *_phoneNumberUtil = [NBPhoneNumberUtil sharedInstance];
    
    _phoneContacts = [NSMutableArray new];
    _userContacts = [NSMutableArray new];
    
    NSLog(@"Contacts Count:%lu",(unsigned long)[allContacts count]);
    for (id record in allContacts){
         NSMutableDictionary *newContactDictionary=[[NSMutableDictionary alloc]init];
         NSMutableDictionary *newUserContactDictionary=[[NSMutableDictionary alloc]init];
        
        ABRecordRef thisContact = (__bridge ABRecordRef)record;
        
         long contactRecID = ABRecordGetRecordID(thisContact);
        
        
       // NSString *contactRecordID= thisContact.
        

        
        
        NSString *firstName = (__bridge NSString*)ABRecordCopyValue(thisContact, kABPersonFirstNameProperty);
        if (!firstName) {
            firstName = @"";
        }
        NSString *middleName = (__bridge NSString*)ABRecordCopyValue(thisContact, kABPersonMiddleNameProperty);
        if (!middleName) {
            middleName = @"";
        }
        NSString *lastName = (__bridge NSString*)ABRecordCopyValue(thisContact, kABPersonLastNameProperty);
        if (!lastName) {
            lastName = @"";
        }
        NSString *fullName = [NSString stringWithFormat:@"%@ %@ %@",firstName,middleName,lastName];
        fullName = [[fullName stringByReplacingOccurrencesOfString:@"  " withString:@" "] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if(fullName.length == 0){
            continue;
        }
        
        
        
        //NSData *contactImage = (__bridge NSData*)ABPersonCopyImageData(thisContact);
        
        [newUserContactDictionary setObject:[NSString stringWithFormat:@"%li",contactRecID] forKey:ContactIDString];
        
        [newUserContactDictionary setObject:fullName forKey:ContactNameString];
        [newUserContactDictionary setObject:firstName forKey:firstNameString];
        [newUserContactDictionary setObject:middleName forKey:middleNameString];
        [newUserContactDictionary setObject:lastName forKey:lastNameString];
        
        
//        if (contactImage != Nil) {
//            [newUserContactDictionary setObject:contactImage forKey:ContactImageString];
//        }
        
  
        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
        ABMultiValueRef multiPhones = ABRecordCopyValue(thisContact, kABPersonPhoneProperty);
        for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
            NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
            phoneNumber = [[[[[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@"\u00A0" withString:@""];
            
            
            if ([self.reachbilityInstance isReachable])
            {
                NSString *phoneNumberParsed =phoneNumber;
                NSString *_phoneNumberWithoutCode = phoneNumber;
                 User *currentUser=[self getCurrentUserProfile];
                NSString* _countryCode = currentUser.userCountryCode;
               
                NSError *_error = nil;
                NBPhoneNumber *_myPhoneNumber = [_phoneNumberUtil parse:_phoneNumberWithoutCode defaultRegion: _countryCode error:&_error];
                
                if (_error == nil) {
                    
                    
                    NBEPhoneNumberType _phoneNumberType = [_phoneNumberUtil getNumberType:_myPhoneNumber];
                    
                    if ([_phoneNumberUtil isValidNumber:_myPhoneNumber]) {//Validate Phone Number
                        if (_phoneNumberType == NBEPhoneNumberTypeMOBILE || _phoneNumberType == NBEPhoneNumberTypeFIXED_LINE_OR_MOBILE) {
                            
                            phoneNumberParsed = [_phoneNumberUtil format:_myPhoneNumber numberFormat: NBEPhoneNumberFormatE164 error:&_error];
                            
                            // check if phone number not the same that registered with
                            ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
                            User* currentUser=[contactBWS getCurrentUserProfile];
                            if (![currentUser.userPhoneNumber isEqualToString:phoneNumberParsed]) {
                                [phoneNumbers addObject:phoneNumberParsed];
                            }
                            
                        }
                    }
                    
                    
                    
                    
                }else{
                    NSLog(@"Error : %@", [_error localizedDescription]);
                    phoneNumberParsed = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
                }
                
                if ([_phoneNumberWithoutCode length] == 0) {
                    phoneNumberParsed = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
                }
            }
            else
            {
                //[self showConnectionDown];
                
            }

            
            
//         if(![self validatePhoneNumber:phoneNumber DefaultCountryCode:defualtCountryCode])
//         {
//             
//             phoneNumber =[NSString stringWithFormat:@"%@%@",defualtCountryCode,phoneNumber];
//             
//             if([self validatePhoneNumber:phoneNumber DefaultCountryCode:defualtCountryCode])
//             {
//                  [phoneNumbers addObject:phoneNumber];
//             }
//             
//         }
//            else
//            {
//                [phoneNumbers addObject:phoneNumber];
//            }
            
            
           
            
        }
        
         [newUserContactDictionary setObject:phoneNumbers forKey:PhoneNumbersString];
         [newContactDictionary setObject:phoneNumbers forKey:PhoneNumbersString];
        
       
        //        NSMutableArray *contactEmails = [NSMutableArray new];
        //        ABMultiValueRef multiEmails = ABRecordCopyValue(thisContact, kABPersonEmailProperty);
        //        for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
        //            CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
        //            NSString *contactEmail = (__bridge NSString *)contactEmailRef;
        //            [contactEmails addObject:contactEmail];
        //        }
        
        [_phoneContacts addObject:newContactDictionary];
        [_userContacts addObject:newUserContactDictionary];
    }
}


-(void)loadPhoneContactsInCallBack:(NSString*)defualtCountryCode addressBook:(ABAddressBookRef) addressBook
{
    // this method will be used in contact callback
    

    NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    if ([allContacts count] > 0) {
        
    
    
    _phoneContacts = [NSMutableArray new];
    _userContacts = [NSMutableArray new];
    
    NSLog(@"Contacts Count for contact fallback:%lu",(unsigned long)[allContacts count]);
    for (id record in allContacts){
        NSMutableDictionary *newContactDictionary=[[NSMutableDictionary alloc]init];
        NSMutableDictionary *newUserContactDictionary=[[NSMutableDictionary alloc]init];
        
        ABRecordRef thisContact = (__bridge ABRecordRef)record;
        
        long contactRecID = ABRecordGetRecordID(thisContact);
        
        
        // NSString *contactRecordID= thisContact.
        
        
        
        
        NSString *firstName = (__bridge NSString*)ABRecordCopyValue(thisContact, kABPersonFirstNameProperty);
        if (!firstName) {
            firstName = @"";
        }
        NSString *middleName = (__bridge NSString*)ABRecordCopyValue(thisContact, kABPersonMiddleNameProperty);
        if (!middleName) {
            middleName = @"";
        }
        NSString *lastName = (__bridge NSString*)ABRecordCopyValue(thisContact, kABPersonLastNameProperty);
        if (!lastName) {
            lastName = @"";
        }
        NSString *fullName = [NSString stringWithFormat:@"%@ %@ %@",firstName,middleName,lastName];
        fullName = [[fullName stringByReplacingOccurrencesOfString:@"  " withString:@" "] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if(fullName.length == 0){
            continue;
        }
        
        
        
        //NSData *contactImage = (__bridge NSData*)ABPersonCopyImageData(thisContact);
        
        [newUserContactDictionary setObject:[NSString stringWithFormat:@"%li",contactRecID] forKey:ContactIDString];
        
        [newUserContactDictionary setObject:fullName forKey:ContactNameString];
        [newUserContactDictionary setObject:firstName forKey:firstNameString];
        [newUserContactDictionary setObject:middleName forKey:middleNameString];
        [newUserContactDictionary setObject:lastName forKey:lastNameString];
        
        
        //        if (contactImage != Nil) {
        //            [newUserContactDictionary setObject:contactImage forKey:ContactImageString];
        //        }
        
        
        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
        ABMultiValueRef multiPhones = ABRecordCopyValue(thisContact, kABPersonPhoneProperty);
        for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
            NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
            phoneNumber = [[[[[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@"\u00A0" withString:@""];
            
            
            if ([self.reachbilityInstance isReachable])
            {
                NSString *phoneNumberParsed =phoneNumber;
                NSString *_phoneNumberWithoutCode = phoneNumber;
                User *currentUser=[self getCurrentUserProfile];
                NSString* _countryCode = currentUser.userCountryCode;
                NBPhoneNumberUtil *_phoneNumberUtil = [NBPhoneNumberUtil sharedInstance];
                NSError *_error = nil;
                NBPhoneNumber *_myPhoneNumber = [_phoneNumberUtil parse:_phoneNumberWithoutCode defaultRegion: _countryCode error:&_error];
                
                if (_error == nil) {
                    
                    
                    NBEPhoneNumberType _phoneNumberType = [_phoneNumberUtil getNumberType:_myPhoneNumber];
                    
                    if ([_phoneNumberUtil isValidNumber:_myPhoneNumber]) {//Validate Phone Number
                        if (_phoneNumberType == NBEPhoneNumberTypeMOBILE || _phoneNumberType == NBEPhoneNumberTypeFIXED_LINE_OR_MOBILE) {
                            
                            phoneNumberParsed = [_phoneNumberUtil format:_myPhoneNumber numberFormat: NBEPhoneNumberFormatE164 error:&_error];
                            
                            // check if phone number not the same that registered with
                            ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
                            User* currentUser=[contactBWS getCurrentUserProfile];
                            if (![currentUser.userPhoneNumber isEqualToString:phoneNumberParsed]) {
                                [phoneNumbers addObject:phoneNumberParsed];
                            }
                            
                        }
                    }
                    
                    
                    
                    
                }else{
                    NSLog(@"Error : %@", [_error localizedDescription]);
                    phoneNumberParsed = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
                }
                
                if ([_phoneNumberWithoutCode length] == 0) {
                    phoneNumberParsed = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
                }
            }
            else
            {
                //[self showConnectionDown];
                
            }
            
            
            
            //         if(![self validatePhoneNumber:phoneNumber DefaultCountryCode:defualtCountryCode])
            //         {
            //
            //             phoneNumber =[NSString stringWithFormat:@"%@%@",defualtCountryCode,phoneNumber];
            //
            //             if([self validatePhoneNumber:phoneNumber DefaultCountryCode:defualtCountryCode])
            //             {
            //                  [phoneNumbers addObject:phoneNumber];
            //             }
            //
            //         }
            //            else
            //            {
            //                [phoneNumbers addObject:phoneNumber];
            //            }
            
            
            
            
        }
        
        [newUserContactDictionary setObject:phoneNumbers forKey:PhoneNumbersString];
        [newContactDictionary setObject:phoneNumbers forKey:PhoneNumbersString];
        
        
        //        NSMutableArray *contactEmails = [NSMutableArray new];
        //        ABMultiValueRef multiEmails = ABRecordCopyValue(thisContact, kABPersonEmailProperty);
        //        for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
        //            CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
        //            NSString *contactEmail = (__bridge NSString *)contactEmailRef;
        //            [contactEmails addObject:contactEmail];
        //        }
        
        [_phoneContacts addObject:newContactDictionary];
        [_userContacts addObject:newUserContactDictionary];
    }
        
        
    }
    
    
    
}


-(void)loadPhoneContactWithLoading:(NSString*)defualtCountryCode
{
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Load Contacts", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                           [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       NSError *error;
                       
                       [self loadPhoneContacts:defualtCountryCode];
                       [self compareContactsWithDB:&error];
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                          
                            
                                          
                                          
                                      });
                       
                   });
    
}

-(void)loadPhoneContactWithOutLoading:(NSString*)defualtCountryCode
{

    NSError *error;
    [self loadPhoneContacts:defualtCountryCode];
    [self compareContactsWithDB:&error];
    
    


    
    
}


-(void)compareContactsWithDB:(NSError**)error
{

    User *currentUser=[self getCurrentUserProfile];
    ContactsWebService *contactWS=[[ContactsWebService alloc]init];
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    NSMutableArray *newUserContacts=[[NSMutableArray alloc]init];
    
    NSString *deviceToken=[[NSUserDefaults standardUserDefaults] valueForKey:KVentDeviceToken];
    
    if (currentUser.userContatcs){
      //compare with local DB
        
        for (NSDictionary *ucontact in _userContacts) {
            Contacts *cotactDBexist=[contactRepo getUserContactFullName:[ucontact valueForKey:ContactNameString]  contactUser:currentUser Error:error];
            
            if (cotactDBexist == Nil) {
                //contact not exist in DB
                // then it is a new User
                // add to DB and send to check
                
                NSDictionary *contactPhoneNumbers=[ucontact objectForKey:PhoneNumbersString];
                if (contactPhoneNumbers.count >0) {
                    
                Contacts *addedContact=[contactRepo addContact:[ucontact valueForKey:firstNameString] contactMiddleName:[ucontact valueForKey:middleNameString] contactLastName:[ucontact valueForKey:lastNameString] contactFullName:[ucontact valueForKey:ContactNameString] contactIsVentUser:[NSNumber numberWithBool:FALSE] contactUser:currentUser contactImage:[ucontact valueForKey:ContactImageString] contactImageUrl:@"" contactID:[ucontact valueForKey:ContactIDString] Error:error];
                
                for (NSString *uPhoneNumber in [ucontact objectForKey:PhoneNumbersString]) {
                    [contactRepo addPhonesToContacts:uPhoneNumber phoneIsVentNumber:[NSNumber numberWithBool:FALSE] contact:addedContact Error:error];
                }
                
                
                [newUserContacts addObject:ucontact];
                }
                
                
            }
            else
            {
                //check if this user is vernt
                if ([cotactDBexist.contactIsVentUser isEqualToNumber:[NSNumber numberWithBool:NO]]) {
                     [newUserContacts addObject:ucontact];
                }
            }
            
            
            
        }
        
    }
    else
    {
        //send all list to contacts service
        newUserContacts =_userContacts;
        
        //add all contacts to DB
        
         for (NSDictionary *ucontact in _userContacts) {
             
             NSDictionary *contactPhoneNumbers=[ucontact objectForKey:PhoneNumbersString];
             if (contactPhoneNumbers.count >0) {

            Contacts *addedContact= [contactRepo addContact:[ucontact valueForKey:firstNameString] contactMiddleName:[ucontact valueForKey:middleNameString] contactLastName:[ucontact valueForKey:lastNameString] contactFullName:[ucontact valueForKey:ContactNameString] contactIsVentUser:[NSNumber numberWithBool:FALSE] contactUser:currentUser contactImage:[ucontact valueForKey:ContactImageString] contactImageUrl:@"" contactID:[ucontact valueForKey:ContactIDString] Error:error];
             
             for (NSString *uPhoneNumber in [ucontact objectForKey:PhoneNumbersString]) {
                 [contactRepo addPhonesToContacts:uPhoneNumber phoneIsVentNumber:[NSNumber numberWithBool:FALSE] contact:addedContact Error:error];
             }
                 
             }
             
         }
        
        
        
        
    }
    
    
    // send all contact to web service , and recieve array of phonenumbers
    // then update DB to mark contactas as vent users
    
    //[contactWS sendContacts:_userContacts phoneNumber:[[UserRepository sharedProfile]userPhoneNumber ] deviceToken:[[UserRepository sharedProfile]userDeviceUUID ] Error:error];
    
    NSMutableArray* ventContactsPhones= [contactWS sendContacts:newUserContacts phoneNumber:currentUser.userPhoneNumber deviceToken:deviceToken Error:error];
    
    for (NSString *vcontactphone in ventContactsPhones) {
        //NSString *vContactName=[vcontactphone valueForKey:ContactNameString];
        Contacts *cotactDBexist=[contactRepo getUserContactByPhoneNumber:vcontactphone contactUser:currentUser Error:error];
        
         if ([cotactDBexist.contactIsVentUser isEqualToNumber:[NSNumber numberWithBool:FALSE]]) {
             // make it vent user
             [cotactDBexist setContactIsVentUser:[NSNumber numberWithBool:TRUE]];
             [contactRepo updateContact:cotactDBexist Error:error];
         }
        
    }
    
    
}

- (NSArray*) loadContactsFromDB
{

    User *currentUser=[self getCurrentUserProfile];
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    NSArray *allContacts=[[NSArray alloc]init];
    NSError *error;
    
    allContacts=[contactRepo getAllUserContact:currentUser Error:&error];
    
    return allContacts;
}

- (NSArray*) checkAndLoadContactsFromDB:(NSString*)defualtCountryCode
{
 
    // check contacts Update then retrive updated from DB
     NSError *error;
    [self loadPhoneContacts:defualtCountryCode];
    [self compareContactsWithDB:&error];
    
    User *currentUser=[self getCurrentUserProfile];
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    NSArray *allContacts=[[NSArray alloc]init];
   
    
    allContacts=[contactRepo getAllUserContact:currentUser Error:&error];
    
    return allContacts;
}


-(BOOL)validatePhoneNumber:(NSString*)phoneNumber DefaultCountryCode:(NSString*)defaultCountryCode
{
    
    if ([self.reachbilityInstance isReachable])
    {
        NSString *phoneNumberParsed =phoneNumber;
        NSString *_phoneNumberWithoutCode = phoneNumber;
        NSString* _countryCode = defaultCountryCode;
        NBPhoneNumberUtil *_phoneNumberUtil = [NBPhoneNumberUtil sharedInstance];
        NSError *_error = nil;
        NBPhoneNumber *_myPhoneNumber = [_phoneNumberUtil parse:_phoneNumberWithoutCode defaultRegion: _countryCode error:&_error];
        
        if (_error == nil) {
            
            
            NBEPhoneNumberType _phoneNumberType = [_phoneNumberUtil getNumberType:_myPhoneNumber];
            
            if ([_phoneNumberUtil isValidNumber:_myPhoneNumber]) {//Validate Phone Number
                if (_phoneNumberType == NBEPhoneNumberTypeMOBILE || _phoneNumberType == NBEPhoneNumberTypeFIXED_LINE_OR_MOBILE) {
                    
                    phoneNumberParsed = [_phoneNumberUtil format:_myPhoneNumber numberFormat: NBEPhoneNumberFormatE164 error:&_error];
                    
                    return TRUE;
                }
            }
            
            
            
            
        }else{
            NSLog(@"Error : %@", [_error localizedDescription]);
            phoneNumberParsed = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
        }
        
        if ([_phoneNumberWithoutCode length] == 0) {
            phoneNumberParsed = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
        }
    }
    else
    {
        //[self showConnectionDown];
        return FALSE;
    }
    
    return FALSE;
    
    
}


-(void)checkOnlineContacts:(NSArray*)onlinePhones
{
    NSError*error;
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
     User *currentUser=[self getCurrentUserProfile];
   NSArray *allContacts= [contactRepo getAllUserContact:currentUser Error:&error];
    
    for (Contacts *contact in allContacts) {
        BOOL online=FALSE;
        for (ContactPhones *contactPhone in contact.contactPhones) {
            if ([onlinePhones containsObject:contactPhone.phoneNumber]) {
                online = TRUE;
            }
            
            
            if (online) {
                contact.contactIsOnline =[NSNumber numberWithBool:TRUE];
            }
            else
            {
                contact.contactIsOnline =[NSNumber numberWithBool:FALSE];
            }
            
            [contactRepo updateContact:contact Error:nil];
            
        }
    }
    
    
}

-(void)userISOnline:(NSString*)onlinePhone
{
    NSError*error;
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    User *currentUser=[self getCurrentUserProfile];
    NSArray *allContacts= [contactRepo getAllUserContact:currentUser Error:&error];
    
    for (Contacts *contact in allContacts) {
        for (ContactPhones *contactPhone in contact.contactPhones) {
            if ([onlinePhone isEqualToString:contactPhone.phoneNumber]) {
                contact.contactIsOnline =[NSNumber numberWithBool:TRUE];
                [contactRepo updateContact:contact Error:nil];
                break;
            }
  
            
        }
    }
    
    
}

-(void)userISOffline:(NSString*)onlinePhone
{
    NSError*error;
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    User *currentUser=[self getCurrentUserProfile];
    NSArray *allContacts= [contactRepo getAllUserContact:currentUser Error:&error];
    
    for (Contacts *contact in allContacts) {
        for (ContactPhones *contactPhone in contact.contactPhones) {
            if ([onlinePhone isEqualToString:contactPhone.phoneNumber]) {
                contact.contactIsOnline =[NSNumber numberWithBool:FALSE];
                [contactRepo updateContact:contact Error:nil];
                break;
            }
            
            
        }
    }
    
    
}


#pragma UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self openSettings];
    }
}

- (void)openSettings
{
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}

@end
