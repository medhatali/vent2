//
//  ChatListTableViewCell.h
//  Vent
//
//  Created by Medhat Ali on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"


@interface ChatListTableViewCell :SWTableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblLastMessageDate;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;

@property (strong, nonatomic) IBOutlet UIButton *lblNumberOfUnreadMessages;

- (void)updateCellDataFromModel:(id)model;


@end
