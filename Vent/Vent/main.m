//
//  main.m
//  Vent
//
//  Created by Medhat Ali on 2/26/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
//        #warning force RTL arabic
//       NSArray* languages = [NSArray arrayWithObjects:@"ar", nil];
//       [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
