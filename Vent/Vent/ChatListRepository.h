//
//  ChatListRepository.h
//  Vent
//
//  Created by Medhat Ali on 3/26/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseRepository.h"
#import "ChatList.h"
#import "ChatListUsersPhones.h"



@interface ChatListRepository : BaseRepository

-(ChatList *) addNewChatListWithPhoneNumber:(NSString*)chatStartDate
                                chatEnabled:(NSNumber*)chatEnabled
                                   chatType:(NSString*)chatType
                        chatLastUpdatedDate:(NSString*)chatLastUpdatedDate
                               chatSequence:(NSNumber*)chatSequence
                                   chatMute:(NSNumber*)chatMute
                                     chatID:(NSString*)chatID
                                   chatName:(NSString*)chatName
                            chatLastMessage:(NSString*)chatLastMessage
                              chatUserImage:(NSData*)chatUserImage
                           chatUserImageUrl:(NSString*)chatUserImageUrl
                              chatFromPhone:(NSString*)chatFromPhone
                                chatToPhone:(NSString*)chatToPhone
                            chatIsFavourite:(NSNumber*)chatIsFavourite
                              chatListOwner:(User*)chatListOwner
                            chatListSession:(ChatSession*)chatListSession
                              chatListUsers:(NSArray*)chatListUsers
                               chatIsSecure:(NSNumber*)chatIsSecure
                                      Error:(NSError **)error;



-(ChatListUsersPhones *) addUserPhoneToChatList:(NSString*)chatUserPhone
                                chatUserEnabled:(NSNumber*)chatUserEnabled
                               chatUserJoinDate:(NSString*)chatUserJoinDate
                                  phoneChatList:(ChatList*)phoneChatList
                                          Error:(NSError **)error;

-(void)deleteChatList:(ChatList *)ChatList Error:(NSError **)error;

-(void)updateChatList:(ChatList *)ChatList Error:(NSError **)error;

-(void)deleteChatListUsersPhones:(ChatListUsersPhones *)ChatListUsersPhones Error:(NSError **)error;

-(void)updateChatListUsersPhones:(ChatListUsersPhones *)ChatListUsersPhones Error:(NSError **)error;


-(NSArray*)getUserChatList:(User*)User Error:(NSError **)error;

-(NSArray*)getUserChatListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error;

-(ChatList*)getChatListByStartDate:(User*)User StartDate:(NSString*)startDate Error:(NSError **)error;

-(ChatList*)getChatListByID:(NSString*)Id Error:(NSError **)error;

-(NSArray*)getUserFavouriteChatListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error;

-(NSArray*)getChatListPhones:(ChatList*)ChatList Error:(NSError **)error;

-(ChatList*)getChatListByPhoneNumber:(NSString*)phone Error:(NSError **)error;

-(NSArray*)getChatListUserbyPhones:(NSArray*)phoneNumber Error:(NSError **)error;

-(ChatList*)getChatListByPhoneNumbers:(NSArray*)phone Error:(NSError **)error;

-(NSArray*)getChatListUserbyPhonesStrings:(NSArray*)phoneNumber Error:(NSError **)error;
-(ChatList*)getChatListByPhoneNumbersStrings:(NSArray*)phone Error:(NSError **)error;
-(ChatList*)getConversationByPhoneNumber:(NSArray*)phoneNumbers Error:(NSError **)error;

-(NSArray*)getChatListSessions:(ChatList*)ChatList Error:(NSError **)error;
-(NSArray*)getChatListSessionsWithRecordsNumber:(ChatList*)ChatList RecordsLimit:(NSInteger)recordsLimit Error:(NSError **)error;
-(NSArray*)getUnreadChatListSessions:(ChatList*)ChatList Error:(NSError **)error;
-(NSInteger)getAllUnreadChatListSessions:(ChatList*)ChatList Error:(NSError **)error;



@end
