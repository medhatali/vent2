//
//  ResultsSearchTableViewController.h
//  Vent
//
//  Created by Sameh Farouk on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultsSearchTableViewControllerDelegate.h"


@interface ResultsSearchTableViewController : UITableViewController

@property (nonatomic, assign) id<ResultsSearchTableViewControllerDelegate>resultsDelegate;
@property (nonatomic, strong) NSMutableArray *filteredEntries;

@end
