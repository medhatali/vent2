//
//  CommonBussinessSevice.m
//  Vent
//
//  Created by Medhat Ali on 9/10/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "CommonBussinessSevice.h"
#import "MessageDM.h"
#import "UserDeviceIdentifierDM.h"
#import "GroupDM.h"
#import "MessageNotificationBussiness.h"
#import "GroupListRepository.h"
#import "GroupDM.h"
#import "UploadDownLoadBussinessService.h"
#import "ContactsBussinessService.h"
#import "GroupNotificationBussiness.h"
#import "MessageTimeDM.h"
#import "ChatList.h"
#import "GroupList.h"
#import "BroadcastList.h"
#import "NSUserDefaultsExtensions.h"
#import "PushScreenDM.h"
#import "MessageNotificationBussiness.h"
#import "NSDate+DateFormater.h"


@interface CommonBussinessSevice ()
@property (strong,nonatomic) MessageNotificationBussiness * messageBussiness;


@end

@implementation CommonBussinessSevice

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.messageBussiness =[[MessageNotificationBussiness alloc]init];
    }
    return self;
}


-(id) recieveMessagesFromServer:(NSArray *) messages isAPNRecieved:(BOOL)isAPNRecieved
{
    
    // get message from server
    
    for (MessageDM *messageModel in messages) {
        
        
        NSError* error;
       // if (messageModel.contentType == text) {
          
            id currentlist= [self.messageBussiness didRecieveSignalRMessage:messageModel IsSent:FALSE  Error:&error];
            
            if (isAPNRecieved) {
                PushScreenDM *pushScreenObj=[[PushScreenDM alloc]init];
                
                if ([currentlist isKindOfClass:[ChatList class]]) {
                    pushScreenObj.screenType =@"1";
                    pushScreenObj.listId =((ChatList*)currentlist).chatID;
                    
                   
                    
                }
                else if ([currentlist isKindOfClass:[GroupList class]])
                {
                    pushScreenObj.screenType =@"2";
                    pushScreenObj.listId =((GroupList*)currentlist).groupID;
                }
                else if ([currentlist isKindOfClass:[BroadcastList class]])
                {
                    pushScreenObj.screenType =@"3";
                    pushScreenObj.listId =((BroadcastList*)currentlist).broadcastID;
                }
                
                [[NSUserDefaults standardUserDefaults] saveCustomObject:pushScreenObj key:kScreenPushNotification];
                
                
                
            }
            
     //   }
        
        
        return currentlist;
    }
    
    return nil;
    
}

-(void) markMessageAsDelivered:(NSArray *) messages
{
    
     for (MessageTimeDM *messageTimeModel in messages) {

    NSString *date = [[NSDate alloc] getDateString:[messageTimeModel messageTime]];
    
    NSError* error;
   
    [self.messageBussiness didRecieveSignalRSignalRMarkAsDelivered:messageTimeModel.messageId OnMessageTime:date IsSent:FALSE  Error:&error];
     }
    
}

-(void) markMessageAsSent:(NSArray *) messages
{
    
    
    for (MessageTimeDM *messageTimeModel in messages) {
        NSError* error;
        [self.messageBussiness didRecieveSignalRSignalRMarkAsSent:messageTimeModel IsSent:FALSE  Error:&error];

    }
    

         
                       
}

-(void) markMessageAsRead:(NSArray *) messages
{
    for (MessageTimeDM *messageTimeModel in messages) {
        NSError* error;
        NSString *date = [[NSDate alloc] getDateString:[messageTimeModel messageTime]];

        [self.messageBussiness didRecieveSignalRSignalRMarkAsRead:messageTimeModel.messageId OnMessageTime:date IsSent:FALSE  Error:&error];
        
    }
    
}



@end
