//
//  CommonBussinessSevice.h
//  Vent
//
//  Created by Medhat Ali on 9/10/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BussinessSevericeController.h"

@interface CommonBussinessSevice : BussinessSevericeController


-(id) recieveMessagesFromServer:(NSArray *) messages isAPNRecieved:(BOOL)isAPNRecieved;
-(void) markMessageAsDelivered:(NSArray *) messages;
-(void) markMessageAsSent:(NSArray *) messages;
-(void) markMessageAsRead:(NSArray *) messages;



@end
