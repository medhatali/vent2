//
//  GroupNotificationBussiness.m
//  Vent
//
//  Created by Medhat Ali on 5/31/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupNotificationBussiness.h"
#import "DataModelObjectParent.h"
#import "GlobalHandler.h"
#import "GroupListRepository.h"
#import "ContactsBussinessService.h"

@implementation GroupNotificationBussiness


- (void)didGetGroupList:(NSArray*)groupList Error:(NSError**)error
{
   /// check groups ids if exist do nothing
    /// if not add to group list
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    for (NSString * groupId in groupList) {
       GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:groupId Error:error];
        
        if (foundGroup  == nil) {
            [groupRepo addNewGroupList:groupId groupName:@"" groupStartDate:@"" groupEnabled:[NSNumber numberWithBool:NO] groupMuted:[NSNumber numberWithBool:NO] groupLastUpdatedDate:@"" groupSequence:[NSNumber numberWithInt:0] groupImage:nil groupImageUrl:@"" groupLastMessage:@"" groupListOwner:currentUser groupListSession:nil groupListUsers:Nil  AdminPhoneNumber:nil Error:error];
            
            [GlobalHandler getGroup:groupId];
        }

        
    }
    
    
    // revoke to get droup details
    
    
    
    
}

- (void)didgetGroupInfo:(GroupDM*)GroupDM Error:(NSError**)error
{
    /// check groups ids if exist just update DB
    /// if not add to group list
    
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
        GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:GroupDM.GroupId Error:error];
        
        if (foundGroup  == nil) {
            [groupRepo addNewGroupList:GroupDM.GroupId groupName:GroupDM.GroupTitle groupStartDate:@"" groupEnabled:[NSNumber numberWithBool:NO] groupMuted:[NSNumber numberWithBool:NO] groupLastUpdatedDate:@"" groupSequence:[NSNumber numberWithInt:0] groupImage:nil groupImageUrl:GroupDM.GroupIconPath groupLastMessage:@"" groupListOwner:currentUser groupListSession:nil groupListUsers:GroupDM.MembersPhoneNumber AdminPhoneNumber:GroupDM.AdminPhoneNumber Error:error];
            
        }
        else
        {
            //update existing one
            
            //foundGroup.groupImageUrl=GroupDM.GroupIconPath;
            foundGroup.groupName = GroupDM.GroupTitle;
            //foundGroup.groupListUsers = [NSSet setWithArray:GroupDM.MembersPhoneNumber];
            
            //[groupRepo updateGroupList:foundGroup Error:nil];
            [groupRepo updateGroupList:foundGroup groupListUsers:GroupDM.MembersPhoneNumber  Error:nil];
        }
        
    
    
    
    // revoke to get droup details
    
    
    
    
}


- (void)changeGroupTitle:(NSString*)groupID Title:(NSString*)title Error:(NSError**)error
{
    /// check groups ids if exist just update title in DB
    
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];

    
    GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:groupID Error:error];
    
    if (foundGroup  == nil) {
        // get group info
        [GlobalHandler getGroup:groupID];
        
    }
    else
    {
        //update existing one
        
        foundGroup.groupName = title ;
 
        [groupRepo updateGroupList:foundGroup Error:nil];
    }
    
    
    
    
    // revoke to get droup details
    
    
    
    
}


- (void)removeUserFromGroup:(NSString*)useridentify GroupId:(NSString*)groupID Error:(NSError**)error
{
    /// check groups ids if exist just update title in DB
    
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];
    
    
    GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:groupID Error:error];
    
    if (foundGroup  == nil) {
        // get group info
        [GlobalHandler getGroup:groupID];
        
    }
    else
    {
        //delete user phone from group existing one
        
        
        [groupRepo deletePhoneNumberExistInGroupList:useridentify GroupList:foundGroup Error:error];
    }
    
    
    
    
    // revoke to get droup details
    
    
    
    
}

- (void)addUserToGroup:(NSString*)useridentify GroupId:(NSString*)groupID Error:(NSError**)error
{
    /// check groups ids if exist just add
    
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];
    
    
    GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:groupID Error:error];
    
    if (foundGroup  == nil) {
        // get group info
        [GlobalHandler getGroup:groupID];
        
    }
    else
    {
        //delete user phone from group existing one
        
        
        [groupRepo addUserPhoneToGroupList:useridentify groupUserEnabled:[NSNumber numberWithBool:YES] groupUserJoinDate:@"" phoneGroupList:foundGroup Error:error];
        
    }
    
    
    
    
    // revoke to get droup details
    
    
    
    
}


- (void)whenILeaveGroup:(NSString*)groupID Error:(NSError**)error
{
    /// delete the group from my list
    
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];
    
    
    GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:groupID Error:error];
    
    
    if (foundGroup  == nil) {
        // get group info
        [GlobalHandler getGroup:groupID];
        
    }
    else
    {
        //delete user phone from group existing one
        
        
        [groupRepo deleteGroupList:foundGroup Error:error];
    }
    
    
    
    
    // revoke to get droup details
    
    
    
    
}

- (void)changeGroupAdmin:(NSString*)useridentify GroupId:(NSString*)groupID Error:(NSError**)error
{
    /// check groups ids if exist just add
    
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];
    
    
    GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:groupID Error:error];
    
    if (foundGroup  == nil) {
        // get group info
        [GlobalHandler getGroup:groupID];
        
    }
    else
    {
        //change group admin
        
        
        [groupRepo changeGroupListAdmin:useridentify userGroupList:foundGroup Error:error];
        
    }
    
    
    
    
    // revoke to get droup details
    
    
    
    
}

@end
