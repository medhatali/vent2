//
//  ResultsContactsTableView.h
//  Vent
//
//  Created by Sameh Farouk on 3/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseContactsTableView.h"

@interface ResultsContactsTableView : BaseContactsTableView

@property (nonatomic, strong) NSMutableArray *filteredContacts;

@end
