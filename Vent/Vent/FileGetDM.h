//
//  FileGetDM.h
//  Vent
//
//  Created by Sameh Farouk on 5/19/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "DataModelObjectParent.h"

@interface FileGetDM : DataModelObjectParent

typedef enum {
    Thumbnail = 0,
    File = 1
} StoreType;

/// properties for data retrieved
@property (nonatomic) StoreType type;
@property (strong ,nonatomic) NSString* FileId;

@end
