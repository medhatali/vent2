//
//  AboutViewController.m
//  Vent
//
//  Created by Medhat Ali on 11/3/15.
//  Copyright © 2015 ITSC. All rights reserved.
//

#import "AboutViewController.h"
#import "AppUtility.h"

@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblVersion.text =( NSString *)[[AppUtility sharedUtilityInstance] getAppVersionBuild];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
     [self.tabBarController.tabBar setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
