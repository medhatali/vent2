//
//  ContactsDM.h
//  Vent
//
//  Created by Medhat Ali on 3/10/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "DataModelObjectParent.h"

@interface ContactsDM : DataModelObjectParent

@property (strong, nonatomic) NSMutableArray *phoneContatcs;
@property (strong, nonatomic) NSMutableArray *userContatc;

@end
