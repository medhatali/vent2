//
//  StatusListTableViewController.h
//  Vent
//
//  Created by Medhat Ali on 7/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusListTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
