//
//  PushNotifiactionHandler.m
//  Vent
//
//  Created by Medhat Ali on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "PushNotifiactionHandler.h"

@interface PushNotifiactionHandler ()

@property (copy)void (^NotifTokenHandler)(NSString* token);

@end


@implementation PushNotifiactionHandler

- (id)init
{
    self = [super init];
    if (self) {
     

        
    }
    return self;
}


-(void)initPushNotification:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
        
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    [self registerForNotificationsWithCompletionHandler:nil];
    
    
//    if (launchOptions != nil)
//    {
//        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//        if (userInfo != nil)
//        {
//            NSLog(@"Launched from push notification: %@", userInfo);
//            [self handleRemoteNotification:userInfo];
//        }
//    }

        
}


-(void) registerForNotificationsWithCompletionHandler:(void (^)(NSString* token)) tokenHandler
{
    UIUserNotificationType types;
    
    
    types= UIUserNotificationTypeBadge| UIUserNotificationTypeAlert;
    types= UIUserNotificationTypeBadge |
    UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    
    UIUserNotificationSettings *mySettings =
    [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    self.NotifTokenHandler = tokenHandler;
}

//-(void) handleRemoteNotification:(NSDictionary *)userInfo
//{
//    for (id key in userInfo)
//    {
//        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
//    }
//    
//    NSLog(@"remote notification: %@",[userInfo description]);
//    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
//    
//    NSString *alert = [apsInfo objectForKey:@"alert"];
//    NSLog(@"Received Push Alert: %@", alert);
//    
//    // [apsInfo setValue:@"" forKey:@"sound"];
//    NSString *sound = [apsInfo objectForKey:@"sound"];
//    NSLog(@"Received Push Sound: %@", sound);
//    
//    NSString *badge = [apsInfo objectForKey:@"badge"];
//    NSLog(@"Received Push Badge: %@", badge);
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];
//    
//    
//    //        // play custom sound
//    //        NSURL *fileURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds/ReceivedMessage.caf"]; // see list below
//    //        SystemSoundID soundID;
//    //        AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)fileURL,&soundID);
//    //        AudioServicesPlaySystemSound(soundID);
//    
//    
//
//    
////    UIAlertView *pushAlert=[[UIAlertView alloc]initWithTitle:@"Push Recieved" message:alert delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:@"Ok", nil];
////    [pushAlert show];
//    
//    
//    
//    /*    "{\"NotificationType\":1,\"NotificationOrMessageId\":\"4038f4d5-8330-c1fb-1ff1-d4ec0e31b9a2\",\"InfoDto\":{\"MessageId\":\"4038f4d5-8330-c1fb-1ff1-d4ec0e31b9a2\",\"GroupId\":null,\"ConversationId\":null,\"Content\":\"ccvc\",\"FromPhoneNumber\":\"+2002\",\"ToPhoneNumbers\":[\"+2001\",\"+2002\"],\"DeviceToken\":\"AN\",\"ContentType\":1,\"SendingDateTime\":\"2015-08-18T14:44:14.8714236+02:00\",\"MessageTimer\":0}}"
//     */
//    //{"aps":{"alert":"Hello from APNs Tester.","badge":"1" , "NotificationDetail" : ""}}
//    
//
//    
//    
//    
//    
//    // [self byPassLoginScreen];
//    
//    if ([self isLoggedIn]) {
//        //if user loged in
//        // go to notification screen and reoad data
//        [self byPassLoginScreen];
//        
//    }
//    
//}
//

-(void) byPassLoginScreen
{
    //    //     go to main notification screen
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    UINavigationController *navigationController = (UINavigationController *) self.window.rootViewController;
    //    // FirstViewController *firstController = (FirstViewController *) self.window.rootViewController;
    //    LoginViewController * loginVC=(LoginViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Login"];
    //    [navigationController pushViewController:loginVC animated:NO];
    //
    //    MainMenuController * mainVC=(MainMenuController*)[storyboard instantiateViewControllerWithIdentifier:@"MainMenu"];
    //    [navigationController pushViewController:mainVC animated:NO];
    //
    //
    //   self.window.rootViewController = navigationController;
}

-(BOOL) isLoggedIn
{
    //    User *user = [User currentUser];
    //    if (user) {
    //
    //        if (user.userId) {
    //            return YES;
    //        }
    //        
    //    }
    return NO;
}

-(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
     NSString *tokenString = [self stringWithDeviceToken:deviceToken];
    
    [self setDeviceToken:tokenString];
    if (self.NotifTokenHandler) {
        
        NSString *tokenString = [self stringWithDeviceToken:deviceToken];
        self.NotifTokenHandler(tokenString);
    }
}


- (NSString*)stringWithDeviceToken:(NSData*)deviceToken {
    const char* data = [deviceToken bytes];
    NSMutableString* token = [NSMutableString string];
    
    for (int i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }
    
    return [token copy];
}



- (NSString*)deviceToken
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:KVentDeviceToken];
}

- (void)setDeviceToken:(NSString*)token
{
    if (token) {
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:KVentDeviceToken];
    }
}





- (void)showAlarm:(NSString *)text {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alarm"
                                                        message:text delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

@end
