//
//  BaseRepository.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "BaseRepository.h"

@implementation BaseRepository

#pragma -mark Basic Functions

#pragma -mark cretae UUID
-(NSString *)uniqueUUID
{
    CFUUIDRef theUniqueString = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUniqueString);
    CFRelease(theUniqueString);
    return (__bridge NSString *)string ;
}

-(void)insertModel:(NSManagedObject*)momodel EntityName:(NSString *)entityName Error:(NSError **)error
{
    NSManagedObjectContext *context = [[CoreDataManager sharedManager] managedObjectContext];
    
    // Save the context.
    [self saveContextInMainQueue:context Error:error];
    
}

-(void)deleteModel:(NSManagedObject *)momodel EntityName:(NSString *)entityName Error:(NSError **)error
{
    NSManagedObjectContext *context = [[CoreDataManager sharedManager] managedObjectContext];
    [context deleteObject:momodel];
    
    // Save the context.
    [self saveContextInMainQueue:context Error:error];
    
}

-(void)updateModel:(NSManagedObject *)momodel EntityName:(NSString *)entityName Error:(NSError **)error
{
    NSManagedObjectContext *context = [[CoreDataManager sharedManager] managedObjectContext];
    
    // Save the context.
    [self saveContextInMainQueue:context Error:error];
}

-(void)insertModelInPrivateContext:(NSManagedObject *)momodel Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock
{
    
    [[CoreDataManager sharedManager] saveDataInPrivateContext:^(NSManagedObjectContext *localContext) {
        
        
    } completionBlock:^(BOOL success, NSManagedObjectContext *context, NSError *error) {
        
        NSLog(@"data base saved ");
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (!success) {
                completionBlock(NO,nil,error);
                
            } else {
                
                completionBlock(YES,nil,error);
                
            }
        });
        
    }
     
     
     ];
    
    
    
}

-(void)deleteModelInPrivateContext:(NSManagedObject *)momodel Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock
{
    
    [[CoreDataManager sharedManager] saveDataInPrivateContext:^(NSManagedObjectContext *localContext) {
        
        [localContext deleteObject:momodel];
        
        
    } completionBlock:^(BOOL success, NSManagedObjectContext *context, NSError *error) {
        
        NSLog(@"data base saved ");
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (!success) {
                completionBlock(NO,nil,error);
                
            } else {
                
                completionBlock(YES,nil,error);
                
            }
        });
        
    }
     
     
     ];
    
    
    
}

-(void)updateModelInPrivateContext:(NSManagedObject *)momodel Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock
{
    
    [[CoreDataManager sharedManager] saveDataInPrivateContext:^(NSManagedObjectContext *localContext) {
        
        
        
    } completionBlock:^(BOOL success, NSManagedObjectContext *context, NSError *error) {
        
        NSLog(@"data base saved ");
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (!success) {
                completionBlock(NO,nil,error);
                
            } else {
                
                completionBlock(YES,nil,error);
                
            }
        });
        
    }
     
     
     ];
    
    
}


#pragma -mark methods for all rows

-(NSMutableArray*)getAllRows:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError **)error
{
    NSMutableArray *returnData=[[NSMutableArray alloc]init];
    returnData= [self getResultsFromEntity:entityName predicateOrNil:predicateOrNil ascSortStringOrNil:ascSortStringOrNil descSortStringOrNil:descSortStringOrNil Error:error];
    
    return returnData;
}

-(void)getAllRows:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError **)error CompletionBlock:(myCompletionBlock)completionBlock
{
    [[CoreDataManager sharedManager] retrieveDataInPrivateContextFromDB:entityName predicateOrNil:predicateOrNil ascSortStringOrNil:ascSortStringOrNil descSortStringOrNil:descSortStringOrNil Error:error CompletionBlock:completionBlock];
    
}


-(void)deleteAllRows:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError **)error
{
    NSMutableArray *returnData=[[NSMutableArray alloc]init];
    returnData= [self getAllRows:entityName predicateOrNil:predicateOrNil ascSortStringOrNil:ascSortStringOrNil descSortStringOrNil:descSortStringOrNil Error:error];
    
    for (NSManagedObject *row in returnData) {
        [self deleteModel:row EntityName:entityName Error:error];
    }
    
}

-(void)deleteAllRows:(NSString *)entityName Error:(NSError **)error CompletionBlock:(myCompletionBlock)completionBlock
{
    
    
    [[CoreDataManager sharedManager] saveDataInPrivateContext:^(NSManagedObjectContext *localContext) {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
        [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
        
        NSError *error;
        NSArray *fetchedObjects = [localContext executeFetchRequest:fetchRequest error:&error];
        for (NSManagedObject *object in fetchedObjects)
        {
            [localContext deleteObject:object];
        }
        
    } completionBlock:^(BOOL success, NSManagedObjectContext *context, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (!success) {
                completionBlock(NO,nil,error);
                
            } else {
                
                completionBlock(YES,nil,error);
                
            }
        });
        
    }
     
     
     ];
    
    
    
}

#pragma -mark common methods

-(NSManagedObject*)createModelByEntityName:(NSString *)entityName Error:(NSError **)error
{
    
    NSManagedObjectContext *context = [[CoreDataManager sharedManager] managedObjectContext];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    return newManagedObject;
}

-(NSManagedObject*)createModelForCurrentEntity:(NSError **)error
{
    return [self createModelByEntityName:self.repositoryEntityName Error:error];
}




#pragma -mark helper methods

- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName Error:(NSError **)error
{
    return [self getResultsFromEntity:entityName predicateOrNil:nil Error:error] ;
}
- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil Error:(NSError **)error
{
    return [self getResultsFromEntity:entityName predicateOrNil:predicateOrNil ascSortStringOrNil:nil descSortStringOrNil:nil Error:error];
}

- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil Context:( NSManagedObjectContext *)context Error:(NSError **)error
{
    return [self getResultsFromEntity:entityName predicateOrNil:predicateOrNil ascSortStringOrNil:nil descSortStringOrNil:nil Context:context Error:error];
}

- (void) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil Error:(NSError **)error CompletionBlock:(myCompletionBlock)completionBlock
{
    [[CoreDataManager sharedManager] retrieveDataInPrivateContextFromDB:entityName predicateOrNil:predicateOrNil ascSortStringOrNil:nil descSortStringOrNil:nil Error:error CompletionBlock:completionBlock];
}

- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError **)error
{
    
    NSManagedObjectContext *context = [[CoreDataManager sharedManager] managedObjectContext];
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    
    if(predicateOrNil != nil)
    {
        [request setPredicate:predicateOrNil];
    }
    
    // Edit the sort key as appropriate.
    NSMutableArray *sortDescriptors = [[NSMutableArray alloc]init];
    
    if(ascSortStringOrNil != nil)
    {
        for (NSString *asc in ascSortStringOrNil)
        {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:asc ascending:NO] ;
            [sortDescriptors addObject:sortDescriptor];
        }
    }
    if(descSortStringOrNil != nil)
    {
        for (NSString *desc in descSortStringOrNil)
        {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:desc ascending:YES] ;
            [sortDescriptors addObject:sortDescriptor];
        }
    }
    [request setSortDescriptors:sortDescriptors];
    
    //NSError *fetcherror;
    
    NSMutableArray *mutableFetchResults = [[context
                                            executeFetchRequest:request error:error] mutableCopy];
    
    if (mutableFetchResults == nil) {
        // Handle the error.
        
        
    }
    
    
    return mutableFetchResults;
}

- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Context:( NSManagedObjectContext *)context Error:(NSError **)error
{
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    
    if(predicateOrNil != nil)
    {
        [request setPredicate:predicateOrNil];
    }
    
    // Edit the sort key as appropriate.
    NSMutableArray *sortDescriptors = [[NSMutableArray alloc]init];
    
    if(ascSortStringOrNil != nil)
    {
        for (NSString *asc in ascSortStringOrNil)
        {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:asc ascending:NO] ;
            [sortDescriptors addObject:sortDescriptor];
        }
    }
    if(descSortStringOrNil != nil)
    {
        for (NSString *desc in descSortStringOrNil)
        {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:desc ascending:YES] ;
            [sortDescriptors addObject:sortDescriptor];
        }
    }
    [request setSortDescriptors:sortDescriptors];
    
    //NSError *fetcherror;
    
    NSMutableArray *mutableFetchResults = [[context
                                            executeFetchRequest:request error:error] mutableCopy];
    
    if (mutableFetchResults == nil) {
        // Handle the error.
        
        
    }
    
    
    return mutableFetchResults;
}


-(void)getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock
{
    
    [[CoreDataManager sharedManager] saveDataInPrivateContext:^(NSManagedObjectContext *localContext) {
        
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                                  inManagedObjectContext:localContext];
        
        
        [request setEntity:entity];
        
        if(predicateOrNil != nil)
        {
            [request setPredicate:predicateOrNil];
        }
        
        // Edit the sort key as appropriate.
        NSMutableArray *sortDescriptors = [[NSMutableArray alloc]init];
        
        if(ascSortStringOrNil != nil)
        {
            for (NSString *asc in ascSortStringOrNil)
            {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:asc ascending:NO] ;
                [sortDescriptors addObject:sortDescriptor];
            }
        }
        if(descSortStringOrNil != nil)
        {
            for (NSString *desc in descSortStringOrNil)
            {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:desc ascending:YES] ;
                [sortDescriptors addObject:sortDescriptor];
            }
        }
        [request setSortDescriptors:sortDescriptors];
        
        //NSError *fetcherror;
        
        NSMutableArray *mutableFetchResults = [[localContext
                                                executeFetchRequest:request error:error] mutableCopy];
        
        
        if (mutableFetchResults == nil) {
            // Handle the error.
        }
        
        if (error) {
            completionBlock(NO,nil,*error);
            
        }else {
            completionBlock(YES,[mutableFetchResults objectAtIndex:0],nil);
        }
        
        
        
    } completionBlock:^(BOOL success, NSManagedObjectContext *context, NSError *error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (!success) {
                completionBlock(NO,nil,error);
                
            } else {
                
                completionBlock(YES,nil,error);
                
            }
        });
        
    }
     
     
     ];
    
    
}




@end
