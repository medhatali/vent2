//
//  GroupListTableViewController.h
//  Vent
//
//  Created by Medhat Ali on 4/5/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MainMasterTableTVC.h"
#import "SWTableViewCell.h"
#import "ChatListHeaderTableViewCell.h"
#import "ChatListTableViewCell.h"
#import "ContactListDelegate.h"
#import "MainMasterTableTVC.h"

@interface GroupListTableViewController : MainMasterTableTVC <FCVerticalMenuDelegate,SWTableViewCellDelegate,ContactListDelegate>

@property (nonatomic, copy) NSArray *GroupList;

@end
