//
//  ChatList.h
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChatListUsersPhones, ChatSession, User;

@interface ChatList : NSManagedObject

@property (nonatomic, retain) NSNumber * chatEnabled;
@property (nonatomic, retain) NSString * chatFromPhone;
@property (nonatomic, retain) NSString * chatID;
@property (nonatomic, retain) NSNumber * chatIsFavourite;
@property (nonatomic, retain) NSNumber * chatIsSecure;
@property (nonatomic, retain) NSString * chatLastMessage;
@property (nonatomic, retain) NSString * chatLastUpdatedDate;
@property (nonatomic, retain) NSNumber * chatMute;
@property (nonatomic, retain) NSString * chatName;
@property (nonatomic, retain) NSNumber * chatSequence;
@property (nonatomic, retain) NSString * chatStartDate;
@property (nonatomic, retain) NSString * chatToPhone;
@property (nonatomic, retain) NSString * chatType;
@property (nonatomic, retain) NSData * chatUserImage;
@property (nonatomic, retain) NSString * chatUserImageUrl;
@property (nonatomic, retain) NSNumber * chatIsBlocked;
@property (nonatomic, retain) User *chatListOwner;
@property (nonatomic, retain) NSSet *chatListSession;
@property (nonatomic, retain) NSSet *chatListUsers;
@end

@interface ChatList (CoreDataGeneratedAccessors)

- (void)addChatListSessionObject:(ChatSession *)value;
- (void)removeChatListSessionObject:(ChatSession *)value;
- (void)addChatListSession:(NSSet *)values;
- (void)removeChatListSession:(NSSet *)values;

- (void)addChatListUsersObject:(ChatListUsersPhones *)value;
- (void)removeChatListUsersObject:(ChatListUsersPhones *)value;
- (void)addChatListUsers:(NSSet *)values;
- (void)removeChatListUsers:(NSSet *)values;

@end
