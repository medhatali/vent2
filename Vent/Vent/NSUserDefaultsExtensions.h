//
//  NSUserDefaultsExtensions.h
//  Vent
//
//  Created by Medhat Ali on 8/20/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (NSUserDefaultsExtensions)

- (void)saveCustomObject:(id<NSCoding>)object
                     key:(NSString *)key;
- (id<NSCoding>)loadCustomObjectWithKey:(NSString *)key;

@end
