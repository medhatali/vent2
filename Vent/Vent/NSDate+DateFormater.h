//
//  NSDate+DateFormater.h
//  Vent
//
//  Created by Medhat Ali on 9/10/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DefaultFormatDateTime @"yyyy-MM-dd'T'HH:mm:ss"
#define DefaultShortFormatDateTime @"yyyy-MM-dd HH:mm"
#define ServerDateTimeFormat @"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZ"


@interface NSDate (DateFormater)

#pragma -mark Date Handling

-(NSString *)dateFormatedWithStyle:(NSDateFormatterStyle)style;

- (NSString*)getDateString:(NSDate*)date;
- (NSString*)getShortDateString:(NSDate*)date;
- (NSDate*)getDateFromString:(NSString*)date;
- (NSDate*)getDateFromServerString:(NSString*)date;
- (NSDate*)getShortDateFromServerString:(NSString*)date;
- (NSDate*)getFormatDateFromDate:(NSDate*)date;

-(NSDate*) getDatefromServerDateString:(NSString*)serverDateString;
- (NSString*)getServerDateString:(NSDate*)date;
- (NSDate*)getServerDateFromDate:(NSDate*)date;


#pragma -mark Time Handling
-(NSString *)timeFormatedWithStyle:(NSDateFormatterStyle)style;

-(NSString *)timeFormatedWithStyle:(NSDateFormatterStyle)style withTimeZone:(NSTimeZone*)timezone;

#pragma -mark Date And Time Handling

-(NSString *)dateFormatedWithFormat:(NSString*)format;

-(NSString *)dateFormatedWithFormat:(NSString*)format withTimeZone:(NSTimeZone*)timezone;

-(NSString *)dateFormatedWithStyle:(NSDateFormatterStyle)dateStyle timeFormatedWithStyle:(NSDateFormatterStyle)timeStyle;

-(NSString *)dateFormatedWithStyle:(NSDateFormatterStyle)dateStyle timeFormatedWithStyle:(NSDateFormatterStyle)timeStyle withTimeZone:(NSTimeZone*)timezone;

@end

