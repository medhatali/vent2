//
//  RegisterWebService.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "CommonWebService.h"
#import "UserDM.h"
#import "VerifyDM.h"

@interface RegisterWebService : CommonWebService

- (NSDictionary *)registerUser:(UserDM *)userDM Error:(NSError**)error;

- (NSDictionary *)verifyPhoneNumber:(VerifyDM *)verifyDM Error:(NSError**)error ;

@end
