//
//  AppUtility.h
//  Education
//
//  Created by Medhat Ali on 3/28/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppUtility : NSObject

@property (strong,nonatomic) NSMutableDictionary *infolist;
@property (strong,nonatomic) NSDictionary *EnvironmentParamters;

typedef NS_ENUM(NSInteger, LanguageType) {
    English          = 0,
    Arabic     = 1
};


+ (AppUtility *)sharedUtilityInstance;


-(id)getAppConfigInfoByKey:(NSString*)key SecondKeyorNil:(NSString*)secondKey;
-(id)getEnvironmentByKey:(NSString*)key SecondKeyorNil:(NSString*)secondKey;

- (NSDictionary*)loadEnvironmentsParamters:(NSString*)envkey;


-(NSString*)getServiceUrl;
-(NSString*)getSignalRServiceUrl;
-(NSString*)getSignalRPhoneNumber;
-(NSString*)getSignalRDeviceToken;
-(NSString*)getSignalRAppHub;
-(NSArray*)getTopMenuList;
-(NSArray*)getMediaDirectory;

-(UIColor *)colorBaseTheme;
-(UIColor *)colorBackThemeColor;
-(NSString *) getAppVersionBuild;

-(LanguageType)getCurrentLanguage;

@end
