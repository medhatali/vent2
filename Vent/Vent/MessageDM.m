//
//  MessageDM.m
//  Vent
//
//  Created by Sameh Farouk on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "MessageDM.h"
#import "NSDate+DateFormater.h"

@implementation MessageDM


NSString *const mMessageId = @"MessageId";
NSString *const mGroupId = @"GroupId";
NSString *const mContent = @"Content";
NSString *const mFromPhoneNumber = @"FromPhoneNumber";
NSString *const mToPhoneNumbers = @"ToPhoneNumbers";
NSString *const mDeviceToken = @"DeviceToken";
NSString *const mContentType = @"ContentType";
NSString *const mSendingDateTime = @"SendingDateTime";
NSString *const mMessageTimer = @"MessageTimer";


- (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    MessageDM *message = [[MessageDM alloc]init];
    
    message.messageId = [dataDictionary objectForKey:mMessageId];
    message.groupId = [dataDictionary objectForKey:mGroupId];
    message.content = [dataDictionary objectForKey:mContent];
    message.fromPhoneNumber = [dataDictionary objectForKey:mFromPhoneNumber];
    message.toPhoneNumbers = [dataDictionary objectForKey:mToPhoneNumbers];
    message.deviceToken = [dataDictionary objectForKey:mDeviceToken];
    message.contentType = (ContentType)[[dataDictionary objectForKey:mContentType] integerValue];
    message.messageTimer = (NSInteger)[dataDictionary objectForKey:mMessageTimer];
    

    NSDate *date = [[NSDate alloc]getDateFromServerString:mSendingDateTime];
    message.sendingDateTime = date;
    
    return message;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    
    NSMutableArray *keysArray = [[NSMutableArray alloc]init];
    NSMutableArray *valuesArray = [[NSMutableArray alloc]init];
    
    
    if (self.messageId) {
        [keysArray addObject:mMessageId];
        [valuesArray addObject:self.messageId];
    }
    
    if (self.groupId) {
        [keysArray addObject:mGroupId];
        [valuesArray addObject:self.groupId];
    }
    
    if (self.content) {
        [keysArray addObject:mContent];
        [valuesArray addObject:[NSString stringWithFormat:@"%@", self.content]];
    }
    
    if (self.fromPhoneNumber) {
        [keysArray addObject:mFromPhoneNumber];
        [valuesArray addObject:self.fromPhoneNumber];
    }
    
    if (self.toPhoneNumbers) {
        [keysArray addObject:mToPhoneNumbers];
        [valuesArray addObject:self.toPhoneNumbers];
    }
    
    if (self.deviceToken) {
        [keysArray addObject:mDeviceToken];
        [valuesArray addObject:[NSString stringWithFormat:@"%@",self.deviceToken]];
    }
    
    if (self.contentType) {
        [keysArray addObject:mContentType];
        [valuesArray addObject:[NSNumber numberWithInt:self.contentType]];
    }
    
    if (self.sendingDateTime) {
        [keysArray addObject:mSendingDateTime];

        NSString *date=[[NSDate alloc]getServerDateString:[self sendingDateTime]];
        [valuesArray addObject:date];
    }
    
    if (self.messageTimer) {
        [keysArray addObject:mMessageTimer];
        [valuesArray addObject:[NSNumber numberWithLong:self.messageTimer]];
    }
    
    NSDictionary *dictionary = [[NSMutableDictionary alloc]initWithObjects:valuesArray forKeys:keysArray];
    
    return dictionary;
}

@end
