//
//  BaseSearchTableViewController.m
//  Vent
//
//  Created by Sameh Farouk on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseSearchTableViewController.h"
#import "ResultsSearchTableViewControllerDelegate.h"

@interface BaseSearchTableViewController () <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating,ResultsSearchTableViewControllerDelegate>


// for state restoration
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation BaseSearchTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        [self setDelegate: delegate];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _resultsTableController = [[ResultsSearchTableViewController alloc] init];
    _resultsTableController.tableView.rowHeight =self.tableView.rowHeight;
    [[self resultsTableController] setResultsDelegate:self];
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    self.resultsTableController.tableView.delegate = [self delegate];
    self.searchController.delegate = self;
    //self.searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    
    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    //
    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // restore the searchController's active state
    if (self.searchControllerWasActive) {
        self.searchController.active = self.searchControllerWasActive;
        _searchControllerWasActive = NO;
        
        if (self.searchControllerSearchFieldWasFirstResponder) {
            [self.searchController.searchBar becomeFirstResponder];
            _searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = nil;
    
    if ([[self delegate] respondsToSelector:@selector(searchResultsForSearchText:)]) {
        searchResults = [_delegate searchResultsForSearchText:searchText];
    }else{
         NSLog(@"BaseSearchTableViewController Delegate : searchResultsForSearchText is not implemented");
    }
    
    // hand over the filtered results to our search results table
    ResultsSearchTableViewController *tableController = (ResultsSearchTableViewController *)self.searchController.searchResultsController;
    tableController.filteredEntries = searchResults;
    [tableController.tableView reloadData];
}


#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const BaseSearchViewControllerTitleKey = @"ViewControllerTitleKey";
NSString *const BaseSearchControllerIsActiveKey = @"SearchControllerIsActiveKey";
NSString *const BaseSearchBarTextKey = @"SearchBarTextKey";
NSString *const BaseSearchBarIsFirstResponderKey = @"SearchBarIsFirstResponderKey";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:BaseSearchViewControllerTitleKey];
    
    UISearchController *searchController = self.searchController;
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:BaseSearchControllerIsActiveKey];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:BaseSearchBarIsFirstResponderKey];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:BaseSearchBarTextKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:BaseSearchControllerIsActiveKey];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:BaseSearchControllerIsActiveKey];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:BaseSearchBarIsFirstResponderKey];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:BaseSearchBarTextKey];
}

#pragma mark - ResultsSearchTableViewController Delegate

- (UITableViewCell*)resultsCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if ([[self delegate] respondsToSelector:@selector(resultsCellForRowAtIndexPath:)]) {
        cell = [_delegate resultsCellForRowAtIndexPath:indexPath];
    }else{
        NSLog(@"BaseSearchTableViewController Delegate : resultsCellForRowAtIndexPath is not implemented");
    }
    
    return cell;
}

#pragma mark -

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    
    if ([_delegate respondsToSelector:@selector(resultsHeightForRowAtIndexPath:)]) {
        height = [_delegate resultsHeightForRowAtIndexPath:indexPath];
    }else{
        NSLog(@"ResultsSearchTableViewController Delegate : resultsCellForRowAtIndexPath is not implemented");
    }
    
    return height;
}


@end
