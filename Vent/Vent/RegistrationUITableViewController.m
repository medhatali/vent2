//
//  RegistrationUITableViewController.m
//  Smiley
//
//  Created by Sameh Farouk on 5/25/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import "RegistrationUITableViewController.h"
#import "Country.h"
#import "SMSTableViewController.h"
#import "NBPhoneNumber.h"
#import "NBPhoneNumberUtil.h"
#import "CommonHeader.h"
#import "RegisterBussinessService.h"
#import "SMSTableViewController.h"
#import "MainCountryListViewController.h"

NSInteger const DISPLAY_NAME_LENGTH = 20;

@interface RegistrationUITableViewController (){
    //BOOL _canConnect;
}

@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *displayNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLable;
@property (weak, nonatomic) IBOutlet UILabel *countryNameLable;
@property (weak, nonatomic) IBOutlet UILabel *lblTextNote;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *displayNameLengthLable;


@property (strong, nonatomic) Country* country;
@property (strong, nonatomic) NSString* phoneNumber;
@property BOOL canConnect;
//@property (strong, nonatomic) RegistrationDTO* registrationDTO;
@property (strong, nonatomic) IBOutlet UINavigationItem *topBar;

@property (strong, nonatomic) UserDM *currentUser;



@end

@implementation RegistrationUITableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    
    //localization
    self.title =[NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
    self.countryNameLable.text =[NSString stringWithFormat:NSLocalizedString(@"Country", nil)];
    [self.phoneNumberTextField setValue:[NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)] forKeyPath:@"_placeholderLabel.text"];
    [self.displayNameTextField setValue:[NSString stringWithFormat:NSLocalizedString(@"User Name", nil)] forKeyPath:@"_placeholderLabel.text"];
    self.lblTextNote.text =[NSString stringWithFormat:NSLocalizedString(@"Country Code Confirmation", nil)];
    
    [[self displayNameLengthLable] setText:[NSString stringWithFormat:@"%ld",(long)DISPLAY_NAME_LENGTH]];
    //start push notification
    // AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //[appDelegate startPushNotification];
    
    
    // detect local device country
//    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
//    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    
    //NSLog(@"current device country = %@" , countryCode);
    
    // set country by telephone
    [self setCountry:[[Country alloc]initWithDefaultValue]];
    [[self countryCodeLable] setText:[[self country]dial_code]];
    [[self countryNameLable] setText:[[self country] name]];
 
    
    self.displayNameTextField.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  // [[self navigationItem] setTitle:[self phoneNumber]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Country List View delegate

-(void)didSelectCountry:(NSDictionary *)country{
    [self setCountry: [[Country alloc] initWithJSONNSData:(NSData*) country]];
    [[self countryCodeLable] setText:[[self country]dial_code]];
    [[self countryNameLable] setText:[[self country] name]];
    [[self navigationController] popViewControllerAnimated:YES];
}


#pragma mark - Text Feild delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _phoneNumberTextField) {
        // allow backspace
        if (!string.length)
        {
            return YES;
        }
        
        // Prevent invalid character input, if keyboard is numberpad
        if (textField.keyboardType == UIKeyboardTypeNumberPad)
        {
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                //NSLog(@"This field accepts only numeric entries.");
                return NO;
            }
        }

    }else if (textField == _displayNameTextField){
        NSUInteger newLength = (_displayNameTextField.text.length - range.length) + string.length;
        if(newLength <= DISPLAY_NAME_LENGTH)
        {
            [_displayNameLengthLable setText:[NSString stringWithFormat:@"%ld",(DISPLAY_NAME_LENGTH - newLength)]];
            return YES;
        } else {
            NSUInteger emptySpace = DISPLAY_NAME_LENGTH - (_displayNameTextField.text.length - range.length);
            _displayNameTextField.text = [[[_displayNameTextField.text substringToIndex:range.location]
                              stringByAppendingString:[string substringToIndex:emptySpace]]
                             stringByAppendingString:[_displayNameTextField.text substringFromIndex:(range.location + range.length)]];
            return NO;
        }
    }
    
    return YES;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}


//-(BOOL)textFieldShouldReturn:(UITextField *)textField{
//    BOOL canReturn = [[[self navigationItem] rightBarButtonItem] isEnabled];
//    if (!canReturn) {
//        UIAlertView *_alert = [[UIAlertView alloc] initWithTitle:@"NUMBER VALIDATION" message:@"Your mobile number is requierd" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
//        [_alert show];
//    } else {
//       // [self sendSMS:self];
//    }
//    return canReturn;
//}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 4;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        MainCountryListViewController *countryView =[[MainCountryListViewController alloc] initWithNibName:@"MainCountryListViewController" delegate:self country:_country];
        [[countryView navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Your Country", nil)]];
        [[self navigationController] pushViewController:countryView animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else if(indexPath.section == 0 && indexPath.row == 3){
        [[self view] endEditing:YES];
    }
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"SMS"]) {
        SMSTableViewController *dest=(SMSTableViewController*)[segue destinationViewController];
        dest.registrationData = self.currentUser;
         dest.country = self.country;
        //registrationData
    }

}


#pragma mark - UIAlert View delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //[self setCanConnect:YES];
    [self.tableView endEditing:YES];
    [[self view] endEditing:YES];
    
    if ([alertView.title isEqualToString:[NSString stringWithFormat:NSLocalizedString(@"Push Notification is off", nil)]]) {
        if (buttonIndex == 1) {
            [self openSettings];
        }
        
    }
    else
    {
    
    if (buttonIndex == 1) {
        if([self.reachbilityInstance isReachable]){
            [self sendSMS];
        }else{
           [self showConnectionDown];
            [self.tableView endEditing:YES];
        }
    }
    }
}


#pragma mark -

- (IBAction)phoneNumberChanged:(UITextField *)sender {
    [self enableSendActivationCode];
}


- (IBAction)displayNameChanged:(UITextField *)sender {
    [self enableSendActivationCode];

}

-(void)enableSendActivationCode
{
    bool notEmptyDisplayNmae=![self.displayNameTextField.text isEqualToString:@""];
    
    bool _isEnabled = [self validatePhoneNumber] && notEmptyDisplayNmae;
    [[self navigationItem] setTitle:_phoneNumber];
    [[[self navigationItem] rightBarButtonItem] setEnabled:_isEnabled];
}



-(BOOL)validatePhoneNumber
{
    
    if ([self.reachbilityInstance isReachable])
    {
        NSString *_phoneNumberWithoutCode = [_phoneNumberTextField text];
        NSString* _countryCode = [[self country] code];
        NBPhoneNumberUtil *_phoneNumberUtil = [NBPhoneNumberUtil sharedInstance];
        NSError *_error = nil;
        NBPhoneNumber *_myPhoneNumber = [_phoneNumberUtil parse:_phoneNumberWithoutCode defaultRegion: _countryCode error:&_error];
        
        if (_error == nil) {
            
            
            NBEPhoneNumberType _phoneNumberType = [_phoneNumberUtil getNumberType:_myPhoneNumber];
            
            if ([_phoneNumberUtil isValidNumber:_myPhoneNumber]) {//Validate Phone Number
                if (_phoneNumberType == NBEPhoneNumberTypeMOBILE || _phoneNumberType == NBEPhoneNumberTypeFIXED_LINE_OR_MOBILE) {
                    
                    _phoneNumber = [_phoneNumberUtil format:_myPhoneNumber numberFormat: NBEPhoneNumberFormatE164 error:&_error];
                    
                    return TRUE;
                }
            }
            
                
       
            
        }else{
            NSLog(@"Error : %@", [_error localizedDescription]);
            _phoneNumber = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
        }
        
        if ([_phoneNumberWithoutCode length] == 0) {
            _phoneNumber = [NSString stringWithFormat:NSLocalizedString(@"Your Phone Number", nil)];
        }
    }
    else
    {
        //[self showConnectionDown];
        return FALSE;
    }
    
    return FALSE;
    
    
}

- (IBAction)goToValidationCodeScreen:(id)sender {
    [self.view endEditing:TRUE];
    UIAlertView *_alert =[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"NUMBER CONFIRMATION:", nil)] message:[NSString stringWithFormat:@"\n%@\n\n%@", _phoneNumber, NSLocalizedString(@"Is your phone number above correct?", nil)] delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Edit", nil)] otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Yes", nil)], nil];
    [_alert show];
    [self.tableView endEditing:YES];
    
}


-(void)sendSMS
{
    
    RegisterBussinessService *registerServiceObject =[[RegisterBussinessService alloc]init];
    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Sending SMS", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    [self.tableView endEditing:YES];
    [[self view] endEditing:YES];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       // send sms
                       self.currentUser = [[UserDM alloc]init];
                       self.currentUser.DeviceName =[[UIDevice currentDevice] name];
                       self.currentUser.DisplayName =self.displayNameTextField.text;
                       self.currentUser.PhoneNumber =_phoneNumber;
                       self.currentUser.DeviceToken =[[NSUserDefaults standardUserDefaults] valueForKey:KVentDeviceToken];
                       self.currentUser.OSType =[NSNumber numberWithInt:1];
                       
                       if ( self.currentUser.DeviceToken == nil) {
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              [self.tableView endEditing:YES];
                                              [[self view] endEditing:YES];
                                              UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Push Notification is off", nil)]
                                                                                                    message:[NSString stringWithFormat:NSLocalizedString(@"We cannot complete registartion\nPlease go to device setting to enable push notification to complete registartion", nil)]
                                                                                                   delegate:self
                                                                                          cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                                                          otherButtonTitles: [NSString stringWithFormat:NSLocalizedString(@"Settings", nil)],nil];
                                              
                                              [myAlertView show];
                                            [self.loadingView hideLoadingViewAnimated:YES];
                                              
                                              [self.tableView endEditing:YES];
                                              [[self view] endEditing:YES];
                                              [self.phoneNumberTextField resignFirstResponder];
                                              [self.displayNameTextField resignFirstResponder];
                                          });
                       }
                       else
                       {
                       NSError *error;
                       bool success=[registerServiceObject registerUser: self.currentUser Error:&error];
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                          
                                          if (success) {
                                              
                                              [self performSegueWithIdentifier:@"SMS" sender:self];
                                              
                                          }
                                          else
                                          {
                                              [self.tableView endEditing:YES];
                                               [[self view] endEditing:YES];
                                              
                                             UIAlertView * alertError= [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registeration Error", nil)
                                                                          message:NSLocalizedString(@"error in registering ", nil)
                                                                         delegate:self
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                otherButtonTitles:nil] ;
                                              
                                              [self.tableView endEditing:YES];
                                              [[self view] endEditing:YES];
                                              
                                              [alertError show];
                                              
                                               [self.phoneNumberTextField resignFirstResponder];
                                              [self.displayNameTextField resignFirstResponder];

                                              
                                              //[self performSegueWithIdentifier:@"SMS" sender:self];
                                              
                                              
                                          }
                                          
                                          
                                      });
                       
                   
    
    
                   }
                   });
    
    
}


- (void)openSettings
{
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}





@end
