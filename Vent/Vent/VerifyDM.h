//
//  VerifyDM.h
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "DataModelObjectParent.h"

@interface VerifyDM : DataModelObjectParent

/// properties for data retrieved
@property (nonatomic, strong) NSString *DeviceToken;
@property (nonatomic, strong) NSString *PhoneNumber;
@property (nonatomic, strong) NSString *VerificationCode;

@end
