//
//  GroupListRepository.h
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseRepository.h"
#import "GroupList.h"
#import "GroupListUsersPhones.h"
#import "ChatSession.h"

@interface GroupListRepository : BaseRepository

-(GroupList *) addNewGroupList:(NSString*)groupID
                     groupName:(NSString*)groupName
                groupStartDate:(NSString*)groupStartDate
                  groupEnabled:(NSNumber*)groupEnabled
                    groupMuted:(NSNumber*)groupMuted
          groupLastUpdatedDate:(NSString*)groupLastUpdatedDate
                 groupSequence:(NSNumber*)groupSequence
                    groupImage:(NSData*)groupImage
                 groupImageUrl:(NSString*)groupImageUrl
              groupLastMessage:(NSString*)groupLastMessage
                groupListOwner:(User*)groupListOwner
              groupListSession:(ChatSession*)groupListSession
                groupListUsers:(NSArray*)groupListUsers
                AdminPhoneNumber:(NSString*)AdminPhoneNumber
                         Error:(NSError **)error;


-(GroupListUsersPhones *) addUserPhoneToGroupList:(NSString*)groupUserPhone
                                 groupUserEnabled:(NSNumber*)groupUserEnabled
                                groupUserJoinDate:(NSString*)groupUserJoinDate
                                   phoneGroupList:(GroupList*)phoneGroupList
                                            Error:(NSError **)error;

-(GroupListUsersPhones *) addUserPhoneToGroupList:(NSString*)groupUserPhone
                                 groupUserEnabled:(NSNumber*)groupUserEnabled
                                groupUserJoinDate:(NSString*)groupUserJoinDate
                                   phoneGroupList:(GroupList*)phoneGroupList
                                 groupUserIsAdmin:(NSNumber*)groupUserIsAdmin
                                            Error:(NSError **)error;

-(void)deleteGroupList:(GroupList *)GroupList Error:(NSError **)error;

-(void)updateGroupList:(GroupList *)GroupList Error:(NSError **)error;

-(void)deleteGroupListUsersPhones:(GroupListUsersPhones *)GroupListUsersPhones Error:(NSError **)error;

-(void)updateGroupListUsersPhones:(GroupListUsersPhones *)GroupListUsersPhones Error:(NSError **)error;
-(void) changeGroupListAdmin:(NSString*)useridentity userGroupList:(GroupList*) userGroupList Error:(NSError **)error;

-(void) updateGroupList:(GroupList *)GroupListUpdated
         groupListUsers:(NSArray*)groupListUsers
                  Error:(NSError **)error;

-(NSArray*)getUserGroupList:(User*)User Error:(NSError **)error;

-(GroupList*)getUserGroupListbyGroupID:(NSString*)groupID Error:(NSError **)error;

-(NSArray*)getUserGroupListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error;

-(GroupList*)getGroupListByStartDate:(User*)User StartDate:(NSString*)startDate Error:(NSError **)error;


-(NSArray*)getGroupListPhones:(GroupList*)GroupList Error:(NSError **)error;

// new one
-(BOOL)isPhoneNumberExistInGroupList:(NSString*)phoneNumber GroupList:(GroupList*)GroupListSelected Error:(NSError **)error;
-(BOOL)deletePhoneNumberExistInGroupList:(NSString*)phoneNumber GroupList:(GroupList*)GroupListSelected Error:(NSError **)error;

-(NSArray*)getGroupListUserbyPhones:(NSArray*)phoneNumber Error:(NSError **)error;
-(GroupList*)getGroupListByUserPhones:(NSArray*)phoneNumber Error:(NSError **)error;

-(GroupList*)getGrouptListByPhoneNumbers:(NSArray*)phone Error:(NSError **)error;
-(NSArray*)getGroupListSessions:(GroupList*)GroupList Error:(NSError **)error;
-(NSArray*)getGroupListSessionsWithRecordsNumber:(GroupList*)GroupList RecordsLimit:(NSInteger)recordsLimit Error:(NSError **)error;
-(NSArray*)getUnreadGroupListSessions:(GroupList*)ChatList Error:(NSError **)error;
-(NSInteger)getAllUnreadGroupListSessions:(GroupList*)ChatList Error:(NSError **)error;


@end
