//
//  RegistrationUITableViewController.h
//  Smiley
//
//  Created by Sameh Farouk on 5/25/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryListViewDelegate.h"
#import "RegisterationTVC.h"

@interface RegistrationUITableViewController : RegisterationTVC <UITextFieldDelegate,UIAlertViewDelegate,CountryListViewDelegate>
@end
