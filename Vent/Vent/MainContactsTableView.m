//
//  MainContactsTableView.m
//  Vent
//
//  Created by Sameh Farouk on 3/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "MainContactsTableView.h"
#import "ResultsContactsTableView.h"
#import "ContactListDataSource.h"
#import "Contacts.h"
#import "ContactsTableViewCell.h"
#import "ContactsBussinessService.h"
#import "AppDelegate.h"


@interface MainContactsTableView () <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController *searchController;

// our secondary search results table view
@property (nonatomic, strong) ResultsContactsTableView *resultsTableController;

// for state restoration
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@property (nonatomic, strong) ContactListDataSource *dataSource;

@end

@implementation MainContactsTableView

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        [self setDelegate: delegate];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _resultsTableController = [[ResultsContactsTableView alloc] init];
    
    SelectionType selectionType = [[self delegate] selectionType];
    // if single selection display cancel only
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]
                                   initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)]
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(cancelSelection:)];
    self.navigationItem.rightBarButtonItem = cancelButton;
    
    
    // if multi selection display done button
    
    if (selectionType == kMultipleSelect) {
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                         initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Done", nil)]
                                         style:UIBarButtonItemStylePlain
                                         target:self
                                         action:@selector(doneSelection:)];
        self.navigationItem.leftBarButtonItem = doneButton;
        
        self.tableView.allowsMultipleSelection = YES;
        _resultsTableController.tableView.allowsMultipleSelection = YES;

    }
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ContactsTableViewCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:kCellIdentifier];
    [self setDataSource: [ContactListDataSource new]];
    self.dataSource.allContacts = self.allContacts;
    
    _contacts = [self partitionObjects:[[self dataSource] contacts] collationStringSelector:@selector(contactFullName)];
    
    [self.tableView reloadData];
    
    _searchController = [[UISearchController alloc]initWithSearchResultsController:self.resultsTableController];

    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    self.resultsTableController.tableView.delegate = self;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = YES;
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    
    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    //
    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
    
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([[appDelegate reachabilityApp] isReachable])
        [self refresh:refreshControl];
    
}


- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    NSError *error;
//    [contactBWS loadPhoneContacts:@"+2"];
    [contactBWS loadPhoneContacts:currentUser.userCountryCode];
    [contactBWS compareContactsWithDB:&error];
    
   //  [contactBWS loadPhoneContactWithLoading:@"+2"];
    
    _contacts = [self partitionObjects:[[self dataSource] contacts] collationStringSelector:@selector(contactFullName)];
    
    [self.tableView reloadData];
    
    [refreshControl endRefreshing];
}

-(void)viewWillAppear:(BOOL)animated
{

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // restore the searchController's active state
    if (self.searchControllerWasActive) {
        self.searchController.active = self.searchControllerWasActive;
        _searchControllerWasActive = NO;
        
        if (self.searchControllerSearchFieldWasFirstResponder) {
            [self.searchController.searchBar becomeFirstResponder];
            _searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return (tableView == self.tableView) ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count] : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return (tableView == self.tableView) ? [[[self contacts] objectAtIndex:section] count]:[[[self resultsTableController] filteredContacts] count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContactsTableViewCell *cell = (ContactsTableViewCell*) [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if(cell == nil) {
        cell = [[ContactsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
        
    }
    
    id contact = [[[self contacts] objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
   
    [cell setContacts:contact];
    
    for (Contacts *selected in [self selectedContacts]) {
        if (selected == contact) {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            break;
        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

// here we are the table view delegate for both our main table and filtered table, so we can
// push from the current navigation controller (resultsTableController's parent view controller
// is not this UINavigationController)
//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ([[self delegate] respondsToSelector:@selector(selectionType)]) {
        SelectionType selectionType = [[self delegate] selectionType];
        
        if (selectionType == kSingleSelect) {
            if ([[self delegate] respondsToSelector:@selector(didSelectContacts:)]) {
                Contacts *selectedContact = (tableView == self.tableView) ?
                [[[self contacts] objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] :[[[self resultsTableController]filteredContacts]objectAtIndex:[indexPath row]];
                
                [self.delegate didSelectContacts:selectedContact];
                [self dismissViewControllerAnimated:YES completion:Nil];
                
                
            } else {
                NSLog(@"ContactListView Delegate : didSelectContact is not implemented");
            }
            
        }
    } else {
        NSLog(@"ContactListView Delegate : selectionType is not implemented");
    }
    
}
    
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
   BOOL showSection = [[_contacts objectAtIndex:section] count] != 0;
    //only show the section title if there are rows in the section
    return (showSection) ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    if (title == UITableViewIndexSearch) {
        [tableView scrollRectToVisible:self.searchController.searchBar.frame animated:NO];
        return -1;
    } else {
        return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
    }
}

- (NSIndexPath*) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Contacts *selectedContact = (tableView == self.tableView) ?
    [[[self contacts] objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] :[[[self resultsTableController]filteredContacts]objectAtIndex:[indexPath row]];
    [[self selectedContacts] addObject:selectedContact];
    if (tableView == self.resultsTableController.tableView) {
    NSIndexPath *indexPath;
        
        for (int section = 0; section < self.contacts.count; section++) {
            for (int row = 0; row < ((NSArray*) self.contacts[section]).count; row ++) {
                if (self.contacts[section][row] == selectedContact)
                {
                    indexPath = [NSIndexPath indexPathForRow:row inSection:section];
                    break;
                }
            }
        }
        [[self tableView] selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    return indexPath;
}

- (NSIndexPath*) tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    Contacts *selectedContact = (tableView == self.tableView) ?
    [[[self contacts] objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] :[[[self resultsTableController]filteredContacts]objectAtIndex:[indexPath row]];
    [[self selectedContacts] removeObjectIdenticalTo:selectedContact];
    if (tableView == self.resultsTableController.tableView) {
        NSIndexPath *indexPath;
        
        for (int section = 0; section < self.contacts.count; section++) {
            for (int row = 0; row < ((NSArray*) self.contacts[section]).count; row ++) {
                if (self.contacts[section][row] == selectedContact)
                {
                    indexPath = [NSIndexPath indexPathForRow:row inSection:section];
                    break;
                }
            }
        }
        [[self tableView] deselectRowAtIndexPath:indexPath animated:NO];
    }

    return indexPath;
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = [NSMutableArray arrayWithCapacity:[[[self dataSource] contacts] count]];
    
    /*
     Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
     */
    for (NSArray *section in self.contacts) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"contactFullName contains[c] %@ ",searchText];
        [searchResults addObjectsFromArray: [section filteredArrayUsingPredicate:predicate]];
    }
    
    // hand over the filtered results to our search results table
    ResultsContactsTableView *tableController = (ResultsContactsTableView *)self.searchController.searchResultsController;
    tableController.filteredContacts = searchResults;
    tableController.selectedContacts = self.selectedContacts;
    [tableController.tableView reloadData];
}


#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const ContactViewControllerTitleKey = @"ViewControllerTitleKey";
NSString *const ContactSearchControllerIsActiveKey = @"SearchControllerIsActiveKey";
NSString *const ContactSearchBarTextKey = @"SearchBarTextKey";
NSString *const ContactSearchBarIsFirstResponderKey = @"SearchBarIsFirstResponderKey";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:ContactViewControllerTitleKey];
    
    UISearchController *searchController = self.searchController;
    
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:ContactSearchControllerIsActiveKey];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:ContactSearchBarIsFirstResponderKey];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:ContactSearchBarTextKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:ContactViewControllerTitleKey];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:ContactSearchControllerIsActiveKey];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:ContactSearchBarIsFirstResponderKey];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:ContactSearchBarTextKey];
}

#pragma mark -

-(NSArray *)partitionObjects:(NSArray *)array collationStringSelector:(SEL)selector{
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    NSInteger sectionCount = [[collation sectionTitles] count]; //section count is take from sectionTitles and not sectionIndexTitles
    NSMutableArray *unsortedSections = [NSMutableArray arrayWithCapacity:sectionCount];
    
    //create an array to hold the data for each section
    for(int i = 0; i < sectionCount; i++)
    {
        [unsortedSections addObject:[NSMutableArray array]];
    }
    
    //put each object into a section
    for (id object in array)
    {
        NSInteger index = [collation sectionForObject:object
                              collationStringSelector:selector];
        [[unsortedSections objectAtIndex:index] addObject:object];
    }
    
    NSMutableArray *sections = [NSMutableArray arrayWithCapacity:sectionCount];
    
    //sort each section
    for (NSMutableArray *section in unsortedSections)
    {
        [sections addObject:[collation sortedArrayFromArray:section collationStringSelector:selector]];
    }
    
    return sections;
}

#pragma mark - navigatoin and action

-(void)cancelSelection:(UIBarButtonItem *)sender{
    
    //perform your action
    [self dismissViewControllerAnimated:YES completion:Nil];
}

-(void)doneSelection:(UIBarButtonItem *)sender{
    
    //perform your action
    
    
    if ([[self delegate] respondsToSelector:@selector(didSelectContacts:)]) {
                
        [self.delegate didSelectContactsList:[self selectedContacts]];
        [self dismissViewControllerAnimated:YES completion:Nil];
                
    } else {
        NSLog(@"ContactListView Delegate : didSelectContact is not implemented");
    }
    
}


@end
