//
//  ProfileTableViewController.h
//  Smiley
//
//  Created by Sameh Farouk on 8/19/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterationTVC.h"

@interface ProfileTableViewController : RegisterationTVC<UITextFieldDelegate>

@property (strong, nonatomic) UserDM* registrationData;

@end
