//
//  ContactListDelegate.h
//  Vent
//
//  Created by Sameh Farouk on 3/10/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#ifndef Vent_ContactListDelegate_h
#define Vent_ContactListDelegate_h

#import "SelectionType.h"

@class Contacts;

@protocol ContactListDelegate <NSObject>

- (SelectionType)selectionType;

@optional
- (void)didSelectContacts:(Contacts *)contacts;

- (void)didSelectContactsList:(NSArray *)contactsList;

@end

#endif
