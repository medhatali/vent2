//
//  CoreDataManager.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define CDModelName @"VentCDModel"
#define CDModelExtension @"momd"
#define CDModelFullName @"VentCDModel.sqlite"

typedef void (^myContextBlock)(BOOL success,NSManagedObjectContext *context, NSError *error);
typedef void (^myCompletionBlock)(BOOL success, NSDictionary *result, NSError *error);

@interface CoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *privateObjectContext;
dispatch_queue_t background_save_queue();

+ (id) sharedManager;


#pragma mark - save context & save contet in thread

- (void)retrieveDataFromContextFromDBQueue:(void(^)(NSManagedObjectContext *context))retrieveBlock completionBlock:(myContextBlock)completionBlock;
- (void)saveDataInPrivateContextFromDBQueue:(void(^)(NSManagedObjectContext *context))saveBlock completionBlock:(myContextBlock)completionBlock;
- (void)saveDataInPrivateContext:(void(^)(NSManagedObjectContext *context))saveBlock completionBlock:(myContextBlock)completionBlock;
- (BOOL) saveContextInMainQueue:(NSManagedObjectContext *)context Error:(NSError **)error;
- (void)saveContextInMainQueue:(NSManagedObjectContext *)sharedContext CompletionBlock:(myContextBlock)completionBlock;
- (void)saveContextInDBQueue:(NSManagedObjectContext *)sharedContext CompletionBlock:(myContextBlock)completionBlock;


#pragma mark retrieve data from DB

-(void)retrieveDataInPrivateContextFromDBQueue:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock;
-(void)retrieveDataInPrivateContextFromDB:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock;


@end
