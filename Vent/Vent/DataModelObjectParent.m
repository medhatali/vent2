//
//  DataModelObjectParent.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "DataModelObjectParent.h"

@implementation DataModelObjectParent

+ (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

#pragma -mark cretae UUID
-(NSString *)uniqueUUID
{
    CFUUIDRef theUniqueString = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUniqueString);
    CFRelease(theUniqueString);
    return (__bridge NSString *)string ;
}


#pragma mark - Helper messages

- (NSString *)convertDateToString:(NSDate *)date
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    dateString = [dateString stringByAppendingString:@"T00:00:00"];
    return dateString;
}

- (NSDate *)convertStringToDate:(NSString *)dateObject
{
    //Get the date only from the string
    NSString *dateString;
    NSRange range = [dateObject rangeOfString:@"T"];
    if (range.location == NSNotFound) {
        dateString = [NSString stringWithString:dateObject];
    }
    else {
        dateString = [dateObject substringToIndex:range.location];
    }
    //Create the NSDate Object
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *date = [formatter dateFromString:dateString];
    return date;
}



@end
