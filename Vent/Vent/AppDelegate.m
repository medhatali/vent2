//
//  AppDelegate.m
//  Vent
//
//  Created by Medhat Ali on 2/26/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "AppDelegate.h"
#import "NSUserDefaults+DemoSettings.h"
#import "GlobalHandler.h"
#import "ContactsBussinessService.h"
#import "User.h"
#import "AppUtility.h"
#import "HDNotificationView.h"
#import <AddressBookUI/AddressBookUI.h>

//#import "SignalR.h"
//#import "NSString+URLEncoding.h"
//#import "SRAutoTransport.h"
//#import "SRServerSentEventsTransport.h"

@interface AppDelegate ()

//@property (copy)void (^NotifTokenHandler)(NSString* token);
//@property (strong,nonatomic) SRHubProxy *proxy;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
   // NSLog(@"current language =%ld" ,(long)[[AppUtility sharedUtilityInstance]getCurrentLanguage]);
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
   
    NSInteger securelimit = [[NSUserDefaults standardUserDefaults] integerForKey:KVentSecureTimeLimit];

    if (securelimit == 0 ) {
        [[NSUserDefaults standardUserDefaults] setInteger:10 forKey:KVentSecureTimeLimit];
    }
    
    
    self.reachabilityApp = [Reachability reachabilityWithHostname:@"google.com"];//[[AppUtility sharedUtilityInstance]getServiceUrl]];
    
    self.reachabilityApp.reachableBlock = ^(Reachability *reachability) {
        NSLog(@"StartUp: Network is reachable.");
        
//        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
//        User* currentUser=[contactBWS getCurrentUserProfile];
//        if (currentUser) {
//            if (currentUser.userIsRegistered) {
//                [[GlobalHandler sharedInstance] startSignalR];
//            }
//        }
        
    };
    
    self.reachabilityApp.unreachableBlock = ^(Reachability *reachability) {
        NSLog(@"StartUp: Network is unreachable.");
        
//        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
//        User* currentUser=[contactBWS getCurrentUserProfile];
//        if (currentUser) {
//            if (currentUser.userIsRegistered) {
//                [[GlobalHandler sharedInstance] stopSignalR];
//            }
//        }
        
        
    };
    
    // Start Monitoring
  
    
    
    [[GlobalHandler sharedInstance]initReachability];
      [self.reachabilityApp startNotifier];
    
    
    //NSLog(@"name: %@", [[UIDevice currentDevice] name]);
    
    // force PN badge to be 0
     [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    // Generate the path to the new directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *newDirectory = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0],[[AppUtility sharedUtilityInstance] getMediaDirectory]];
    
    // Check if the directory already exists
    if (![[NSFileManager defaultManager] fileExistsAtPath:newDirectory]) {
        // Directory does not exist so create it
        [[NSFileManager defaultManager] createDirectoryAtPath:newDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    

    /// check contact change
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, nil);
    // ABAddressBookRef addressBookRef = ABAddressBookCreate();
    ABAddressBookRegisterExternalChangeCallback(addressBook, addressBookDidChange, (__bridge void *)self);
    
    

    
//    if (launchOptions != nil)
//    {
//        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//        if (userInfo != nil)
//        {
//            NSLog(@"Launched from push notification: %@", userInfo);
//            [[[GlobalHandler sharedInstance]pushInstance] handleRemoteNotification:userInfo];
//        }
//    }
    
//    // notification
//    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
//        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
//    }
//    
//    [NSUserDefaults saveIncomingAvatarSetting:YES];
//    [NSUserDefaults saveOutgoingAvatarSetting:YES];
//    
//    
//    
//    // push notification settings
//    
//    [self registerForNotificationsWithCompletionHandler:nil];
//    
//    
//    if (launchOptions != nil)
//    {
//        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//        if (userInfo != nil)
//        {
//            NSLog(@"Launched from push notification: %@", userInfo);
//            [self handleRemoteNotification:userInfo];
//        }
//    }
//    
//    //    if ([self isLoggedIn]) {
//    //        [self byPassLoginScreen];
//    //        if (![[AppUtility sharedUtilityInstance] registeredForPushNotificationsFromDefaults]) {
//    //            [[AppUtility sharedUtilityInstance] registerForPushNotifications];
//    //        }
//    //    }
//    
//    
//    /// signal R initilization
//    
//    NSString * phoneNumber = @"+2000";
//    
//    NSDictionary *queryString = @{
//                                  @"PhoneNumber" : [phoneNumber urlEncodeUsingEncoding: NSUTF8StringEncoding],
//                                  @"DeviceToken" : @"AA",
//                                  };
//
//    
//    
//    SRHubConnection *hubConnection = [SRHubConnection connectionWithURL:@"http://192.168.1.103:54486" query:queryString];
//    
//    
//    self.proxy = [hubConnection createHubProxy:@"ChattingHub"];
//    
//   // [proxy on:@"onGetNewMessages" perform:self selector:@selector(joined:when:)];
//    [self.proxy on:@"HelloSignalR" perform:self selector:@selector(joined:when:)];
//    //[proxy on:@"joined" perform:self selector:@selector(joined:when:)];
//    [self.proxy on:@"rejoined" perform:self selector:@selector(rejoined:when:)];
//    [self.proxy on:@"leave" perform:self selector:@selector(leave:when:)];
//    hubConnection.started = ^{
//        NSLog(@"SignalR:start Connection");
//    };
//    hubConnection.received = ^(NSDictionary * data){
//        NSLog(@"SignalR:recived");
//        
//
//        
//        
//    };
//    hubConnection.closed = ^{
//        NSLog(@"SignalR: Connection closed");
//    };
//    hubConnection.error = ^(NSError *error){
//       NSLog(@"SignalR: Connection error");
//    };
//    
//    [hubConnection start];
//

    [[GlobalHandler sharedInstance] initPush:application didFinishLaunchingWithOptions:launchOptions];

    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    if (currentUser) {
        if (currentUser.userIsRegistered) {
        [[GlobalHandler sharedInstance] startSignalR];
        }
    }


  
    
    return YES;
}


//-(void) registerForNotificationsWithCompletionHandler:(void (^)(NSString* token)) tokenHandler
//{
//    UIUserNotificationType types;
//
//
//        types= UIUserNotificationTypeBadge| UIUserNotificationTypeAlert;
//        types= UIUserNotificationTypeBadge |
//        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
//
//    
//    UIUserNotificationSettings *mySettings =
//    [UIUserNotificationSettings settingsForTypes:types categories:nil];
//    
//    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
//    [[UIApplication sharedApplication] registerForRemoteNotifications];
//    
//    self.NotifTokenHandler = tokenHandler;
//}


#pragma mark - recieve Push Notification


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //[self handleRemoteNotification:userInfo];
    [[GlobalHandler sharedInstance] handleRemoteNotification:userInfo];
}

//-(void) application:(UIApplication *) application didReceiveRemoteNotification:(NSDictionary *)userInfo
//{
//    [self handleRemoteNotification:userInfo];
//}

//-(void) handleRemoteNotification:(NSDictionary *)userInfo
//{
//    for (id key in userInfo)
//    {
//        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
//    }
//    
//    NSLog(@"remote notification: %@",[userInfo description]);
//    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
//    
//    NSString *alert = [apsInfo objectForKey:@"alert"];
//    NSLog(@"Received Push Alert: %@", alert);
//    
//    // [apsInfo setValue:@"" forKey:@"sound"];
//    NSString *sound = [apsInfo objectForKey:@"sound"];
//    NSLog(@"Received Push Sound: %@", sound);
//    
//    NSString *badge = [apsInfo objectForKey:@"badge"];
//    NSLog(@"Received Push Badge: %@", badge);
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];
//    
//
////        // play custom sound
////        NSURL *fileURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds/ReceivedMessage.caf"]; // see list below
////        SystemSoundID soundID;
////        AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)fileURL,&soundID);
////        AudioServicesPlaySystemSound(soundID);
//    
//        
//        
//        
//        
//    
//    
//    
//    
//    UIAlertView *pushAlert=[[UIAlertView alloc]initWithTitle:@"Push Recieved" message:@"push notification recieved" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:@"Ok", nil];
//    [pushAlert show];
//    
//    
//    
//    
//    
//    // [self byPassLoginScreen];
//    
//    if ([self isLoggedIn]) {
//        //if user loged in
//        // go to notification screen and reoad data
//        [self byPassLoginScreen];
//        
//    }
//    
//}


//-(void) byPassLoginScreen
//{
////    //     go to main notification screen
////    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
////    UINavigationController *navigationController = (UINavigationController *) self.window.rootViewController;
////    // FirstViewController *firstController = (FirstViewController *) self.window.rootViewController;
////    LoginViewController * loginVC=(LoginViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Login"];
////    [navigationController pushViewController:loginVC animated:NO];
////    
////    MainMenuController * mainVC=(MainMenuController*)[storyboard instantiateViewControllerWithIdentifier:@"MainMenu"];
////    [navigationController pushViewController:mainVC animated:NO];
////    
////    
////    self.window.rootViewController = navigationController;
//}
//
//-(BOOL) isLoggedIn
//{
////    User *user = [User currentUser];
////    if (user) {
////        
////        if (user.userId) {
////            return YES;
////        }
////        
////    }
//    return NO;
//}
//

#pragma mark - handler Push Notification

-(void) application:(UIApplication *) application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
//    NSLog(@"My token is: %@", deviceToken);
//    [self setDeviceToken:[NSString stringWithUTF8String:[deviceToken bytes]]];
//    if (self.NotifTokenHandler) {
//        
//        NSString *tokenString = [self stringWithDeviceToken:deviceToken];
//        self.NotifTokenHandler(tokenString);
//    }
    
    [[[GlobalHandler sharedInstance]pushInstance] didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

-(void) application:(UIApplication *) application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed to get token, error: %@", error);
    
//    #warning medhat remove this line when go to real testing
//    [[NSUserDefaults standardUserDefaults] setObject:@"MA" forKey:KVentDeviceToken];
//    
    
}

//- (NSString*)stringWithDeviceToken:(NSData*)deviceToken {
//    const char* data = [deviceToken bytes];
//    NSMutableString* token = [NSMutableString string];
//    
//    for (int i = 0; i < [deviceToken length]; i++) {
//        [token appendFormat:@"%02.2hhX", data[i]];
//    }
//    
//    return [token copy];
//}
//
//
//
//- (NSString*)deviceToken
//{
//    return [[NSUserDefaults standardUserDefaults] stringForKey:DeviceTokenKey];
//}
//
//- (void)setDeviceToken:(NSString*)token
//{
//    if (token) {
//        [[NSUserDefaults standardUserDefaults] setObject:token forKey:DeviceTokenKey];
//    }
//}
//






- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    if (currentUser) {
        if (currentUser.userIsRegistered) {
            [[GlobalHandler sharedInstance] startSignalR];
        }

    }
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    if (currentUser) {
        [[GlobalHandler sharedInstance] stopSignalR];
        
        
        
    }
    
    if ([currentUser.userIsSecureLogin isEqualToNumber:[NSNumber numberWithBool:TRUE]])
    {
        // go to Pin login push segue
        [[NSNotificationCenter defaultCenter] postNotificationName:@"popToRoot" object:nil];

    }


}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    if (currentUser) {
        if (currentUser.userIsRegistered) {
        [[GlobalHandler sharedInstance] startSignalR];
        }
    }

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    if (currentUser) {
        if (currentUser.userIsRegistered) {
        [[GlobalHandler sharedInstance] startSignalR];
        }
    }

    if ([currentUser.userIsSecureLogin isEqualToNumber:[NSNumber numberWithBool:TRUE]])
    {
        // go to Pin login push segue
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"popToRoot" object:nil];

    }
    
     [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    if (currentUser) {
        [[GlobalHandler sharedInstance] stopSignalR];
    }

}

//
//-(void)startPushNotification
//{
//    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
//        // For iOS 8:
//
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |UIUserNotificationTypeSound categories:nil];
//        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//        [[UIApplication sharedApplication] registerForRemoteNotifications];
//        
//    }
//    
//}
//
//-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
//    NSString* _myDeviceToken = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]stringByReplacingOccurrencesOfString: @">" withString: @""] stringByReplacingOccurrencesOfString: @" " withString: @""];
//    [self setDeviceToken:_myDeviceToken];
//    NSLog(@"%@",deviceToken);
//    NSLog(@"My token is: %@", _myDeviceToken);
//    
//    [[NSUserDefaults standardUserDefaults] setValue:deviceToken forKey:KVentDeviceToken];
//    
//}
//
//-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
//{
//    #warning to be removed
//    [[NSUserDefaults standardUserDefaults] setValue:@"fb5efeffb5ef" forKey:KVentDeviceToken];
//    NSLog(@"Failed to get token, error: %@", error);
//}
//
//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
//    [self showAlarm:notification.alertBody];
//    application.applicationIconBadgeNumber = 0;
//    NSLog(@"AppDelegate didReceiveLocalNotification %@", notification.userInfo);
//}

//- (void)showAlarm:(NSString *)text {
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alarm"
//                                                        message:text delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//    [alertView show];
//}




//#pragma mark Connect Disconnect Sample Project
//
//- (void)joined:(NSString *)id when:(NSString *)when
//{
//    NSLog(@"signalR : Hello Signal R");
//    
//
//    // send signalR
//    NSArray * ar = [[NSArray alloc] initWithObjects:nil];
//    [self.proxy invoke:@"hello" withArgs:ar completionHandler:^(NSString* response)
//     {
//         NSLog(@"hubResult %@",response);
//     }];
//
//
//}
//
//- (void)rejoined:(NSString *)id when:(NSString *)when
//{
//     NSLog(@"signalR : Rejoied");
//}
//
//- (void)leave:(NSString *)id when:(NSString *)when
//{
//   NSLog(@"/Users/medhatali/Documents/Repository/itsc-repo/Vent/Vent/CommonWebService.hsignalR : Leave");
//}

void addressBookDidChange(ABAddressBookRef addressBook, CFDictionaryRef info, void  *context)
{
    //Something changed from last application launch, insert your logic here...
    
    //If you want to handle it in a "Objective-C" method you can do something like:
    // [((__bridge ABManager*) context) yourObjectiveCMethod];
    //NSLog(@"contact change");
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    NSError *error;
    //    [contactBWS loadPhoneContacts:@"+2"];
    [contactBWS loadPhoneContactsInCallBack:currentUser.userCountryCode addressBook:addressBook];
    [contactBWS compareContactsWithDB:&error];
    
    
    
}



@end
