//
//  AppDelegate.h
//  Vent
//
//  Created by Medhat Ali on 2/26/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"

//static NSString * const DeviceTokenKey = @"DeviceToken_event";

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Reachability *reachabilityApp;
//@property (readwrite,strong,nonatomic) NSString *deviceToken;


//- (NSString*)deviceToken;
//-(void) registerForNotificationsWithCompletionHandler:(void (^)(NSString* token)) tokenHandler;
//
//
//
//-(void)startPushNotification;

@end

