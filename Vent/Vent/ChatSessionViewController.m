//
//  ChatSessionViewController.m
//  Vent
//
//  Created by Medhat Ali on 4/18/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatSessionViewController.h"
#import "ContactsRepository.h"
#import "ContactsBussinessService.h"
#import "ChatSessionRepository.h"
#import "MessageNotificationBussiness.h"
#import "ChatListRepository.h"
#import "GroupListRepository.h"
#import "BroadcastRepository.h"
#import "GroupProfileViewControler.h"
#import "ChatTopBarView.h"
#import "MainContactsTableView.h"
#import "SignalRContctivtyProtocol.h"
#import "AppUtility.h"
#import "UserProfileTableViewController.h"
#import "GroupProfileTableViewController.h"
#import "NewGroupListViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "VcardImporter.h"
#import "UploadDownLoadBussinessService.h"
#import "NSDate+DateFormater.h"
#import "GGFullscreenImageViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ActiveChatSessionViewController.h"

@interface ChatSessionViewController ()<SignalRContctivtyProtocol>
@property (weak, nonatomic)  UIButton *profileImageView;
@property (retain, nonatomic)  UIImageView *fbprofileImageView;
@property (retain, nonatomic)  UIImage *currentProfileImage;

@property (retain, nonatomic)  NSArray *broadcastsession;
@property (retain, nonatomic) NSArray *groupsession;


@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;

@property (retain, nonatomic) NSMutableArray *timersList;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (strong, nonatomic) NSDate *lastUpdatedDate;

@end

@implementation ChatSessionViewController

/**
 *  Override point for customization.
 *
 *  Customize your view.
 *  Look at the properties on `JSQMessagesViewController` and `JSQMessagesCollectionView` to see what is possible.
 *
 *  Customize your layout.
 *  Look at the properties on `JSQMessagesCollectionViewFlowLayout` to see what is possible.
 */

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title =[NSString stringWithFormat:NSLocalizedString(@"Chat", nil)] ;
    
    self.numberOflimitedRecords =10;
    
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;

    self.lastUpdatedDate = nil;
    
    // make it center
    NSArray* nibViews= [[NSBundle mainBundle] loadNibNamed:@"TopChatView" owner:self options:nil];
    self.topView = [ nibViews objectAtIndex: 0];
    
    self.topView.contentMode = UIViewContentModeScaleAspectFit;
    self.topView.center = CGPointMake(self.navigationController.navigationBar.frame.size.width / 2.0, self.navigationController.navigationBar.frame.size.height / 2.0);
    self.topView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.topView.lblSubTitle.text=@"";
    self.navigationItem.titleView =self.topView;
    self.topView.delegate = self;
    

    
    ContactsRepository * contactRepo=[[ContactsRepository alloc]init];
    self.contactBWS=[[ContactsBussinessService alloc]init];
    NSError *error;
    
    self.currentUser=[ self.contactBWS getCurrentUserProfile];
    self.reciverContactPhones =[[NSMutableArray alloc]init];
    
    self.currentProfileImage=[UIImage imageNamed:@"DefaultProfile"];
    
    if(self.chatTypeSession == SingleChat)
    {
      
        self.senderContact =self.selectedChatList.chatListOwner;
    
       
        self.reciverContact = [contactRepo getUserContactByPhoneNumber:self.selectedChatList.chatToPhone contactUser:self.currentUser Error:&error];
        //self.reciverContact.contactFullName =@"";
        self.senderId = self.senderContact.userPhoneNumber;
        self.senderDisplayName = self.senderContact.userDisplayName;
        self.senderDisplayName = @"";
        
        [self.reciverContactPhones addObject:self.selectedChatList.chatToPhone];

        self.ventChatDM =[[VentChatDataModel alloc] initWithSenderAndReciever:self.senderContact.userPhoneNumber SenderName:self.senderContact.userDisplayName SenderImage:self.senderContact.userProfileImage ReciverId:self.reciverContact.contactID ReciverName:self.reciverContact.contactFullName ReciverImage:self.reciverContact.contactImage];
        
        self.topView.lblChatOwner.text = self.reciverContact.contactFirstName;
        if (self.reciverContact.contactImage != nil) {
            self.currentProfileImage =[UIImage imageWithData:self.reciverContact.contactImage];
        }
        
        
                if (self.totalUnread != 0) {
                    NSString *title=[NSString stringWithFormat:@"<%@ (%ld)",NSLocalizedString(@"Chats", nil),(long)self.totalUnread];
                    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
                    self.navigationItem.leftBarButtonItem = backButtonItem;
        
        
                }
                else
                {
                     self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:NSLocalizedString(@"Chats", nil)];
                }
        
    }
    else if(self.chatTypeSession == ConversationChat)
    {
        
        
        
        self.senderContact =self.selectedChatList.chatListOwner;
        
        

        self.senderId = self.senderContact.userPhoneNumber;
        self.senderDisplayName = self.senderContact.userDisplayName;
        self.senderDisplayName = @"";
        
        for (ChatListUsersPhones * userPhone in self.selectedChatList.chatListUsers) {
            [self.reciverContactPhones addObject:userPhone.chatUserPhone];
        }
        
        self.ventChatDM =[[VentChatDataModel alloc] initWithSenderAndReciever:self.senderContact.userPhoneNumber SenderName:self.senderContact.userDisplayName SenderImage:self.senderContact.userProfileImage ReciverId:self.selectedChatList.chatID ReciverName:self.selectedChatList.chatName ReciverImage:nil];
        
        self.topView.lblChatOwner.text =[NSString stringWithFormat:NSLocalizedString(@"Conversation", nil)] ;
        
                if (self.totalUnread != 0) {
                     NSString *title=[NSString stringWithFormat:@"<%@ (%ld)",NSLocalizedString(@"Chats", nil),(long)self.totalUnread];
                    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:nil action:@selector(goBack:)];
                    self.navigationItem.leftBarButtonItem = backButtonItem;
        
        
                }
                else
                {
                     self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:NSLocalizedString(@"Chats", nil)];
                }
        
        
    }
    else if(self.chatTypeSession == BroacastChat)
    {
        self.senderContact =self.selectedBroadcastList.broadcastListOwner;
        
        
        self.senderId = self.senderContact.userPhoneNumber;
        self.senderDisplayName = self.senderContact.userDisplayName;
         self.senderDisplayName = @"";
        
        for (BroadcastListUserPhones * userPhone in self.selectedBroadcastList.broadcastListUsers) {
            [self.reciverContactPhones addObject:userPhone.broadcastUserPhone];
        }
        
        self.ventChatDM = [[VentChatDataModel alloc] initWithSenderAndReciever:self.senderContact.userPhoneNumber SenderName:self.senderContact.userDisplayName SenderImage:self.senderContact.userProfileImage ReciverId:self.selectedBroadcastList.broadcastID ReciverName:self.selectedBroadcastList.broadcastName ReciverImage:nil];

        
        self.topView.lblChatOwner.text =self.selectedBroadcastList.broadcastName;
        
        
    }
    else if(self.chatTypeSession == GroupChat)
    {
        self.senderContact =self.selectedGroupList.groupListOwner;
        
        self.senderId = self.senderContact.userPhoneNumber;
        self.senderDisplayName = self.senderContact.userDisplayName;
         self.senderDisplayName = @"";
        
        for (GroupListUsersPhones * userPhone in self.selectedGroupList.groupListUsers) {
            [self.reciverContactPhones addObject:userPhone.groupUserPhone];
        }
        
        self.ventChatDM = [[VentChatDataModel alloc] initWithSenderAndReciever:self.senderContact.userPhoneNumber SenderName:self.senderContact.userDisplayName SenderImage:self.senderContact.userProfileImage ReciverId:self.selectedGroupList.groupID ReciverName:self.selectedGroupList.groupName ReciverImage:self.selectedGroupList.groupImage];
        
         self.topView.lblChatOwner.text =self.selectedGroupList.groupName;
        if (self.selectedGroupList.groupImage != nil) {
            self.currentProfileImage =[UIImage imageWithData:self.selectedGroupList.groupImage];
        }
        
        
                if (self.totalUnread != 0) {
                     NSString *title=[NSString stringWithFormat:@"<%@ (%ld)",NSLocalizedString(@"Groups", nil),(long)self.totalUnread];
                    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:nil action:@selector(goBack:) ];
                    self.navigationItem.leftBarButtonItem = backButtonItem;
        
        
                }
                else
                {
                     self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:NSLocalizedString(@"Groups", nil)];
                }
        
    }
    
    [self reloadAllData];
    
    //create the button and assign the image
    self.fbprofileImageView =[[UIImageView alloc]initWithImage:self.currentProfileImage];
    
  //   [self displayImage:self.fbprofileImageView withImage:self.currentProfileImage];
    
    
    //set the frame of the button to the size of the image (see note below)
    self.fbprofileImageView.frame = CGRectMake(0, 0, 30, 30);
    
    //[button addTarget:self action:@selector(selectChatProfileImage:) forControlEvents:UIControlEventTouchUpInside];
    
    [[self.fbprofileImageView layer] setCornerRadius: self.fbprofileImageView.frame.size.width / 2 ];
    [[self.fbprofileImageView layer] setBorderWidth: 1.0f];
    [[self.fbprofileImageView layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [self.fbprofileImageView setClipsToBounds: YES];
    
//    //create a UIBarButtonItem with the button as a custom view
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:self.fbprofileImageView ];
   // [customBarItem addTarget:self action:@selector(selectContact:) forControlEvents:UIControlEventTouchUpInside];

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectProfileView:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.fbprofileImageView  addGestureRecognizer:tapRecognizer];
    

    UIBarButtonItem *addContactButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(selectContact:)];

    if(self.chatTypeSession == GroupChat)
    {
       self.navigationItem.rightBarButtonItems = @[customBarItem];
    }
    else
    {
       self.navigationItem.rightBarButtonItems = @[customBarItem,addContactButton];
    }
   
    

    
     // set background image
    self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ChatBackground.jpg"]];

    
    UISwipeGestureRecognizer *downRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(downSwipeHandle:)];
    downRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [downRecognizer setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:downRecognizer];
    
    

 

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tabBarController.tabBar setHidden:YES];
    
    if (self.delegateModal) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                              target:self
                                                                                              action:@selector(closePressed:)];
    }
    [[[GlobalHandler sharedInstance] SignalRInstance] setDelegate:self];
    
    self.topView.lblSubTitle.text= [self.GlobalHandlerInstance getSignalRStatus];

    
//    if ([self.selectedChatList.chatIsSecure isEqualToNumber:[NSNumber numberWithBool:YES]]) {
//        self.timerSecure = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(calculateTimer:) userInfo:nil repeats:YES];
//        
//        [[NSRunLoop mainRunLoop] addTimer: self.timerSecure forMode:NSRunLoopCommonModes];
//        
//        [ self.timerSecure fire];
//        
//    }
    
    
   // [self.collectionView reloadData];
    [self reloadAllData];
    
    
}


-(void)reloadAllData
{
    NSError *error;
    
    // get data from DB to UI
    
    if(self.chatTypeSession == SingleChat)
    {
        
        
        ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
        self.chatsession =[chatListRepo getChatListSessionsWithRecordsNumber:self.selectedChatList RecordsLimit: self.numberOflimitedRecords Error:&error];
        
        if (self.chatsession.count >= 10 &&  self.chatsession.count >= self.numberOflimitedRecords) {
             self.showLoadEarlierMessagesHeader = YES;
        }
        else
        {
             self.showLoadEarlierMessagesHeader = NO;
        }
        
        self.ventChatDM .parentVC =self;
        [self.ventChatDM ConvertChatSessionToModel:self.chatsession];
        
        
    }
    else if(self.chatTypeSession == ConversationChat)
    {
         ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
        self.chatsession =[chatListRepo getChatListSessionsWithRecordsNumber:self.selectedChatList RecordsLimit: self.numberOflimitedRecords Error:&error];
        
        if (self.chatsession.count >= 10 &&  self.chatsession.count >= self.numberOflimitedRecords) {
            self.showLoadEarlierMessagesHeader = YES;
        }
        else
        {
            self.showLoadEarlierMessagesHeader = NO;
        }
        
        self.ventChatDM .parentVC =self;
        [self.ventChatDM ConvertChatSessionToModel:self.chatsession];
        
    }
    else if(self.chatTypeSession == BroacastChat)
    {
        
        BroadcastRepository *broadCastRepo=[[BroadcastRepository alloc]init];
        self.broadcastsession =[broadCastRepo getBroadcastListSessionsWithRecordsNumber:self.selectedBroadcastList RecordsLimit:self.numberOflimitedRecords  Error:&error];
        
        
        if (self.broadcastsession.count >= 10 &&  self.broadcastsession.count >= self.numberOflimitedRecords) {
            self.showLoadEarlierMessagesHeader = YES;
        }
        else
        {
            self.showLoadEarlierMessagesHeader = NO;
        }
        
        self.ventChatDM.parentVC = self;
        
        [ self.ventChatDM ConvertChatSessionToModel:self.broadcastsession];
        
        
        
        
    }
    else if(self.chatTypeSession == GroupChat)
    {
        
        GroupListRepository *groupListRepo=[[GroupListRepository alloc]init];
        self.groupsession =[groupListRepo getGroupListSessionsWithRecordsNumber:self.selectedGroupList RecordsLimit:self.numberOflimitedRecords Error:&error];
        
        
        if (self.groupsession.count >= 10 && self.groupsession.count >= self.numberOflimitedRecords) {
            self.showLoadEarlierMessagesHeader = YES;
        }
        else
        {
            self.showLoadEarlierMessagesHeader = NO;
        }
        
        self.ventChatDM.parentVC = self;
        
        [ self.ventChatDM ConvertChatSessionToModel: self.groupsession];
        
        
    }


    
    [self.collectionView reloadData];
    
}


-(void)sendMessageToServer:(MessageDM *)messageModel ImageDate:(NSData*)imageData Contacts:(Contacts*)contact
{
    

    NSError* error;
    // save message to DB based on chat type
    MessageNotificationBussiness * messageBussiness=[[MessageNotificationBussiness alloc]init];
    if ([self chatTypeSession] == GroupChat) {
        [messageBussiness didSendSignalRMessage:messageModel currentGroupList: self.selectedGroupList IsSent:TRUE  Error:&error];
        
         [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedGroupList.groupIsBlocked IsMute:self.selectedGroupList.groupMuted];
        
    }
    else if ([self chatTypeSession] == BroacastChat) {
            [messageBussiness didSendSignalRMessage:messageModel currentBroadcastList:self.selectedBroadcastList IsSent:TRUE  Error:&error];
        
          [JSQSystemSoundPlayer jsq_playMessageSentSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
        
    }
    else
    {
        [messageBussiness didSendSignalRMessage:messageModel currentChatList:self.selectedChatList IsSent:FALSE  Error:&error];

          [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedChatList.chatIsBlocked IsMute:self.selectedChatList.chatMute];
    }
    
    
    
    
    
        if (messageModel.contentType == text) {
        
        
        // add new message to the model
        [self.ventChatDM addTextMessageFromMe:messageModel.content Date:messageModel.sendingDateTime MsgId:messageModel.messageId];
        

        [self finishSendingMessageAnimated:YES];
        
        [GlobalHandler sendMessage:messageModel];
        
        [self.collectionView reloadData];

        
    }else if (messageModel.contentType == hyperLink){
        
        // add new message to the model
        [self.ventChatDM addTextMessageFromMe:messageModel.content Date:messageModel.sendingDateTime MsgId:messageModel.messageId];
        
        [self finishSendingMessageAnimated:YES];
        
        [GlobalHandler sendMessage:messageModel];
        
        [self.collectionView reloadData];
        
        
    }else if (messageModel.contentType == image){
        
        if (imageData != nil) {
            
            NSString __block *webPath = nil;
            NSString * extention = @"PNG";
            enum uploadFileTypes fileType = KImage;
            UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
            
            
            dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            
            dispatch_async(taskQ,
                           ^{
                               NSError *error;
                               
                               //[self finishSendingMessageAnimated:NO];
                               // upload chosen image to server
                               
                               webPath = [fileServiceObject uploadFileWithType:fileType FileData:imageData fileName:@"file" FileExt:extention Error:&error];
                             
                               messageModel.content =webPath;
                               
                                dispatch_sync(dispatch_get_main_queue(), ^{
//                               [GlobalHandler sendMessage:newMessage];
                               // [self.ventChatDM addPhotoMediaMessageFromMe:messageModel.content MsgId:messageModel.messageId];

                               [GlobalHandler sendMessage:messageModel];
                             
                               
                               
                               });
                               
                           });
            
            [self reloadAllData];
            [self finishSendingMessageAnimated:YES];
            
            
        }

    
    }else if (messageModel.contentType == video){
        
   
        if (imageData != nil) {
            
            NSString __block *webPath = nil;
            NSString * extention = @"MOV";
            enum uploadFileTypes fileType = KImage;
            UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
            
            
            dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            
            dispatch_async(taskQ,
                           ^{
                               NSError *error;
                               
                              // [self finishSendingMessageAnimated:NO];
                               // upload chosen image to server
                               
                               webPath = [fileServiceObject uploadFileWithType:fileType FileData:imageData fileName:@"file" FileExt:extention Error:&error];
                               
                               messageModel.content =webPath;
                               
                               dispatch_sync(dispatch_get_main_queue(), ^{
                                   //                               [GlobalHandler sendMessage:newMessage];
                                  // [self.ventChatDM addPhotoMediaMessageFromMe:messageModel.content MsgId:messageModel.messageId];
                                   
//                                   
//                                   
//                                   [self reloadAllData];
//                                   
//                                   
//                                   
//                                   [self finishSendingMessageAnimated:YES];
//                                   
//                                   [GlobalHandler sendMessage:messageModel];
                                   
                                  // [self reloadAllData];
                                   
                                   [GlobalHandler sendMessage:messageModel];
                                   
                               });
                               
                           });
            
            
            [self reloadAllData];
            [self finishSendingMessageAnimated:YES];
            
        }

    }else if (messageModel.contentType == location){
        
        NSArray *locationArray=[ messageModel.content componentsSeparatedByString:@","];
        
        BOOL isLocationPermitted= [self.ventChatDM addLocationMediaMessageFromMe:[locationArray[0] doubleValue] Longitude:[locationArray[1] doubleValue] MsgId:messageModel.messageId];
        
        if (isLocationPermitted) {
            [self finishSendingMessageAnimated:YES];
            
            [GlobalHandler sendMessage:messageModel];
            
            [self.collectionView reloadData];
        }
        
       
        
    }else if (messageModel.contentType == vCard){
        
        [self.ventChatDM addVcardMediaMessageFromMe:[UIImage imageWithData:contact.contactImage] FullName:contact.contactFullName VCard:[self vCardRepresentation:contact] MsgId:messageModel.messageId];
        
        [self finishReceivingMessageAnimated:YES];
        
        //NSLog(@"add vcard to chat");
        
        self.isVcardSelection = FALSE;
        [self finishSendingMessageAnimated:YES];
        
        [GlobalHandler sendMessage:messageModel];
        
        [self.collectionView reloadData];

        
        
        
    }else if (messageModel.contentType == audio){
        
    }

    
  //  NSString * deliverydate=[[NSDate alloc]getServerDateString: [NSDate date]];
  //  [messageBussiness didRecieveSignalRSignalRMarkAsDelivered:messageModel.messageId OnMessageTime:deliverydate IsSent:TRUE Error:&error];


    
}

-(void)recieveMessageFromServer:(MessageDM *)messageModel
{
    
    [self reloadAllData];
    
    if (messageModel.contentType == text) {
        
        
     
        // save message to DB based on chat type
        // already saved in global handler
 
        // add new message to the model
       // [self.ventChatDM addTextMessageFromServer:messageModel.content Date:messageModel.sendingDateTime MsgId:messageModel.messageId];
        
       [self finishReceivingMessageAnimated:YES];
        
              
        
        
    }else if (messageModel.contentType == hyperLink){
        
    }else if (messageModel.contentType == image){
        
        
                UIImage __block *thumbnailImage = nil;
                dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        
        
                dispatch_async(taskQ,
                               ^{
                                   UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
                                   NSError *error;
        
                                   NSData *thumbnailData =  [fileServiceObject downLoadThumbnail:messageModel.content Error:&error];
                                   thumbnailImage = [[UIImage alloc] initWithData:thumbnailData];
        
                                   NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                   NSString *documentsDirectory = [paths objectAtIndex:0];
        
                                   MessageDM *newMessage=[[MessageDM alloc]init];
                                   newMessage.messageId=[newMessage uniqueUUID];
        
                                   NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",[[AppUtility sharedUtilityInstance]getMediaDirectory], messageModel.content]];
                                   [thumbnailData writeToFile:localFilePath atomically:YES];
                                   NSLog(@"localFilePath.%@",localFilePath);
        
                                   //newMessage.content =localFilePath;
                                   newMessage.content =messageModel.content;
                                   newMessage.sendingDateTime=[[NSDate alloc]getServerDateFromDate: [NSDate date]];
                                   newMessage.deviceToken = self.currentUser.userDeviceUUID;
                                   newMessage.fromPhoneNumber= self.currentUser.userPhoneNumber;
        
                                   newMessage.toPhoneNumbers=self.reciverContactPhones;
                                   newMessage.messageTimer =0;
                                   newMessage.contentType = image;
                                   newMessage.groupId = nil;
        
//        
//                                   MessageNotificationBussiness * messageBussiness=[[MessageNotificationBussiness alloc]init];
//                                   [messageBussiness didSendSignalRMessage:newMessage currentChatList:self.selectedChatList IsSent:TRUE  Error:&error];
        
                                  // [JSQSystemSoundPlayer jsq_playMessageSentSound];
                                   if(self.chatTypeSession == SingleChat)
                                   {
                                       [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedChatList.chatIsBlocked IsMute:self.selectedChatList.chatMute];
                                   }
                                   else
                                   {
                                       [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedGroupList.groupIsBlocked IsMute:self.selectedGroupList.groupMuted];
                                   }
                                   
        
                                 //   [self.ventChatDM addPhotoMediaMessageFromServer:localFilePath MsgId:newMessage.messageId];
                                            dispatch_sync(dispatch_get_main_queue(), ^{
                                                
                                   [[self collectionView] reloadData];
                                   [self finishReceivingMessageAnimated:YES];
                                   });
                                   
                               });
                
        
        

        
    }else if (messageModel.contentType == video){
        UIImage __block *thumbnailImage = nil;
        dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        
        
        dispatch_async(taskQ,
                       ^{
                           UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
                           NSError *error;
                           
                           NSData *thumbnailData =  [fileServiceObject downLoadThumbnail:messageModel.content Error:&error];
                           thumbnailImage = [[UIImage alloc] initWithData:thumbnailData];
                           
                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                           NSString *documentsDirectory = [paths objectAtIndex:0];
                           
                           MessageDM *newMessage=[[MessageDM alloc]init];
                           newMessage.messageId=[newMessage uniqueUUID];
                           
                           NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",[[AppUtility sharedUtilityInstance]getMediaDirectory], messageModel.content]];
                           [thumbnailData writeToFile:localFilePath atomically:YES];
                           NSLog(@"localFilePath.%@",localFilePath);
                           
                           //newMessage.content =localFilePath;
                           newMessage.content =messageModel.content;
                           newMessage.sendingDateTime=[[NSDate alloc]getServerDateFromDate: [NSDate date]];
                           newMessage.deviceToken = self.currentUser.userDeviceUUID;
                           newMessage.fromPhoneNumber= self.currentUser.userPhoneNumber;
                           
                           newMessage.toPhoneNumbers=self.reciverContactPhones;
                           newMessage.messageTimer =0;
                           newMessage.contentType = video;
                           newMessage.groupId = nil;
                           
                           //
                           //                                   MessageNotificationBussiness * messageBussiness=[[MessageNotificationBussiness alloc]init];
                           //                                   [messageBussiness didSendSignalRMessage:newMessage currentChatList:self.selectedChatList IsSent:TRUE  Error:&error];
                           
                          // [JSQSystemSoundPlayer jsq_playMessageSentSound];
                           if(self.chatTypeSession == SingleChat)
                           {
                               [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedChatList.chatIsBlocked IsMute:self.selectedChatList.chatMute];
                           }
                           else
                           {
                               [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedGroupList.groupIsBlocked IsMute:self.selectedGroupList.groupMuted];
                           }
                           
                           
                           [self.ventChatDM addVideoMediaMessageFromServer:localFilePath MsgId:newMessage.messageId];
                           dispatch_sync(dispatch_get_main_queue(), ^{
                               
                               [[self collectionView] reloadData];
                               [self finishReceivingMessageAnimated:YES];
                           });
                           
                       });
        
    }else if (messageModel.contentType == location){
        
        NSArray *locationArray=[ messageModel.content componentsSeparatedByString:@","];
        
        [self.ventChatDM addLocationMediaMessageFromServer:[locationArray[0] doubleValue] Longitude:[locationArray[1] doubleValue] MsgId:messageModel.messageId];
        
        
    }else if (messageModel.contentType == vCard){
        
       [self.ventChatDM addVcardMediaMessageFromServer:[UIImage imageNamed:@"DefaultProfile"] FullName:@"First Name" VCard:messageModel.content  MsgId:messageModel.messageId];
        
        
    }else if (messageModel.contentType == audio){
        
    }
    

    [GlobalHandler markMessageAsDelivered:messageModel.messageId];
    
    
    
   // [self.collectionView reloadData];
    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
    self.collectionView.collectionViewLayout.springinessEnabled = [NSUserDefaults springinessSetting];
}

-(void)viewWillDisappear:(BOOL)animated
{
      [self.tabBarController.tabBar setHidden:NO];
      [self.ventChatDM stopUpdatingLocation];
    
     // [ self.timerSecure invalidate];
}

-(void)viewDidDisappear:(BOOL)animated
{
       [self.ventChatDM stopUpdatingLocation];
     //[ self.timerSecure invalidate];
}

#pragma mark - Testing

- (void)pushMainViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nc = [sb instantiateInitialViewController];
    [self.navigationController pushViewController:nc.topViewController animated:YES];
}


#pragma mark - Actions

- (void)closePressed:(UIBarButtonItem *)sender
{
    [self.delegateModal didDismissJSQDemoViewController:(DemoMessagesViewController*)self];
}

- (void)receiveMessagePressed:(UIBarButtonItem *)sender
{
    /**
     *  DEMO ONLY
     *
     *  The following is simply to simulate received messages for the demo.
     *  Do not actually do this.
     */
    
    
    /**
     *  Show the typing indicator to be shown
     */
    self.showTypingIndicator = !self.showTypingIndicator;
    
    /**
     *  Scroll to actually view the indicator
     */
    [self scrollToBottomAnimated:YES];
    
    /**
     *  Copy last sent message, this will be the new "received" message
     */
    JSQMessage *copyMessage = [[self.ventChatDM.messages lastObject] copy];
    
    if (!copyMessage) {
        copyMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdJobs
                                          displayName:kJSQDemoAvatarDisplayNameJobs
                                                 text:@"First received!"];
    }

    /**
     *  Allow typing indicator to show
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSMutableArray *userIds = [[self.ventChatDM.users allKeys] mutableCopy];
        [userIds removeObject:self.senderId];
//        NSString *randomUserId = userIds[arc4random_uniform((int)[userIds count])];
        
        JSQMessage *newMessage = nil;
        id<JSQMessageMediaData> newMediaData = nil;
        id newMediaAttachmentCopy = nil;
        
        if (copyMessage.isMediaMessage) {
            /**
             *  Last message was a media message
             */
            id<JSQMessageMediaData> copyMediaData = copyMessage.media;
            
            if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                JSQPhotoMediaItem *photoItemCopy = [((JSQPhotoMediaItem *)copyMediaData) copy];
                photoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [UIImage imageWithCGImage:photoItemCopy.image.CGImage];
                
                /**
                 *  Set image to nil to simulate "downloading" the image
                 *  and show the placeholder view
                 */
                photoItemCopy.image = nil;
                
                newMediaData = photoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                JSQLocationMediaItem *locationItemCopy = [((JSQLocationMediaItem *)copyMediaData) copy];
                locationItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [locationItemCopy.location copy];
                
                /**
                 *  Set location to nil to simulate "downloading" the location data
                 */
                locationItemCopy.location = nil;
                
                newMediaData = locationItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                JSQVideoMediaItem *videoItemCopy = [((JSQVideoMediaItem *)copyMediaData) copy];
                videoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [videoItemCopy.fileURL copy];
                
                /**
                 *  Reset video item to simulate "downloading" the video
                 */
                videoItemCopy.fileURL = nil;
                videoItemCopy.isReadyToPlay = NO;
                
                newMediaData = videoItemCopy;
            }
            else {
                NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
            }
            
            newMessage = [JSQMessage messageWithSenderId:self.reciverContact.contactID
                                             displayName:self.reciverContact.contactFirstName
                                                   media:newMediaData];
        }
        else {
            /**
             *  Last message was a text message
             */
            newMessage = [JSQMessage messageWithSenderId:self.reciverContact.contactID
                                             displayName:self.reciverContact.contactFirstName
                                                    text:copyMessage.text];
        }
        
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishReceivingMessage`
         */
        //[JSQSystemSoundPlayer jsq_playMessageReceivedSound];
        [self.ventChatDM.messages addObject:newMessage];
        [self finishReceivingMessageAnimated:YES];
        
        
        if (newMessage.isMediaMessage) {
            /**
             *  Simulate "downloading" media
             */
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                /**
                 *  Media is "finished downloading", re-display visible cells
                 *
                 *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
                 *
                 *  Reload the specific item, or simply call `reloadData`
                 */
                
                if ([newMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                    ((JSQPhotoMediaItem *)newMediaData).image = newMediaAttachmentCopy;
                   // [self.collectionView reloadData];
                    [self reloadAllData];
                    
                }
                else if ([newMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                    [((JSQLocationMediaItem *)newMediaData)setLocation:newMediaAttachmentCopy withCompletionHandler:^{
                        //[self.collectionView reloadData];
                        [self reloadAllData];
                    }];
                }
                else if ([newMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                    ((JSQVideoMediaItem *)newMediaData).fileURL = newMediaAttachmentCopy;
                    ((JSQVideoMediaItem *)newMediaData).isReadyToPlay = YES;
                   // [self.collectionView reloadData];
                    [self reloadAllData];
                }
                else {
                    NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
                }
                
            });
        }
        
    });
    
    

}






#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    

    if (text != nil && ![text isEqualToString:@""]) {
        
    
    
    MessageDM *newMessage=[[MessageDM alloc]init];
    newMessage.messageId=[newMessage uniqueUUID];
    newMessage.content =text;
    newMessage.contentType = 1;
    newMessage.sendingDateTime=[[NSDate alloc]getServerDateFromDate: [NSDate date]];
    newMessage.deviceToken = self.currentUser.userDeviceUUID;
    newMessage.fromPhoneNumber= self.currentUser.userPhoneNumber;
    
    newMessage.messageTimer =0;
    newMessage.toPhoneNumbers=self.reciverContactPhones;

    if ([self chatTypeSession] == GroupChat)
    {
        
        newMessage.groupId = [[self selectedGroupList] groupID];
        [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedGroupList.groupIsBlocked IsMute:self.selectedGroupList.groupMuted];
        
    }else{
        newMessage.groupId = nil;
        [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedChatList.chatIsBlocked IsMute:self.selectedChatList.chatMute];
        
    }
    
    
    [self sendMessageToServer:newMessage ImageDate:nil Contacts:nil];
    
    //[JSQSystemSoundPlayer jsq_playMessageSentSound];
      
    
    self.inputToolbar.contentView.textView.text =@"";
    self.inputToolbar.contentView.textView.detailText =@"";
    
    }
    
     [self.inputToolbar toggleSendButtonEnabled];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Send photo", @"Send location", @"Send video", nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
//    switch (buttonIndex) {
//        case 0:
//            [self.demoData addPhotoMediaMessage];
//            break;
//            
//        case 1:
//        {
//            __weak UICollectionView *weakView = self.collectionView;
//            
//            [self.demoData addLocationMediaMessageCompletion:^{
//                [weakView reloadData];
//            }];
//        }
//            break;
//            
//        case 2:
//            [self.demoData addVideoMediaMessage];
//            break;
//    }
    
//    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
//    [self finishSendingMessageAnimated:YES];
}



#pragma mark - JSQMessages CollectionView DataSource

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
//    CGRect bounds = scrollView.bounds;
//    CGSize size = scrollView.contentSize;
//    UIEdgeInsets inset = scrollView.contentInset;
//    float y = offset.x + bounds.size.width - inset.right;
//    float h = size.width;
    
    float offsetY=((self.inputToolbar.frame.origin.y/2)+(self.inputToolbar.frame.size.height)/2) * -1;
    
    NSLog(@"\n offsets: %f , middle: %f" ,offset.y, offsetY);
    
    
    if (offset.y <  offsetY) {
        [self hideSmilyControl];
    }
    
}


- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.ventChatDM.messages objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.ventChatDM.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.ventChatDM.outgoingBubbleImageData;
    }
    
    return self.ventChatDM.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    JSQMessage *message = [self.ventChatDM.messages objectAtIndex:indexPath.item];
    
//    if ([message.senderId isEqualToString:self.senderId]) {
//        if (![NSUserDefaults outgoingAvatarSetting]) {
//            return nil;
//        }
//    }
//    else {
//        if (![NSUserDefaults incomingAvatarSetting]) {
//            return nil;
//        }
//    }
    
    
    return [self.ventChatDM.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
//    if (indexPath.item % 3 == 0) {
//        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
//    }
    

        
    // add logic to display date for each day
    JSQMessage *message = [self.ventChatDM.messages objectAtIndex:indexPath.item];

    if (indexPath.row == 0) {
        self.lastUpdatedDate = message.date;
        
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];

    }
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute
                                                 fromDate: self.lastUpdatedDate toDate: message.date options: 0];
    NSInteger days = [components day];
    
    if (days > 1 ) {
        self.lastUpdatedDate = message.date;
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];

    }
    
//    self.lastUpdatedDate = message.date;
//    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    

    return nil;
}




- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.ventChatDM.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
         return [[NSAttributedString alloc] initWithString:@""];
    }
    
    if(self.chatTypeSession != SingleChat)
    {
        
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.ventChatDM.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
     
        ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
        Contacts *userContact=     [contactRepo getUserContactByPhoneNumber:message.senderDisplayName contactUser:self.currentUser Error:nil];
        if (userContact != nil) {
            return [[NSAttributedString alloc] initWithString:userContact.contactFirstName];
        }
        
        
        
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
        
    }
    
    
    return [[NSAttributedString alloc] initWithString:@""];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
//    NSError *error;
//    self.chatsession =[chatListRepo getChatListSessions:self.selectedChatList Error:&error];
    
//    if (self.selectedIndexPath != nil) {
//        
//       //[self.demoData.messages removeObjectAtIndex:self.selectedIndexPath.row];
//        self.selectedIndexPath = nil;
//    }
    
    return [self.ventChatDM.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    
   // JSQMessagesCollectionViewCell *cell = [[JSQMessagesCollectionViewCell alloc]init];
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.ventChatDM.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
        
        
       
        
    }
    
    cell.selectedCellIndexPath=indexPath;
    
    
    ChatSessionRepository *repo =[[ChatSessionRepository alloc]init];
    NSArray *chatsessionlist = [repo getChatSessionsByMessageId:msg.messageId Error:nil];
    
    if (chatsessionlist.count >0) {
        ChatSession * selectedChat=(ChatSession*)chatsessionlist[0];
        if ([selectedChat.chatSessionDelivered isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            cell.lblMessgaeStatusAndDateOutgoing.text = [NSString stringWithFormat:NSLocalizedString(@"Delivered", nil)];
        }
        else
        {
            cell.lblMessgaeStatusAndDateOutgoing.text =[NSString stringWithFormat:NSLocalizedString(@"Sending", nil)];
        }
        
        if ([selectedChat.chatSessionIsSent isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            cell.lblMessgaeStatusAndDateOutgoing.text =[NSString stringWithFormat:NSLocalizedString(@"Sent", nil)];
        }
        
        if ([selectedChat.chatSessionRead isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            cell.lblMessgaeStatusAndDateOutgoing.text =[NSString stringWithFormat:NSLocalizedString(@"Read", nil)];
        }
        
    }

   
        
    
    if ([self.selectedChatList.chatIsSecure isEqualToNumber:[NSNumber numberWithBool:YES]]) {
        cell.currentMessageId= msg.messageId;
         cell.selectedCellIndexPath=indexPath;
        cell.startTime = [NSDate date];
        if (chatsessionlist.count >0) {
            ChatSession * selectedChat2=(ChatSession*)chatsessionlist[0];
            cell.startTime = [[NSDate alloc] getDateFromServerString:selectedChat2.chatSessionDate];
        }
        
        cell.cellDelegate = self;
        [cell startTimer];
        
    }

    
    
    
    return cell;
}




#pragma mark timer for secure chat
- (void) startTimerForCell:(JSQMessagesCollectionViewCell*)cell
{
    
  //  NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];

    
      cell.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(calculateTimer:) userInfo:cell repeats:YES];
    

    
    // invalidate a previous timer in case of reuse
    if (cell.timer)
    {
        return;
    }
    
    
    //    [self.timer invalidate];
    
    
    cell.startTime = [[NSDate alloc]getServerDateFromDate: [NSDate date]];
    cell.startTick = 0;
    // create a new timer
  
    
    [[NSRunLoop currentRunLoop] addTimer:cell.timer forMode:NSRunLoopCommonModes];
    
    [cell.timer fire];
}

- (void)calculateTimer:(NSTimer*)theTimer
{
    //  JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    for (JSQMessagesCollectionViewCell *cell in [self.collectionView visibleCells]) {
        
        if (cell.selectedCellIndexPath.row < self.ventChatDM.messages.count) {
            
        
        JSQMessage *msg = [self.ventChatDM.messages objectAtIndex:cell.selectedCellIndexPath.row];
        NSDate *currentMsgDate=msg.date;
            
        NSTimeInterval interval = [currentMsgDate timeIntervalSinceNow] ;
       
        interval = (-1 * interval);
        
        
        NSString *intervalMinutesString = [NSString stringWithFormat:@"%.0f",  interval/60];
        
        NSString *intervalSecondsString = [NSString stringWithFormat:@"%.2f", ( interval/60)];
        
        NSString *pureSeconds =[NSString stringWithFormat:@"%.2f", [intervalSecondsString floatValue]-[intervalMinutesString floatValue] ];
        
       // NSLog(@"cell timer =%@ with minutes = %@ , seconds %@  , %@" , currentMsgDate ,intervalMinutesString ,intervalSecondsString,pureSeconds );
        
        cell.lblMessageTimer.text = intervalSecondsString;
        
        if ([intervalMinutesString intValue] >= 1) {
            //delete message
          
            [self.ventChatDM.messages removeObjectAtIndex:cell.selectedCellIndexPath.row];
            
           // [self.collectionView reloadData];
           
            
           // [self.collectionView performBatchUpdates:^{
                
               
                ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
                NSError *error;
                self.chatsession =[chatListRepo getChatListSessions:self.selectedChatList Error:&error];
            
                ChatSession *selectedSession=[self.chatsession objectAtIndex:cell.selectedCellIndexPath.row];
                
                ChatSessionRepository *repo =[[ChatSessionRepository alloc]init];
                [repo deleteChatSession:selectedSession Error:&error];
//                ChatListRepository *clRepo =[[ChatListRepository alloc]init];
//                self.chatsession =[clRepo getChatListSessions:self.selectedChatList Error:&error];
//
//               [ self.demoData loadOldChatSessions: self.chatsession];
                
                
          //  } completion:nil];
            
             [self reloadAllData];
            
            

            
        }
        }
        
        
    }
  
    
//   JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell*)[theTimer userInfo];
//
//    NSTimeInterval interval = [cell.startTime timeIntervalSinceNow];
//    interval = (-1 * interval);
//    cell.startTick+=1;
//    
//    NSString *intervalString = [NSString stringWithFormat:@"00:%i",  cell.startTick];
//    
//    cell.lblMessageTimer.text = intervalString;
//    
//    //if we want the timer to stop after a certain number of seconds we can do
//    if( cell.startTick >= 60){
//        //stop the timer after 60 seconds
//        [cell.timer invalidate];
//        
//        // delete cell
//        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
//        // JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
//        ChatSession *selectedSession=[self.chatsession objectAtIndex:indexPath.row];
//        
//        ChatSessionRepository *repo =[[ChatSessionRepository alloc]init];
//        [repo deleteChatSession:selectedSession Error:nil];
//        [self.demoData.messages removeObjectAtIndex:indexPath.row];
//        
////        NSArray *deleteArry=[[NSArray alloc]initWithObjects:indexPath, nil];
////        [self.collectionView deleteItemsAtIndexPaths:deleteArry];
//        [self.collectionView reloadData];
//    }
    
    
//    [self.collectionView reloadData];
    
}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label height

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{

  
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
   // if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
   // }
    
   // return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    
 
    
    JSQMessage *currentMessage = [self.ventChatDM.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.ventChatDM.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
    
    self.numberOflimitedRecords=self.numberOflimitedRecords+10;
    [self reloadAllData];
    
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
    NSError *error;
    
//    ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
//    self.chatsession =[chatListRepo getChatListSessions:self.selectedChatList Error:&error];
    
    ChatSession *selectedSession= [self.chatsession objectAtIndex:indexPath.row];
    
    
    
    if ([selectedSession.chatSessionType integerValue] == 4 ) {
        
        NSURL *vedioURL;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
        NSLog(@"files array %@", filePathsArray);
        
        NSString *fullpath;
        
        
        fullpath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", [[AppUtility sharedUtilityInstance]getMediaDirectory],selectedSession.chatSessionMessage]];
        vedioURL =[NSURL fileURLWithPath:fullpath];
        
        NSLog(@"vurl %@",vedioURL);
        
        
        _moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL: vedioURL];
        
        _moviePlayer.controlStyle = MPMovieControlStyleDefault;
        _moviePlayer.shouldAutoplay = YES;
        [self.view addSubview:_moviePlayer.view];
        [_moviePlayer setFullscreen:YES animated:YES];

    
    }
    
else  if ([selectedSession.chatSessionType integerValue] == 5 ) {
    //location open in maps
    
    NSArray * location =[selectedSession.chatSessionMessage componentsSeparatedByString:@","];
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake([location[0] doubleValue], [location[1] doubleValue]);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    
    [endingItem openInMapsWithLaunchOptions:launchOptions];
    
}
else  if ([selectedSession.chatSessionType integerValue] == 6 ) {
    //vcard
    
    ContactsBussinessService *contBS=[[ContactsBussinessService alloc]init];
    if ([contBS isContatcsAccessGrantSilent]) {
        
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserProfileTableViewController *userProfile = [sb instantiateViewControllerWithIdentifier:@"UserProfileInfo"];
    userProfile.vcardProfile= YES;
    userProfile.vcardString=selectedSession.chatSessionMessage;
    
    [self.navigationController pushViewController:userProfile animated:YES];
    }
    else
    {

            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               
                               
                               UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                               
                               [cantLoadContactsAlert show];
                               
                               
                               
                           });
            
        

    }
    
    
    
}
    
    
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
    
//    [self.demoData.messages removeObjectAtIndex:indexPath.row];
//    
//    [self.collectionView reloadData];
    
}


#pragma mark - toolbar actions
- (IBAction)selectChatProfileImage:(id)sender {
   //  [self displayImage:self.fbprofileImageView withImage:[UIImage imageNamed:@"DefaultProfile"]];
}

- (IBAction)selectContact:(id)sender {
    NSLog(@"select Contacts");
    
    ContactsBussinessService *contactBWS=[[ContactsBussinessService alloc]init];
    if (![contactBWS isContatcsAccessGrantInDispath]) {
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           
                           
                           UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                           
                           [cantLoadContactsAlert show];
                           
                           
                           
                       });
        return;
    }
    
    MainContactsTableView *contactView =[[MainContactsTableView alloc]initWithNibName:@"ContactsTableView" delegate:self];
    self.isVcardSelection = FALSE;
    [[contactView navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Contacts", nil)]];
    //   [[self navigationController] pushViewController:contactView animated:YES];
    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactView];
    
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self showDetailViewController:navController sender:self];
    
    
}


#pragma mark - BaseSearchSelectionDelegate

- (SelectionType)selectionType{
    
    if (self.isVcardSelection == FALSE) {
        return kMultipleSelect;
    }
    return kSingleSelect;
}



#pragma mark - Contact Selection


-(void)didSelectContacts:(Contacts *)contacts
{
    NSError *error;
    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
    
    NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:contacts Error:&error];
    
    
    NSLog(@"contact selection %@",ventphonenumber);
    
    
     if (self.isVcardSelection == FALSE) {
         
    ChatListRepository * chatlistRepo=[[ChatListRepository alloc]init];
    
    NSArray *phonelist=[[NSArray alloc]initWithObjects:ventphonenumber, nil];
    NSString *currentdate=[[NSDate alloc]getServerDateString:[NSDate date]];

    
    ChatList* currentChatList=[chatlistRepo getChatListByPhoneNumber:ventphonenumber Error:&error];
    
    if (currentChatList.fault || currentChatList == nil)
    {
        [chatlistRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Chat" chatLastUpdatedDate:currentdate  chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:NO] chatID:@"" chatName:contacts.contactFullName chatLastMessage:@"" chatUserImage:contacts.contactImage chatUserImageUrl:@"" chatFromPhone:self.currentUser.userPhoneNumber  chatToPhone:ventphonenumber chatIsFavourite:[NSNumber numberWithBool:NO] chatListOwner:self.currentUser chatListSession:Nil  chatListUsers:phonelist chatIsSecure:[NSNumber numberWithBool:NO] Error:&error];
    }
     }
    else
    {
        
        // add vcard to chat session
        
        NSError *error;
        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
        
        User* currentUser=[contactBWS getCurrentUserProfile];
        
        MessageDM *newMessage=[[MessageDM alloc]init];
        newMessage.messageId=[newMessage uniqueUUID];
        
       // ABRecordRef contactperson = ABAddressBookGetPersonWithRecordID(addressBookRef, contacts.contactID);
        
        newMessage.content =[self vCardRepresentation:contacts];
        newMessage.sendingDateTime=[[NSDate alloc]getServerDateFromDate: [NSDate date]];
        newMessage.deviceToken = currentUser.userDeviceUUID;
        newMessage.fromPhoneNumber= currentUser.userPhoneNumber;
        
        newMessage.toPhoneNumbers=self.reciverContactPhones;
        newMessage.messageTimer =0;
        newMessage.contentType = vCard;
        newMessage.groupId = nil;
        
        [self sendMessageToServer:newMessage ImageDate:nil Contacts:contacts];
        
        if(self.chatTypeSession == SingleChat)
        {
          [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedChatList.chatIsBlocked IsMute:self.selectedChatList.chatMute];
        }
        else
        {
          [JSQSystemSoundPlayer jsq_playMessageSentSound:self.selectedGroupList.groupIsBlocked IsMute:self.selectedGroupList.groupMuted];
        }
        
       // [JSQSystemSoundPlayer jsq_playMessageSentSound];
 
        
      //  [self.ventChatDM addVcardMediaMessage:[UIImage imageWithData:contacts.contactImage] FullName:contacts.contactFullName VCard:[self vCardRepresentation:contacts]];
        
        
       // [self reloadAllData];
    }
    
 
}

-(void)didSelectContactsList:(NSArray *)contactsList
{
    // NSLog(@"contacts list selection for conversation ");
    
    if (contactsList.count >0) {
        
    
    NSError *error;
       
        
    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
    NSMutableArray *VentPhoneList=[[NSMutableArray alloc]init];
//    ChatListRepository * chatlistRepo=[[ChatListRepository alloc]init];
    NSString *currentdate=[[NSDate alloc]getServerDateString: [NSDate date]];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
        for (Contacts *selectedCont in contactsList) {
            NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:selectedCont Error:&error];
            
            NSLog(@"contact selection %@",ventphonenumber);
            [VentPhoneList addObject:ventphonenumber];
        }
        
        
        
        if(self.chatTypeSession == SingleChat)
        {
          
            NSString * recieverPhonenumber =[contactsRepo getVentPhoneNumberForContact:self.reciverContact Error:&error];
            
            [VentPhoneList addObject:recieverPhonenumber];
            
            NSArray *phonelist=[[NSArray alloc]initWithArray:VentPhoneList];
            
            ChatListRepository * chatlistRepo=[[ChatListRepository alloc]init];
            // ChatList* currentChatList=[chatlistRepo getChatListByPhoneNumbers:phonelist Error:&error];
            
            ChatList* currentChatList=[chatlistRepo getConversationByPhoneNumber:phonelist Error:nil];
            
            if (currentChatList.fault || currentChatList == nil)
            {
                
                currentChatList=[chatlistRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Conversation" chatLastUpdatedDate:currentdate  chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:NO] chatID:@"" chatName:@"Conversation" chatLastMessage:@"" chatUserImage:Nil chatUserImageUrl:@"" chatFromPhone:currentUser.userPhoneNumber  chatToPhone:nil chatIsFavourite:[NSNumber numberWithBool:NO]  chatListOwner:currentUser chatListSession:Nil chatListUsers:phonelist chatIsSecure:[NSNumber numberWithBool:NO] Error:&error];
                
                [self.navigationController popViewControllerAnimated:NO];
                self.parentMainVC.enableDeepLinking = TRUE;
                self.parentMainVC.selectedModelForDeepLinking=currentChatList;
                
            }
            else
            {
                [self.navigationController popViewControllerAnimated:NO];
                
                self.parentMainVC.enableDeepLinking = TRUE;
                self.parentMainVC.selectedModelForDeepLinking=currentChatList;
                
                
            }

            
        }
        else if(self.chatTypeSession == ConversationChat)
        {
            
            
//            for (ChatListUsersPhones *selectedConvCont in self.selectedChatList.chatListUsers) {
//                
//                [VentPhoneList addObject:selectedConvCont.chatUserPhone];
//            }

            NSArray *phonelist=[[NSArray alloc]initWithArray:VentPhoneList];

           
            
            ChatListRepository * chatlistRepo=[[ChatListRepository alloc]init];
            // ChatList* currentChatList=[chatlistRepo getChatListByPhoneNumbers:phonelist Error:&error];
            
           // ChatList* currentChatList=[chatlistRepo getConversationByPhoneNumber:phonelist Error:nil];
            
            for (NSString *phoneNumber in phonelist) {
                [chatlistRepo addUserPhoneToChatList:phoneNumber chatUserEnabled:[NSNumber numberWithBool:YES] chatUserJoinDate:currentdate phoneChatList:(ChatList*)self.selectedChatList Error:nil];
            }
            
                [self.navigationController popViewControllerAnimated:NO];
                
                self.parentMainVC.enableDeepLinking = TRUE;
                self.parentMainVC.selectedModelForDeepLinking=self.selectedChatList;
                
                
            

            
            
        }
        
        
    }
    
    
    
}


- (NSString *)vCardRepresentation:(Contacts *)selectedCont
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    ABRecordRef contactperson = ABAddressBookGetPersonWithRecordID(addressBookRef,[selectedCont.contactID intValue]);
    
    ABRecordRef person = contactperson;
    ABRecordRef people[1];
    people[0] = person;
    CFArrayRef peopleArray = CFArrayCreate(NULL, (void *)people, 1, &kCFTypeArrayCallBacks);
    NSData *vCardData = CFBridgingRelease(ABPersonCreateVCardRepresentationWithPeople(peopleArray));
    NSString *vCard = [[NSString alloc] initWithData:vCardData encoding:NSUTF8StringEncoding];
    NSLog(@"vCard > %@", vCard);
//    
//    VcardImporter *vcardParser=[[VcardImporter alloc]init];
//    [vcardParser parse:vCard];
    
    return vCard;
}


#pragma mark fbimageviewer
//- (void) displayImage:(UIImageView*)imageView withImage:(UIImage*)image  {
//    [imageView setImage:image];
//    imageView.contentMode = UIViewContentModeScaleAspectFill;
//    [imageView setupImageViewer];
//    imageView.clipsToBounds = YES;
// 
//    
//}

#pragma mark SignalR Connectivty Protocol

#pragma mark signalR connection status protocol
-(void)started
{
    //NSLog(@"ChatListTableViewController => GlobalHandler =>  SignalR: started");
    self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"connected", nil)] ;
  //  [self.inputToolbar.contentView.rightBarButtonItem setEnabled:YES];
}

-(void)received:(NSDictionary *) data
{
    self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"connected", nil)] ;
   // [self.inputToolbar.contentView.rightBarButtonItem setEnabled:YES];
}

-(void)error:(NSError *)error
{
    
}

-(void)closed
{
    self.topView.lblSubTitle.text = [NSString stringWithFormat:NSLocalizedString(@"dis-connected", nil)];
   //   [self.inputToolbar.contentView.rightBarButtonItem setEnabled:NO];

    
}

-(void)reconnecting
{
    self.topView.lblSubTitle.text = [NSString stringWithFormat:NSLocalizedString(@"re-connecting", nil)];
   // [self.inputToolbar.contentView.rightBarButtonItem setEnabled:NO];
     [self.inputToolbar toggleSendButtonEnabled];
}

-(void)reconnected
{
    self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"connected", nil)] ;
 //   [self.inputToolbar.contentView.rightBarButtonItem setEnabled:YES];
     [self.inputToolbar toggleSendButtonEnabled];
}

-(void)stateChanged:(connectionState) connectionState
{
    if (connectionState == connected) {
        self.topView.lblSubTitle.text = [NSString stringWithFormat:NSLocalizedString(@"connected", nil)];
      //  [self.inputToolbar.contentView.rightBarButtonItem setEnabled:YES];
    }
    else
    {
        if (connectionState == connecting) {
            self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"connecting", nil)];
        }
        else if (connectionState == reconnecting) {
            self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"reconnecting", nil)];
        }
        else if (connectionState == disconnected) {
            self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"disconnected", nil)];
        }
        
     //  [self.inputToolbar.contentView.rightBarButtonItem setEnabled:NO];
    }
    
     [self.inputToolbar toggleSendButtonEnabled];
}

-(void)connectionSlow
{
    
}


#pragma mark top chat header Protocol

- (void)selecTopChatHeader:(id)sender
{
    NSLog(@"select top chat header");
    
    
    if(self.chatTypeSession == SingleChat)
    {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UserProfileTableViewController *userProfile = [sb instantiateViewControllerWithIdentifier:@"UserProfileInfo"];
        userProfile.selectedChatList = self.selectedChatList;
        userProfile.reciverContact = self.reciverContact;
        
        [self.navigationController pushViewController:userProfile animated:YES];
        
    }
    else if(self.chatTypeSession == ConversationChat)
    {

        
    }
    else if(self.chatTypeSession == BroacastChat)
    {

        
    }
    else if(self.chatTypeSession == GroupChat)
    {

        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewGroupListViewController *groupProfile = [sb instantiateViewControllerWithIdentifier:@"NewGroupList"];
        groupProfile.selectedGroupList = self.selectedGroupList;
        groupProfile.UpdateGroup = YES;
        
        [self.navigationController pushViewController:groupProfile animated:YES];
        
    }
    
    [self.collectionView endEditing:YES];


}


- (void)downSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@"downSwipeHandle");
}




#pragma mark cell update protocol

-(void)UpdateChatCellAtIndex:(NSIndexPath *)indexPath
{
  //  NSArray *indexArray=[[NSArray alloc]initWithObjects:indexPath, nil];
  //  [ self.collectionView deleteItemsAtIndexPaths:indexArray];
    [ self reloadAllData];
   
}


#pragma mark select profile image


- (IBAction)selectProfileView:(id)sender {
    GGFullscreenImageViewController *vc = [[GGFullscreenImageViewController alloc] init];
    vc.liftedImageView = self.fbprofileImageView;
    [self presentViewController:vc animated:YES completion:nil];
    
    
}


#pragma mark - pop back actions
- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
