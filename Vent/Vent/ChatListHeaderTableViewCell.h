//
//  ChatListHeaderTableViewCell.h
//  Vent
//
//  Created by Medhat Ali on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListHeaderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UISearchBar *searchControl;
@property (strong, nonatomic) IBOutlet UIButton *btnBroadCast;

@end
