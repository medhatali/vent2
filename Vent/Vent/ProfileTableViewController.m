//
//  ProfileTableViewController.m
//  Smiley
//
//  Created by Sameh Farouk on 8/19/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import "ProfileTableViewController.h"
#import "CommonHeader.h"
#import "RegisterBussinessService.h"
#import "UploadDownLoadBussinessService.h"
#import "ContactsBussinessService.h"
#import "User.h"
#import "GlobalHandler.h"
#import "ProfileWebService.h"

@interface ProfileTableViewController ()<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *profileImageView;
@property (strong, nonatomic) IBOutlet UITextField *enterName;
@property (strong, nonatomic) IBOutlet UITextField *enterMail;

@property BOOL updateUserData;
@end

@implementation ProfileTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // call signal R initor
//    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
//    User* currentUser=[contactBWS getCurrentUserProfile];
//    if (currentUser) {
//            if (currentUser.userIsRegistered) {
//                [[GlobalHandler sharedInstance] startSignalR];
//            }
//    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    self.updateUserData = FALSE;
    
     self.title =[NSString stringWithFormat:NSLocalizedString(@"Profile", nil)];
    
    if ([self.registrationData.DisplayName isEqualToString:@""] || self.registrationData.DisplayName == Nil) {
          [self.enterName setValue:[NSString stringWithFormat:NSLocalizedString(@"Enter your name", nil)] forKeyPath:@"_placeholderLabel.text"];
    }
    else
    {
        [self.enterName setText:self.registrationData.DisplayName];

    }
    
    [self.enterMail setValue:[NSString stringWithFormat:NSLocalizedString(@"Enter your email", nil)] forKeyPath:@"_placeholderLabel.text"];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [[[self profileImageView] layer] setCornerRadius: self.profileImageView.frame.size.width / 2 ];
    [[[self profileImageView] layer] setBorderWidth: 3.0f];
    [[[self profileImageView] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [[self profileImageView] setClipsToBounds: YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
       [self.navigationItem.rightBarButtonItem setTitle:[NSString stringWithFormat:NSLocalizedString(@"Finish", nil)]];
    
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 4;
}

#pragma mark -

- (IBAction)doneAction:(id)sender {
    
    
 
    // finish action
    // update user profile in web service and save to local DB
//    [[[[self slidingViewController] topViewController] view] setHidden:NO];
//    [[self slidingViewController] resetTopViewAnimated:YES];
//    [[self navigationController] popViewControllerAnimated:YES];
    
    if (![self.enterName.text isEqualToString:self.registrationData.DisplayName]) {
        self.updateUserData = TRUE;
    }
    
    if (![self.enterMail.text isEqualToString:@""]) {
        self.updateUserData = TRUE;
        
        if (![self isValidEmail:self.enterMail.text]) {
            
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Email validation Error", nil)
                                        message:NSLocalizedString(@"Email not in a correct format", nil)
                                       delegate:self
                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                              otherButtonTitles:nil] show];
            
            return;
        }
        
    }

    

    
    if (self.updateUserData == TRUE) {
        
    RegisterBussinessService *registerServiceObject =[[RegisterBussinessService alloc]init];
    UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Update Profile", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       NSError *error;
                       
                       // upload profile image to server
                       UIImage *profileimage=[[self profileImageView]backgroundImageForState:UIControlStateNormal];
                       NSData *imageData = UIImagePNGRepresentation(profileimage);
                       
                       NSString *imagePath = [fileServiceObject uploadImage:imageData ImageName:@"file" ImageExt:@"png" Error:&error];
                       
                       // update user profile online and local DB
                       UserProfileDM *profileCurrentUser = [[UserProfileDM alloc]init];
                       profileCurrentUser.FirstName =self.enterName.text;
                       profileCurrentUser.LastName =@"";
                       profileCurrentUser.Status = @"";
                       profileCurrentUser.ImagePath =imagePath;
                       profileCurrentUser.DateOfBirth =Nil;
                       profileCurrentUser.PhoneNumber =self.registrationData.PhoneNumber;
                    

                       bool success=[registerServiceObject updateUserProfile:profileCurrentUser UserModel:self.registrationData Error:&error];
                       
                       if (![self.enterMail.text isEqualToString:@""])
                       {
                       ProfileWebService *profileService=[[ProfileWebService alloc]init];
                       [profileService addEmailToCurrentUser:self.enterMail.text Error:nil];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                          
                                          if (success) {
                                              
                                              // go to contacts
                                              ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
                                              
                                              User *currentUser=[contactBWS getCurrentUserProfile];
                                              currentUser.userProfileImage =imageData;
                                              UserRepository *userRepo=[[UserRepository alloc]init];
                                              [userRepo updateUser:currentUser Error:nil];
                                              
                                               [self performSegueWithIdentifier:@"LoadContacts" sender:self];
                                             // [self performSegueWithIdentifier:@"GoProfile" sender:self];
                                              
                                          }
                                          else
                                          {
                                              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update Error", nil)
                                                                          message:NSLocalizedString(@"error in Update Profile", nil)
                                                                         delegate:nil
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                otherButtonTitles:nil] show];
                                              
                                             // [self performSegueWithIdentifier:@"GoProfile" sender:self];
                                              
                                          }
                                          
                                          
                                      });
                       
                   });
    
    }
    else
    {
     
        [self performSegueWithIdentifier:@"LoadContacts" sender:self];
        
    }
    
    
}



- (IBAction)changeProfilePicture:(id)sender {
    UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)], [NSString stringWithFormat:NSLocalizedString(@"Choose Existing", nil)], nil];
    
    [_actionSheet showInView:[self view]];
}

#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UIImagePickerController *_picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = YES;
    
    switch (buttonIndex) {
        case 0:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Device has no camera", nil)]
                                                                     delegate:nil
                                                            cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            else{
                
                _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:_picker animated:YES completion:NULL];
                
            }
            break;
        case 1:
            _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:_picker animated:YES completion:NULL];
            break;
        default:
            break;
    }
}

#pragma mark - Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *_chosenImage = info[UIImagePickerControllerEditedImage];
    self.updateUserData = TRUE;
    [[self profileImageView] setBackgroundImage:_chosenImage forState:UIControlStateNormal];
    [[self profileImageView] setTitle:nil forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


- (BOOL)isValidEmail:(NSString *)email
{
    NSString *regex1 = @"\\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z";
    NSString *regex2 = @"^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*";
    NSPredicate *test1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    NSPredicate *test2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex2];
    return [test1 evaluateWithObject:email] && [test2 evaluateWithObject:email];
}

@end
