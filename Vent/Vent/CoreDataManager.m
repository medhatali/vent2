//
//  CoreDataManager.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "CoreDataManager.h"


@interface CoreDataManager ()

@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation CoreDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize privateObjectContext = _privateObjectContext;


static dispatch_once_t pred;
static CoreDataManager *sharedManager;

+ (id) sharedManager {
    
    dispatch_once(&pred, ^{
        sharedManager = [[CoreDataManager alloc] init];
    });
    
    return sharedManager;
    
}

// private queue for DB operations

static dispatch_queue_t coredata_background_save_queue;

dispatch_queue_t background_save_queue()
{
    if (coredata_background_save_queue == NULL)
    {
        coredata_background_save_queue = dispatch_queue_create("com.Vent.coredata.backgroundsaves", 0);
    }
    return coredata_background_save_queue;
}



#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
    
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:CDModelName withExtension:CDModelExtension];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
    
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:CDModelFullName];
    
#ifdef DEBUG
    NSLog(@"CoreData path : %@" , storeURL);
#endif
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @(YES), NSMigratePersistentStoresAutomaticallyOption,
                                       @(YES), NSInferMappingModelAutomaticallyOption,
                                       nil];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:optionsDictionary
                                                           error:&error]) {
        abort();
    }
    
    return _persistentStoreCoordinator;
}


#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



#pragma mark - save context & save contet in thread


- (void)saveDataInPrivateContextFromDBQueue:(void(^)(NSManagedObjectContext *context))saveBlock completionBlock:(myContextBlock)completionBlock
{
    
    dispatch_async(background_save_queue(), ^{
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        //NSManagedObjectContext *ParentContext = [self managedObjectContext];			//step 1
        //
        //        NSManagedObjectContext *privateContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        //        NSManagedObjectContext *ParentContext = [self managedObjectContext];
        //        [privateContext setParentContext:ParentContext];
        
        // NSManagedObjectContext *privateContext = [self privateObjectContext];
        
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        
        
        NSManagedObjectContext *privateContext =[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [privateContext setPersistentStoreCoordinator:coordinator];
        [privateContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        
        [privateContext performBlock:^{
            
            saveBlock(privateContext);
            
            // [NSThread sleepForTimeInterval:5.0f];
            
            NSError *error = nil;
            if (![privateContext save:&error]) {
                NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
                completionBlock(NO,nil,error);
            }
            else
            {
                completionBlock(YES, nil,error);
            }
        }];
        
    });
    
}


- (void)saveDataInPrivateContext:(void(^)(NSManagedObjectContext *context))saveBlock completionBlock:(myContextBlock)completionBlock
{
    
   // dispatch_async(background_save_queue(), ^{
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        //NSManagedObjectContext *ParentContext = [self managedObjectContext];			//step 1
        //
        //        NSManagedObjectContext *privateContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        //        NSManagedObjectContext *ParentContext = [self managedObjectContext];
        //        [privateContext setParentContext:ParentContext];
        
        // NSManagedObjectContext *privateContext = [self privateObjectContext];
        
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        
        
        NSManagedObjectContext *privateContext =[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [privateContext setPersistentStoreCoordinator:coordinator];
        [privateContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        
        [privateContext performBlock:^{
            
            saveBlock(privateContext);
            
            // [NSThread sleepForTimeInterval:5.0f];
            
            NSError *error = nil;
            if (![privateContext save:&error]) {
                NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
                completionBlock(NO,nil,error);
            }
            else
            {
                completionBlock(YES, nil,error);
            }
        }];
        
   // });
    
}


- (BOOL) saveContextInMainQueue:(NSManagedObjectContext *)context Error:(NSError **)error
{
    
    if (context != nil) {
        if ([context hasChanges] && ![context save:error]) {
            
            /*
             Replace this implementation with code to handle the error appropriately.
             
             */
            NSLog(@"Unresolved error %@, %@", *error, [*error userInfo]);
            
        } else {
            return YES;
        }
    }
    
    
    return NO;
    
}

- (void)saveContextInMainQueue:(NSManagedObjectContext *)sharedContext CompletionBlock:(myContextBlock)completionBlock
{
    

        NSError *error = nil;
        if (![sharedContext save:&error]) {
            NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
            completionBlock(NO,nil,error);
        }
        else
        {
            completionBlock(YES, nil,error);
        }
    
    
}

- (void)saveContextInDBQueue:(NSManagedObjectContext *)sharedContext CompletionBlock:(myContextBlock)completionBlock
{
    
    dispatch_async(background_save_queue(), ^{
        
        // [NSThread sleepForTimeInterval:5.0f];
        
        NSError *error = nil;
        if (![sharedContext save:&error]) {
            NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
            completionBlock(NO,nil,error);
        }
        else
        {
            completionBlock(YES, nil,error);
        }
        
        
    });
    
}


- (void)mergeChanges:(NSNotification *)notification
{
    NSManagedObjectContext *mainContext = [self managedObjectContext];
    
    // Merge changes into the main context on the main thread
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:)
                                  withObject:notification
                               waitUntilDone:YES];
}



#pragma mark retrieve data from DB

-(void)retrieveDataInPrivateContextFromDBQueue:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock
{
    __block NSArray *mutableFetchResults;
    
    
    
    dispatch_async(background_save_queue(), ^{
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        //NSManagedObjectContext *ParentContext = [self managedObjectContext];			//step 1
        //
        //        NSManagedObjectContext *privateContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        //        NSManagedObjectContext *ParentContext = [self managedObjectContext];
        //        [privateContext setParentContext:ParentContext];
        
        // NSManagedObjectContext *privateContext = [self privateObjectContext];
        
        NSPersistentStoreCoordinator *coordinator = [[CoreDataManager sharedManager]  persistentStoreCoordinator];
        
        
        NSManagedObjectContext *privateContext =[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        // [privateContext setParentContext:[self managedObjectContext]];
        
        [privateContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        
        
        [privateContext setPersistentStoreCoordinator:coordinator];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setIncludesPendingChanges:YES];
        [request setReturnsObjectsAsFaults:NO];
        [request setIncludesPropertyValues:YES];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                                  inManagedObjectContext:privateContext];
        
        
        [request setEntity:entity];
        
        
        
        if(predicateOrNil != nil)
        {
            [request setPredicate:predicateOrNil];
        }
        
        // Edit the sort key as appropriate.
        NSMutableArray *sortDescriptors = [[NSMutableArray alloc]init];
        
        if(ascSortStringOrNil != nil)
        {
            for (NSString *asc in ascSortStringOrNil)
            {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:asc ascending:NO] ;
                [sortDescriptors addObject:sortDescriptor];
            }
        }
        if(descSortStringOrNil != nil)
        {
            for (NSString *desc in descSortStringOrNil)
            {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:desc ascending:YES] ;
                [sortDescriptors addObject:sortDescriptor];
            }
        }
        [request setSortDescriptors:sortDescriptors];
        
        
        
        [privateContext performBlock:^{
            
            //NSError *fetcherror;
            
            
            
            mutableFetchResults = [privateContext  executeFetchRequest:request error:error] ;
            
            
            
            if (mutableFetchResults == nil) {
                // Handle the error.
            }
            
            if (error) {
                
                
            }else {
                
                if (mutableFetchResults.count > 0)
                {
                    

                    
                    completionBlock(YES,[mutableFetchResults copy],nil);
                    

                    
                }
                else
                {
                    
                    //   dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    completionBlock(NO,nil,nil);
                    
                    //         });
                    
                    
                }
                
                
            }
            
            
            
            
        }];
        
    });
    
    
    
    
    
}


-(void)retrieveDataInPrivateContextFromDB:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError**)error CompletionBlock:(myCompletionBlock)completionBlock
{
    __block NSArray *mutableFetchResults;
    
    

        
        NSPersistentStoreCoordinator *coordinator = [[CoreDataManager sharedManager]  persistentStoreCoordinator];
        
        
        NSManagedObjectContext *privateContext =[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        // [privateContext setParentContext:[self managedObjectContext]];
        
        [privateContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        
        
        [privateContext setPersistentStoreCoordinator:coordinator];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setIncludesPendingChanges:YES];
        [request setReturnsObjectsAsFaults:NO];
        [request setIncludesPropertyValues:YES];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                                  inManagedObjectContext:privateContext];
        
        
        [request setEntity:entity];
        
        
        
        if(predicateOrNil != nil)
        {
            [request setPredicate:predicateOrNil];
        }
        
        // Edit the sort key as appropriate.
        NSMutableArray *sortDescriptors = [[NSMutableArray alloc]init];
        
        if(ascSortStringOrNil != nil)
        {
            for (NSString *asc in ascSortStringOrNil)
            {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:asc ascending:NO] ;
                [sortDescriptors addObject:sortDescriptor];
            }
        }
        if(descSortStringOrNil != nil)
        {
            for (NSString *desc in descSortStringOrNil)
            {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:desc ascending:YES] ;
                [sortDescriptors addObject:sortDescriptor];
            }
        }
        [request setSortDescriptors:sortDescriptors];
        
        
        
        [privateContext performBlock:^{
            
            //NSError *fetcherror;
            
            
            
            mutableFetchResults = [privateContext  executeFetchRequest:request error:error] ;
            
            
            
            if (mutableFetchResults == nil) {
                // Handle the error.
            }
            
            if (error) {
                
                
            }else {
                
                if (mutableFetchResults.count > 0)
                {
                    
                    
                    
                    completionBlock(YES,[mutableFetchResults copy],nil);
                    
                    
                    
                }
                else
                {
                    
                    //   dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    completionBlock(NO,nil,nil);
                    
                    //         });
                    
                    
                }
                
                
            }
            
            
            
            
        }];


    
}


@end
