//
//  GroupListRepository.m
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupListRepository.h"

@implementation GroupListRepository

- (id)init
{
    self = [super init];
    if (self) {
        self.repositoryEntityName=@"GroupList";
        
    }
    return self;
}


-(GroupList *) addNewGroupList:(NSString*)groupID
                                groupName:(NSString*)groupName
                                   groupStartDate:(NSString*)groupStartDate
                        groupEnabled:(NSNumber*)groupEnabled
                               groupMuted:(NSNumber*)groupMuted
                                   groupLastUpdatedDate:(NSString*)groupLastUpdatedDate
                                     groupSequence:(NSNumber*)groupSequence
                                   groupImage:(NSData*)groupImage
                            groupImageUrl:(NSString*)groupImageUrl
                              groupLastMessage:(NSString*)groupLastMessage
                              groupListOwner:(User*)groupListOwner
                            groupListSession:(ChatSession*)groupListSession
                              groupListUsers:(NSArray*)groupListUsers
              AdminPhoneNumber:(NSString*)AdminPhoneNumber
                                      Error:(NSError **)error
{
    
    NSManagedObject *newManagedObject = [self createModelForCurrentEntity:error];
    
    // If appropriate, configure the new managed object.
    if([groupID isEqualToString:@""] || groupID == nil)
    {
    groupID = [self uniqueUUID];
    }
    
    [newManagedObject setValue:[groupID lowercaseString] forKey:@"groupID"];
    [newManagedObject setValue:groupName forKey:@"groupName"];
    [newManagedObject setValue:groupStartDate forKey:@"groupStartDate"];
    [newManagedObject setValue:groupEnabled forKey:@"groupEnabled"];
    
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"groupMuted"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"groupIsBlocked"];
    
    [newManagedObject setValue:groupLastUpdatedDate forKey:@"groupLastUpdatedDate"];
    [newManagedObject setValue:groupSequence forKey:@"groupSequence"];
    [newManagedObject setValue:groupLastMessage forKey:@"groupLastMessage"];
    [newManagedObject setValue:groupImageUrl forKey:@"groupImageUrl"];
    if (groupImage != Nil) {
        [newManagedObject setValue:groupImage forKey:@"groupImage"];
    }
    
    
    [newManagedObject setValue:groupListOwner forKey:@"groupListOwner"];
    
    if (groupListSession != Nil) {
        [newManagedObject setValue:groupListSession forKey:@"GroupListSession"];
    }
    
    [self insertModel:newManagedObject EntityName:self.repositoryEntityName Error:error];
    
    // add list of phone numbers to chat list
    
    for (NSString *phoneNumber in groupListUsers) {
        NSNumber *isadmmin= [NSNumber numberWithBool:FALSE];
        
        if ([phoneNumber isEqualToString:AdminPhoneNumber]) {
            isadmmin= [NSNumber numberWithBool:TRUE];
        }
        
        [self addUserPhoneToGroupList:phoneNumber groupUserEnabled:groupEnabled groupUserJoinDate:groupStartDate phoneGroupList:(GroupList*)newManagedObject groupUserIsAdmin:isadmmin Error:error];
    }
    
    
    
    return (GroupList *)newManagedObject;
    
    
}


-(GroupListUsersPhones *) addUserPhoneToGroupList:(NSString*)groupUserPhone
                                 groupUserEnabled:(NSNumber*)groupUserEnabled
                                groupUserJoinDate:(NSString*)groupUserJoinDate
                                   phoneGroupList:(GroupList*)phoneGroupList
                                            Error:(NSError **)error
{
    
    if (![self isPhoneNumberExistInGroupList:groupUserPhone GroupList:phoneGroupList Error:nil]) {
        NSManagedObject *newManagedObject = [self createModelByEntityName:@"GroupListUsersPhones" Error:error];
        
        // If appropriate, configure the new managed object.
        [newManagedObject setValue:groupUserPhone forKey:@"groupUserPhone"];
        [newManagedObject setValue:groupUserEnabled forKey:@"groupUserEnabled"];
        [newManagedObject setValue:groupUserJoinDate forKey:@"groupUserJoinDate"];
        [newManagedObject setValue:phoneGroupList forKey:@"phoneGroupList"];
        [self insertModel:newManagedObject EntityName:@"GroupListUsersPhones" Error:error];
        
        return (GroupListUsersPhones *)newManagedObject;
        
    }
    else
    {
        NSMutableArray *userdata=[[NSMutableArray alloc]init];
        NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupUserPhone == %@) AND (phoneGroupList == %@) ",groupUserPhone,phoneGroupList];
        userdata= [self getResultsFromEntity:@"GroupListUsersPhones" predicateOrNil:userpredicate Error:error];
        
        if (userdata.count>0) {
            return (GroupListUsersPhones *)userdata[0];
        }
        
        return nil;
        
    }
    
    
    
    return nil;
    
    
}



-(GroupListUsersPhones *) addUserPhoneToGroupList:(NSString*)groupUserPhone
                                groupUserEnabled:(NSNumber*)groupUserEnabled
                               groupUserJoinDate:(NSString*)groupUserJoinDate
                                  phoneGroupList:(GroupList*)phoneGroupList
                                 groupUserIsAdmin:(NSNumber*)groupUserIsAdmin
                                          Error:(NSError **)error
{
    
    if (![self isPhoneNumberExistInGroupList:groupUserPhone GroupList:phoneGroupList Error:nil]) {
        NSManagedObject *newManagedObject = [self createModelByEntityName:@"GroupListUsersPhones" Error:error];
        
        // If appropriate, configure the new managed object.
        [newManagedObject setValue:groupUserPhone forKey:@"groupUserPhone"];
        [newManagedObject setValue:groupUserEnabled forKey:@"groupUserEnabled"];
        [newManagedObject setValue:groupUserJoinDate forKey:@"groupUserJoinDate"];
        [newManagedObject setValue:phoneGroupList forKey:@"phoneGroupList"];
        [newManagedObject setValue:groupUserIsAdmin forKey:@"groupUserIsAdmin"];
        [self insertModel:newManagedObject EntityName:@"GroupListUsersPhones" Error:error];
        
        return (GroupListUsersPhones *)newManagedObject;

    }
    else
    {
        NSMutableArray *userdata=[[NSMutableArray alloc]init];
        NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupUserPhone == %@) AND (phoneGroupList == %@) ",groupUserPhone,phoneGroupList];
        userdata= [self getResultsFromEntity:@"GroupListUsersPhones" predicateOrNil:userpredicate Error:error];
        
        if (userdata.count>0) {
            return (GroupListUsersPhones *)userdata[0];
        }
        
        return nil;

    }
    
    
    
    return nil;
    
    
}

-(void)deleteGroupList:(GroupList *)GroupList Error:(NSError **)error
{
    
    [self deleteModel:GroupList EntityName:self.repositoryEntityName Error:error];
    
}

-(void)updateGroupList:(GroupList *)GroupList Error:(NSError **)error
{
    
    [self updateModel:GroupList EntityName:self.repositoryEntityName Error:error];
    
}

-(void) updateGroupList:(GroupList *)GroupListUpdated
                groupListUsers:(NSArray*)groupListUsers
                         Error:(NSError **)error
{
    
    // add list of phone numbers to chat list
    
    for (NSString *phoneNumber in groupListUsers) {
        
        //check if phone number already exist
        // if not then add
        if ([self isPhoneNumberExistInGroupList:phoneNumber GroupList:GroupListUpdated Error:nil]) {
            // do nothing
        }
        else
        {
            // not exist add
        [self addUserPhoneToGroupList:phoneNumber groupUserEnabled:[NSNumber numberWithBool:YES] groupUserJoinDate:nil phoneGroupList:GroupListUpdated Error:error];
        }
    }
    
    
    
 [self updateModel:GroupListUpdated EntityName:self.repositoryEntityName Error:error];
    
}

-(void)deleteGroupListUsersPhones:(GroupListUsersPhones *)GroupListUsersPhones Error:(NSError **)error
{
    
    [self deleteModel:GroupListUsersPhones EntityName:@"GroupListUsersPhones" Error:error];
    
}

-(void)updateGroupListUsersPhones:(GroupListUsersPhones *)GroupListUsersPhones Error:(NSError **)error
{
    
    [self updateModel:GroupListUsersPhones EntityName:@"GroupListUsersPhones" Error:error];
    
}


-(void) changeGroupListAdmin:(NSString*)useridentity userGroupList:(GroupList*) userGroupList Error:(NSError **)error
{
    for (GroupListUsersPhones * userphone in userGroupList.groupListUsers) {
        
        if ([userphone.groupUserPhone isEqualToString:useridentity]) {
            userphone.groupUserIsAdmin =[NSNumber numberWithBool:TRUE];
        }
        else
        {
           userphone.groupUserIsAdmin =[NSNumber numberWithBool:FALSE];
        }
        
        [self updateGroupListUsersPhones:userphone Error:error];
    }
    
    
    
}

-(NSArray*)getUserGroupList:(User*)User Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupListOwner == %@) ",User];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    
    return userdata;
}

-(GroupList*)getUserGroupListbyGroupID:(NSString*)groupID Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupID == %@) ",groupID];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return (GroupList*)userdata[0];
    }
    
    return nil;
}

-(NSArray*)getUserGroupListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupListOwner == %@) ",User];
  
    NSArray *sortList;
    
    if ([sortOption isEqualToString:sortDefault]) {
        sortList=[[NSArray alloc]initWithObjects:@"groupLastUpdatedDate", nil];
            userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:sortList descSortStringOrNil:Nil Error:error];
    }
    else if ([sortOption isEqualToString:sortAtoZ]) {
        sortList=[[NSArray alloc]initWithObjects:@"groupName", nil];
            userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
    }
    else if ([sortOption isEqualToString:sorttype]) {
        sortList=[[NSArray alloc]initWithObjects:@"groupLastUpdatedDate", nil];
            userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
    }
    
    
    

    
    
    return userdata;
}

-(GroupList*)getGroupListByStartDate:(User*)User StartDate:(NSString*)startDate Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupListOwner == %@) AND (chatStartDate == %@) ",User , startDate];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return (GroupList*)userdata;
    }
    
    return nil;
}


-(NSArray*)getGroupListPhones:(GroupList*)GroupList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(phoneGroupList == %@) ",GroupList];
    userdata= [self getResultsFromEntity:@"GroupListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    
    
    return userdata;
}

/// start here

-(NSArray*)getGroupListUserbyPhones:(NSArray*)phoneNumber Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupUserPhone in %@) ",phoneNumber];
    userdata= [self getResultsFromEntity:@"GroupListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    return userdata;
    
}

-(GroupList*)getGroupListByUserPhones:(NSArray*)phoneNumber Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
     NSString *userPhones=nil;
    
    if (phoneNumber.count >0) {
       
        for (NSString *userPhoneNumber in phoneNumber) {
            userPhones = [NSString stringWithFormat:@"(groupUserPhone == %@)",userPhoneNumber];
            userPhones =[ NSString stringWithFormat:@"%@ AND ",userPhones];
        }
    }
 
   userPhones= [userPhones substringToIndex:[userPhones length] - 4];
    
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"%@ ",userPhones];
    userdata= [self getResultsFromEntity:@"GroupListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    if (userdata.count > 0) {
        return userdata[0];
    }
    
    return nil;
    
}


-(BOOL)isPhoneNumberExistInGroupList:(NSString*)phoneNumber GroupList:(GroupList*)GroupListSelected Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupUserPhone == %@) AND (phoneGroupList == %@) ",phoneNumber,GroupListSelected];
    userdata= [self getResultsFromEntity:@"GroupListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    if (userdata.count>0) {
        return true;
    }
    
    return FALSE;
    
}


-(BOOL)deletePhoneNumberExistInGroupList:(NSString*)phoneNumber GroupList:(GroupList*)GroupListSelected Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(groupUserPhone == %@) AND (phoneGroupList == %@) ",phoneNumber,GroupListSelected];
    userdata= [self getResultsFromEntity:@"GroupListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    if (userdata.count>0) {
        [self deleteGroupListUsersPhones:(GroupListUsersPhones*)userdata[0] Error:error];
        return TRUE;
    }
    
    return FALSE;
    
}

-(GroupList*)getGrouptListByPhoneNumbers:(NSArray*)phone Error:(NSError **)error
{
    
    NSArray *userPhone=[self getGroupListUserbyPhones:phone Error:error];
    
    if (userPhone != nil && userPhone.count > 0 ) {
        return ((GroupListUsersPhones*)[userPhone objectAtIndex:0]).phoneGroupList;
    }
    
    return nil;
}


#pragma -mark chat session

-(NSArray*)getGroupListSessions:(GroupList*)GroupList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionGroupList == %@) ",GroupList];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    
    return userdata;
}

-(NSArray*)getGroupListSessionsWithRecordsNumber:(GroupList*)GroupList RecordsLimit:(NSInteger)recordsLimit Error:(NSError **)error
{
    NSArray *userdata=[[NSArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionGroupList == %@) ",GroupList];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist NumberOfRecords:recordsLimit   Error:error];
    
    
    return userdata;
}

-(NSArray*)getUnreadGroupListSessions:(GroupList*)ChatList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionGroupList == %@) AND (chatSessionRead == 0) AND (chatSessionMyMessage == 0) ",ChatList];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    
    return userdata;
}

-(NSInteger)getAllUnreadGroupListSessions:(GroupList*)ChatList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionRead == 0) AND (chatSessionMyMessage == 0) "];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    

    
    if (userdata.count > 0) {
        NSMutableDictionary * activeList=[[NSMutableDictionary alloc]init];
        
        for (ChatSession * sessionObj in userdata) {
            if (sessionObj.chatSessionGroupList.groupID) {

            if ([activeList valueForKey:sessionObj.chatSessionGroupList.groupID] == NULL) {
                [activeList setValue:sessionObj.chatSessionGroupList.groupID forKey:sessionObj.chatSessionGroupList.groupID];
            }
            }
       
        }
        
        return activeList.count;
    }
    
    return 0;
}

@end
