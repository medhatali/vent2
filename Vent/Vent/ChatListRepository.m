//
//  ChatListRepository.m
//  Vent
//
//  Created by Medhat Ali on 3/26/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatListRepository.h"
#import "ChatSession.h"

@implementation ChatListRepository

- (id)init
{
    self = [super init];
    if (self) {
        self.repositoryEntityName=@"ChatList";
        
    }
    return self;
}


-(ChatList *) addNewChatListWithPhoneNumber:(NSString*)chatStartDate
       chatEnabled:(NSNumber*)chatEnabled
         chatType:(NSString*)chatType
         chatLastUpdatedDate:(NSString*)chatLastUpdatedDate
chatSequence:(NSNumber*)chatSequence
chatMute:(NSNumber*)chatMute
chatID:(NSString*)chatID
chatName:(NSString*)chatName
chatLastMessage:(NSString*)chatLastMessage
chatUserImage:(NSData*)chatUserImage
chatUserImageUrl:(NSString*)chatUserImageUrl
chatFromPhone:(NSString*)chatFromPhone
chatToPhone:(NSString*)chatToPhone
chatIsFavourite:(NSNumber*)chatIsFavourite
        chatListOwner:(User*)chatListOwner
chatListSession:(ChatSession*)chatListSession
             chatListUsers:(NSArray*)chatListUsers
            chatIsSecure:(NSNumber*)chatIsSecure
                   Error:(NSError **)error
{
    
    NSManagedObject *newManagedObject = [self createModelForCurrentEntity:error];
    
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:chatStartDate forKey:@"chatStartDate"];
    [newManagedObject setValue:chatEnabled forKey:@"chatEnabled"];
    [newManagedObject setValue:chatType forKey:@"chatType"];
    [newManagedObject setValue:chatLastUpdatedDate forKey:@"chatLastUpdatedDate"];

     [newManagedObject setValue:chatSequence forKey:@"chatSequence"];
    
     [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"chatMute"];
    [newManagedObject setValue:[NSNumber numberWithBool:NO] forKey:@"chatIsBlocked"];
    
     [newManagedObject setValue:[chatID lowercaseString] forKey:@"chatID"];
     [newManagedObject setValue:chatName forKey:@"chatName"];
     [newManagedObject setValue:chatLastMessage forKey:@"chatLastMessage"];
    if (chatUserImage != Nil) {
         [newManagedObject setValue:chatUserImage forKey:@"chatUserImage"];
    }
    
     [newManagedObject setValue:chatUserImageUrl forKey:@"chatUserImageUrl"];
     [newManagedObject setValue:chatIsFavourite forKey:@"chatIsFavourite"];
    [newManagedObject setValue:chatListOwner forKey:@"chatListOwner"];
    
    [newManagedObject setValue:chatFromPhone forKey:@"chatFromPhone"];
    [newManagedObject setValue:chatToPhone forKey:@"chatToPhone"];
    
    if (chatListSession != Nil) {
        [newManagedObject setValue:chatListSession forKey:@"chatListSession"];
    }
    
    if(chatID == nil || [chatID isEqualToString:@"" ])
    {
        [newManagedObject setValue:[self uniqueUUID] forKey:@"chatID"];
    }
    
    [self insertModel:newManagedObject EntityName:self.repositoryEntityName Error:error];
    
    [newManagedObject setValue:chatIsSecure forKey:@"chatIsSecure"];
    
    // add list of phone numbers to chat list
    
    for (NSString *phoneNumber in chatListUsers) {
        [self addUserPhoneToChatList:phoneNumber chatUserEnabled:chatEnabled chatUserJoinDate:chatStartDate phoneChatList:(ChatList*)newManagedObject Error:error];
    }
    
    
    
    return (ChatList *)newManagedObject;
    
    
}



-(ChatListUsersPhones *) addUserPhoneToChatList:(NSString*)chatUserPhone
                     chatUserEnabled:(NSNumber*)chatUserEnabled
                               chatUserJoinDate:(NSString*)chatUserJoinDate
                            phoneChatList:(ChatList*)phoneChatList
                                 Error:(NSError **)error
{
    
    
    NSManagedObject *newManagedObject = [self createModelByEntityName:@"ChatListUsersPhones" Error:error];
    
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:chatUserPhone forKey:@"chatUserPhone"];
    [newManagedObject setValue:chatUserEnabled forKey:@"chatUserEnabled"];
    [newManagedObject setValue:chatUserJoinDate forKey:@"chatUserJoinDate"];
     [newManagedObject setValue:phoneChatList forKey:@"phoneChatList"];
    
    [self insertModel:newManagedObject EntityName:@"ChatListUsersPhones" Error:error];
    
    return (ChatListUsersPhones *)newManagedObject;
    
    
    
    
}

-(void)deleteChatList:(ChatList *)ChatList Error:(NSError **)error
{
    
    [self deleteModel:ChatList EntityName:self.repositoryEntityName Error:error];
    
}

-(void)updateChatList:(ChatList *)ChatList Error:(NSError **)error
{
    
    [self updateModel:ChatList EntityName:self.repositoryEntityName Error:error];
    
}

-(void)deleteChatListUsersPhones:(ChatListUsersPhones *)ChatListUsersPhones Error:(NSError **)error
{
    
    [self deleteModel:ChatListUsersPhones EntityName:@"ChatListUsersPhones" Error:error];
    
}

-(void)updateChatListUsersPhones:(ChatListUsersPhones *)ChatListUsersPhones Error:(NSError **)error
{
    
    [self updateModel:ChatListUsersPhones EntityName:@"ChatListUsersPhones" Error:error];
    
}


-(NSArray*)getUserChatList:(User*)User Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatListOwner == %@) ",User];
   // userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    NSArray *sortList=[[NSArray alloc]initWithObjects:@"chatLastUpdatedDate", nil];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
    
    
    return userdata;
}


-(NSArray*)getUserChatListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatListOwner == %@) ",User];
    // userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    NSArray *sortList;
    
    if ([sortOption isEqualToString:sortDefault]) {
         sortList=[[NSArray alloc]initWithObjects:@"chatLastUpdatedDate", nil];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:sortList descSortStringOrNil:Nil Error:error];
    }
    else if ([sortOption isEqualToString:sortAtoZ]) {
         sortList=[[NSArray alloc]initWithObjects:@"chatName", nil];
           userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
        
    }
    else if ([sortOption isEqualToString:sorttype]) {
        sortList=[[NSArray alloc]initWithObjects:@"chatType", nil];
           userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
    }
    
    
   
 
    
    
    return userdata;
}


-(NSArray*)getUserFavouriteChatListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatListOwner == %@) AND (chatIsFavourite == 1)",User];
    // userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    NSArray *sortList;
    
    if ([sortOption isEqualToString:sortDefault]) {
        sortList=[[NSArray alloc]initWithObjects:@"chatLastUpdatedDate", nil];
        userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:sortList descSortStringOrNil:Nil Error:error];
    }
    else if ([sortOption isEqualToString:sortAtoZ]) {
        sortList=[[NSArray alloc]initWithObjects:@"chatName", nil];
        userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
        
    }
    else if ([sortOption isEqualToString:sorttype]) {
        sortList=[[NSArray alloc]initWithObjects:@"chatType", nil];
        userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
    }
    
    
    
    
    
    
    return userdata;
}


-(ChatList*)getChatListByStartDate:(User*)User StartDate:(NSString*)startDate Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatListOwner == %@) AND (chatStartDate == %@) ",User , startDate];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return (ChatList*)userdata;
    }
    
    return nil;
}

-(ChatList*)getChatListByID:(NSString*)Id Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatID == %@) " , Id];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return (ChatList*)userdata[0];
    }
    
    return nil;
}

-(NSArray*)getChatListPhones:(ChatList*)ChatList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(phoneChatList == %@) ",ChatList];
 //   NSArray *sortlist=[NSArray alloc]initWithObjects:@"", nil];
   userdata= [self getResultsFromEntity:@"ChatListUsersPhones" predicateOrNil:userpredicate Error:error];
//    userdata= [self getResultsFromEntity:@"ChatListUsersPhones" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];

    
    return userdata;
}


-(ChatListUsersPhones*)getChatListUserbyPhone:(NSString*)phoneNumber Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatUserPhone == %@) ",phoneNumber];
    userdata= [self getResultsFromEntity:@"ChatListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return (ChatListUsersPhones*)[userdata objectAtIndex:0];
    }
    return nil;
    
}



-(ChatList*)getChatListByPhoneNumber:(NSString*)phone Error:(NSError **)error
{
    
    ChatListUsersPhones *userPhone=[self getChatListUserbyPhone:phone Error:error];
    
    if (userPhone != nil) {
        return userPhone.phoneChatList;
    }

    return nil;
}


-(ChatList*)getConversationByPhoneNumber:(NSArray*)phoneNumbers Error:(NSError **)error
{
    
    NSMutableArray *userPhone=[[NSMutableArray alloc]init];
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"$chatListUsers.chatUserPhone == %@) ",@"+201091978894"];
    
    NSString *predictString=@"";

    
    if (phoneNumbers.count > 0) {
        
        for (NSString *phoneNum in phoneNumbers) {
            predictString = [predictString stringByAppendingString:[NSString stringWithFormat:@"(SUBQUERY(chatListUsers, $chatListUsers,  ( $chatListUsers.chatUserPhone == '%@')).@count > 0) AND ",phoneNum]];
        }
        
    }
    
    predictString =[predictString substringToIndex:[predictString length] - 4];
    
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:predictString];
    
    userPhone= [self getResultsFromEntity:@"ChatList" predicateOrNil:userpredicate Error:error];
    
   // NSArray *parentBs = [userPhone valueForKeyPath:@"@distinctUnionOfObjects.chatListUsers"];
    
    if (userPhone.count > 0 ) {

        for (ChatList * seaarchResult in userPhone) {
            if (seaarchResult.chatListUsers.count == phoneNumbers.count) {
                return seaarchResult;
            }
        }
       
        
        
    }
    
    return nil;
}


-(NSArray*)getChatListUserbyPhones:(NSArray*)phoneNumber Error:(NSError **)error
{
    
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatUserPhone in %@) ",phoneNumber];
    userdata= [self getResultsFromEntity:@"ChatListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    return userdata;
    
}

-(ChatList*)getChatListByPhoneNumbers:(NSArray*)phone Error:(NSError **)error
{
    
    NSArray *userPhone=[self getChatListUserbyPhones:phone Error:error];
    
    if (userPhone != nil && userPhone.count >0) {
        return ((ChatListUsersPhones*)[userPhone objectAtIndex:0]).phoneChatList;
    }
    
    return nil;
}


-(NSArray*)getChatListUserbyPhonesStrings:(NSArray*)phoneNumber Error:(NSError **)error
{
    NSString *allPhones=@"";
    for (NSString *phone in phoneNumber) {
       allPhones= [allPhones stringByAppendingString:[NSString stringWithFormat:@"%@" ,phone ]];
        
       allPhones= [allPhones stringByAppendingString:@","];
    }
    
    if (![allPhones isEqualToString:@""]) {
        
        allPhones= [allPhones substringToIndex:[allPhones length]- 1];
       allPhones= [NSString stringWithFormat:@"(chatUserPhone in (%@))",allPhones];
                                            
        
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:allPhones];
    userdata= [self getResultsFromEntity:@"ChatListUsersPhones" predicateOrNil:userpredicate Error:error];
    
    return userdata;
    }
    
    return nil;
    
}

-(ChatList*)getChatListByPhoneNumbersStrings:(NSArray*)phone Error:(NSError **)error
{
    
    NSArray *userPhone=[self getChatListUserbyPhonesStrings:phone Error:error];
    
    if (userPhone != nil && userPhone.count >0) {
        return ((ChatListUsersPhones*)[userPhone objectAtIndex:0]).phoneChatList;
    }
    
    return nil;
}

#pragma -mark chat session

-(NSArray*)getChatListSessions:(ChatList*)ChatList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionChatList == %@) ",ChatList];
    
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    
    return userdata;
}

-(NSArray*)getChatListSessionsWithRecordsNumber:(ChatList*)ChatList RecordsLimit:(NSInteger)recordsLimit Error:(NSError **)error
{
    NSArray *userdata=[[NSArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionChatList == %@) ",ChatList];
    
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist NumberOfRecords:recordsLimit   Error:error];
    
    
    return userdata;
}


-(NSArray*)getUnreadChatListSessions:(ChatList*)ChatList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionChatList == %@) AND (chatSessionRead == 0) AND (chatSessionMyMessage == 0) ",ChatList];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    
    return userdata;
}

-(NSInteger)getAllUnreadChatListSessions:(ChatList*)ChatList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionRead == 0) AND (chatSessionMyMessage == 0) "];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    if (userdata.count > 0) {
        NSMutableDictionary * activeList=[[NSMutableDictionary alloc]init];
        
        for (ChatSession * sessionObj in userdata) {
            if (sessionObj.chatSessionChatList.chatID) {
            if ([activeList valueForKey:sessionObj.chatSessionChatList.chatID] == NULL) {
                [activeList setValue:sessionObj.chatSessionChatList.chatID forKey:sessionObj.chatSessionChatList.chatID];
            }
            }
            
        }
        
        return activeList.count;
    }
    
    return 0;
}


@end
