//
//  GroupProfileTableViewController.m
//  Vent
//
//  Created by Medhat Ali on 8/3/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupProfileTableViewController.h"
#import "ContactsBussinessService.h"
#import "CommonHeader.h"
#import "RegisterBussinessService.h"
#import "UploadDownLoadBussinessService.h"
#import "ContactsBussinessService.h"
#import "User.h"
#import "GlobalHandler.h"
#import "Contacts.h"
#import "ContactsRepository.h"
#import "ChatListRepository.h"
#import "ChatList.h"
#import "GroupList.h"

@interface GroupProfileTableViewController ()

@end

@implementation GroupProfileTableViewController

-(void)viewDidLoad
{
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    self.currentUser=[contactBWS getCurrentUserProfile];
    
    UIImage *userProfile=[UIImage imageWithData:self.selectedGroupList.groupImage];
    if (userProfile != nil) {
        [[self profileImageBtn]  setBackgroundImage:userProfile forState:UIControlStateNormal];
    }
    
    
    [[[self profileImageBtn] layer] setCornerRadius: self.profileImageBtn.frame.size.width / 2 ];
    [[[self profileImageBtn] layer] setBorderWidth: 3.0f];
    [[[self profileImageBtn] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [[self profileImageBtn] setClipsToBounds: YES];
    
    
    self.lblGroupName.text=self.selectedGroupList.groupName;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
//    self.lblDisplayName.text=self.currentUser.userDisplayName;
//    self.lblFirstName.text=self.currentUser.userFirstName;
//    self.lblLastName.text=self.currentUser.userLastName;
//    self.lblStatus.text=self.currentUser.userStatus;
//    
//    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected row %ld in section %ld" , (long)indexPath.row , (long)indexPath.section);
    
    if (indexPath.section == 2) {
        
        if (indexPath.row ==0) {
            [self performSegueWithIdentifier:@"StatusControl" sender:self];
        }
        
        
    }
    
    if (indexPath.section == 0) {
        
        UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)],[NSString stringWithFormat:NSLocalizedString(@"Choose Existing", nil)] , nil];
        
        [_actionSheet showInView:[self view]];
        
    }
    
}

#pragma mark - screen navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //    if([segue.identifier isEqualToString:@"NewBroadcast"])
    //    {
    //        NewBroadCastViewController *dest=(NewBroadCastViewController *)segue.destinationViewController;
    //        dest.inUpdateMode = TRUE;
    //        dest.grouContactList =self.selectedModelToUpdate;
    //
    //    }
    
    //     [self performSegueWithIdentifier:@"NewBroadcast" sender:self];
    
    
}


- (IBAction)changeProfilePicture:(id)sender {
    UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)] ,[NSString stringWithFormat:NSLocalizedString(@"Choose Existing", nil)]  , nil];
    
    [_actionSheet showInView:[self view]];
}

#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UIImagePickerController *_picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = YES;
    
    switch (buttonIndex) {
        case 0:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Device has no camera", nil)]
                                                                     delegate:nil
                                                            cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            else{
                
                _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:_picker animated:YES completion:NULL];
                
            }
            break;
        case 1:
            _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:_picker animated:YES completion:NULL];
            break;
        default:
            break;
    }
}


@end
