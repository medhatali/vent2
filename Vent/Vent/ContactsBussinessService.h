//
//  ContactsBussinessService.h
//  Vent
//
//  Created by Medhat Ali on 3/10/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "BussinessSevericeController.h"
#import "ContactsWebService.h"
#import "LoadingView.h"
#import <AddressBookUI/AddressBookUI.h>

@interface ContactsBussinessService : BussinessSevericeController <UIAlertViewDelegate>

@property (strong, nonatomic) LoadingView *loadingView;

@property (strong, nonatomic) NSMutableArray *phoneContacts;
@property (strong, nonatomic) NSMutableArray *userContacts;
@property (strong, nonatomic) NSMutableArray *userContactsDB;

-(BOOL) isContatcsAccessGrant;
-(BOOL) isContatcsAccessGrantSilent;
-(BOOL) isContatcsAccessGrantInDispath;
-(BOOL) loadContacts:(NSString*)defualtCountryCode ParentView:(UIViewController*)parentView;

-(void)loadPhoneContacts:(NSString*)defualtCountryCode;
-(void)loadPhoneContactsInCallBack:(NSString*)defualtCountryCode addressBook:(ABAddressBookRef) addressBook;

-(void)loadPhoneContactWithLoading:(NSString*)defualtCountryCode;

-(void)loadPhoneContactWithOutLoading:(NSString*)defualtCountryCode;


-(void)compareContactsWithDB:(NSError**)error;

- (NSArray*) loadContactsFromDB;

- (NSArray*) checkAndLoadContactsFromDB:(NSString*)defualtCountryCode;

-(void)checkOnlineContacts:(NSArray*)onlinePhones;
-(void)userISOnline:(NSString*)onlinePhone;
-(void)userISOffline:(NSString*)onlinePhone;

@end
