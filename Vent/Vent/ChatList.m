//
//  ChatList.m
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatList.h"
#import "ChatListUsersPhones.h"
#import "ChatSession.h"
#import "User.h"


@implementation ChatList

@dynamic chatEnabled;
@dynamic chatFromPhone;
@dynamic chatID;
@dynamic chatIsFavourite;
@dynamic chatIsSecure;
@dynamic chatLastMessage;
@dynamic chatLastUpdatedDate;
@dynamic chatMute;
@dynamic chatName;
@dynamic chatSequence;
@dynamic chatStartDate;
@dynamic chatToPhone;
@dynamic chatType;
@dynamic chatUserImage;
@dynamic chatUserImageUrl;
@dynamic chatIsBlocked;
@dynamic chatListOwner;
@dynamic chatListSession;
@dynamic chatListUsers;

@end
