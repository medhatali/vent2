//
//  ResultsSearchTableViewControllerDelegate.h
//  Vent
//
//  Created by Sameh Farouk on 3/30/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#ifndef Vent_ResultsSearchTableViewControllerDelegate_h
#define Vent_ResultsSearchTableViewControllerDelegate_h


@protocol ResultsSearchTableViewControllerDelegate <UITableViewDelegate>

- (UITableViewCell*)resultsCellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
#endif
