//
//  ContactsTableViewCell.m
//  Vent
//
//  Created by Sameh Farouk on 3/23/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ContactsTableViewCell.h"
#import "Contacts.h"

@interface ContactsTableViewCell(){
    
}

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;

@end

@implementation ContactsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    // Initialization code
    if (self) {
        // Initialization code
        [[[self profileImageView] layer] setCornerRadius: self.profileImageView.frame.size.width / 1.1];
        [[[self profileImageView] layer] setBorderWidth: 3.0f];
        [[[self profileImageView] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [[self profileImageView] setClipsToBounds: YES];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated

{
    
    [super setSelected:selected animated:animated];
    
    
    
    // Configure the view for the selected state
    
    if (selected) {
        
        self.accessoryType = UITableViewCellAccessoryCheckmark;
        
    } else {
        
        self.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
}

- (void)setProfileImage:(UIImageView *)profileImage{
    _profileImage = profileImage;
    [self setProfileImageView:profileImage];
}

- (void)setName:(NSString *)name{
    _name = name;
    [[self nameLable]setText:_name];
}

- (void)setIsVentUser:(BOOL)isVentUser{
    _isVentUser = isVentUser;
    if (isVentUser) {
        [[[self profileImageView] layer] setBorderColor:[[UIColor blueColor] CGColor]];
    } else {
        [[[self profileImageView] layer] setBorderColor:[[UIColor grayColor] CGColor]];
    }
}

- (void)setContacts:(Contacts *)contacts{
    //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //        NSString *documentsDirectory = [paths objectAtIndex:0];
    //        NSString* path = [documentsDirectory stringByAppendingPathComponent:[contacts imagePath]];
    //        UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    
    //        [self setProfileImage: [[UIImageView alloc] initWithImage:image]];
    [self setName:[contacts contactFullName]];
    [self setIsVentUser: [[contacts contactIsVentUser] boolValue]];
}

@end
