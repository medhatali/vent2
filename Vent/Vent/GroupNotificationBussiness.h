//
//  GroupNotificationBussiness.h
//  Vent
//
//  Created by Medhat Ali on 5/31/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "RegisterBussinessService.h"
#import "GroupDM.h"

@interface GroupNotificationBussiness : BussinessSevericeController


- (void)didGetGroupList:(NSArray*)groupList Error:(NSError**)error;
- (void)didgetGroupInfo:(GroupDM*)GroupDM Error:(NSError**)error;

- (void)changeGroupTitle:(NSString*)groupID Title:(NSString*)title Error:(NSError**)error;
- (void)removeUserFromGroup:(NSString*)useridentify GroupId:(NSString*)groupID Error:(NSError**)error;
- (void)addUserToGroup:(NSString*)useridentify GroupId:(NSString*)groupID Error:(NSError**)error;
- (void)whenILeaveGroup:(NSString*)groupID Error:(NSError**)error;
- (void)changeGroupAdmin:(NSString*)useridentify GroupId:(NSString*)groupID Error:(NSError**)error;



@end
