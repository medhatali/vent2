//
//  AppUtility.m
//  Education
//
//  Created by Medhat Ali on 3/28/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "AppUtility.h"
#import "AppDelegate.h"


@implementation AppUtility

static AppUtility *myInstane = nil;

+ (AppUtility *)sharedUtilityInstance
{
    if (!myInstane) {
        myInstane = [[[self class]allocWithZone:nil]init];
  
        myInstane.infolist=[[NSBundle mainBundle] objectForInfoDictionaryKey:@"App Configuration"];
        
        NSString *envKey =( NSString *)[myInstane getAppConfigInfoByKey:@"Environment" SecondKeyorNil:Nil];
        [myInstane loadEnvironmentsParamters:envKey];
        
    }
    return myInstane;
}


-(id)getAppConfigInfoByKey:(NSString*)key SecondKeyorNil:(NSString*)secondKey
{
    if (secondKey == Nil) {
    return [self.infolist objectForKey:key];
    }
    else
    {
        return [[self.infolist objectForKey:key]objectForKey:secondKey];
    }
    
}




-(id)getEnvironmentByKey:(NSString*)key SecondKeyorNil:(NSString*)secondKey
{
    if (secondKey == Nil) {
        return [self.EnvironmentParamters objectForKey:key];
    }
    else
    {
        return [[self.EnvironmentParamters objectForKey:key]objectForKey:secondKey];
    }
    
}


- (NSDictionary*)loadEnvironmentsParamters:(NSString*)envkey
{
    NSArray *envList =(NSArray *)[myInstane getAppConfigInfoByKey:@"EnvironmnetList" SecondKeyorNil:Nil];
    
    for (NSDictionary *EnvData in envList) {
        if ([[EnvData objectForKey:@"EnvName"] isEqualToString:envkey]) {
            myInstane.EnvironmentParamters = EnvData;
            return EnvData;
        }
    }
    
    return nil;
}


-(NSString*)getServiceUrl
{

    return [myInstane getEnvironmentByKey:@"WebServiceUrl" SecondKeyorNil:Nil];
   
}

-(NSString*)getSignalRServiceUrl
{
    
    return [myInstane getEnvironmentByKey:@"SignalRServiceUrl" SecondKeyorNil:Nil];
    
}

-(NSString*)getSignalRPhoneNumber
{
    
    return [myInstane getEnvironmentByKey:@"SignalRPhoneNumber" SecondKeyorNil:Nil];
    
}

-(NSString*)getSignalRDeviceToken
{
    
    return [myInstane getEnvironmentByKey:@"SignalRDeviceToken" SecondKeyorNil:Nil];
    
}


-(NSString*)getSignalRAppHub
{
    
    return [myInstane getEnvironmentByKey:@"SignalRAppHub" SecondKeyorNil:Nil];
    
}

-(NSArray*)getTopMenuList
{
    
    return [myInstane getEnvironmentByKey:@"TopMenuList" SecondKeyorNil:Nil];
    
}


-(NSArray*)getMediaDirectory
{
    
    return [myInstane getEnvironmentByKey:@"MediaDirectory" SecondKeyorNil:Nil];
    
}



#pragma -mark theme methods



-(UIColor *)colorBaseTheme
{
    return [self colorFromHexString:[self getEnvironmentByKey:@"ThemeBaseColor" SecondKeyorNil:nil ] ];
}

-(UIColor *)colorBackThemeColor
{
    return [self colorFromHexString:[self getEnvironmentByKey:@"ThemeBackColor" SecondKeyorNil:nil ] ];
}

- (void)applythemeColor:(UIColor *)color toButton:(UIButton *) button
{
    [self addFrameToView:button withColor:color withCornerRadius:0];
    [button setTitleColor:color forState:UIControlStateNormal];
}

- (void)applythemeHexColor:(NSString *)hexString toButton:(UIButton *) button
{
    UIColor *currentColor=[self colorFromHexString:hexString];
    [self addFrameToView:button withColor:currentColor withCornerRadius:0];
    [button setTitleColor:currentColor forState:UIControlStateNormal];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:[[AppUtility sharedUtilityInstance]getEnvironmentByKey:@"ThemeBaseColor" SecondKeyorNil:nil ]];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

-(void) addFrameToView:(UIView *) view withColor:(UIColor *) color withCornerRadius:(int) radius
{
    CALayer *layer = view.layer;
    layer.borderColor = color.CGColor;
    layer.borderWidth = 1;
    layer.cornerRadius = radius;
}

-(void) addFrameToView:(UIView *) view withHexColor:(NSString *)hexcolor withCornerRadius:(int) radius
{
    CALayer *layer = view.layer;
    layer.borderColor = [self colorFromHexString:hexcolor].CGColor;
    layer.borderWidth = 1;
    layer.cornerRadius = radius;
}

-(NSString *) getAppVersionBuild
{
    NSString * buildNUmber=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *envKey =( NSString *)[[AppUtility sharedUtilityInstance] getAppConfigInfoByKey:@"Environment" SecondKeyorNil:Nil];
    
    return [NSString stringWithFormat:@"Version: %@ %@ %@ ", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],buildNUmber,envKey ];
    
    
}


-(LanguageType)getCurrentLanguage
{
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
    if ([language isEqualToString:@"ar"] || [language isEqualToString:@"AR"]) {
        return Arabic;
    }
    else
    {
        return English;
    }
    
    return English;
}


@end
