//
//  ContactUISearchController.m
//  Vent
//
//  Created by Sameh Farouk on 4/5/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ContactUISearchController.h"

@interface ContactUISearchController(){
   
    ContactUISearchBar *_searchBar;
}

@end

@implementation ContactUISearchController

-(instancetype)initWithSearchResultsController:(UIViewController *)searchResultsController{
    self = [super initWithSearchResultsController:searchResultsController];
    _searchBar = [[ContactUISearchBar alloc] initWithSearchBar:[super searchBar]];
    return self;
}

-(UISearchBar*)searchBar{
    return _searchBar;
}


@end
