//
//  PushScreenDM.m
//  Vent
//
//  Created by Medhat Ali on 8/20/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "PushScreenDM.h"

@implementation PushScreenDM

@synthesize screenType = _screenType;
@synthesize listId = _listId;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.screenType = [aDecoder decodeObjectForKey:@"screenType"];
        self.listId = [aDecoder decodeObjectForKey:@"listId"];
        
        }
    return self;
}
                       
                       
- (void)encodeWithCoder:(NSCoder *)aCoder {
                           [aCoder encodeObject:_screenType forKey:@"screenType"];
                           [aCoder encodeObject:_listId forKey:@"listId"];
}
                       
@end
