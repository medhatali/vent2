//
//  Country.h
//  Smiley
//
//  Created by Sameh Farouk on 6/3/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject
@property (strong,nonatomic) NSString* name;
@property (strong,nonatomic) NSString* dial_code;
@property (strong,nonatomic) NSString* code;

-(id)initWithDefaultValue;
-(id)initWithJSONNSData:(NSData*) json;
@end
