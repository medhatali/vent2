//
//  BroadcastList.h
//  Vent
//
//  Created by Medhat Ali on 6/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BroadcastListUserPhones, ChatSession, User;

@interface BroadcastList : NSManagedObject

@property (nonatomic, retain) NSString * broadcastID;
@property (nonatomic, retain) NSString * broadcastName;
@property (nonatomic, retain) NSString * broadcastDate;
@property (nonatomic, retain) User *broadcastListOwner;
@property (nonatomic, retain) NSSet *broadcastListUsers;
@property (nonatomic, retain) NSSet *broadcastChatSession;
@end

@interface BroadcastList (CoreDataGeneratedAccessors)

- (void)addBroadcastListUsersObject:(BroadcastListUserPhones *)value;
- (void)removeBroadcastListUsersObject:(BroadcastListUserPhones *)value;
- (void)addBroadcastListUsers:(NSSet *)values;
- (void)removeBroadcastListUsers:(NSSet *)values;

- (void)addBroadcastChatSessionObject:(ChatSession *)value;
- (void)removeBroadcastChatSessionObject:(ChatSession *)value;
- (void)addBroadcastChatSession:(NSSet *)values;
- (void)removeBroadcastChatSession:(NSSet *)values;

@end
