

#define Dimension_Loading_View_Default_Width 230
#define Dimension_Loading_View_Default_Height 100

#define Dimension_Loading_View_Label_Origin_X 5
#define Dimension_Loading_View_Label_Height 20
#define Dimension_Loading_View_Label_Width 170
#define Dimension_Loading_View_Label_Font_Size 16

#define Dimension_Space_Label_Indicator 10



#define Duration_Show_Animation_First 0.25
#define Duration_Show_Animation_Second 0.1
#define Duration_Show_Animation_Final 0.05

#define Duration_Hide_Animation_First 0.1
#define Duration_Hide_Animation_Final 0.25