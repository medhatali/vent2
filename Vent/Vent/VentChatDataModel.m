//
//  VentChatDataModel.m
//  Vent
//
//  Created by Medhat Ali on 9/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "VentChatDataModel.h"
#import "BaseRepository.h"
#import "NSDate+DateFormater.h"
#import "AppUtility.h"
#import "ChatSessionViewController.h"
#import "ChatSession.h"
#import "MessageNotificationBussiness.h"
#import "ContactsRepository.h"

@implementation VentChatDataModel

- (instancetype)initWithSenderAndReciever:(NSString*)senderId SenderName:(NSString*)senderName SenderImage:(NSData*)senderImage
                                ReciverId:(NSString*)reciverId ReciverName:(NSString*)reciverName ReciverImage:(NSData*)reciverImage
{
    
    self = [super init];
    if (self) {
        
        
        self.locationManager = [[CLLocationManager alloc] init];
        
        self.locationManager.delegate = self;
        if([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
            NSUInteger code = [CLLocationManager authorizationStatus];
            if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
                // choose one request according to your business.
                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                    [self.locationManager requestAlwaysAuthorization];
                } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                    [self.locationManager  requestWhenInUseAuthorization];
                } else {
                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
                }
            }
        }
        [self.locationManager startUpdatingLocation];
        
        self.messages = [NSMutableArray new];
        
        self.senderAvatarID = senderId;
        self.senderAvatarName = senderName;
        
        if (reciverId == nil || [reciverId isEqualToString:@""])
        {
            BaseRepository *repositoryObj=[[BaseRepository alloc]init];
            reciverId = repositoryObj.uniqueUUID;
        }
        
        self.recieverAvatarID = reciverId;
        self.recieverAvatarName =reciverName;
        
        /**
         *  Create avatar images once.
         *
         *  Be sure to create your avatars one time and reuse them for good performance.
         *
         *  If you are not using avatars, ignore this.
         */
        JSQMessagesAvatarImage *senderAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithUserInitials:self.senderAvatarName
                                                                                               backgroundColor:[UIColor colorWithWhite:0.85f alpha:1.0f]
                                                                                                     textColor:[UIColor colorWithWhite:0.60f alpha:1.0f]
                                                                                                          font:[UIFont systemFontOfSize:14.0f]
                                                                                                      diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        
        JSQMessagesAvatarImage *ReciverAvatarImage;
        
        if(reciverImage == nil)
        {
            ReciverAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"DefaultProfile"]
                                                                            diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        }
        else
        {
            
            ReciverAvatarImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageWithData:reciverImage]
                                                                            diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        }
        
        
        self.avatars = @{  self.senderAvatarID : senderAvatarImage,
                           self.recieverAvatarID : ReciverAvatarImage};
        
        self.users = @{ self.senderAvatarID : self.senderAvatarName,
                        self.recieverAvatarID : self.recieverAvatarName };
        
        
        
        /**
         *  Create message bubble images objects.
         *
         *  Be sure to create your bubble images one time and reuse them for good performance.
         *
         */
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleBlueColor]];
    }
    
    return self;
    
}


- (BOOL)ConvertChatSessionToModel:(NSArray*)chatsession
{
    
    BOOL isSenderMe=FALSE;
    self.messages =[[NSMutableArray alloc]init];

    MessageNotificationBussiness * messageBussiness=[[MessageNotificationBussiness alloc]init];
    NSString * dreaddate=[[NSDate alloc] getServerDateString:[NSDate date]];
    
    for (ChatSession* chatEntry in chatsession) {
        
        isSenderMe=FALSE;
        
        NSDate *date = [[NSDate alloc] getDateFromServerString:chatEntry.chatSessionDate];
        
        if (date == nil) {
            date =[[NSDate alloc] getDateFromString:chatEntry.chatSessionDate];
        }
        
        if ([chatEntry.chatSessionMyMessage isEqualToNumber:[NSNumber numberWithBool:TRUE]]) {
            
            // chat message send by me
            isSenderMe = TRUE;
        }
        
        
        self.senderAvatarName =chatEntry.chatSessionFrom;
        
     
        
        if ([chatEntry.chatSessionType isEqualToString:@"1"] ) {
            
            if (isSenderMe) {
                [self addTextMessageFromMe:chatEntry.chatSessionMessage Date:date MsgId:chatEntry.chatSessionID];
            }
            else
            {
                [self addTextMessageFromServer:chatEntry.chatSessionMessage Date:date MsgId:chatEntry.chatSessionID SenderName:chatEntry.chatSessionFrom];
                
            }
            
        }
        else  if ([chatEntry.chatSessionType isEqualToString:@"3"] ) {
            
            //image
            
            if (isSenderMe) {
                [self addPhotoMediaMessageFromMe:chatEntry.chatSessionMessage  MsgId:chatEntry.chatSessionID];
            }
            else
            {
                [self addPhotoMediaMessageFromServer:chatEntry.chatSessionMessage  MsgId:chatEntry.chatSessionID SenderName:chatEntry.chatSessionFrom];
                
            }
            
            
        }
        else  if ([chatEntry.chatSessionType isEqualToString:@"4"] ) {
            
            
            // video
            
            if (isSenderMe) {
                [self addVideoMediaMessageFromMe:chatEntry.chatSessionMessage MsgId:chatEntry.chatSessionID];
            }
            else
            {
                [self addVideoMediaMessageFromServer:chatEntry.chatSessionMessage MsgId:chatEntry.chatSessionID SenderName:chatEntry.chatSessionFrom];
                
            }
            
        }
        
        else  if ([chatEntry.chatSessionType isEqualToString:@"5"] ) {
            
            // location
            NSArray *locationArray=[ chatEntry.chatSessionMessage componentsSeparatedByString:@","];
            
            
            
            if (isSenderMe) {
               [self addLocationMediaMessageFromMe:[locationArray[0] doubleValue] Longitude:[locationArray[1] doubleValue]  MsgId:chatEntry.chatSessionID];
                
                
            }
            
            
            else
            {
                [self addLocationMediaMessageFromServer:[locationArray[0] doubleValue] Longitude:[locationArray[1] doubleValue]  MsgId:chatEntry.chatSessionID SenderName:chatEntry.chatSessionFrom];
            }
            
        }
        
        else if ([chatEntry.chatSessionType isEqualToString:@"6"] )
        {
            // vacard
            
            if (isSenderMe) {
                [self addVcardMediaMessageFromMe:[UIImage imageNamed:@"DefaultProfile"] FullName:@"First Name" VCard:chatEntry.chatSessionMessage  MsgId:chatEntry.chatSessionID];
            }
            else
            {
                [self addVcardMediaMessageFromServer:[UIImage imageNamed:@"DefaultProfile"] FullName:@"First Name" VCard:chatEntry.chatSessionMessage  MsgId:chatEntry.chatSessionID SenderName:chatEntry.chatSessionFrom];
            }
            
        }
        
        NSError * error;
 
        if (!isSenderMe) {
            if ([chatEntry.chatSessionRead isEqualToNumber:[NSNumber numberWithBool:FALSE]]) {
                
                [messageBussiness didRecieveSignalRSignalRMarkAsRead:chatEntry.chatSessionID OnMessageTime:dreaddate IsSent:YES Error:&error];
                [GlobalHandler markMessageAsRead:chatEntry.chatSessionID];
            }
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    return TRUE;
    
}



#pragma mark messages to add with types from Me

- (void)addTextMessageFromMe:(NSString*)text Date:(NSDate*)date MsgId:(NSString*)msgId
{
    JSQMessage * message = [JSQMessage messageWithSenderId:self.senderAvatarID
                                            displayName:self.senderAvatarName
                                                   date:date
                                                      text:text];
    message.messageId =msgId;
     [self.messages addObject:message];
}


- (void)addPhotoMediaMessageFromMe:(NSString*)imageLocalPath MsgId:(NSString*)msgId
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullpath;
    fullpath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", [[AppUtility sharedUtilityInstance]getMediaDirectory],imageLocalPath]];
    //   fullpath = [fullpath stringByAppendingPathComponent:selectedSession.chatSessionMessage];
    //                NSURL* imageURL =[NSURL fileURLWithPath:fullpath];
    //    vedioURL =[NSURL fileURLWithPath:selectedSession.chatSessionMessage];
    
    UIImage *image=[UIImage imageWithContentsOfFile:fullpath];
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
    
     JSQMessage * message = [JSQMessage messageWithSenderId:self.senderAvatarID
                                            displayName:self.senderAvatarName
                                                  media:photoItem
                         ];
    
    message.messageId =msgId;
     [self.messages addObject:message];
    
}


- (void)addVideoMediaMessageFromMe:(NSString*)videoLocalPath MsgId:(NSString*)msgId
{
    
    NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", videoLocalPath]];
    
    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
    JSQMessage * message = [JSQMessage messageWithSenderId:self.senderAvatarID
                                            displayName:self.senderAvatarName
                                                  media:videoItem];
    message.messageId =msgId;
     [self.messages addObject:message];
}

- (BOOL)addLocationMediaMessageFromMe:(double)latitude Longitude:(double)longitude MsgId:(NSString*)msgId
{

    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
    
    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

    JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];

    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{
        
        ChatSessionViewController *chatVC=(ChatSessionViewController *)self.parentVC;

        [chatVC.collectionView reloadData];
       
        
        
    }];
    
    
    
   JSQMessage * message= [JSQMessage messageWithSenderId:self.senderAvatarID
                                            displayName:self.senderAvatarName
                                                  media:locationItem];
    
    message.messageId =msgId;
    [self.messages addObject:message];
         return TRUE;
        
    }
    else
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            UIAlertView *locationDisabled = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Warning", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"Location services are disabled.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:nil];
            
            [locationDisabled show];
        });
        
      
         return FALSE;
    }
    
    return FALSE;
}

- (void)addVcardMediaMessageFromMe:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard MsgId:(NSString*)msgId
{
    UIImage *tempImage=image;
    if (tempImage == nil) {
        tempImage =[UIImage imageNamed:@"DefaultProfile"];
    }
    
    JSQVCardMediaItem *vcardItem = [[JSQVCardMediaItem alloc] initWithImage:tempImage FullName:fullName VCard:vcard];
    
    JSQMessage * message= [JSQMessage messageWithSenderId:self.senderAvatarID
                                              displayName:self.senderAvatarName
                                                    media:vcardItem];
    
    message.messageId =msgId;
    [self.messages addObject:message];
    
}


#pragma mark messages to add with types from Server

- (void)addTextMessageFromServer:(NSString*)text Date:(NSDate*)date MsgId:(NSString*)msgId
{
    JSQMessage * message = [JSQMessage messageWithSenderId:self.recieverAvatarID
                                               displayName:self.recieverAvatarName
                                                      date:date
                                                      text:text];
    message.messageId =msgId;
    [self.messages addObject:message];
}


- (void)addPhotoMediaMessageFromServer:(NSString*)imageLocalPath MsgId:(NSString*)msgId
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullpath;
    fullpath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", [[AppUtility sharedUtilityInstance]getMediaDirectory],imageLocalPath]];
    //   fullpath = [fullpath stringByAppendingPathComponent:selectedSession.chatSessionMessage];
    //                NSURL* imageURL =[NSURL fileURLWithPath:fullpath];
    //    vedioURL =[NSURL fileURLWithPath:selectedSession.chatSessionMessage];
    
    UIImage *image=[UIImage imageWithContentsOfFile:fullpath];
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
    
    JSQMessage * message = [JSQMessage messageWithSenderId:self.recieverAvatarID
                                               displayName:self.recieverAvatarName
                                                     media:photoItem
                            ];
    
    message.messageId =msgId;
    [self.messages addObject:message];
    
}


- (void)addVideoMediaMessageFromServer:(NSString*)videoLocalPath MsgId:(NSString*)msgId
{
    
    NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", videoLocalPath]];
    
    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
    JSQMessage * message = [JSQMessage messageWithSenderId:self.recieverAvatarID
                                               displayName:self.recieverAvatarName
                                                     media:videoItem];
    message.messageId =msgId;
    [self.messages addObject:message];
}

- (void)addLocationMediaMessageFromServer:(double)latitude Longitude:(double)longitude MsgId:(NSString*)msgId
{
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        
    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
    
 
        
    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{
        
        ChatSessionViewController *chatVC=(ChatSessionViewController *)self.parentVC;
        
        [chatVC.collectionView reloadData];
    }];
    
    
    
    JSQMessage * message= [JSQMessage messageWithSenderId:self.recieverAvatarID
                                              displayName:self.recieverAvatarName
                                                    media:locationItem];
    message.messageId =msgId;
    [self.messages addObject:message];
    
    }
    else
    {
       // [[[UIAlertView alloc] initWithTitle:@"Warning." message:@"Location services are disabled."
       //                            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    
}

- (void)addVcardMediaMessageFromServer:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard MsgId:(NSString*)msgId
{
    UIImage *tempImage=image;
    if (tempImage == nil) {
        tempImage =[UIImage imageNamed:@"DefaultProfile"];
    }
    
    JSQVCardMediaItem *vcardItem = [[JSQVCardMediaItem alloc] initWithImage:tempImage FullName:fullName VCard:vcard];
    
    JSQMessage * message= [JSQMessage messageWithSenderId:self.recieverAvatarID
                                              displayName:self.recieverAvatarName
                                                    media:vcardItem];
    message.messageId =msgId;
    [self.messages addObject:message];
    
}


- (void)addTextMessageFromServer:(NSString*)text Date:(NSDate*)date MsgId:(NSString*)msgId SenderName:(NSString*)sendName
{
    JSQMessage * message = [JSQMessage messageWithSenderId:self.recieverAvatarID
                                               displayName:sendName
                                                      date:date
                                                      text:text];
    message.messageId =msgId;
    [self.messages addObject:message];
}


- (void)addPhotoMediaMessageFromServer:(NSString*)imageLocalPath MsgId:(NSString*)msgId SenderName:(NSString*)sendName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullpath;
    fullpath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", [[AppUtility sharedUtilityInstance]getMediaDirectory],imageLocalPath]];
    //   fullpath = [fullpath stringByAppendingPathComponent:selectedSession.chatSessionMessage];
    //                NSURL* imageURL =[NSURL fileURLWithPath:fullpath];
    //    vedioURL =[NSURL fileURLWithPath:selectedSession.chatSessionMessage];
    
    UIImage *image=[UIImage imageWithContentsOfFile:fullpath];
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
    
    JSQMessage * message = [JSQMessage messageWithSenderId:self.recieverAvatarID
                                               displayName:sendName
                                                     media:photoItem
                            ];
    
    message.messageId =msgId;
    [self.messages addObject:message];
    
}


- (void)addVideoMediaMessageFromServer:(NSString*)videoLocalPath MsgId:(NSString*)msgId SenderName:(NSString*)sendName
{
    
    NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", videoLocalPath]];
    
    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
    JSQMessage * message = [JSQMessage messageWithSenderId:self.recieverAvatarID
                                               displayName:sendName
                                                     media:videoItem];
    message.messageId =msgId;
    [self.messages addObject:message];
}

- (void)addLocationMediaMessageFromServer:(double)latitude Longitude:(double)longitude MsgId:(NSString*)msgId SenderName:(NSString*)sendName
{
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        
        CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        
        JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
        
        
        
        [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{
            
            ChatSessionViewController *chatVC=(ChatSessionViewController *)self.parentVC;
            
            [chatVC.collectionView reloadData];
        }];
        
        
        
        JSQMessage * message= [JSQMessage messageWithSenderId:self.recieverAvatarID
                                                  displayName:sendName
                                                        media:locationItem];
        message.messageId =msgId;
        [self.messages addObject:message];
        
    }
    else
    {
        // [[[UIAlertView alloc] initWithTitle:@"Warning." message:@"Location services are disabled."
        //                            delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    
}

- (void)addVcardMediaMessageFromServer:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard MsgId:(NSString*)msgId SenderName:(NSString*)sendName
{
    UIImage *tempImage=image;
    if (tempImage == nil) {
        tempImage =[UIImage imageNamed:@"DefaultProfile"];
    }
    
    JSQVCardMediaItem *vcardItem = [[JSQVCardMediaItem alloc] initWithImage:tempImage FullName:fullName VCard:vcard];
    
    JSQMessage * message= [JSQMessage messageWithSenderId:self.recieverAvatarID
                                              displayName:sendName
                                                    media:vcardItem];
    message.messageId =msgId;
    [self.messages addObject:message];
    
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //silent error
    NSLog(@"didFailWithError: %@", error);
    
//    [[[UIAlertView alloc] initWithTitle:@"Warning." message:@"Location services are disabled."
//                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        self.longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        self.latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        [self.locationManager stopUpdatingLocation];
        
 
        
    }
    
    
    
}

-(void)stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
}

@end
