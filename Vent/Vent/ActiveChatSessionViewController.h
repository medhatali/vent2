//
//  ActiveChatSessionViewController.h
//  Vent
//
//  Created by Medhat Ali on 5/17/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatSessionViewController.h"


@interface ActiveChatSessionViewController : ChatSessionViewController <InputTextToolBarDelegate,OtherToolBarDelegate,HandlerProtocol,SmilySelectionProtocol>






@end
