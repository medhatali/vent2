//
//  GlobalHandler.m
//  Vent
//
//  Created by Medhat Ali on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GlobalHandler.h"
#import <UIKit/UIKit.h>
#import "MessageNotificationBussiness.h"
#import "GroupListRepository.h"
#import "GroupDM.h"
#import "UploadDownLoadBussinessService.h"
#import "ContactsBussinessService.h"
#import "GroupNotificationBussiness.h"
#import "MessageTimeDM.h"
#import "ChatList.h"
#import "GroupList.h"
#import "BroadcastList.h"
#import "NSUserDefaultsExtensions.h"
#import "PushScreenDM.h"
#import "ContactsBussinessService.h"
#import "HDNotificationView.h"
#import "ContactsRepository.h"

#import "JSQSystemSoundPlayer.h"
#import "JSQSystemSoundPlayer+JSQMessages.h"

#import "SmilesCollectionViewDataSource.h"


@interface GlobalHandler()

@end

@implementation GlobalHandler

static GlobalHandler *myInstane = nil;

+ (GlobalHandler *)sharedInstance
{
    if (!myInstane) {
        myInstane = [[[self class]allocWithZone:nil]init];
        myInstane.pushInstance =[[PushNotifiactionHandler alloc]init];
        myInstane.SignalRInstance =[[SignalRHandler alloc] init];
        myInstane.commonBussiness =[[CommonBussinessSevice alloc] init];
        
        
        
        

        
        
        
    }
    return myInstane;
}
-(void)setSignalRInstance:(SignalRHandler*) instance{
    instance.delegate = self;
    _SignalRInstance = instance;
}



#pragma mark reachability
-(void)initReachability
{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter]
                 addObserver:self
                 selector:@selector(checkNetworkStatus:)
                 name:kReachabilityChangedNotification
                 object:nil];
            });
    
}



#pragma mark push handler
-(void)initPush:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
     [[[GlobalHandler sharedInstance] pushInstance] initPushNotification:application didFinishLaunchingWithOptions:launchOptions];
    
    
    if (launchOptions != nil)
    {
        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo != nil)
        {
            NSLog(@"Launched from push notification: %@", userInfo);
            [self handleRemoteNotification:userInfo];
        }
    }

}


-(void) handleRemoteNotification:(NSDictionary *)userInfo
{
    for (id key in userInfo)
    {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    NSLog(@"remote notification: %@",[userInfo description]);
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    
    NSString *alert = [apsInfo objectForKey:@"alert"];
    NSLog(@"Received Push Alert: %@", alert);
    
    // [apsInfo setValue:@"" forKey:@"sound"];
    NSString *sound = [apsInfo objectForKey:@"sound"];
    NSLog(@"Received Push Sound: %@", sound);
    
//    NSString *badge = [apsInfo objectForKey:@"badge"];
//    NSLog(@"Received Push Badge: %@", badge);
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];
    
    
    //        // play custom sound
    //        NSURL *fileURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds/ReceivedMessage.caf"]; // see list below
    //        SystemSoundID soundID;
    //        AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)fileURL,&soundID);
    //        AudioServicesPlaySystemSound(soundID);
    

    
//    UIAlertView *pushAlert=[[UIAlertView alloc]initWithTitle:@"Push Recieved" message:@"push notification recieved" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:@"Ok", nil];
//    [pushAlert show];
//    
    

    NSDictionary *notificationDetail = [apsInfo objectForKey:@"NotificationDetail"];
    NSLog(@"notification Detail  message: %@", notificationDetail);
    
    
    if (notificationDetail != nil ) {
        // message come in the notification
        NSDictionary *notificationDetailDictionary = [apsInfo objectForKey:@"NotificationDetail"];
        
        [self handleNotificationRecievedFromServer:notificationDetailDictionary];
        
        
        
    }
    
    
    
    // [self byPassLoginScreen];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
//    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
//    User* currentUser=[contactBWS getCurrentUserProfile];
    
    if ([self isLoggedIn]) {
        //if user loged in
        // go to notification screen and reoad data
        [self byPassLoginScreen];
        
        //
        
    }
    
}


-(void) handleNotificationRecievedFromServer:(NSDictionary *)notificationDetail
{
    

    
        NSString* NotificationType = [notificationDetail objectForKey:@"NotificationType"];
        self.isAPNRecieved = TRUE;
        if ([NotificationType intValue]== 1) {
            
            //normal message
            NSDictionary *message=[notificationDetail valueForKey:@"InfoDto"];
            [self onSendMessage:message];
            
        }
        else if ([NotificationType intValue]== 2)
        {
            //change group icon
            NSDictionary *message=[notificationDetail valueForKey:@"InfoDto"];
            [self onUpdatedGroupIcon:message];
            
            
        }
        else if ([NotificationType intValue]== 3)
        {
            //change group Name
            NSDictionary *message=[notificationDetail valueForKey:@"InfoDto"];
            [self onUpdatedGroupTitle:message];
            
        }
        else if ([NotificationType intValue]== 4)
        {
            //add group phone number
            NSDictionary *message=[notificationDetail valueForKey:@"InfoDto"];
            [self onUpdatedGroupTitle:message];
            
        }
        else if ([NotificationType intValue]== 8)
        {
            //new group details
            
            
            
        }
        else if ([NotificationType intValue]== 14)
        {
            //message timer
            
            
            
        }
        
        
     [GlobalHandler updateNotificationRecipient:[notificationDetail valueForKey:@"NotificationId"]];

    
    
}


-(void) byPassLoginScreen
{
    //    //     go to main notification screen
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    UINavigationController *navigationController = (UINavigationController *) self.window.rootViewController;
    //    // FirstViewController *firstController = (FirstViewController *) self.window.rootViewController;
    //    LoginViewController * loginVC=(LoginViewController*)[storyboard instantiateViewControllerWithIdentifier:@"Login"];
    //    [navigationController pushViewController:loginVC animated:NO];
    //
    //    MainMenuController * mainVC=(MainMenuController*)[storyboard instantiateViewControllerWithIdentifier:@"MainMenu"];
    //    [navigationController pushViewController:mainVC animated:NO];
    //
    //
    //   self.window.rootViewController = navigationController;
}

-(BOOL) isLoggedIn
{
    //    User *user = [User currentUser];
    //    if (user) {
    //
    //        if (user.userId) {
    //            return YES;
    //        }
    //
    //    }
    return NO;
}

#pragma mark local notification logic

-(void)displayLocalNotification:(NSString*)Title Body:(NSString*)body
{
    /// Show notification view
   // [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
    
    
    [HDNotificationView showNotificationViewWithImage:[UIImage imageNamed:@"DefaultProfile"]
                                                title:Title
                                              message:body
                                           isAutoHide:YES
                                              onTouch:^{
                                                  
                                                  /// On touch handle. You can hide notification view or do something
                                                  [HDNotificationView hideNotificationViewOnComplete:nil];
                                              }];
    
    

}


-(void)displayLocalNotificationGlobal:(NSString*)Title Body:(MessageDM*)body
{
    /// Show notification view
   // [JSQSystemSoundPlayer jsq_playMessageReceivedSound];

    NSString *lastTitle=Title;

        if (body.contentType == 1) {
            // self.lblLastMessage.text = ((ChatSession*)lastchatsession).chatSessionMessage;
            //lastTitle =[[SmilesCollectionViewDataSource sharedInstance] parseStringtoAttributeString:Title FontSize:30];
            
            lastTitle =body.content;
            
        }
        else if (body.contentType == 2) {
           lastTitle =[NSString stringWithFormat:NSLocalizedString(@"Image", nil)];
        }
        else if (body.contentType == 4) {
            lastTitle =[NSString stringWithFormat:NSLocalizedString(@"Vedio", nil)];
        }
        else if (body.contentType == 5) {
            lastTitle =[NSString stringWithFormat:NSLocalizedString(@"Location", nil)];
        }
        else if (body.contentType == 6) {
            lastTitle =[NSString stringWithFormat:NSLocalizedString(@"Contact", nil)];
        }

        
    [HDNotificationView showNotificationViewWithImage:[UIImage imageNamed:@"DefaultProfile"]
                                                title:Title
                                              message:lastTitle
                                           isAutoHide:YES
                                              onTouch:^{
                                                  
                                                  /// On touch handle. You can hide notification view or do something
                                                  [HDNotificationView hideNotificationViewOnComplete:nil];
                                              }];
   

    
}

#pragma mark signal r handler
-(void)initSignalR
{
    [self handleSignalRRecievedEvents];
    
}

-(void)startSignalR{
    
    if (self.ConnectoinStarted == FALSE) {
        self.ConnectoinStarted = TRUE;
        //[[[GlobalHandler sharedInstance] SignalRInstance] stop];
        [[GlobalHandler sharedInstance] setSignalRInstance:[[SignalRHandler alloc] initSignalR]];
        [self initSignalR];
        [[[GlobalHandler sharedInstance] SignalRInstance] start];
    }

}

-(void)stopSignalR{
    self.ConnectoinStarted = FALSE;
    [[[GlobalHandler sharedInstance] SignalRInstance] stop];
}

-(NSString*)getSignalRStatus
{
    if (  [[[[GlobalHandler sharedInstance] SignalRInstance]hubConnection] state ] ==  connected) {
        return @"connected";
    }
    else if (  [[[[GlobalHandler sharedInstance] SignalRInstance]hubConnection] state ] ==  disconnected)
    {
        [[[GlobalHandler sharedInstance] SignalRInstance] start];
        return @"disconnected";
    }
    else if (  [[[[GlobalHandler sharedInstance] SignalRInstance]hubConnection] state ] ==  connecting)
    {
        return @"connecting";
    }
    
    return @"";
    
}

-(void)handleSignalRRecievedEvents
{
    SRHubProxy *proxy = [[[GlobalHandler sharedInstance] SignalRInstance] proxy];
    

    
    [proxy on:@"OnSendMessage" perform:self selector:@selector(onSendMessage:)];
    [proxy on:@"OnGetAllOnlineFriends" perform:self selector:@selector(onGetAllOnlineFriends:)];
    [proxy on:@"OnNotifyFriendsUserJoined" perform:self selector:@selector(onNotifyFriendsUserJoined:)];
    [proxy on:@"OnNotifyFriendsUserLeft" perform:self selector:@selector(onNotifyFriendsUserLeft:)];
    [proxy on:@"OnMarkMessageAsDelivered" perform:self selector:@selector(onMarkMessageAsDelivered:)];
    [proxy on:@"OnMarkMessageAsSent" perform:self selector:@selector(onMarkMessageAsSent:)];
    [proxy on:@"OnMarkMessageAsRead" perform:self selector:@selector(onMarkMessageAsRead:)];
    [proxy on:@"OnGetNewMessages" perform:self selector:@selector(onGetNewMessages:)];
    [proxy on:@"OnUserStartTyping" perform:self selector:@selector(onUserStartTyping:)];
    [proxy on:@"OnLoadAllGroups" perform:self selector:@selector(onCanResend::)];
    [proxy on:@"OnNotifyUserJoinApp" perform:self selector:@selector(onNotifyUserJoinApp:)];
    [proxy on:@"OnStopClient" perform:self selector:@selector(onStopClient)];
    [proxy on:@"OnGetNotifications" perform:self selector:@selector(onGetNotifications:)];
    [proxy on:@"OnEmailVerified" perform:self selector:@selector(onEmailVerified:)];
    [proxy on:@"OnCreatedGroup" perform:self selector:@selector(onCreatedGroup:)];
    [proxy on:@"OnAddedToGroup" perform:self selector:@selector(onAddedToGroup:)];
    [proxy on:@"OnAddUserToGroup" perform:self selector:@selector(onAddUserToGroup::)];
    [proxy on:@"OnNotifyMembersUserJoinedGroup" perform:self selector:@selector(onNotifyMembersUserJoinedGroup::)];
    [proxy on:@"OnNotifyMembersUserLeftGroup" perform:self selector:@selector(onNotifyMembersUserLeftGroup::)];
    [proxy on:@"OnLeavedGroup" perform:self selector:@selector(onLeavedGroup:)];
    [proxy on:@"OnChangeGroupAdmin" perform:self selector:@selector(onChangeGroupAdmin::)];
    [proxy on:@"OnRemovedUserFromGroup" perform:self selector:@selector(onRemovedUserFromGroup::)];
    [proxy on:@"OnUpdatedGroupTitle" perform:self selector:@selector(onUpdatedGroupTitle:)];
    [proxy on:@"OnNotifyMembersTitleChanged" perform:self selector:@selector(onNotifyMembersTitleChanged::)];
    [proxy on:@"OnNotifyMembersIconChanged" perform:self selector:@selector(onNotifyMembersIconChanged::)];
    [proxy on:@"OnUpdatedGroupIcon" perform:self selector:@selector(onUpdatedGroupTitle:)];
    [proxy on:@"OnGetAllGroups" perform:self selector:@selector(onGetAllGroups:)];
    [proxy on:@"OnGetGroup" perform:self selector:@selector(onGetGroup:)];
}




#pragma -mark events handler protocols


-(void) onSendMessage: message{
    
    
    BOOL isBlocked = NO;
     self.isAPNRecieved=FALSE;
    // get message from server
    
    NSLog(@"GlobalHandler recieve from server : OnSendMessage %@",message);
    
    MessageDM *messageModel=[[MessageDM alloc]init];
    messageModel =(MessageDM*) [messageModel getDataModelObjectFromAPIDictionary:message];
    
//    if (self.isAPNRecieved == FALSE) {
//        [self displayLocalNotificationGlobal:@"New Message recieved" Body:messageModel];
//    }
    
   id chatList= [self.commonBussiness recieveMessagesFromServer:[[NSArray alloc]initWithObjects:messageModel, nil] isAPNRecieved:self.isAPNRecieved];
    
    //TODO:Reachabilty check
    [GlobalHandler markMessageAsDelivered:messageModel.messageId];
    
    if (chatList != nil) {
    if ([chatList isKindOfClass:[ChatList class]]) {
        messageModel.chatlistId =((ChatList*)chatList).chatID;
        ChatList * currentlist = (ChatList*)chatList;
        isBlocked = [((ChatList*)chatList).chatIsBlocked boolValue];
        [JSQSystemSoundPlayer jsq_playMessageReceivedSound:currentlist.chatIsBlocked IsMute:currentlist.chatMute];
        
    }
    else if ([chatList isKindOfClass:[GroupList class]])
    {
        messageModel.chatlistId =((GroupList*)chatList).groupID;
        GroupList * currentlist = (GroupList*)chatList;
        isBlocked = [((GroupList*)chatList).groupIsBlocked boolValue];
          [JSQSystemSoundPlayer jsq_playMessageReceivedSound:currentlist.groupIsBlocked IsMute:currentlist.groupMuted];
        
    }
    else if ([chatList isKindOfClass:[BroadcastList class]])
    {
        messageModel.chatlistId =((BroadcastList*)chatList).broadcastID;
         [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
    }

    
    
   
     
    }
   
    if (!isBlocked) {
        if ([_delegate respondsToSelector:@selector(onSendMessage:)])
            [[self delegate] onSendMessage:messageModel];
    }

}

-(void) onGetAllOnlineFriends: onlineFriendsIdentifiers{
    NSLog(@"GlobalHandler: OnGetAllOnlineFriends %@",onlineFriendsIdentifiers);
    
    // recieve list of phone numbers to online users ,array of strings
    //update contacts with people who online then update the lists with users online, otheres ofline
    // by adding green circle to the user profile image
    // called when join the system
    
    ContactsBussinessService *contactBussiness=[[ContactsBussinessService alloc]init];
    [contactBussiness checkOnlineContacts:onlineFriendsIdentifiers];
    
    if([_delegate respondsToSelector:@selector(onGetAllOnlineFriends:)])
        [[self delegate] onGetAllOnlineFriends:onlineFriendsIdentifiers];
}

-(void) onNotifyFriendsUserJoined: userIdentifier{
    NSLog(@"GlobalHandler: OnNotifyFriendsUserJoined %@", userIdentifier);
    
    // notify user in contacts is online , while working in the app
    // the same as above , but for only one user
    ContactsBussinessService *contactBussiness=[[ContactsBussinessService alloc]init];
    [contactBussiness checkOnlineContacts:[[NSArray alloc]initWithObjects:userIdentifier, nil]];
    
    if([_delegate respondsToSelector:@selector(onNotifyFriendsUserJoined:)])
        [[self delegate] onNotifyFriendsUserJoined:userIdentifier];
}

-(void) onNotifyFriendsUserLeft: userIdentifier{
    NSLog(@"GlobalHandler: OnNotifyFriendsUserLeft %@",userIdentifier);
    
    // user disconnected from the system
    ContactsBussinessService *contactBussiness=[[ContactsBussinessService alloc]init];
    [contactBussiness userISOffline:userIdentifier];
    
    if([_delegate respondsToSelector:@selector(onNotifyFriendsUserLeft:)])
        [[self delegate] onNotifyFriendsUserLeft:userIdentifier];
}

-(void) onMarkMessageAsDelivered: messageTimeDM{
    NSLog(@"GlobalHandler: OnMarkMessageAsDelivered %@",messageTimeDM);
    MessageTimeDM * messageTimeModel = [MessageTimeDM new];
    messageTimeModel = (MessageTimeDM*)[messageTimeModel getDataModelObjectFromAPIDictionary:messageTimeDM];

        [self.commonBussiness markMessageAsDelivered:[[NSArray alloc]initWithObjects:messageTimeModel, nil]];
    
    if([_delegate respondsToSelector:@selector(onMarkMessageAsDelivered:)])
    [[self delegate] onMarkMessageAsDelivered:messageTimeDM];

    
}

-(void) onMarkMessageAsSent: messageTimeDM{
    NSLog(@"GlobalHandler: OnMarkMessageAsSent %@",messageTimeDM);
    MessageTimeDM * messageTimeModel = [MessageTimeDM new];
    messageTimeModel = (MessageTimeDM*)[messageTimeModel getDataModelObjectFromAPIDictionary:messageTimeDM];
    
    [self.commonBussiness markMessageAsSent:[[NSArray alloc]initWithObjects:messageTimeModel, nil]];
    
    
    if([_delegate respondsToSelector:@selector(onMarkMessageAsSent:)])
        [[self delegate] onMarkMessageAsSent:messageTimeDM];
    
}

-(void) onMarkMessageAsRead: messageTimeDM{
    NSLog(@"GlobalHandler: OnMarkMessageAsRead %@",messageTimeDM);
    
    MessageTimeDM * messageTimeModel = [MessageTimeDM new];
    messageTimeModel = (MessageTimeDM*)[messageTimeModel getDataModelObjectFromAPIDictionary:messageTimeDM];
    
    [self.commonBussiness markMessageAsRead:[[NSArray alloc]initWithObjects:messageTimeModel, nil]];
    
    
    if([_delegate respondsToSelector:@selector(onMarkMessageAsRead:)])
        [[self delegate] onMarkMessageAsRead:messageTimeModel];
    
}

-(void) onGetNewMessages: messages{
    NSLog(@"GlobalHandler: OnGetNewMessages %@",messages);
    
    // the same as on get message but with array of messages
    
    for (NSDictionary *message in messages) {

        [self onSendMessage:message];
    }
    
//    if([_delegate respondsToSelector:@selector(onGetNewMessages:)])
//        [[self delegate] onGetNewMessages:messages];
    
}

-(void) onUserStartTyping: userIdentifier{
    NSLog(@"GlobalHandler: OnUserStartTyping %@",userIdentifier);
    // on user text box chaneg  this method called to indicate user is typing
    
    
    if([_delegate respondsToSelector:@selector(onUserStartTyping:)])
        [[self delegate] onUserStartTyping:userIdentifier];
}

-(void) onCanResend: canResend : messageId{
    NSLog(@"GlobalHandler: OnCanResend %@ %@",canResend, messageId);
    if([_delegate respondsToSelector:@selector(onCanResend::)])
        [[self delegate]onCanResend:canResend :messageId];
}

-(void) onLoadAllGroups: groups{
    NSLog(@"GlobalHandler: OnLoadAllGroups %@",groups);
    // one you connect to server , send array of string for groups ids
    // if not exist , call methos with group id to get group info
    
    if([_delegate respondsToSelector:@selector(onLoadAllGroups:)])
        [[self delegate] onLoadAllGroups : groups];
}

-(void) onNotifyUserJoinApp: userIdentifier{
    NSLog(@"GlobalHandler: OnNotifyUserJoinApp %@",userIdentifier);
    
    // called when new of your friend joined the app (registerd)
  
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    Contacts *userjoined=[contactRepo getUserContactByPhoneNumber:userIdentifier contactUser:currentUser Error:nil];
    
    if (self.isAPNRecieved == FALSE) {
        if ([userjoined.contactFirstName isEqualToString:@""] ) {
             [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
           [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"New User Joined Vent", nil)] Body:[NSString stringWithFormat:@"%@ %@",userIdentifier ,[NSString stringWithFormat:NSLocalizedString(@"joined Vent", nil)]]];
        }
        else
        {
             [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
        [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"New User Joined Vent", nil)] Body:[NSString stringWithFormat:@"%@ %@",userjoined.contactFirstName,[NSString stringWithFormat:NSLocalizedString(@"joined Vent", nil)]]];
        }
    }

    
    // convert user in contact as vent user
    userjoined.contactIsVentUser = [NSNumber numberWithBool:YES];
    [contactRepo updateContact:userjoined Error:nil];
    
    if([_delegate respondsToSelector:@selector(onNotifyUserJoinApp:)])
        [[self delegate] onNotifyUserJoinApp : userIdentifier];
}

-(void) onStopClient{
    NSLog(@"GlobalHandler: OnStopClient");
    [[[GlobalHandler sharedInstance] SignalRInstance] stop];
    if([_delegate respondsToSelector:@selector(onStopClient)])
        [[self delegate] onStopClient];
}

-(void) onGetNotifications: notifications{
    NSLog(@"GlobalHandler: OnGetNotifications %@",notifications);
    
    //like messge , and like push notifications
    // called once you connect
    
    for (NSDictionary *notificationDetail in notifications) {
        if (notificationDetail != nil ) {
            // message come in the notification
            // NSDictionary *notificationDetailDictionary = [apsInfo objectForKey:@"NotificationDetail"];
            
            [self handleNotificationRecievedFromServer:notificationDetail];
            
            
        }

    }
    
    if([_delegate respondsToSelector:@selector(onGetNotifications:)])
        [[self delegate] onGetNotifications:notifications];
}

-(void) onEmailVerified: email{
    NSLog(@"GlobalHandler: OnEmailVerified %@",email);
    
    // once the email verified
    // add another field in profile screen for email , once profile updated woith email
    // and user open the link send by mail , signal r send verified signal to verify email
    // and user can use the pin functionality
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    currentUser.userEmailVerified = [NSNumber numberWithBool:TRUE];
    UserRepository *repo=[[UserRepository alloc]init];
    [repo updateUser:currentUser Error:nil];
    
    if([_delegate respondsToSelector:@selector(onEmailVerified:)])
        [[self delegate] onEmailVerified:email];
}

-(void) onCreatedGroup: groupId{
    NSLog(@"GlobalHandler: OnCreatedGroup %@",groupId);
    
    // group created , not needed yet
    
    if([_delegate respondsToSelector:@selector(onCreatedGroup:)])
        [[self delegate] onCreatedGroup:groupId];
}

-(void) onAddedToGroup: group{
    NSLog(@"GlobalHandler: OnAddedToGroup %@",group);
    NSError *error;
    
    GroupDM *groupDM = (GroupDM*)[GroupDM getDataModelObjectFromAPIDictionary:(NSDictionary*)group];
    GroupListRepository* grouplistRepo=[[GroupListRepository alloc]init];
    NSString *currentdate=[NSString stringWithFormat:@"%@" ,[NSDate date]];
    
    UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
    NSData * imageData =nil;
    
    if (![[groupDM GroupIconPath] isKindOfClass:[NSNull class]]) {
        imageData = [fileServiceObject downLoadFile:[groupDM GroupIconPath] Error:&error];
    }
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    NSArray *phonelist=[[NSArray alloc]initWithArray:[groupDM MembersPhoneNumber]];
    
    [grouplistRepo addNewGroupList:[groupDM GroupId] groupName:[groupDM GroupTitle] groupStartDate:currentdate groupEnabled:[NSNumber numberWithBool:YES] groupMuted:[NSNumber numberWithBool:NO] groupLastUpdatedDate:currentdate groupSequence:[NSNumber numberWithInt:0] groupImage:imageData groupImageUrl:@"" groupLastMessage:@"" groupListOwner:currentUser groupListSession:Nil groupListUsers:phonelist AdminPhoneNumber:groupDM.AdminPhoneNumber Error:&error];
    


    if (self.isAPNRecieved == FALSE) {
        
        
      [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
            [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"User Joined a group", nil)] Body:[NSString stringWithFormat:@"%@ %@",[NSString stringWithFormat:NSLocalizedString(@"You joined Group", nil)],[groupDM GroupTitle]]];
        
    }



    
    
    if([_delegate respondsToSelector:@selector(onAddedToGroup:)])
        [[self delegate] onAddedToGroup:group];
}

-(void) onAddUserToGroup: userIdentifier : groupId{
    NSLog(@"GlobalHandler: OnAddUserToGroup %@, %@",userIdentifier,groupId);
    
    //on user added to group in youd DB
    // if not exist call server to chech this group id
    //
    
    NSError* error;
    GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
    [groupBussiness addUserToGroup:userIdentifier GroupId:groupId Error:&error];
    
    if([_delegate respondsToSelector:@selector(onAddUserToGroup::)])
        [[self delegate] onAddUserToGroup:userIdentifier :groupId];
}

-(void) onNotifyMembersUserJoinedGroup: userIdentifier : groupId{
    NSLog(@"GlobalHandler: OnNotifyMembersUserJoinedGroup %@, %@",userIdentifier,groupId);
    
    //info , send phone number for user added to this group
    // just display no actions needed
    
    NSError* error;
    GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
    [groupBussiness addUserToGroup:userIdentifier GroupId:groupId Error:&error];
    
    GroupListRepository * groupRepo =[[GroupListRepository alloc]init];
    
    
    GroupList *foundGroup= [groupRepo getUserGroupListbyGroupID:groupId Error:&error];
    
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    Contacts *userjoined=[contactRepo getUserContactByPhoneNumber:userIdentifier contactUser:currentUser Error:nil];
    if (self.isAPNRecieved == FALSE) {
        
          [JSQSystemSoundPlayer jsq_playMessageReceivedSound:foundGroup.groupIsBlocked IsMute:foundGroup.groupMuted];
        
        if ([userjoined.contactFirstName isEqualToString:@""] ) {
            [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"User Joined a group", nil)] Body:[NSString stringWithFormat:@"%@ %@",userIdentifier,[NSString stringWithFormat:NSLocalizedString(@"joined Group", nil)]]];
        }
        else
        {
           [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"User Joined a group", nil)] Body:[NSString stringWithFormat:@"%@ %@ %@",userjoined.contactFirstName ,[NSString stringWithFormat:NSLocalizedString(@"joined Group", nil)], foundGroup.groupName]];
        }
        
    }
    
    if([_delegate respondsToSelector:@selector(onNotifyMembersUserJoinedGroup::)])
        [[self delegate] onNotifyMembersUserJoinedGroup:userIdentifier :groupId];
}

-(void) onNotifyMembersUserLeftGroup: userIdentifier : groupId{
    NSLog(@"GlobalHandler: OnNotifyMembersUserLeftGroup %@, %@",userIdentifier , groupId);
    
    // ifo , user leave the group
    
    NSError* error;
    GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
    [groupBussiness removeUserFromGroup:userIdentifier GroupId:groupId Error:&error];
    
    ContactsRepository *contactRepo=[[ContactsRepository alloc]init];
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    Contacts *userjoined=[contactRepo getUserContactByPhoneNumber:userIdentifier contactUser:currentUser Error:nil];
    if (self.isAPNRecieved == FALSE) {
        
         [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
        
        if ([userjoined.contactFirstName isEqualToString:@""] ) {
            [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"User left group", nil)] Body:[NSString stringWithFormat:@"%@ %@",userIdentifier,[NSString stringWithFormat:NSLocalizedString(@"left group", nil)]]];
        }
        else
        {
            [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"User left group", nil)] Body:[NSString stringWithFormat:@"%@ %@",userjoined.contactFirstName,[NSString stringWithFormat:NSLocalizedString(@"left group", nil)]]];
        }
        
       
    }
    
    if([_delegate respondsToSelector:@selector(onNotifyMembersUserLeftGroup::)])
        [[self delegate] onNotifyMembersUserLeftGroup:userIdentifier :groupId];
}

-(void) onLeavedGroup : groupId{
    NSLog(@"GlobalHandler: OnLeavedGroup %@",groupId);
    
    // me left the group , og removed from group
    // remove the group from my list
   
    NSError* error;
    GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
    [groupBussiness whenILeaveGroup:groupId Error:&error];
    
    if (self.isAPNRecieved == FALSE) {
         [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
        [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"User removed from group", nil)] Body:[NSString stringWithFormat:NSLocalizedString(@"you removed from group", nil)] ];
    }
    
    if([_delegate respondsToSelector:@selector(onLeavedGroup:)])
        [[self delegate] onLeavedGroup:groupId];
}

-(void) onChangeGroupAdmin: userIdentifier : groupId{
    NSLog(@"GlobalHandler: OnChangeGroupAdmin %@, %@",userIdentifier,groupId);
    
    // on change admine for group
    //
    
    NSError* error;
    GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
    [groupBussiness changeGroupAdmin:userIdentifier GroupId:groupId Error:&error];
    
    
    if([_delegate respondsToSelector:@selector(onChangeGroupAdmin::)])
        [[self delegate] onChangeGroupAdmin:userIdentifier :groupId];
}

-(void) onRemovedUserFromGroup: userIdentifier : groupId{
    NSLog(@"GlobalHandler: OnRemovedUserFromGroup %@, %@",userIdentifier,groupId);
    
    // remove user phone from group list
    NSError* error;
    GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
    [groupBussiness removeUserFromGroup:userIdentifier GroupId:groupId Error:&error];
    
    if (self.isAPNRecieved == FALSE) {
         [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
        [self displayLocalNotification:[NSString stringWithFormat:NSLocalizedString(@"User removed from group", nil)] Body:[NSString stringWithFormat:NSLocalizedString(@"you removed from group", nil)]];
    }
    
    if([_delegate respondsToSelector:@selector(onRemovedUserFromGroup::)])
        [[self delegate] onRemovedUserFromGroup:userIdentifier :groupId];
}

-(void) onUpdatedGroupTitle:groupId{
    NSLog(@"GlobalHandler: OnUpdatedGroupTitle %@",groupId);
    
    // update group title
    

    
    if([_delegate respondsToSelector:@selector(onUpdatedGroupTitle:)])
        [[self delegate] onUpdatedGroupTitle:groupId];
}

-(void) onNotifyMembersTitleChanged: title : groupId{
    NSLog(@"GlobalHandler: OnNotifyMembersTitleChanged %@, %@",title,groupId);
    

    
    
    if([_delegate respondsToSelector:@selector(onNotifyMembersTitleChanged::)])
        [[self delegate] onNotifyMembersTitleChanged:title :groupId];
}

-(void) onNotifyMembersIconChanged: path : groupId{
    NSLog(@"GlobalHandler: OnNotifyMembersIconChanged %@, %@",path,groupId);
    
    // group image change and updtae DB
    
    if([_delegate respondsToSelector:@selector(onNotifyMembersIconChanged::)])
    [[self delegate] onNotifyMembersIconChanged:path :groupId];
}

-(void) onUpdatedGroupIcon: groupId{
    NSLog(@"GlobalHandler: OnUpdatedGroupIcon %@",groupId);
    
    // confirmation for group icon changed
    
    if([_delegate respondsToSelector:@selector(onUpdatedGroupIcon:)])
        [[self delegate] onUpdatedGroupIcon:groupId];
}

-(void) onGetAllGroups : groupIds{
    NSLog(@"GlobalHandler: OnGetAllGroups %@",groupIds);

    // get list of all groups that you are member in
    
    // check if the goup is already exis tin the list or not
    
    
    
    
    NSArray *groupList =[[NSArray alloc]initWithArray:groupIds];

                       
                       NSError* error;
                       GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
                       [groupBussiness didGetGroupList:groupList  Error:&error];
    
                        if([_delegate respondsToSelector:@selector(onGetAllGroups:)])
                                              [[self delegate] onGetAllGroups:groupIds];
}


-(void) onGetGroup : group{
    NSLog(@"GlobalHandler: OnGetGroup %@",group);
    
    // use this as responce to call getgroup signal r to get group info detail
    GroupDM *currentgroup=(GroupDM *)[GroupDM getDataModelObjectFromAPIDictionary:group];
    
    NSError* error;
    GroupNotificationBussiness * groupBussiness=[[GroupNotificationBussiness alloc]init];
    [groupBussiness didgetGroupInfo:currentgroup  Error:&error];
    
    if([_delegate respondsToSelector:@selector(onGetGroup:)])
        [[self delegate] onGetGroup:currentgroup];
    
 
    
}



#pragma -mark invoke SignalR

+(void) sendMessage:(MessageDM*)message{
    

//    NSError* error;
//    MessageNotificationBussiness * messageBussiness=[[MessageNotificationBussiness alloc]init];
//    [messageBussiness didRecieveSignalRMessage:message IsSent:YES  Error:&error];
    
    
    NSArray *args = [[NSArray alloc] initWithObjects:[message createApiDictionaryFromData:YES], nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"SendMessage" withArgs:args];
    
    
}

+(void) getAllOnlineFriends:(NSArray*) onlinefriendsPhoneNumbers{
    NSArray *args = [[NSArray alloc] initWithObjects:onlinefriendsPhoneNumbers, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"GetAllOnlineFriends" withArgs:args];
}

+(void) notifyFriendsUserJoined:(UserDeviceIdentifierDM*) userId{
    NSArray *args = [[NSArray alloc] initWithObjects:userId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"NotifyFriendsUserJoined" withArgs:args];
}

+(void) notifyFriendsUserLeft:(UserDeviceIdentifierDM*) userId{
    NSArray *args = [[NSArray alloc] initWithObjects:userId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"NotifyFriendsUserLeft" withArgs:args];
}

+(void) markMessageAsDelivered:(NSString*) messageId{
    NSArray *args = [[NSArray alloc] initWithObjects:messageId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"MarkMessageAsDelivered" withArgs:args];
}

+(void) markMessageAsRead:(NSString*) messageId{
    NSArray *args = [[NSArray alloc] initWithObjects:messageId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"MarkMessageAsRead" withArgs:args];
}

+(void) getNewMessages:(UserDeviceIdentifierDM*) userId{
    NSArray *args = [[NSArray alloc] initWithObjects:userId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"GetNewMessages" withArgs:args];
}

+(void) userStartTyping:(NSString*) userIdentifier{
    NSArray *args = [[NSArray alloc] initWithObjects:userIdentifier, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"UserStartTyping" withArgs:args];
}

+(void) canResend:(NSString*) messageId{
    NSArray *args = [[NSArray alloc] initWithObjects:messageId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"CanResend" withArgs:args];
}

+(void) getNotifications:(UserDeviceIdentifierDM*) userIdentifier{
    NSArray *args = [[NSArray alloc] initWithObjects:userIdentifier, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"GetNotifications" withArgs:args];
}

+(void) updateNotificationRecipient:(NSString*) notificationId{
    NSArray *args = [[NSArray alloc] initWithObjects:notificationId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"UpdateNotificationRecipient" withArgs:args];
}

+(void) GetMessageState:(NSString*) messageId{
    NSArray *args = [[NSArray alloc] initWithObjects:messageId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"GetMessageState" withArgs:args];
}

# pragma - mark  invoke SignalR Group Management

+(void) CreateGroup:(GroupDM*) group{
    NSArray *args = [[NSArray alloc] initWithObjects:group, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"CreateGroup" withArgs:args];
}

+(void) addedToGroup:(NSString*) groupId{
    NSArray *args = [[NSArray alloc] initWithObjects:groupId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"AddedToGroup" withArgs:args];
}

+(void) addUserToGroup:(NSString*) userPhone WithGroupId: (NSString*) groupId{
    NSArray *args = [[NSArray alloc] initWithObjects:userPhone,groupId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"AddUserToGroup" withArgs:args];
}

+(void) RemoveUserFromGroup:(NSString*) userPhone WithGroupId: (NSString*) groupId{
    NSArray *args = [[NSArray alloc] initWithObjects:userPhone,groupId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"RemoveUserFromGroup" withArgs:args];
}

+(void) leaveGroup:(NSString*) groupId{
    NSArray *args = [[NSArray alloc] initWithObjects:groupId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"LeaveGroup" withArgs:args];
}

+(void) updateGroupTitle:(NSString*) groupId WithTitle: (NSString*) title{
    NSArray *args = [[NSArray alloc] initWithObjects:groupId,title, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"UpdateGroupTitle" withArgs:args];
}

+(void) updateGroupIcon:(NSString*) groupId WithIconPath: (NSString*) path{
    NSArray *args = [[NSArray alloc] initWithObjects:groupId,path, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"UpdateGroupIcon" withArgs:args];
}

+(void) getAllGroups{
    NSArray *args = [[NSArray alloc] initWithObjects:nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"GetAllGroups" withArgs:args];
}

+(void) getGroup:(NSString*) groupId{
    NSArray *args = [[NSArray alloc] initWithObjects:groupId, nil];
    [[[[GlobalHandler sharedInstance] SignalRInstance] proxy] invoke:@"GetGroup" withArgs:args];
}


#pragma -mark SignalR Contctivty Protocol
-(void)started{
    NSLog(@"GlobalHandler: started");
    if([_delegate respondsToSelector:@selector(started)])
        [[self delegate] started];
}
-(void)received:(NSDictionary *) data{
    NSLog(@"GlobalHandler: received:%@",data);
    if([_delegate respondsToSelector:@selector(received:)])
        [[self delegate] received:data];
}
-(void)error:(NSError *)error{
    NSLog(@"GlobalHandler: error:%@",error);
    if([_delegate respondsToSelector:@selector(error:)])
        [[self delegate] error:error];
}
-(void)closed{
    NSLog(@"GlobalHandler: closed");
    if([_delegate respondsToSelector:@selector(closed)])
        [[self delegate] closed];

        [[GlobalHandler sharedInstance] startSignalR];
    
    
}
-(void)reconnecting{
    NSLog(@"GlobalHandler: reconnecting");
    if([_delegate respondsToSelector:@selector(reconnecting)])
        [[self delegate] reconnecting];
}
-(void)reconnected{
    NSLog(@"GlobalHandler: reconnected");
    if([_delegate respondsToSelector:@selector(reconnected)])
        [[self delegate] reconnected];
}
-(void)stateChanged:(connectionState) connectionState{
    NSLog(@"GlobalHandler: stateChanged:%u",connectionState);
    if([_delegate respondsToSelector:@selector(stateChanged:)])
        [[self delegate] stateChanged:connectionState];
}
-(void)connectionSlow{
    NSLog(@"GlobalHandler: connectionSlow");
    if([_delegate respondsToSelector:@selector(connectionSlow)])
        [[self delegate] connectionSlow];
}




-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NetworkStatus internetStatus =appDelegate.reachabilityApp.currentReachabilityStatus;
    
   // NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            
            if([_delegate respondsToSelector:@selector(closed)])
                [[self delegate] closed];
            
//            if([_delegate respondsToSelector:@selector(reconnecting)])
//                [[self delegate] reconnecting];
            
            [[GlobalHandler sharedInstance] stopSignalR];
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            
            if([_delegate respondsToSelector:@selector(started)])
                [[self delegate] started];
            
            //if([_delegate respondsToSelector:@selector(reconnected)])
            //    [[self delegate] reconnected];
            [[GlobalHandler sharedInstance] stopSignalR];
             [[GlobalHandler sharedInstance] startSignalR];
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            
            if([_delegate respondsToSelector:@selector(started)])
                [[self delegate] started];
            
            //if([_delegate respondsToSelector:@selector(reconnected)])
            //    [[self delegate] reconnected];
            [[GlobalHandler sharedInstance] stopSignalR];
             [[GlobalHandler sharedInstance] startSignalR];
            
            break;
        }
    }
}

@end
