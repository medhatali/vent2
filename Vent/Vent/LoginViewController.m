//
//  LoginViewController.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "LoginViewController.h"
#import "CommonHeader.h"
#import "User.h"
#import "UserRepository.h"
#import "ContactsBussinessService.h"
#import "GlobalHandler.h"
#import "AppUtility.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.lblVersion.text =( NSString *)[[AppUtility sharedUtilityInstance] getAppVersionBuild];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popToRootViewControllerAnimated) name:@"popToRoot" object:nil];

    
}


- (void)popToRootViewControllerAnimated
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    

    
}

-(void)viewWillAppear:(BOOL)animated
{
   // [self.navigationController setNavigationBarHidden:YES];
    
    // logic to decide if we are going to secure login or register new screen
    NSError * error;
    NSString *deviceToken= [[NSUserDefaults standardUserDefaults] valueForKey:KVentDeviceToken];
    UserRepository *userRepo =[[UserRepository alloc]init];
    User *currentExistUser=(User *)[userRepo getUserbyUserDeviceUUID:deviceToken Error:&error];
    
    if (currentExistUser == Nil) {
        
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        UIViewController *vc = [sb instantiateInitialViewController];
//        [self.navigationController pushViewController:vc animated:YES];
//        return;
        
        // go to register
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Register" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        //UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"register"];
        
        [self.navigationController pushViewController:vc animated:YES];
        //  [self presentViewController:vc animated:NO completion:NULL];

        
    }
    else if ([currentExistUser.userIsSecureLogin isEqualToNumber:[NSNumber numberWithBool:TRUE]])
    {
        // go to Pin login push segue
        [self performSegueWithIdentifier:@"PinLogin" sender:self];
    }
    else if ([currentExistUser.userIsRegistered isEqualToNumber:[NSNumber numberWithBool:TRUE]])
    {
        // go to main menu
            ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
            User* currentUser=[contactBWS getCurrentUserProfile];
            if (currentUser) {
                    if (currentUser.userIsRegistered) {
                        [[GlobalHandler sharedInstance] startSignalR];
                    }
            }
        
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [sb instantiateInitialViewController];
        
                [self.navigationController pushViewController:vc animated:YES];
            // [self presentViewController:vc animated:YES completion:NULL];
    }
    else
    {
       
        // go to register
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Register" bundle:nil];
        UIViewController *vc = [sb instantiateInitialViewController];
        //UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"register"];
        
        [self.navigationController pushViewController:vc animated:YES];
        //  [self presentViewController:vc animated:NO completion:NULL];
        
    }
    
    
    

    
    

    
}

-(void)viewDidAppear:(BOOL)animated
{
//    // go to register
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Register" bundle:nil];
//    UIViewController *vc = [sb instantiateInitialViewController];
//    
//    [self.navigationController pushViewController:vc animated:YES];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    if([segue.identifier isEqualToString:@"PinLogin"]){
        
//        NoteViewController *dest =(NoteViewController *) segue.destinationViewController;
//        dest.DirectOrderDoneItem =self.selectedItem;
    }
    
}

@end
