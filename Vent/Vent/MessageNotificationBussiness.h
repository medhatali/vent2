//
//  MessageNotificationBussiness.h
//  Vent
//
//  Created by Medhat Ali on 5/17/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "RegisterBussinessService.h"
#import "MessageTimeDM.h"

@interface MessageNotificationBussiness : BussinessSevericeController

- (id)didRecieveSignalRMessage:(DataModelObjectParent*)messageModel IsSent:(Boolean)isSent Error:(NSError**)error;
- (void)didSendSignalRMessage:(DataModelObjectParent*)messageModel currentGroupList:(GroupList*)currentGroupList IsSent:(Boolean)isSent Error:(NSError**)error;
- (void)didSendSignalRMessage:(DataModelObjectParent*)messageModel currentBroadcastList:(BroadcastList*)currentBroadcastList IsSent:(Boolean)isSent Error:(NSError**)error;
- (void)didSendSignalRMessage:(DataModelObjectParent*)messageModel currentChatList:(ChatList*)currentChatList IsSent:(Boolean)isSent Error:(NSError**)error;
- (void)didRecieveSignalRSignalRMarkAsDelivered:(NSString*)messageId OnMessageTime:(NSString*) messageTime IsSent:(Boolean)isSent Error:(NSError**)error;
- (void)didRecieveSignalRSignalRMarkAsSent:(MessageTimeDM*)messageTM   IsSent:(Boolean)isSent Error:(NSError**)error;
- (void)didRecieveSignalRSignalRMarkAsRead:(NSString*)messageId OnMessageTime:(NSString*) messageTime IsSent:(Boolean)isSent Error:(NSError**)error;
@end
