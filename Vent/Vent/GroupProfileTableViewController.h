//
//  GroupProfileTableViewController.h
//  Vent
//
//  Created by Medhat Ali on 8/3/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "LoadingView.h"

@interface GroupProfileTableViewController : UITableViewController<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>


@property (strong, nonatomic) User* currentUser;
@property (strong, nonatomic) IBOutlet UILabel *lblGroupName;

@property (strong, nonatomic) IBOutlet UIButton *profileImageBtn;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt;
@property (strong, nonatomic) IBOutlet UITextField *lblDisplayName;

@property (strong, nonatomic) IBOutlet UITextField *lblFirstName;
@property (strong, nonatomic) IBOutlet UITextField *lblLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;

@property (strong, nonatomic) LoadingView *loadingView;

@property (weak, nonatomic) ChatList *selectedChatList;
@property (weak, nonatomic) GroupList *selectedGroupList;
@property (weak, nonatomic) BroadcastList *selectedBroadcastList;

@property (strong, nonatomic) User *senderContact;
@property (strong, nonatomic) Contacts *reciverContact;
@property (strong, nonatomic) IBOutlet UISwitch *secureChatCheck;

@end
