//
//  RegisterWebService.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "RegisterWebService.h"

@implementation RegisterWebService

- (id)init
{
    self = [super init];
    if (self) {
        self.serviceName = @"/api/Register";
        self.odataserviceName = @"";
        self.odataserviceNameWithFilter = @"";
    }
    return self;
}

- (NSDictionary *)registerUser:(UserDM *)userDM Error:(NSError**)error
{
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",self.serviceName];
    
    NSMutableDictionary *sentDict = [[NSMutableDictionary alloc]initWithDictionary:[userDM createApiDictionaryFromData:YES]];
//    [sentDict setObject:[userDM createApiDictionaryFromData:YES]  forKey:NoNameString];
    
   
    
    NSDictionary *result = [self.caller postData:sentDict ToWebserviceForResource:serviceURL Error:error];
    
    
    return result;
}

- (NSDictionary *)verifyPhoneNumber:(VerifyDM *)verifyDM Error:(NSError**)error
{
    

    NSString *serviceURL = [NSString stringWithFormat:@"%@",@"/api/Verify"];
    
    NSMutableDictionary *sentDict = [[NSMutableDictionary alloc]initWithDictionary:[verifyDM createApiDictionaryFromData:YES] ];
    //[sentDict setObject:[verifyDM createApiDictionaryFromData:YES]  forKey:NoNameString];
    
    NSDictionary *result = [self.caller postData:sentDict ToWebserviceForResource:serviceURL Error:error];
    
//    if (result && (error == Nil)) {
//        NSDictionary * objReturn =[result objectForKey:@""] ;
//        //status = (StatusDM *)[StatusDM getDataModelObjectFromAPIDictionary:statusReturn];
//        //userReturn=(UserDM *)[UserDM getDataModelObjectFromAPIDictionary:loginReturn];
//        return objReturn;
//    }
    
    return result;
}

//- (UserDM *)getUserByID:(NSString*)userid
//{
//    
//    
//    NSError *error;
//    NSString* serviceUpdateUrl = [NSString stringWithFormat:@"%@(%@)",self.odataserviceName ,userid];
//    
//    NSDictionary *results =[self.caller getOData:Nil ToWebserviceForResource:serviceUpdateUrl Error:&error];
//    
//    
//    if (results != Nil) {
//        
//        return  (UserDM *)[UserDM getDataModelObjectFromAPIDictionary:results ];
//        
//    }
//    
//    
//    
//    
//    
//    return Nil;
//}


@end
