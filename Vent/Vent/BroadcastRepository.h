//
//  BroadcastRepository.h
//  Vent
//
//  Created by Medhat Ali on 6/11/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseRepository.h"
#import "ChatSession.h"
#import "BroadcastListUserPhones.h"

@interface BroadcastRepository : BaseRepository


-(BroadcastList *) addNewBroadcastListWithPhoneNumber:(NSString*)broadcastDate
                                          broadcastID:(NSString*)broadcastID
                                        broadcastName:(NSString*)broadcastName
                                   broadcastListOwner:(User*)broadcastListOwner
                                 broadcastChatSession:(ChatSession*)broadcastChatSession
                                   broadcastListUsers:(NSArray*)broadcastListUsers
                                                Error:(NSError **)error;
-(BroadcastListUserPhones *) addUserPhoneToBroadcastList:(NSString*)broadcastUserPhone
                                       broadcastUserList:(BroadcastList*)broadcastUserList
                                                   Error:(NSError **)error;
-(void)deleteBroadcastList:(NSManagedObject *)broadcastList Error:(NSError **)error;

-(void)updateBroadcastList:(NSManagedObject *)broadcastList Error:(NSError **)error;

-(void)deleteBroadcastListUsersPhones:(BroadcastListUserPhones *)BroadcastListUsersPhones Error:(NSError **)error;

-(void)updateBroadcastListUsersPhones:(BroadcastListUserPhones *)BroadcastListUsersPhones Error:(NSError **)error;


-(NSArray*)getUserBroadcastList:(User*)User Error:(NSError **)error;

-(NSArray*)getUserBroadcastListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error;
-(BroadcastList*)getBroadcastListByStartDate:(User*)User StartDate:(NSString*)startDate Error:(NSError **)error;


-(NSArray*)getBroadcastListPhones:(BroadcastList*)BroadcastList Error:(NSError **)error;

-(BroadcastListUserPhones*)getBroadcastListUserbyPhone:(NSString*)phoneNumber Error:(NSError **)error;

-(BroadcastList*)getBroadcastListByPhoneNumber:(NSString*)phone Error:(NSError **)error;
-(NSArray*)getBroadcastListUserbyPhones:(NSArray*)phoneNumber Error:(NSError **)error;

-(BroadcastList*)getBroadcastListByPhoneNumbers:(NSArray*)phone Error:(NSError **)error;
#pragma -mark chat session

-(NSArray*)getBroadcastListSessions:(BroadcastList*)BroadcastList Error:(NSError **)error;
-(NSArray*)getBroadcastListSessionsWithRecordsNumber:(BroadcastList*)BroadcastList RecordsLimit:(NSInteger)recordsLimit Error:(NSError **)error;
-(NSArray*)getUnreadBroadcastListSessions:(BroadcastList*)BroadcastList Error:(NSError **)error;

@end
