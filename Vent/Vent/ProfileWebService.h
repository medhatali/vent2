//
//  ProfileWebService.h
//  Vent
//
//  Created by Medhat Ali on 3/8/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "CommonWebService.h"
#import "UserProfileDM.h"

@interface ProfileWebService : CommonWebService

- (NSDictionary *)updateUserProfile:(UserProfileDM *)userProfileDM Error:(NSError**)error;
- (bool)addEmailToCurrentUser:(NSString*)email Error:(NSError**)error;

@end
