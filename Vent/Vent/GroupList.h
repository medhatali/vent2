//
//  GroupList.h
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChatSession, GroupListUsersPhones, User;

@interface GroupList : NSManagedObject

@property (nonatomic, retain) NSNumber * groupEnabled;
@property (nonatomic, retain) NSString * groupID;
@property (nonatomic, retain) NSData * groupImage;
@property (nonatomic, retain) NSString * groupImageUrl;
@property (nonatomic, retain) NSString * groupLastMessage;
@property (nonatomic, retain) NSString * groupLastUpdatedDate;
@property (nonatomic, retain) NSNumber * groupMuted;
@property (nonatomic, retain) NSString * groupName;
@property (nonatomic, retain) NSNumber * groupSequence;
@property (nonatomic, retain) NSString * groupStartDate;
@property (nonatomic, retain) NSNumber * groupIsBlocked;
@property (nonatomic, retain) User *groupListOwner;
@property (nonatomic, retain) NSSet *groupListSession;
@property (nonatomic, retain) NSSet *groupListUsers;
@end

@interface GroupList (CoreDataGeneratedAccessors)

- (void)addGroupListSessionObject:(ChatSession *)value;
- (void)removeGroupListSessionObject:(ChatSession *)value;
- (void)addGroupListSession:(NSSet *)values;
- (void)removeGroupListSession:(NSSet *)values;

- (void)addGroupListUsersObject:(GroupListUsersPhones *)value;
- (void)removeGroupListUsersObject:(GroupListUsersPhones *)value;
- (void)addGroupListUsers:(NSSet *)values;
- (void)removeGroupListUsers:(NSSet *)values;

@end
