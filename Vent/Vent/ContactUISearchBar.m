//
//  ContactUISearchBar.m
//  Vent
//
//  Created by Sameh Farouk on 4/5/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ContactUISearchBar.h"
#import <objc/runtime.h>

@implementation ContactUISearchBar

-(instancetype)initWithSearchBar:(UISearchBar *) aSuper{
    
    self = [self initWithFrame:aSuper.frame];
    [self setPlaceholder:[aSuper placeholder]];
    [self setSearchBarStyle:[aSuper searchBarStyle]];
    
    [self copyParent:aSuper intoChild:self];
    
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.placeholder =[NSString stringWithFormat:NSLocalizedString(@"Search", nil)] ;
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [self setShowsCancelButton:NO animated:NO];
}

- (void) copyParent:(id)parent intoChild:(id) child{
    id parentClass = [parent class];
    NSString *propertyName;
    unsigned int outCount, i;
    
    //Get a list of properties for the parent class.
    objc_property_t *properties = class_copyPropertyList(parentClass, &outCount);
    
    //Loop through the parents properties.
    for (i = 0; i < outCount; i++)
    {
        objc_property_t property = properties[i];
        
        //Convert the property to a string.
        propertyName = [NSString stringWithCString:property_getName(property) encoding:NSASCIIStringEncoding];
//        NSLog(@"%d, %@ of %d",i,propertyName, outCount);
//        if ([propertyName isEqualToString:@"searchBarTextField"]|
//            [propertyName isEqualToString:@"hash"] |
//            [propertyName isEqualToString:@"superclass"] |
//            [propertyName isEqualToString:@"description"] |
//            [propertyName isEqualToString:@"debugDescription"] |
//            [propertyName isEqualToString:@"autocapitalizationType"] |
//            [propertyName isEqualToString:@"autocorrectionType"] |
//            [propertyName isEqualToString:@"spellCheckingType"] |
//            [propertyName isEqualToString:@"keyboardType"] |
//            [propertyName isEqualToString:@"keyboardAppearance"] |
//            [propertyName isEqualToString:@"returnKeyType"] |
//            [propertyName isEqualToString:@"enablesReturnKeyAutomatically"] |
//            [propertyName isEqualToString:@"secureTextEntry"] |
//            [propertyName isEqualToString:@"textTrimmingSet"] |
//            [propertyName isEqualToString:@"insertionPointColor"] |
//            [propertyName isEqualToString:@"selectionBarColor"] |
//            [propertyName isEqualToString:@"selectionHighlightColor"] |
//            [propertyName isEqualToString:@"selectionDragDotImage"] |
//            [propertyName isEqualToString:@"insertionPointWidth"] |
//            [propertyName isEqualToString:@"_leftButton"])
//            continue;
//        if ([propertyName isEqualToString:@"textLoupeVisibility"]) {
//            break;
//        }
//        //Get the parent's value for the property
//        id value = [parent valueForKey:propertyName];
//        
//        //..and copy into the child.
//        if ([value conformsToProtocol:@protocol(NSCopying)])
//        {
//            [child setValue:[value copy] forKey:propertyName];
//        }
//        else
//        {
//            [child setValue:value forKey:propertyName];
//        }
        if ([propertyName isEqualToString:@"_searchController"]) {
            //Get the parent's value for the property
            id value = [parent valueForKey:propertyName];
            
            //..and copy into the child.
            if ([value conformsToProtocol:@protocol(NSCopying)])
            {
                [child setValue:[value copy] forKey:propertyName];
            }
            else
            {
                [child setValue:value forKey:propertyName];
            }
        }
    }
}

@end
