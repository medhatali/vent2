//
//  ChatListUsersPhones.h
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChatList;

@interface ChatListUsersPhones : NSManagedObject

@property (nonatomic, retain) NSNumber * chatUserEnabled;
@property (nonatomic, retain) NSString * chatUserJoinDate;
@property (nonatomic, retain) NSString * chatUserPhone;

@property (nonatomic, retain) ChatList *phoneChatList;

@end
