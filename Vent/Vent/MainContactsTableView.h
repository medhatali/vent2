//
//  MainContactsTableView.h
//  Vent
//
//  Created by Sameh Farouk on 3/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseContactsTableView.h"

@interface MainContactsTableView : BaseContactsTableView

@property (nonatomic, copy) NSArray *contacts;
@property BOOL allContacts;

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate;

@end
