//
//  RegisterationTVC.h
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"

@interface RegisterationTVC :UITableViewController

@property (retain,nonatomic) UIView *offlineView;
@property (retain,nonatomic) Reachability *reachbilityInstance;
@property (strong, nonatomic) LoadingView *loadingView;

-(void) showConnectionDown;
-(void) createOfflineView;

@end

