//
//  ResultsSearchTableViewController.m
//  Vent
//
//  Created by Sameh Farouk on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ResultsSearchTableViewController.h"
#import "ChatListTableViewCell.h"

@implementation ResultsSearchTableViewController

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self filteredEntries] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    if ([[self resultsDelegate] respondsToSelector:@selector(resultsCellForRowAtIndexPath:)]) {
        cell = [_resultsDelegate resultsCellForRowAtIndexPath:indexPath];
    }else{
        NSLog(@"ResultsSearchTableViewController Delegate : resultsCellForRowAtIndexPath is not implemented");
    }
    
    return cell;
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    
    return height;
}




@end
