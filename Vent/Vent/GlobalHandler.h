//
//  GlobalHandler.h
//  Vent
//
//  Created by Medhat Ali on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PushNotifiactionHandler.h"
#import "SignalRHandler.h"
#import "HandlerProtocol.h"
#import "MessageDM.h"
#import "UserDeviceIdentifierDM.h"
#import "GroupDM.h"
#import "SignalRContctivtyProtocol.h"
#import "CommonBussinessSevice.h"


@interface GlobalHandler : NSObject <HandlerProtocol,SignalRContctivtyProtocol>

@property (strong,nonatomic)PushNotifiactionHandler *pushInstance;
@property (strong,nonatomic, setter=setSignalRInstance:)SignalRHandler *SignalRInstance;

@property (strong,nonatomic) CommonBussinessSevice *commonBussiness;

@property (nonatomic, assign) id <HandlerProtocol> delegate;

@property BOOL ConnectoinStarted;
@property BOOL isAPNRecieved;

+ (GlobalHandler *)sharedInstance;

-(void)initReachability;

-(void)initPush:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
-(void) handleRemoteNotification:(NSDictionary *)userInfo;
-(void)displayLocalNotification:(NSString*)Title Body:(NSString*)body;
-(void)displayLocalNotificationGlobal:(NSString*)Title Body:(MessageDM*)body;
-(void)initSignalR;
-(void)startSignalR;
-(void)stopSignalR;
-(NSString*)getSignalRStatus;

+(void) sendMessage:(MessageDM*)message;
+(void) getAllOnlineFriends:(NSArray*) onlinefriendsPhoneNumbers;
+(void) notifyFriendsUserJoined:(UserDeviceIdentifierDM*) userId;
+(void) notifyFriendsUserLeft:(UserDeviceIdentifierDM*) userId;
+(void) markMessageAsDelivered:(NSString*) messageId;
+(void) markMessageAsRead:(NSString*) messageId;
+(void) getNewMessages:(UserDeviceIdentifierDM*) userId;
+(void) userStartTyping:(NSString*) userIdentifier;
+(void) canResend:(NSString*) messageId;
+(void) getNotifications:(UserDeviceIdentifierDM*) userIdentifier;
+(void) updateNotificationRecipient:(NSString*) notificationId;
+(void) GetMessageState:(NSString*) messageId;
+(void) CreateGroup:(GroupDM*) group;
+(void) addedToGroup:(NSString*) groupId;
+(void) addUserToGroup:(NSString*) userPhone WithGroupId: (NSString*) groupId;
+(void) RemoveUserFromGroup:(NSString*) userPhone WithGroupId: (NSString*) groupId;
+(void) leaveGroup:(NSString*) groupId;
+(void) updateGroupTitle:(NSString*) groupId WithTitle: (NSString*) title;
+(void) updateGroupIcon:(NSString*) groupId WithIconPath: (NSString*) path;
+(void) getAllGroups;
+(void) getGroup:(NSString*) groupId;


-(void)started;
-(void)received:(NSDictionary *) data;
-(void)error:(NSError *)error;
-(void)closed;
-(void)reconnecting;
-(void)reconnected;
-(void)stateChanged:(connectionState) connectionState;
-(void)connectionSlow;

@end
