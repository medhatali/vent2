//
//  ContactsRepository.h
//  Vent
//
//  Created by Medhat Ali on 3/10/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "BaseRepository.h"
#import "Contacts.h"
#import "ContactPhones.h"

@interface ContactsRepository : BaseRepository


-(Contacts *) addContact:(NSString*)contactFirstName
       contactMiddleName:(NSString*)contactMiddleName
         contactLastName:(NSString*)contactLastName
         contactFullName:(NSString*)contactFullName
       contactIsVentUser:(NSNumber*)contactIsVentUser
             contactUser:(User*)contactUser
            contactImage:(NSData*)contactImage
         contactImageUrl:(NSString*)contactImageUrl
               contactID:(NSString*)contactID
                   Error:(NSError **)error;



-(ContactPhones *) addPhonesToContacts:(NSString*)phoneNumber
                     phoneIsVentNumber:(NSNumber*)phoneIsVentNumber
                               contact:(Contacts*)contact
                                 Error:(NSError **)error;

-(void)deleteContactr:(Contacts *)contact Error:(NSError **)error;
-(void)updateContact:(Contacts *)contact Error:(NSError **)error;
-(void)deletePhoneNumber:(ContactPhones *)contactphones Error:(NSError **)error;
-(void)updatePhoneNumber:(ContactPhones *)contactphones Error:(NSError **)error;



-(Contacts*)getUserContactFullName:(NSString*)contactFullName contactUser:(User*)contactUser Error:(NSError **)error;
-(NSArray*)getAllUserContact:(User*)contactUser Error:(NSError **)error;
-(NSArray*)getAllVentUserContact:(User*)contactUser Error:(NSError **)error;

-(ContactPhones*)getPhoneNumber:(NSString*)phonenumber Error:(NSError **)error;
-(Contacts*)getUserContactByPhoneNumber:(NSString*)phoneNumber contactUser:(User*)contactUser Error:(NSError **)error;

-(Contacts*)getUserContactByID:(NSString*)contactID contactUser:(User*)contactUser Error:(NSError **)error;

-(NSString*)getVentPhoneNumberForContact:(Contacts*)Contact Error:(NSError **)error;

@end
