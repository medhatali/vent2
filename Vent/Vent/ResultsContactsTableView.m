//
//  ResultsContactsTableView.m
//  Vent
//
//  Created by Sameh Farouk on 3/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ResultsContactsTableView.h"
#import "ContactsTableViewCell.h"
#import "Contacts.h"

@interface ResultsContactsTableView () <UIGestureRecognizerDelegate>{
    
}

@end

@implementation ResultsContactsTableView


-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ContactsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kCellIdentifier];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self filteredContacts] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContactsTableViewCell *cell = (ContactsTableViewCell*) [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if(cell == nil) {
        cell = [[ContactsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
        
    }
    
    Contacts *contact =[[self filteredContacts] objectAtIndex:[indexPath row]];
    
    [cell setContacts:contact];
    
    for (Contacts *selected in self.selectedContacts) {
        if (contact == selected) {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            break;
        }
    }

    
    return cell;
}

@end
