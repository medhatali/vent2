//
//  ChatSessionRepository.m
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatSessionRepository.h"
#import "ChatList.h"
#import "ChatListRepository.h"

@implementation ChatSessionRepository

- (id)init
{
    self = [super init];
    if (self) {
        self.repositoryEntityName=@"ChatSession";
        
    }
    return self;
}



-(ChatSession *) addChatSession:(NSString*)chatSessionID
                 chatSessionFrom:(NSString*)chatSessionFrom
           chatSessionTo:(NSString*)chatSessionTo
          chatSessionMessage:(NSString*)chatSessionMessage
            chatSessionDate:(NSString*)chatSessionDate
                 chatSessionRead:(NSNumber*)chatSessionRead
            chatSessionDelivered:(NSNumber*)chatSessionDelivered
              chatSessionSecured:(NSNumber*)chatSessionSecured
chatSessionMessageID:(NSString*)chatSessionMessageID
chatSessionGroupID:(NSString*)chatSessionGroupID
chatSessionType:(NSString*)chatSessionType
chatSessionDeliveredDate:(NSString*)chatSessionDeliveredDate
       chatSessionReadDate:(NSString*)chatSessionReadDate
              chatSessionIsSent:(NSNumber*)chatSessionIsSent
              chatSessionFaild:(NSNumber*)chatSessionFaild
              chatSessionError:(NSString*)chatSessionError
            chatSessionTimer:(NSString*)chatSessionTimer
              chatSessionMediaUrl:(NSString*)chatSessionMediaUrl
              chatSessionSmily:(NSString*)chatSessionSmily
              chatSessionLocation:(NSString*)chatSessionLocation
               chatSessionVCard:(NSString*)chatSessionVCard
            chatSessionMyMessage:(NSNumber*)chatSessionMyMessage
                chatSessionChatList:(ChatList*)chatSessionChatList
                chatSessionGroupList:(GroupList*)chatSessionGroupList
       chatSessionBroadcastList:(BroadcastList*)chatSessionBroadcastList
                         Error:(NSError **)error
{
    
    NSManagedObject *newManagedObject = [self createModelForCurrentEntity:error];
    
    NSArray * sessionAlreadyExists= [[NSArray alloc]init];
    
    if (!chatSessionID) {
        chatSessionID = [self uniqueUUID];
    }
    
    
    sessionAlreadyExists=[self getChatSessionsByMessageId:chatSessionID Error:nil];
    
    ChatListRepository *repo=[[ChatListRepository alloc]init];
    
    ((ChatList*)chatSessionChatList).chatLastUpdatedDate = chatSessionDate;
    [repo updateChatList:((ChatList*)chatSessionChatList) Error:nil];
    
    if (sessionAlreadyExists.count >0) {
        // message already exist then upadte
        
        [self updateChatSession:sessionAlreadyExists[0] Error:nil];
    }
    else
    {
        
    // new message
    
        
        
        
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:chatSessionID forKey:@"chatSessionID"];
    [newManagedObject setValue:chatSessionFrom forKey:@"chatSessionFrom"];
    [newManagedObject setValue:chatSessionTo forKey:@"chatSessionTo"];
    [newManagedObject setValue:chatSessionMessage forKey:@"chatSessionMessage"];
    [newManagedObject setValue:chatSessionDate forKey:@"chatSessionDate"];
    
    [newManagedObject setValue:chatSessionRead forKey:@"chatSessionRead"];
    [newManagedObject setValue:chatSessionDelivered forKey:@"chatSessionDelivered"];
    [newManagedObject setValue:chatSessionSecured forKey:@"chatSessionSecured"];
    [newManagedObject setValue:chatSessionDate forKey:@"chatSessionDate"];
    
    [newManagedObject setValue:chatSessionMessageID forKey:@"chatSessionMessageID"];
    
    if ([chatSessionGroupID class] != [NSNull class]) {
        
    
    if (![chatSessionGroupID isEqualToString:@"<null>"]) {
        [newManagedObject setValue:chatSessionGroupID forKey:@"chatSessionGroupID"];

    }
    }
    
    [newManagedObject setValue:chatSessionType forKey:@"chatSessionType"];
    [newManagedObject setValue:chatSessionDeliveredDate forKey:@"chatSessionDeliveredDate"];
    [newManagedObject setValue:chatSessionReadDate forKey:@"chatSessionReadDate"];
     [newManagedObject setValue:chatSessionIsSent forKey:@"chatSessionIsSent"];
    
    [newManagedObject setValue:chatSessionFaild forKey:@"chatSessionFaild"];
    [newManagedObject setValue:chatSessionError forKey:@"chatSessionError"];
    [newManagedObject setValue:chatSessionTimer forKey:@"chatSessionTimer"];
    [newManagedObject setValue:chatSessionMediaUrl forKey:@"chatSessionMediaUrl"];
    [newManagedObject setValue:chatSessionSmily forKey:@"chatSessionSmily"];
    [newManagedObject setValue:chatSessionLocation forKey:@"chatSessionLocation"];
    [newManagedObject setValue:chatSessionMyMessage forKey:@"chatSessionMyMessage"];
    [newManagedObject setValue:chatSessionVCard forKey:@"chatSessionVCard"];
    
    [newManagedObject setValue:chatSessionChatList forKey:@"chatSessionChatList"];
    
    
        
    [newManagedObject setValue:chatSessionGroupList forKey:@"chatSessionGroupList"];
    [newManagedObject setValue:chatSessionBroadcastList forKey:@"chatSessionBroadcastList"];
    
    [self insertModel:newManagedObject EntityName:self.repositoryEntityName Error:error];
    
    }
    
    
    return (ChatSession *)newManagedObject;
    
    
}


-(void)deleteChatSession:(ChatSession *)ChatSession Error:(NSError **)error
{
    
    [self deleteModel:ChatSession EntityName:self.repositoryEntityName Error:error];
    
}

-(void)updateChatSession:(ChatSession *)ChatSession Error:(NSError **)error
{
    
    [self updateModel:ChatSession EntityName:self.repositoryEntityName Error:error];
    
}


-(NSArray*)getChatSessionsByChatList:(ChatList*)ChatList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionChatList == %@) ",ChatList];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    
    
    return userdata;
}

-(NSArray*)getChatSessionsByMessageId:(NSString*)messageId Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionMessageID ==[c] %@) ",messageId];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    
    
    return userdata;
}

@end
