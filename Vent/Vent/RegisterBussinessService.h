//
//  RegisterBussinessService.h
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "BussinessSevericeController.h"
#import "RegisterWebService.h"
#import "ProfileWebService.h"

@interface RegisterBussinessService : BussinessSevericeController

- (BOOL)registerUser:(UserDM*)UserModel Error:(NSError**)error;
- (BOOL)verifyUserPhone:(VerifyDM*)verifyModel Error:(NSError**)error;
- (BOOL)verifyUserPhoneWithProfile:(VerifyDM*)verifyModel profileModel:(UserProfileDM*)profileModel UserModel:(UserDM*)usermodel CountryCode:(NSString*)countrycode Country:(NSString*)country Error:(NSError**)error;

- (BOOL)updateUserProfile:(UserProfileDM*)profileModel UserModel:(UserDM*)usermodel Error:(NSError**)error;


@end
