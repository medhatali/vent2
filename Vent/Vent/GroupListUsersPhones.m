//
//  GroupListUsersPhones.m
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupListUsersPhones.h"
#import "GroupList.h"


@implementation GroupListUsersPhones

@dynamic groupUserEnabled;
@dynamic groupUserJoinDate;
@dynamic groupUserPhone;
@dynamic groupUserIsAdmin;
@dynamic phoneGroupList;

@end
