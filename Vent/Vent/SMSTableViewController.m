//
//  SMSTableViewController.m
//  Smiley
//
//  Created by Sameh Farouk on 6/13/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import "SMSTableViewController.h"
#import "AppDelegate.h"
#import "RegisterBussinessService.h"
#import "ProfileTableViewController.h"


@interface SMSTableViewController ()
{
    NSInteger _secondsLeft;
    __block bool _isSentRegistration;
    __block NSMutableURLRequest *_registrationRequest;
    __block NSMutableURLRequest *_verifyRequest;
    //__block VerifyingViewController *_verifyingViewController;
}

@property (strong, nonatomic) IBOutlet UILabel *lblText1;
@property (strong, nonatomic) IBOutlet UILabel *lblText2;


@property (strong, nonatomic) IBOutlet UITextField *activationCodeTextField;
@property (strong, nonatomic) IBOutlet UIButton *resendButton;
@property (strong, nonatomic) IBOutlet UILabel *resendLable;
@property (strong, nonatomic) UILabel *timerLable;
@property (strong, nonatomic) NSTimer *timer;
//
//-(void) setIsSentRegistration:(bool) isSentRegistration;
//
@end

@implementation SMSTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
 
    
    self.title =[NSString stringWithFormat:NSLocalizedString(@"Verification Code", nil)];
    self.lblText1.text =[NSString stringWithFormat:NSLocalizedString(@"verification 1", nil)];
    self.lblText2.text =[NSString stringWithFormat:NSLocalizedString(@"verification 2", nil)];
    

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    [self sendRegistration];
    _secondsLeft = 300;
    [_activationCodeTextField becomeFirstResponder];
    [_resendLable setText:[NSString stringWithFormat:@"%@ %02d:%02d",NSLocalizedString(@"Resend in", nil),(int)_secondsLeft / 60,(int)_secondsLeft % 60]];
}


-(void)viewDidAppear:(BOOL)animated{
    _timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 4;
}


#pragma mark - Text Feild delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    // allow backspace
    if (!string.length){
        return YES;
    }
    
    // Prevent invalid character input, if keyboard is numberpad
    if (textField.keyboardType == UIKeyboardTypeNumberPad)
    {
        NSString *text = textField.text;
        // If we're trying to past characters, don't allow it
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            //NSLog(@"This field accepts only numeric entries.");
            return NO;
        }
        
        // If we're trying to add more than the max amount of characters, don't allow it
        if ([text length] == 7 && range.location > 6) {
            return NO;
        }
        // First lets add the whole string we're going for
        text = [text stringByReplacingCharactersInRange:range withString:string];
        
        // Now remove spaces (since we'll add these automatically in a minute)
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        // We need to use an NSMutableString to do insertString calls in a moment
        NSMutableString *mutableText = [text mutableCopy];
        
        // adding space 4th char will be a ' '
        if ([mutableText length] >=3) {
            [mutableText insertString:@" " atIndex:3];
        }
        // lets set text to our new string
        text = mutableText;
        
        // Finally, set the textfield to our newly modified string!
        textField.text = text;
        //If we're type the code verify
        if ([text length] == 7) {
            // go to profile screen
            //GoProfile
            [self validateCode];
            //[self presentTransparentVerifyingViewController];
        }
        return  NO;
    }
    return NO;
}

#pragma mark -

-(void) setIsSentRegistration:(bool) isSentRegisteration{
    _isSentRegistration = isSentRegisteration;
}

-(void) updateCountdown{
    int minutes, seconds;
    if (_secondsLeft == 0) {
        if ([_timer isValid]) {
            [_timer invalidate];
        }
        _timer = nil;
        [_resendLable setHidden:YES];
        [_resendButton setHidden:NO];
        return;
    }
    
    _secondsLeft--;
    minutes = (int)_secondsLeft / 60;
    seconds = (int)_secondsLeft % 60;
    [_resendLable setText:[NSString stringWithFormat:@"%@ %02d:%02d",NSLocalizedString(@"Resend in", nil),minutes,seconds]];
    return;
}


-(void) dismissView{
    UIView *_verifyView = [[[self navigationController] view]viewWithTag:1];
    [_verifyView removeFromSuperview];
    [_activationCodeTextField setText:@""];
    [_activationCodeTextField becomeFirstResponder];
}


-(void)validateCode
{
    
    RegisterBussinessService *registerServiceObject =[[RegisterBussinessService alloc]init];
    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"validate code", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       // send sms
                       VerifyDM *verifyCurrentUser = [[VerifyDM alloc]init];
                       NSString *codeNumber= [self.activationCodeTextField.text  stringByReplacingOccurrencesOfString:@" " withString:@""];
                       
                       verifyCurrentUser.VerificationCode =codeNumber;
                       verifyCurrentUser.PhoneNumber = self.registrationData.PhoneNumber;
                       verifyCurrentUser.DeviceToken =[[NSUserDefaults standardUserDefaults] valueForKey:KVentDeviceToken];
                      
                       
                       UserProfileDM *profileCurrentUser = [[UserProfileDM alloc]init];
                       profileCurrentUser.FirstName =self.registrationData.DisplayName;
                       profileCurrentUser.LastName =@"";
                       profileCurrentUser.Status = @"Avilable";
                       profileCurrentUser.ImagePath =@"";
                       profileCurrentUser.DateOfBirth =Nil;
                       profileCurrentUser.PhoneNumber =self.registrationData.PhoneNumber;
                       
                       NSError *error;
//                      bool success= [registerServiceObject verifyUserPhone:verifyCurrentUser Error:&error];

                       bool success= [registerServiceObject verifyUserPhoneWithProfile:verifyCurrentUser profileModel:profileCurrentUser UserModel:self.registrationData CountryCode:self.country.dial_code Country:self.country.code Error:&error];
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                          
                                          if (success) {
                                              
                                              // save user profile to local DB then go to profile
                                              
                                              
                                               [self performSegueWithIdentifier:@"GoProfile" sender:self];
                                              
                                          }
                                          else
                                          {
                                              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"verification Error", nil)
                                                                          message:NSLocalizedString(@"error in verification", nil)
                                                                         delegate:nil
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                otherButtonTitles:nil] show];
                                              
                                               //[self performSegueWithIdentifier:@"GoProfile" sender:self];
                                              
                                          }
                                          
                                          
                                      });
                       
                   });
    
    

}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    if([segue.identifier isEqualToString:@"GoProfile"]){
        
                ProfileTableViewController *dest =(ProfileTableViewController *) segue.destinationViewController;
                dest.registrationData =self.registrationData;
    }
    
}

- (IBAction)resendSMS:(id)sender {
    [self resendValidationCode];
}

-(void)resendValidationCode
{
    RegisterBussinessService *registerServiceObject =[[RegisterBussinessService alloc]init];
    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Sending SMS", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       // send sms

                       NSError *error;
                       bool success=[registerServiceObject registerUser:self.registrationData Error:&error];
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                          
                                          if (success) {
                                              
                                              [self.tableView reloadData];
                                              
                                          }
                                          else
                                          {
                                              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Registeration Error", nil)
                                                                          message:NSLocalizedString(@"error in registering ", nil)
                                                                         delegate:nil
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                otherButtonTitles:nil] show];
                                              
                                             // [self performSegueWithIdentifier:@"SMS" sender:self];
                                              
                                          }
                                          
                                          
                                      });
                       
                   });
    
    

}
@end
