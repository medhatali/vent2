//
//  UserProfileTableViewController.m
//  Vent
//
//  Created by Medhat Ali on 8/3/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "UserProfileTableViewController.h"
#import "ContactsBussinessService.h"
#import "CommonHeader.h"
#import "RegisterBussinessService.h"
#import "UploadDownLoadBussinessService.h"
#import "ContactsBussinessService.h"
#import "User.h"
#import "GlobalHandler.h"
#import "Contacts.h"
#import "ContactsRepository.h"
#import "ChatListRepository.h"
#import "ChatList.h"
#import "VcardImporter.h"

@interface UserProfileTableViewController () <UIAlertViewDelegate>


@property (strong, nonatomic) IBOutlet UITableViewCell *statusSection;
@property (strong, nonatomic)VcardImporter *vcardPaser;

@end

@implementation UserProfileTableViewController

-(void)viewDidLoad
{
    
    //self.lblGroupName.text =@"My First Group";
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    self.currentUser=[contactBWS getCurrentUserProfile];
    
    [[[self profileImageBtn] layer] setCornerRadius: self.profileImageBtn.frame.size.width / 2 ];
    [[[self profileImageBtn] layer] setBorderWidth: 3.0f];
    [[[self profileImageBtn] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [[self profileImageBtn] setClipsToBounds: YES];
    
    if (self.vcardProfile) {
        self.vcardPaser=[[VcardImporter alloc]init];
        [self.vcardPaser parse:_vcardString];
        //self.lblFirstName.text=@"";
       self.lblFirstName.text= (__bridge NSString*)ABRecordCopyValue(self.vcardPaser.personRecord, kABPersonLastNameProperty);
        self.lblDisplayName.text = (__bridge NSString*)ABRecordCopyValue(self.vcardPaser.personRecord, kABPersonFirstNameProperty);
        
        
        ABMultiValueRef multiPhones = ABRecordCopyValue(self.vcardPaser.personRecord, kABPersonPhoneProperty);
        for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
            NSString *phoneNumber1 = (__bridge NSString *) phoneNumberRef;
            phoneNumber1 = [[[[[phoneNumber1 stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@"\u00A0" withString:@""];
            
             self.lblLastName.text = phoneNumber1;
            
        }
        
      //  self.lblLastName.text = (__bridge NSString*)ABRecordCopyValue(self.vcardPaser.personRecord, kABPersonLastNameProperty);

              //self.lblLastName.text =phoneNumbers[0];
        
        [self.statusSection setHidden:YES];
        self.title =[NSString stringWithFormat:NSLocalizedString(@"New Contact", nil)];
        [self.lblDisplayName setEnabled:NO];
        [self.lblFirstName setEnabled:NO];
        [[self profileImageBtn] setEnabled:NO];
        
        self.tableView.sectionHeaderHeight = 0;
        
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                              target:self
                                                                                              action:@selector(savePressed:)];
        
    }
    else
    {
    UIImage *userProfile=[UIImage imageWithData:self.reciverContact.contactImage];
    if (userProfile != nil) {
         [[self profileImageBtn]  setBackgroundImage:userProfile forState:UIControlStateNormal];
    }
   
    

    
    
    self.lblDisplayName.text=self.reciverContact.contactFirstName;
    self.lblFirstName.text=self.reciverContact.contactLastName;
    
    ContactsRepository *repo=[[ContactsRepository alloc]init];
    
    self.lblLastName.text=[repo getVentPhoneNumberForContact:self.reciverContact Error:nil];
    
    
    if ([self.selectedChatList.chatIsSecure isEqualToNumber:[NSNumber numberWithBool:YES]]) {
        
        [self.secureChatCheck setOn:YES];
    }
    else
    {
        [self.secureChatCheck setOn:NO];
    }
        
        if ([self.selectedChatList.chatIsFavourite isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            
            [self.favouriteChatCheck setOn:YES];
        }
        else
        {
            [self.favouriteChatCheck setOn:NO];
        }
        
    
    }
//    self.lblStatus.text=self.currentUser.userStatus;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
//    self.lblDisplayName.text=self.currentUser.userDisplayName;
//    self.lblFirstName.text=self.currentUser.userFirstName;
//    self.lblLastName.text=self.currentUser.userLastName;
//    self.lblStatus.text=self.currentUser.userStatus;
    [self.tabBarController.tabBar setHidden:YES];

    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    [self.tabBarController.tabBar setHidden:NO];
    
    if (self.vcardProfile) {
        
        
    }
    else
    {
        
//    self.currentUser.userDisplayName=self.lblDisplayName.text;
//    self.currentUser.userFirstName=self.lblFirstName.text;
//    self.currentUser.userLastName=self.lblLastName.text;
    
//    UserRepository *repo=[[UserRepository alloc]init];
//    NSError *error;
//    [repo updateUser:self.currentUser Error:&error];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected row %ld in section %ld" , (long)indexPath.row , (long)indexPath.section);
    
    if (indexPath.section == 2) {
        
        if (indexPath.row ==0) {
            [self performSegueWithIdentifier:@"StatusControl" sender:self];
        }
        
        
    }
    
    if (indexPath.section == 0) {
        
        UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)],[NSString stringWithFormat:NSLocalizedString(@"Choose Existing", nil)] , nil];
        
        [_actionSheet showInView:[self view]];
        
    }
    
}

#pragma mark - Actions

- (void)savePressed:(UIBarButtonItem *)sender
{
     ContactsBussinessService *contBS=[[ContactsBussinessService alloc]init];
     if ([contBS isContatcsAccessGrantSilent]) {
    [self.vcardPaser saveVcardToAddress];
     }
    else
    {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           
                           
                           UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                           
                           [cantLoadContactsAlert show];
                           
                           
                           
                       });

    }
}

#pragma mark - screen navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //    if([segue.identifier isEqualToString:@"NewBroadcast"])
    //    {
    //        NewBroadCastViewController *dest=(NewBroadCastViewController *)segue.destinationViewController;
    //        dest.inUpdateMode = TRUE;
    //        dest.grouContactList =self.selectedModelToUpdate;
    //
    //    }
    
    //     [self performSegueWithIdentifier:@"NewBroadcast" sender:self];
    
    
}


- (IBAction)changeProfilePicture:(id)sender {
//    UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Choose Existing", nil];
//    
//    [_actionSheet showInView:[self view]];
}

#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UIImagePickerController *_picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = YES;
    
    switch (buttonIndex) {
        case 0:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Device has no camera", nil)]
                                                                     delegate:nil
                                                            cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            else{
                
                _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:_picker animated:YES completion:NULL];
                
            }
            break;
        case 1:
            _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:_picker animated:YES completion:NULL];
            break;
        default:
            break;
    }
}

#pragma mark - Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *_chosenImage = info[UIImagePickerControllerEditedImage];
    [[self profileImageBtn] setBackgroundImage:_chosenImage forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)doneAction:(id)sender {
    
    
//    UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Update User Profile Image", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       NSError *error;
                       
                       // upload profile image to server
                       UIImage *profileimage=[[self profileImageBtn]backgroundImageForState:UIControlStateNormal];
                       NSData *imageData = UIImagePNGRepresentation(profileimage);
                       
//                       NSString *imagePath = [fileServiceObject uploadImage:imageData ImageName:@"file" ImageExt:@"png" Error:&error];
                       
                       self.currentUser.userProfileImage=imageData;
                       
                       UserRepository *repo=[[UserRepository alloc]init];
                       [repo updateUser:self.currentUser Error:&error];
                       
                       //                           // update user profile online and local DB
                       //                           UserProfileDM *profileCurrentUser = [[UserProfileDM alloc]init];
                       //                           profileCurrentUser.FirstName =self.enterName.text;
                       //                           profileCurrentUser.LastName =@"";
                       //                           profileCurrentUser.Status = @"";
                       //                           profileCurrentUser.ImagePath =imagePath;
                       //                           profileCurrentUser.DateOfBirth =Nil;
                       //                           profileCurrentUser.PhoneNumber =self.registrationData.PhoneNumber;
                       //
                       //
                       //                           bool success=[registerServiceObject updateUserProfile:profileCurrentUser UserModel:self.registrationData Error:&error];
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                          
                                          if (imageData) {
                                              
                                              // go to contacts
                                              
                                              // [self performSegueWithIdentifier:@"LoadContacts" sender:self];
                                              // [self performSegueWithIdentifier:@"GoProfile" sender:self];
                                              
                                          }
                                          else
                                          {
                                              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update Error", nil)
                                                                          message:NSLocalizedString(@"error in Update Profile", nil)
                                                                         delegate:nil
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                otherButtonTitles:nil] show];
                                              
                                              // [self performSegueWithIdentifier:@"GoProfile" sender:self];
                                              
                                          }
                                          
                                          
                                      });
                       
                   });
    
    
    
}

- (IBAction)checkSecureChat:(id)sender {
    ChatListRepository *repo=[[ChatListRepository alloc]init];
    
    if ([self.secureChatCheck isOn]) {
       self.selectedChatList.chatIsSecure =[NSNumber numberWithBool:YES];
    }
    else
    {
        self.selectedChatList.chatIsSecure =[NSNumber numberWithBool:NO];
        
    }
    
    [repo updateChatList:self.selectedChatList Error:nil];
    
}

- (IBAction)checkFavouriteChat:(id)sender {
    
    ChatListRepository *repo=[[ChatListRepository alloc]init];
    
    if ([self.favouriteChatCheck isOn]) {
        self.selectedChatList.chatIsFavourite =[NSNumber numberWithBool:YES];
    }
    else
    {
        self.selectedChatList.chatIsFavourite =[NSNumber numberWithBool:NO];
        
    }
    
    [repo updateChatList:self.selectedChatList Error:nil];
    

    
}




#pragma UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self openSettings];
    }
}

- (void)openSettings
{
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
