//
//  NewGroupListViewController.h
//  Vent
//
//  Created by Medhat Ali on 4/15/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"
#import "GroupList.h"

@interface NewGroupListViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *goupImageBtn;
@property (strong, nonatomic) IBOutlet UITextField *txtGroupName;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *lblContatcs;
@property (strong, nonatomic) User* currentUser;


@property (retain,nonatomic) Reachability *reachbilityInstance;
@property (strong, nonatomic) LoadingView *loadingView;

@property BOOL UpdateGroup;
@property (strong, nonatomic) GroupList *selectedGroupList;

@end
