//
//  RegisterBussinessService.m
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "RegisterBussinessService.h"
#import "UserRepository.h"

@implementation RegisterBussinessService


#pragma mark - register Business

- (BOOL)registerUser:(UserDM*)UserModel Error:(NSError**)error
{

    if ([self.reachbilityInstance isReachable]) {
        // get from Webservice

        RegisterWebService * objServiceInstance=[[RegisterWebService alloc]init];

       // NSDictionary *loginReturn = [objServiceInstance registerUser:UserModel Error:error];
       NSDictionary *resultReturn= [objServiceInstance registerUser:UserModel Error:error];
  

       if (*error == Nil && resultReturn == Nil) {

           return TRUE;
           
        }
        else
        {

            return  FALSE;
        }



    }
    
    return FALSE;

}

- (BOOL)verifyUserPhone:(VerifyDM*)verifyModel Error:(NSError**)error
{
    
    if ([self.reachbilityInstance isReachable]) {
        // get from Webservice
        
        RegisterWebService * objServiceInstance=[[RegisterWebService alloc]init];
        
        // NSDictionary *loginReturn = [objServiceInstance registerUser:UserModel Error:error];
         NSDictionary *resultReturn=[objServiceInstance verifyPhoneNumber:verifyModel Error:error];
        
        if (*error == Nil && resultReturn == Nil) {
            
            return TRUE;
            
        }
        else
        {
            
            return  FALSE;
        }
        
        
        
    }
    
    return FALSE;
    
}

- (BOOL)verifyUserPhoneWithProfile:(VerifyDM*)verifyModel profileModel:(UserProfileDM*)profileModel UserModel:(UserDM*)usermodel CountryCode:(NSString*)countrycode Country:(NSString*)country Error:(NSError**)error
{
    
    if ([self.reachbilityInstance isReachable]) {
        // get from Webservice
        
        RegisterWebService * objServiceInstance=[[RegisterWebService alloc]init];
        
        // NSDictionary *loginReturn = [objServiceInstance registerUser:UserModel Error:error];
        NSDictionary *resultReturn=[objServiceInstance verifyPhoneNumber:verifyModel Error:error];
        
        if (*error == Nil && resultReturn == Nil) {
            
            //update DB to add user
            
            UserRepository *userrepoObj=[[UserRepository alloc]init];
            NSString *currentdate=[NSString stringWithFormat:@"%@",[NSDate date]];
            
            [userrepoObj addUser:usermodel.DeviceToken userDeviceName:usermodel.DeviceName userDisplayName:usermodel.DisplayName userEmail:@"" userPhoneNumber:usermodel.PhoneNumber userCountry:countrycode userIsRegistered:[NSNumber numberWithBool:TRUE] userIsSecureLogin:[NSNumber numberWithBool:FALSE] userFirstName:profileModel.FirstName userLastName:profileModel.LastName userStatus:profileModel.Status userProfileImageName:profileModel.ImagePath userProfileImagePath:profileModel.ImagePath userDate:currentdate userActive:[NSNumber numberWithBool:TRUE] userProfileImage:Nil userPin:@"" userCountryCode:country Error:error];

            
            
            return TRUE;
            
        }
        else
        {
            
            return  FALSE;
        }
        
        
        
    }
    
    return FALSE;
    
}

- (BOOL)updateUserProfile:(UserProfileDM*)profileModel UserModel:(UserDM*)usermodel Error:(NSError**)error
{
    
    if ([self.reachbilityInstance isReachable]) {
        // get from Webservice
        
        ProfileWebService * objServiceInstance=[[ProfileWebService alloc]init];
        
        // NSDictionary *loginReturn = [objServiceInstance registerUser:UserModel Error:error];
       NSDictionary *resultReturn= [objServiceInstance updateUserProfile:profileModel Error:error];
        
        
        if (*error == Nil && resultReturn == Nil) {
            
            //update DB to add user
            
//            UserRepository *userrepoObj=[[UserRepository alloc]init];
//            
//            [userrepoObj addUser:usermodel.DeviceToken userDeviceName:usermodel.DeviceName userDisplayName:usermodel.DisplayName userEmail:@"" userPhoneNumber:usermodel.PhoneNumber userCountry:@"" userIsRegistered:[NSNumber numberWithBool:TRUE] userIsSecureLogin:[NSNumber numberWithBool:FALSE] userFirstName:profileModel.FirstName userLastName:profileModel.LastName userStatus:profileModel.Status userProfileImageName:profileModel.ImagePath userProfileImagePath:profileModel.ImagePath Error:error];
            
            return TRUE;
            
        }
        else
        {
            
            return  FALSE;
        }
        
        
    }
    
    return FALSE;
    
}

@end
