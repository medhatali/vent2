//
//  WebServiceConnection.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "WebServiceConnection.h"
#import "AppUtility.h"

@interface WebServiceConnection() {
    
    NSMutableURLRequest *urlRequest;
   
    
}

@end


@implementation WebServiceConnection
/*
 Development 		http://104.40.182.7:54486
 Test		 		http://104.40.182.7:54487
 Stress Test 		http://104.40.154.251:63256
 Production         http://104.40.215.190:63256
 Local              http://10.211.55.3:54486
 */

//NSString * const cWebServiceURL = [[AppUtility sharedUtilityInstance]getServiceUrl];
NSString * const cNormalWebServiceURL =  @"";
NSString * const cODataWebServiceURL = @"";


#pragma mark - Initialization messages

static WebServiceConnection *myInstane = nil;

+ (WebServiceConnection *)sharedInstance
{
    if (!myInstane) {
        myInstane = [[[self class]allocWithZone:nil]init];
     myInstane.cWebServiceURL = [[AppUtility sharedUtilityInstance]getServiceUrl];
    }
    return myInstane;
}

+ (id)alloc
{
    NSLog(@"Use +sharedInstance method instead");
    return nil;
}

#pragma mark NSURLConnection Delegate Methods

- (NSArray *)getFromWebserviceForResource:(NSString *)resourceName Error:(NSError**)error
{
    NSString *fullServiceURL = [NSString stringWithFormat:@"%@/%@",self.cWebServiceURL , resourceName];
    
    // Create the request.
    urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullServiceURL]];
    
    NSURLResponse * response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest
                                         returningResponse:&response
                                                     error:error];
    NSArray *resultsArray;
    if (error == nil)
    {
        resultsArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:error];
    }
    resultsArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:error];
    
    return resultsArray;
}

- (NSDictionary *)postData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error
{
    return [self callWebServiceOnData:body ForWebServiceName:resourceName UsingMethod:@"POST" WithHTTPHeader:nil Error:error];
}

- (NSDictionary *)postData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName WithHTTPHeader: (NSDictionary *)header Error:(NSError**)error{
    return [self callWebServiceOnData:body ForWebServiceName:resourceName UsingMethod:@"POST" WithHTTPHeader:header Error:error];
}

- (NSInteger)postDataAndGetStatusCode:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName WithHTTPHeader: (NSDictionary *)header Error:(NSError**)error{
    return [self callWebServiceAndGetStatusCodeOnData:body ForWebServiceName:resourceName UsingMethod:@"POST" WithHTTPHeader:header Error:error];
}

- (NSData *)postDataFile:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error
{
    return [self callWebServiceOnDataFile:body ForWebServiceName:resourceName UsingMethod:@"POST" Error:error];
}

- (NSDictionary *)getData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error
{
    return [self callWebServiceOnData:body ForWebServiceName:resourceName UsingMethod:@"GET" WithHTTPHeader:nil Error:error];
}

- (NSDictionary *)editData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error
{
    return [self callWebServiceOnData:body ForWebServiceName:resourceName UsingMethod:@"PUT" WithHTTPHeader:nil Error:error];
}

- (NSDictionary *)editOData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error
{
    return [self callWebServiceOnData:body ForWebServiceName:resourceName UsingMethod:@"PATCH" WithHTTPHeader:nil Error:error];
}

- (NSDictionary *)deleteData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error
{
    return [self callWebServiceOnData:body ForWebServiceName:resourceName UsingMethod:@"DELETE" WithHTTPHeader:nil Error:error];
}

- (NSDictionary *)callWebServiceOnData:(NSDictionary *)body ForWebServiceName:(NSString *)resourceName UsingMethod:(NSString *)methodName WithHTTPHeader: (NSDictionary *)header Error:(NSError**)error
{
    
    NSString *fullServiceURL = [NSString stringWithFormat:@"%@/%@", self.cWebServiceURL, resourceName];
    // Create the request.
    urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullServiceURL]];
    
    //Specify the method
    [urlRequest setHTTPMethod:methodName];
    
    //Set header fields
    [urlRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"accept"];
    if (header) {
        [header enumerateKeysAndObjectsUsingBlock:^(id httpHeaderField, id value, BOOL *stop) {
            [urlRequest setValue:value forHTTPHeaderField:httpHeaderField];
        }];
    }
        
    //Convert your data and set your request's HTTPBody property
    if (body != Nil) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:error];
       // NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        urlRequest.HTTPBody = jsonData;
    }
    
    
    //Create url connection and fire request
    NSURLResponse * response = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:error];
  
    if (data != Nil)
    {
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
//        if (dataDictionary == Nil) {
//            NSString *stringVlaue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSMutableDictionary *valuedataDictionary=[[NSMutableDictionary alloc]init];
//            [valuedataDictionary setObject:stringVlaue forKey:@"value"];
//            return valuedataDictionary;
//        }
        return dataDictionary;
    }
    else
    {
        return Nil;
    }
    
    
}

- (NSInteger) callWebServiceAndGetStatusCodeOnData:(NSDictionary *)body ForWebServiceName:(NSString *)resourceName UsingMethod:(NSString *)methodName WithHTTPHeader: (NSDictionary *)header Error:(NSError**)error
{
    
    NSString *fullServiceURL = [NSString stringWithFormat:@"%@/%@", self.cWebServiceURL, resourceName];
    // Create the request.
    urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullServiceURL]];
    
    //Specify the method
    [urlRequest setHTTPMethod:methodName];
    
    //Set header fields
    [urlRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"accept"];
    if (header) {
        [header enumerateKeysAndObjectsUsingBlock:^(id httpHeaderField, id value, BOOL *stop) {
            [urlRequest setValue:value forHTTPHeaderField:httpHeaderField];
        }];
    }
    
    //Convert your data and set your request's HTTPBody property
    if (body != Nil) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:error];
        // NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        urlRequest.HTTPBody = jsonData;
    }
    
    
    //Create url connection and fire request
    NSURLResponse * response = nil;
    [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:error];
    
    return  [(NSHTTPURLResponse*)response statusCode];
    
}


- (NSData *)callWebServiceOnDataFile:(NSDictionary *)body ForWebServiceName:(NSString *)resourceName UsingMethod:(NSString *)methodName Error:(NSError**)error
{
    
    NSString *fullServiceURL = [NSString stringWithFormat:@"%@/%@", self.cWebServiceURL, resourceName];
    // Create the request.
    urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullServiceURL]];
    
    //Specify the method
    [urlRequest setHTTPMethod:methodName];
    
    //Set header fields
    [urlRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"accept"];
    
    //Convert your data and set your request's HTTPBody property
    if (body != Nil) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:error];
        // NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        urlRequest.HTTPBody = jsonData;
    }
    
    
    //Create url connection and fire request
    NSURLResponse * response = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:error];
    if (data != Nil)
    {
        return data;
    }
    else
    {
        return Nil;
    }
    
    
}

- (NSDictionary *)postDataForContacts:(NSMutableArray *)body ToWebserviceForResource:(NSString *)resourceName phoneNumber:(NSString *)phoneNumber deviceToken:(NSString *)deviceToken Error:(NSError**)error
{
    return [self callWebServiceOnDataContacts:body ForWebServiceName:resourceName UsingMethod:@"POST" phoneNumber:phoneNumber deviceToken:deviceToken Error:error];
}

- (NSDictionary *)callWebServiceOnDataContacts:(NSMutableArray *)body ForWebServiceName:(NSString *)resourceName UsingMethod:(NSString *)methodName phoneNumber:(NSString *)phoneNumber deviceToken:(NSString *)deviceToken Error:(NSError**)error
{
    
    NSString *fullServiceURL = [NSString stringWithFormat:@"%@/%@", self.cWebServiceURL, resourceName];
    // Create the request.
    urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullServiceURL]];
    
    //Specify the method
    [urlRequest setHTTPMethod:methodName];
    
    //Set header fields
    [urlRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"accept"];

    
    //Convert your data and set your request's HTTPBody property
    
    if (body != Nil) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:error];
         //NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        [urlRequest setValue:phoneNumber forHTTPHeaderField:@"PhoneNumber"];
        [urlRequest setValue:deviceToken forHTTPHeaderField:@"DeviceToken"];
        [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        
        urlRequest.HTTPBody = jsonData;
    }
    

    
    //Create url connection and fire request
    NSURLResponse * response = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:error];
    if (data != Nil)
    {
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:Nil];
        //        if (dataDictionary == Nil) {
        //            NSString *stringVlaue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //            NSMutableDictionary *valuedataDictionary=[[NSMutableDictionary alloc]init];
        //            [valuedataDictionary setObject:stringVlaue forKey:@"value"];
        //            return valuedataDictionary;
        //        }
        return dataDictionary;
    }
    else
    {
        return Nil;
    }
    
    
}


-(NSDictionary*)postUploadWithError:(NSString*)filename
                     FileType:(NSString*)filetype
                     FileExtension:(NSString*)fileextension
                     FileData:(NSData*)FileData
                     ForWebServiceName:resourceName
                     Error:(NSError**)error
{
    
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",self.cWebServiceURL,resourceName]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        
        //start boundry
//        NSString *boundary = @"---------------------------14737809831466499882746641449";
     NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"false" forHTTPHeaderField:@"cache"];
        [request addValue:@"false" forHTTPHeaderField:@"processData"];
        //[request addValue:[NSData dataWithData:FileData] forHTTPHeaderField:@"data"];
    
        NSMutableData *body = [NSMutableData data];
        
        // image/video data
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[@"Content-Disposition: form-data; name=\"userfile\"; filename=\"Test.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.%@\"\r\n",filetype ,filename ,fileextension] dataUsingEncoding:NSUTF8StringEncoding]];

        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: multipart/form-data; name=\"databases\"; filename=\"%@.%@\"\r\n",filename,fileextension] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Type: %@/%@\r\n\r\n", filetype,fileextension ]dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[NSData dataWithData:FileData]];
        
       [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
     [request setHTTPBody:body];
    
    //[request  addValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
    
    
//            [request addValue:@"multipart/form-data" forHTTPHeaderField:@"enctype"];
//            [request addValue:false forHTTPHeaderField:@"Content-Type"];
//            [request addValue:false forHTTPHeaderField:@"cache"];
//            [request addValue:false forHTTPHeaderField:@"processData"];
//            [request  setHTTPBody:FileData];
    
        NSURLResponse *response;
        NSData* data =[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:error];

    if (data != Nil)
    {
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:Nil];
//        if (dataDictionary == Nil) {
//            NSString *stringVlaue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSMutableDictionary *valuedataDictionary=[[NSMutableDictionary alloc]init];
//            [valuedataDictionary setObject:stringVlaue forKey:@"value"];
//            return valuedataDictionary;
//        }
        return dataDictionary;
    }
    else
    {
        return Nil;
    }
}



@end
