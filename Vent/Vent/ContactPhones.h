//
//  ContactPhones.h
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Contacts;

@interface ContactPhones : NSManagedObject

@property (nonatomic, retain) NSNumber * phoneIsVentNumber;
@property (nonatomic, retain) NSString * phoneNumber;

@property (nonatomic, retain) Contacts *phoneContact;

@end
