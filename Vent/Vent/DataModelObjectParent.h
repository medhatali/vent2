//
//  DataModelObjectParent.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataModelObjectParent : NSObject

+ (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary;

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields;

- (NSString *)uniqueUUID;

- (NSString *)convertDateToString:(NSDate *)date;
- (NSDate *)convertStringToDate:(NSString *)dateObject;

@end
