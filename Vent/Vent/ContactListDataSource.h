//
//  ContactListDataSource.h
//  Vent
//
//  Created by Sameh Farouk on 3/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactListDataSource : NSObject
@property BOOL allContacts;

- (NSArray *)contacts;


@end
