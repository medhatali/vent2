//
//  UploadDownLoadBussinessService.m
//  Vent
//
//  Created by Medhat Ali on 3/9/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "UploadDownLoadBussinessService.h"
#import "FilesWebService.h"

@implementation UploadDownLoadBussinessService

- (NSString *)uploadFileWithType:(UploadFileType)uploadFileType FileData:(NSData *)file fileName:(NSString *)fileName FileExt:(NSString *)fileExt Error:(NSError**)error{
    if ([self.reachbilityInstance isReachable]) {
        
        // get from Webservice
        
        FilesWebService * objServiceInstance=[[FilesWebService alloc]init];
        if (uploadFileType == KVideo) {
            NSDictionary *returnedDic= [objServiceInstance uploadVideo:file VideoName:fileName VideoExt:fileExt Error:error];
            
            
            if (*error == Nil && returnedDic == Nil) {
                
                return Nil;
                
            }
            else
            {
                NSString *resut = [NSString stringWithFormat:@ "%@", returnedDic];
                return  resut;
            }
            
        }else if(uploadFileType == KImage){
            NSDictionary *returnedDic= [objServiceInstance uploadImage:file ImageName:fileName ImageExt:fileExt Error:error];
            
            
            if (*error == Nil && returnedDic == Nil) {
                
                return Nil;
                
            }
            else
            {
                NSString *resut = [NSString stringWithFormat:@ "%@", returnedDic];
                return  resut;
            }

        }
        
    }
    
    return FALSE;
    
}

- (NSString *)uploadImage:(NSData *)image ImageName:(NSString *)imageName ImageExt:(NSString *)imageExt Error:(NSError**)error{
    
    if ([self.reachbilityInstance isReachable]) {
        
        // get from Webservice
        
        FilesWebService * objServiceInstance=[[FilesWebService alloc]init];
        NSDictionary *returnedDic= [objServiceInstance uploadImage:image ImageName:imageName ImageExt:imageExt Error:error];
        
        
        if (*error == Nil && returnedDic == Nil) {
            
            return Nil;
            
        }
        else
        {
            NSString *resut = [NSString stringWithFormat:@ "%@", returnedDic];
            return  resut;
        }
        
        
        
    }
    
    return FALSE;
    
}

- (NSString *)uploadVideo:(NSData *)video VideoName:(NSString *)videoName VideoExt:(NSString *)videoExt Error:(NSError**)error{
    
    if ([self.reachbilityInstance isReachable]) {
        
        // get from Webservice
        
        FilesWebService * objServiceInstance=[[FilesWebService alloc]init];
        NSDictionary *returnedDic= [objServiceInstance uploadVideo:video VideoName:videoName VideoExt:videoExt Error:error];
        
        
        if (*error == Nil && returnedDic == Nil) {
            
            return Nil;
            
        }
        else
        {
            NSString *resut = [NSString stringWithFormat:@ "%@", returnedDic];
            return  resut;
        }
        
        
        
    }
    
    return FALSE;
    
}

- (NSData *)downLoadFile:(NSString *)fileName Error:(NSError**)error{
    if ([self.reachbilityInstance isReachable]) {
        
        // get from Webservice
        
        FilesWebService * objServiceInstance=[[FilesWebService alloc]init];
        
        NSData *returned= [objServiceInstance downLoadFile:fileName Type:File Error:error];
        
        return returned;
    }
    
    return FALSE;
}

- (NSData *)downLoadThumbnail:(NSString *)fileName Error:(NSError**)error{
    if ([self.reachbilityInstance isReachable]) {
        
        // get from Webservice
        
        FilesWebService * objServiceInstance=[[FilesWebService alloc]init];
        
        NSData *returned= [objServiceInstance downLoadFile:fileName Type:Thumbnail Error:error];
        
        return returned;
    }
    
    return FALSE;
}


@end
