//
//  GroupList.m
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupList.h"
#import "ChatSession.h"
#import "GroupListUsersPhones.h"
#import "User.h"


@implementation GroupList

@dynamic groupEnabled;
@dynamic groupID;
@dynamic groupImage;
@dynamic groupImageUrl;
@dynamic groupLastMessage;
@dynamic groupLastUpdatedDate;
@dynamic groupMuted;
@dynamic groupName;
@dynamic groupSequence;
@dynamic groupStartDate;
@dynamic groupIsBlocked;
@dynamic groupListOwner;
@dynamic groupListSession;
@dynamic groupListUsers;

@end
