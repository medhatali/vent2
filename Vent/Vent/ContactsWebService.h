//
//  ContactsWebService.h
//  Vent
//
//  Created by Medhat Ali on 3/10/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "CommonWebService.h"

#define ContactIDString @"ContactID"
#define ContactNameString @"ContactName"
#define PhoneNumbersString @"PhoneNumbers"
#define ContactImageString @"ContactImage"

@interface ContactsWebService : CommonWebService

- (NSMutableArray *)sendContacts:(NSMutableArray *)contacts phoneNumber:(NSString *)phoneNumber deviceToken:(NSString *)deviceToken Error:(NSError**)error;

@end
