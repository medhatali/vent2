//
//  GroupsWebService.m
//  Vent
//
//  Created by Sameh Farouk on 4/20/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupsWebService.h"
#import "ContactsBussinessService.h"
#import "NSString+URLEncoding.h"

@implementation GroupsWebService

- (id)init
{
    self = [super init];
    if (self) {
        self.serviceName = @"/api/Group/Create";
        self.odataserviceName = @"";
        self.odataserviceNameWithFilter = @"";
    }
    return self;
}

- (bool)creatGroup:(GroupDM *)group Error:(NSError**)error
{
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",self.serviceName];
    
    NSMutableDictionary *sentDict = [[NSMutableDictionary alloc]initWithDictionary:[group createApiDictionaryFromData:YES]];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    NSString * phoneNumber = currentUser.userPhoneNumber;
    
    NSDictionary *header = @{
                                  @"PhoneNumber" : phoneNumber,
                                  @"DeviceToken" : currentUser.userDeviceUUID,
                                  };
    
    
    
    NSInteger result = [self.caller postDataAndGetStatusCode:sentDict ToWebserviceForResource:serviceURL WithHTTPHeader:header Error:error];
    
    
    return (result == 200);
}

@end
