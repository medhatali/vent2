//
//  BroadcastListUserPhones.m
//  Vent
//
//  Created by Medhat Ali on 6/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BroadcastListUserPhones.h"
#import "BroadcastList.h"


@implementation BroadcastListUserPhones

@dynamic broadcastUserPhone;
@dynamic broadcastUserList;

@end
