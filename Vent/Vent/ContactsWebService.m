//
//  ContactsWebService.m
//  Vent
//
//  Created by Medhat Ali on 3/10/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "ContactsWebService.h"

@implementation ContactsWebService

- (id)init
{
    self = [super init];
    if (self) {
        self.serviceName = @"/api/SaveContacts";
        self.odataserviceName = @"";
        self.odataserviceNameWithFilter = @"";
    }
    return self;
}

- (NSMutableArray *)sendContacts:(NSMutableArray *)contacts phoneNumber:(NSString *)phoneNumber deviceToken:(NSString *)deviceToken Error:(NSError**)error
{
    
        NSString *serviceURL = [NSString stringWithFormat:@"%@%@",self.serviceName,@""];

    //NSMutableDictionary * contactdictionary = [[NSMutableDictionary alloc]initWithObjects:contacts forKeys:Nil];
       // NSMutableDictionary *sentDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:contacts, nil];
    
        NSDictionary *result = [self.caller postDataForContacts:contacts ToWebserviceForResource:serviceURL phoneNumber:phoneNumber deviceToken:deviceToken Error:error];
    
    
        return (NSMutableArray *)result;
    

}


@end
