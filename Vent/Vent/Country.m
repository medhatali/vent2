//
//  Country.m
//  Smiley
//
//  Created by Sameh Farouk on 6/3/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import "Country.h"
#import "CountryListDataSource.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@implementation Country

-(id)initWithDefaultValue{
    NSArray* _countriesAr = [[[CountryListDataSource alloc]init] countries];
    
    NSString* _countryIsoCode = [self getIosCountryCodeByCarrer];
    NSString   *_countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    NSPredicate *_predicate;
    if (_countryIsoCode) {
        _predicate = [NSPredicate predicateWithFormat:@"code == [c]%@", _countryIsoCode];
    }
    else
    {
        _predicate = [NSPredicate predicateWithFormat:@"code == %@", _countryCode];
    }
    NSArray* _newCountries = [_countriesAr filteredArrayUsingPredicate:_predicate];
    
        if ([_newCountries count] > 0) {
            self = [self initWithJSONNSData:[_newCountries objectAtIndex:0]];
        }
    if (!self) {
        self = [self init];
    }
    return self;
    }

-(id)initWithJSONNSData:(NSData*) json{
    [self setName:[json valueForKey:kCountryName]];
    [self setDial_code:[json valueForKey:kCountryCallingCode]];
    [self setCode:[json valueForKey:kCountryCode]];
    return self;
}

-(NSString*) getIosCountryCodeByCarrer{
    //tryTogetCountryDialCodeFromCarrier
    CTTelephonyNetworkInfo *_networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *_carrier = [_networkInfo subscriberCellularProvider];
    NSString *_isoCountryCodeStr = [_carrier isoCountryCode];
    return _isoCountryCodeStr;
}

@end
