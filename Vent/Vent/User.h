//
//  User.h
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BroadcastList, ChatList, Contacts, Favourite, GroupList;

@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * userActive;
@property (nonatomic, retain) NSString * userCountry;
@property (nonatomic, retain) NSString * userCountryCode;
@property (nonatomic, retain) NSString * userDate;
@property (nonatomic, retain) NSString * userDeviceName;
@property (nonatomic, retain) NSString * userDeviceUUID;
@property (nonatomic, retain) NSString * userDisplayName;
@property (nonatomic, retain) NSString * userEmail;
@property (nonatomic, retain) NSString * userFirstName;
@property (nonatomic, retain) NSNumber * userIsRegistered;
@property (nonatomic, retain) NSNumber * userIsSecureLogin;
@property (nonatomic, retain) NSString * userLastName;
@property (nonatomic, retain) NSString * userPhoneNumber;
@property (nonatomic, retain) NSString * userPin;
@property (nonatomic, retain) NSData * userProfileImage;
@property (nonatomic, retain) NSString * userProfileImageName;
@property (nonatomic, retain) NSString * userProfileImagePath;
@property (nonatomic, retain) NSString * userStatus;
@property (nonatomic, retain) NSNumber * userIsOnline;
@property (nonatomic, retain) NSString * userPattern;
@property (nonatomic, retain) NSNumber * userEmailVerified;
@property (nonatomic, retain) NSSet *userBroadcast;
@property (nonatomic, retain) NSSet *userChatList;
@property (nonatomic, retain) NSSet *userContatcs;
@property (nonatomic, retain) NSSet *userFavourite;
@property (nonatomic, retain) NSSet *userGroupList;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addUserBroadcastObject:(BroadcastList *)value;
- (void)removeUserBroadcastObject:(BroadcastList *)value;
- (void)addUserBroadcast:(NSSet *)values;
- (void)removeUserBroadcast:(NSSet *)values;

- (void)addUserChatListObject:(ChatList *)value;
- (void)removeUserChatListObject:(ChatList *)value;
- (void)addUserChatList:(NSSet *)values;
- (void)removeUserChatList:(NSSet *)values;

- (void)addUserContatcsObject:(Contacts *)value;
- (void)removeUserContatcsObject:(Contacts *)value;
- (void)addUserContatcs:(NSSet *)values;
- (void)removeUserContatcs:(NSSet *)values;

- (void)addUserFavouriteObject:(Favourite *)value;
- (void)removeUserFavouriteObject:(Favourite *)value;
- (void)addUserFavourite:(NSSet *)values;
- (void)removeUserFavourite:(NSSet *)values;

- (void)addUserGroupListObject:(GroupList *)value;
- (void)removeUserGroupListObject:(GroupList *)value;
- (void)addUserGroupList:(NSSet *)values;
- (void)removeUserGroupList:(NSSet *)values;

@end
