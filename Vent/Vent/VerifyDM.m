//
//  VerifyDM.m
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "VerifyDM.h"

@implementation VerifyDM

NSString *const iDeviceTokenV = @"DeviceToken";
NSString *const iPhoneNumberV = @"PhoneNumber";
NSString *const iVerificationCodeV = @"VerificationCode";


+ (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    VerifyDM *DataInfo = [[VerifyDM alloc]init];
    
    DataInfo.DeviceToken = [dataDictionary objectForKey:iDeviceTokenV];
    DataInfo.PhoneNumber = [dataDictionary objectForKey:iPhoneNumberV];
    DataInfo.VerificationCode = [dataDictionary objectForKey:iVerificationCodeV];
    
    
    return DataInfo;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    
    NSMutableArray *keysArray = [[NSMutableArray alloc]init];
    NSMutableArray *valuesArray = [[NSMutableArray alloc]init];
    
    
    if (self.DeviceToken) {
        [keysArray addObject:iDeviceTokenV];
        [valuesArray addObject:self.DeviceToken];
    }
    

    
    if (self.PhoneNumber) {
        [keysArray addObject:iPhoneNumberV];
        [valuesArray addObject:self.PhoneNumber];
    }
    
    if (self.VerificationCode) {
        [keysArray addObject:iVerificationCodeV];
        [valuesArray addObject:self.VerificationCode];
    }
    

    
    
    NSDictionary *dictionary = [[NSMutableDictionary alloc]initWithObjects:valuesArray forKeys:keysArray];
    
    return dictionary;
}



@end
