//
//  BaseSearchTableViewController.h
//  Vent
//
//  Created by Sameh Farouk on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchTableViewControllerDelegate.h"
#import "ResultsSearchTableViewController.h"

@interface BaseSearchTableViewController : UITableViewController

@property (nonatomic, assign) id<BaseSearchTableViewControllerDelegate>delegate;
@property (nonatomic, copy) NSArray *entries;
// our secondary search results table view
@property (nonatomic, strong) ResultsSearchTableViewController *resultsTableController;
@property (nonatomic, strong) UISearchController *searchController;

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate;

@end
