//
//  BroadcastTableViewController.h
//  Vent
//
//  Created by Medhat Ali on 7/30/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BroadcastRepository.h"

@interface BroadcastTableViewController : UITableViewController

@property (nonatomic, retain) NSArray *broadCastList;
@property (nonatomic,strong) BroadcastRepository * broadcastListRepo;
@property  (nonatomic,strong)NSMutableArray *selectedModelToUpdate;
@property (weak, nonatomic) BroadcastList *selectedBroadcastList;

@property BOOL inUpdateMode;
@end
