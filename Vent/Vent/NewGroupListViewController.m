//
//  NewGroupListViewController.m
//  Vent
//
//  Created by Medhat Ali on 4/15/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "NewGroupListViewController.h"
#import "MainContactsTableView.h"
#import "Contacts.h"
#import "ContactsRepository.h"
#import "GroupListRepository.h"
#import "ContactsBussinessService.h"
#import "GroupDM.h"
#import "GroupsWebService.h"
#import "UploadDownLoadBussinessService.h"
#import "GroupList.h"
#import "GlobalHandler.h"

@interface NewGroupListViewController () <UIGestureRecognizerDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ContactListDelegate>

@property BOOL updateUserData;
@property (strong, nonatomic)  NSData *groupImage;
@property (strong, nonatomic)  NSMutableArray *grouContactList;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneBtn;
@property (strong, nonatomic) Contacts *adminContacts;
@property (weak, nonatomic) IBOutlet UISwitch *groupMuteCheck;



@end

@implementation NewGroupListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.updateUserData = FALSE;
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    self.currentUser=[contactBWS getCurrentUserProfile];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    tapGestureRecognizer.delegate  = self;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    
    [[[self goupImageBtn] layer] setCornerRadius: self.goupImageBtn.frame.size.width / 2 ];
    [[[self goupImageBtn] layer] setBorderWidth: 3.0f];
    [[[self goupImageBtn] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [[self goupImageBtn] setClipsToBounds: YES];
    
    
    self.grouContactList =[[NSMutableArray alloc]init];
    [self.tableView setEditing:YES animated:YES];
    
    
    if ([self.selectedGroupList.groupMuted isEqualToNumber:[NSNumber numberWithBool:YES]]) {
        
        [self.groupMuteCheck setOn:YES];
    }
    else
    {
        [self.groupMuteCheck setOn:NO];
    }
    UISwipeGestureRecognizer *tapRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dissmissKeyboard)];
    
    [tapRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
    [tapRecognizer setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
    
    
    if (self.UpdateGroup) {
        self.txtGroupName.text = self.selectedGroupList.groupName;
        self.title = self.selectedGroupList.groupName;
        [self.doneBtn setTitle:[NSString stringWithFormat:NSLocalizedString(@"Update", nil)]];
        
        UIImage *profile=[UIImage imageWithData:self.selectedGroupList.groupImage];
        if (profile != nil) {
            [self.goupImageBtn setBackgroundImage:profile forState:UIControlStateNormal];
        }
        
        
        ContactsRepository *repo=[[ContactsRepository alloc]init];
        NSError *error;
        [self.grouContactList removeAllObjects];
        
        for (GroupListUsersPhones *groupMember in self.selectedGroupList.groupListUsers) {
            
            Contacts *memberContacts=[repo getUserContactByPhoneNumber:groupMember.groupUserPhone contactUser:self.currentUser Error:&error];
            
            if (memberContacts) {
                [self.grouContactList addObject:memberContacts];
            }
            else
            {
                memberContacts =[[Contacts alloc]init];
                memberContacts.contactFullName =groupMember.groupUserPhone;
                memberContacts.contactFirstName =groupMember.groupUserPhone;
                 [self.grouContactList addObject:memberContacts];
                
            }
            
            if ([groupMember.groupUserIsAdmin isEqualToNumber:[NSNumber numberWithBool:TRUE]]) {
                self.adminContacts =memberContacts;
            }
            
        }
        
        [self.tableView reloadData];
        
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return self.grouContactList.count;
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"groupContactCell" forIndexPath:indexPath];

   
    Contacts *selected=(Contacts *)[self.grouContactList objectAtIndex:indexPath.row];
    cell.textLabel.text =selected.contactFullName;
    cell.imageView.image =[UIImage imageNamed:@"Profile.png"];
    
   
    if ([selected.contactFullName isEqualToString:self.adminContacts.contactFullName]) {
        cell.detailTextLabel.text =[NSString stringWithFormat:NSLocalizedString(@"Admin", nil)] ;
    }
    else
    {
        cell.detailTextLabel.text = @"";
    }
    
    
    
    [[[cell imageView] layer] setCornerRadius: 20];
    [[[cell imageView] layer] setBorderWidth: 3.0f];
    [[[cell imageView] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [[cell imageView] setClipsToBounds: YES];
    
    
    
    return cell;
    
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.

    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.grouContactList removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



-(IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)groupImageClick:(id)sender {
    
    UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)],[NSString stringWithFormat:NSLocalizedString(@"Choose Existing", nil)] , nil];
    
    [_actionSheet showInView:[self view]];
    
}



- (IBAction)doneClick:(id)sender {
    
    if ([self.txtGroupName.text isEqualToString:@""]) {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Empty Group name", nil)]
                                                              message:[NSString stringWithFormat:NSLocalizedString(@"Please insert group name", nil)]
                                                             delegate:nil
                                                    cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        return;
    }
    
    if (self.txtGroupName.text.length > 30) {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Group name length", nil)]
                                                              message:[NSString stringWithFormat:NSLocalizedString(@"Group name is too long", nil)]
                                                             delegate:nil
                                                    cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        return;
    }
    
    if (self.grouContactList.count < 1 || self.grouContactList == nil) {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"No Contacts selected", nil)]
                                                              message:[NSString stringWithFormat:NSLocalizedString(@"Please select Contacts to group", nil)]
                                                             delegate:nil
                                                    cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        return;
    }
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.reachbilityInstance =appDelegate.reachabilityApp;
    
    if ([self.reachbilityInstance isReachable]) {
        
    NSError *error;
    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
    NSMutableArray *VentPhoneList=[[NSMutableArray alloc]init];
    GroupListRepository* grouplistRepo=[[GroupListRepository alloc]init];
    NSString *currentdate=[NSString stringWithFormat:@"%@" ,[NSDate date]];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
        
        for (Contacts *selectedCont in self.grouContactList) {
            NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:selectedCont Error:&error];
            
            NSLog(@"contact selection %@",ventphonenumber);
            [VentPhoneList addObject:ventphonenumber];
        }
        
        NSMutableArray *phonelist=[[NSMutableArray alloc]initWithArray:VentPhoneList];

    
    GroupsWebService *groupServiceObject = [GroupsWebService new];
    UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
        
        
    
    
 if (self.UpdateGroup) {
     // update current group
     
     self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Updating Group", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
     
     dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
     
     dispatch_async(taskQ,
                    ^{
                        dispatch_async(dispatch_get_main_queue(),
                                       ^{
                                           [self.loadingView showLoadingViewAnimated:YES];
                                       });
                        
//                        NSError *error;
                        
                        // upload group icon to server
//                        NSString *groupIconPath = [fileServiceObject uploadImage:self.groupImage ImageName:@"file" ImageExt:@"png" Error:&error];
//                        
//                        // create group online and local DB
//                        GroupDM *groupDM = [[GroupDM alloc]init];
//                        groupDM.GroupTitle =self.txtGroupName.text;
//                        groupDM.MembersPhoneNumber = [phonelist copy];
//                        groupDM.AdminPhoneNumber = [currentUser userPhoneNumber];
//                        groupDM.GroupIconPath = groupIconPath;
//                        groupDM.GroupId = groupID;
                        
                        
//                        bool success=[groupServiceObject creatGroup:groupDM Error:&error];
                        
                        dispatch_async(dispatch_get_main_queue(),
                                       ^{
                                           [self.loadingView hideLoadingViewAnimated:YES];
                                       });
//                        
//                        if (success) {
//                            
//                            [grouplistRepo addNewGroupList:@"" groupName:self.txtGroupName.text groupStartDate:currentdate groupEnabled:[NSNumber numberWithBool:YES] groupMuted:[NSNumber numberWithBool:NO] groupLastUpdatedDate:currentdate groupSequence:[NSNumber numberWithInt:0] groupImage:self.groupImage groupImageUrl:@"" groupLastMessage:@"" groupListOwner:currentUser groupListSession:Nil groupListUsers:phonelist Error:&error];
//                            dispatch_async(dispatch_get_main_queue(),
//                                           ^{
//                                               [self.navigationController popViewControllerAnimated:YES];
//                                           });
//                        }
//                        else
//                        {
//                            dispatch_async(dispatch_get_main_queue(),
//                                           ^{
//                                               [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Create Group Error", nil)message:NSLocalizedString(@"error in Create Group", nil)delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
//                                               
//                                           });
//                            
//                        }
//                        
                        
                    });

     
     
 }
    else
    {
        
    // create new group
    
    NSString *groupID = [[NSUUID UUID] UUIDString];
    // create group online and local DB

    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Creating Group", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       NSError *error;
                       
                       // upload group icon to server
                       NSString *groupIconPath;
                       if (self.groupImage) {
                           groupIconPath = [fileServiceObject uploadImage:self.groupImage ImageName:@"file" ImageExt:@"png" Error:&error];
                           

                       }
                                             // create group online and local DB
                       GroupDM *groupDM = [[GroupDM alloc]init];
                       groupDM.GroupTitle =self.txtGroupName.text;
                       [phonelist addObject:currentUser.userPhoneNumber];
                       groupDM.MembersPhoneNumber = [phonelist copy];
                       
                       groupDM.AdminPhoneNumber = [currentUser userPhoneNumber];
                       groupDM.GroupIconPath = groupIconPath;
                       groupDM.GroupId = groupID;
                       
                       
                       bool success=[groupServiceObject creatGroup:groupDM Error:&error];
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                      });
                   
                       if (success) {
                           
                           [grouplistRepo addNewGroupList:groupDM.GroupId groupName:self.txtGroupName.text groupStartDate:currentdate groupEnabled:[NSNumber numberWithBool:YES] groupMuted:[NSNumber numberWithBool:NO] groupLastUpdatedDate:currentdate groupSequence:[NSNumber numberWithInt:0] groupImage:self.groupImage groupImageUrl:@"" groupLastMessage:@"" groupListOwner:currentUser groupListSession:Nil groupListUsers:phonelist AdminPhoneNumber:groupDM.AdminPhoneNumber Error:&error];
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              [self.navigationController popViewControllerAnimated:YES];
                                          });
                       }
                       else
                       {
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                           [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Create Group Error", nil)message:NSLocalizedString(@"error in Create Group", nil)delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
                                              
                                              });
                           
                       }
                       
                       
                   });

    }
    
    
    }
    else
    {
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connection Error", nil)
                                    message:NSLocalizedString(@"Please check you internet Connection", nil)
                                   delegate:self
                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                          otherButtonTitles:nil] show];
        
    }
    
    
    
}

#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UIImagePickerController *_picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = YES;
    
    switch (buttonIndex) {
        case 0:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Device has no camera", nil)]
                                                                     delegate:nil
                                                            cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            else{
                
                _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:_picker animated:YES completion:NULL];
                
            }
            break;
        case 1:
            _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:_picker animated:YES completion:NULL];
            break;
        default:
            break;
    }
}

#pragma mark - Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *_chosenImage = info[UIImagePickerControllerEditedImage];
    self.groupImage=UIImagePNGRepresentation(_chosenImage);
    
    self.updateUserData = TRUE;
    [[self goupImageBtn] setBackgroundImage:_chosenImage forState:UIControlStateNormal];
    [[self goupImageBtn] setTitle:nil forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - BaseSearchSelectionDelegate

- (SelectionType)selectionType{
    
    return kMultipleSelect; //kSingleSelect;
}

#pragma mark - Contact Selection

- (IBAction)addContactClick:(id)sender {
    
    ContactsBussinessService *contactBWS=[[ContactsBussinessService alloc]init];
    if (![contactBWS isContatcsAccessGrantInDispath]) {
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           
                           
                           UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                           cantLoadContactsAlert.tag =100;
                           [cantLoadContactsAlert show];
                           
                           
                           
                       });
        
        return;
    }
    
    MainContactsTableView *contactView =[[MainContactsTableView alloc]initWithNibName:@"ContactsTableView" delegate:self];
    
    [[contactView navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Contacts", nil)]];
    //   [[self navigationController] pushViewController:contactView animated:YES];
    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactView];
    
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self showDetailViewController:navController sender:self];
    
}

-(void)didSelectContacts:(Contacts *)contacts
{
//    NSError *error;
//    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
//    
//    NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:contacts Error:&error];
//    
//    NSLog(@"contact selection %@",ventphonenumber);
//    
//    
//    ChatListRepository * chatlistRepo=[[ChatListRepository alloc]init];
//    
//    NSArray *phonelist=[[NSArray alloc]initWithObjects:ventphonenumber, nil];
//    NSString *currentdate=[NSString stringWithFormat:@"%@" ,[NSDate date]];
//    
//    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
//    User* currentUser=[contactBWS getCurrentUserProfile];
//    
//    
//    [chatlistRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Chat" chatLastUpdatedDate:currentdate  chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:NO] chatID:@"" chatName:contacts.contactFullName chatLastMessage:@"" chatUserImage:contacts.contactImage chatUserImageUrl:@"" chatIsFavourite:[NSNumber numberWithBool:NO]  chatListOwner:currentUser chatListSession:Nil chatListUsers:phonelist Error:&error];
//    
//    [self reloadData];
}

-(void)didSelectContactsList:(NSArray *)contactsList
{
    NSLog(@"contacts list selection for conversation ");
    
    for (Contacts *selectedCont in contactsList) {
        [self.grouContactList addObject:selectedCont];
    }
    

    [self.tableView reloadData];
}


- (IBAction)groupMuteClick:(id)sender {
    
     GroupListRepository* grouplistRepo=[[GroupListRepository alloc]init];
    
    if ([self.selectedGroupList.groupMuted isEqualToNumber:[NSNumber numberWithBool:YES]]) {
        
        [self.groupMuteCheck setOn:NO];
        self.selectedGroupList.groupMuted =[NSNumber numberWithBool:NO];
    }
    else
    {
        [self.groupMuteCheck setOn:YES];
        self.selectedGroupList.groupMuted =[NSNumber numberWithBool:YES];
    }
    
    [grouplistRepo updateGroupList:self.selectedGroupList Error:nil];
    
}


- (IBAction)leaveGroupClick:(id)sender {
    // call signal r to leave group
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Leave Group", nil)]
                                                    message:[NSString stringWithFormat:NSLocalizedString(@"Are you sure you want ot leave this group", nil)]
                                                   delegate:self
                                          cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"No", nil)]
                                          otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Yes", nil)], nil];
    [alert show];
    
    

    
}




#pragma UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1 && alertView.tag == 100) {
        [self openSettings];
        return;
    }
    
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
            
        case 1: //"Yes" pressed
            
            [GlobalHandler leaveGroup:self.selectedGroupList.groupID];
            self.tabBarController.tabBar.hidden = NO;
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            break;
    }
}

- (void)openSettings
{
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)dissmissKeyboard
{
    [self.view endEditing:YES];
}


@end
