//
//  FavouriteRepository.m
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "FavouriteRepository.h"

@implementation FavouriteRepository

- (id)init
{
    self = [super init];
    if (self) {
        self.repositoryEntityName=@"Favourite";
        
    }
    return self;
}


-(Favourite *) addNewFavourite:(NSString*)favouriteID
                     favouriteType:(NSString*)favouriteType
                favouriteChatListID:(NSString*)favouriteChatListID
                  favouriteGroupListID:(NSString*)favouriteGroupListID
                    favouriteContactID:(NSString*)favouriteContactID
                         favouriteOwner:(User*)favouriteOwner
                         Error:(NSError **)error
{
    
    NSManagedObject *newManagedObject = [self createModelForCurrentEntity:error];
    
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:favouriteID forKey:@"favouriteID"];
    [newManagedObject setValue:favouriteType forKey:@"favouriteType"];
    [newManagedObject setValue:favouriteChatListID forKey:@"favouriteChatListID"];
    [newManagedObject setValue:favouriteGroupListID forKey:@"favouriteGroupListID"];
    
    [newManagedObject setValue:favouriteContactID forKey:@"favouriteContactID"];

    
    [newManagedObject setValue:favouriteOwner forKey:@"favouriteOwner"];
    
 
    [self insertModel:newManagedObject EntityName:self.repositoryEntityName Error:error];
    
    
    
    
    return (Favourite *)newManagedObject;
    
    
}


-(void)deleteFavourite:(Favourite *)favourite Error:(NSError **)error
{
    
    [self deleteModel:favourite EntityName:self.repositoryEntityName Error:error];
    
}

-(void)updateGroupList:(Favourite *)favourite Error:(NSError **)error
{
    
    [self updateModel:favourite EntityName:self.repositoryEntityName Error:error];
    
}



@end
