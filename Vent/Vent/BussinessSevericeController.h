//
//  BussinessSevericeController.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonHeader.h"
#import "User.h"
#import "UserRepository.h"

@interface BussinessSevericeController : NSObject

@property (retain,nonatomic) Reachability *reachbilityInstance;

-(User *)getCurrentUserProfile;


@end
