//
//  VentChatDataModel.h
//  Vent
//
//  Created by Medhat Ali on 9/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "JSQMessages.h"
#import "ChatList.h"

@interface VentChatDataModel : NSObject<CLLocationManagerDelegate>

@property (strong, nonatomic) NSMutableArray *messages;

@property (strong, nonatomic) NSDictionary *avatars;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (strong, nonatomic) NSDictionary *users;

@property (strong, nonatomic) NSString *senderAvatarID;
@property (strong, nonatomic) NSString *senderAvatarName;
@property (strong, nonatomic) NSString *recieverAvatarID;
@property (strong, nonatomic) NSString *recieverAvatarName;

@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic)  CLLocationManager *locationManager;

@property (strong, nonatomic) id parentVC;


- (instancetype)initWithSenderAndReciever:(NSString*)senderId SenderName:(NSString*)senderName SenderImage:(NSData*)senderImage
                                ReciverId:(NSString*)reciverId ReciverName:(NSString*)reciverName ReciverImage:(NSData*)reciverImage;

- (BOOL)ConvertChatSessionToModel:(NSArray*)chatsession;

#pragma mark messages to add with types from Me

- (void)addTextMessageFromMe:(NSString*)text Date:(NSDate*)date MsgId:(NSString*)msgId;

- (void)addPhotoMediaMessageFromMe:(NSString*)imageLocalPath MsgId:(NSString*)msgId;

- (void)addVideoMediaMessageFromMe:(NSString*)videoLocalPath MsgId:(NSString*)msgId;

- (BOOL)addLocationMediaMessageFromMe:(double)latitude Longitude:(double)longitude MsgId:(NSString*)msgId;

- (void)addVcardMediaMessageFromMe:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard MsgId:(NSString*)msgId;

#pragma mark messages to add with types from Server

- (void)addTextMessageFromServer:(NSString*)text Date:(NSDate*)date MsgId:(NSString*)msgId;
- (void)addPhotoMediaMessageFromServer:(NSString*)imageLocalPath MsgId:(NSString*)msgId;


- (void)addVideoMediaMessageFromServer:(NSString*)videoLocalPath MsgId:(NSString*)msgId;

- (void)addLocationMediaMessageFromServer:(double)latitude Longitude:(double)longitude MsgId:(NSString*)msgId;

- (void)addVcardMediaMessageFromServer:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard MsgId:(NSString*)msgId;


- (void)addTextMessageFromServer:(NSString*)text Date:(NSDate*)date MsgId:(NSString*)msgId SenderName:(NSString*)sendName;

- (void)addPhotoMediaMessageFromServer:(NSString*)imageLocalPath MsgId:(NSString*)msgId SenderName:(NSString*)sendName;
- (void)addVideoMediaMessageFromServer:(NSString*)videoLocalPath MsgId:(NSString*)msgId SenderName:(NSString*)sendName;
- (void)addLocationMediaMessageFromServer:(double)latitude Longitude:(double)longitude MsgId:(NSString*)msgId SenderName:(NSString*)sendName;
- (void)addVcardMediaMessageFromServer:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard MsgId:(NSString*)msgId SenderName:(NSString*)sendName;



-(void)stopUpdatingLocation;
@end
