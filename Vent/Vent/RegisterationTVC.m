//
//  RegisterationTVC.m
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "RegisterationTVC.h"


@interface RegisterationTVC ()



@end

@implementation RegisterationTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //reachability handler
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.reachbilityInstance =appDelegate.reachabilityApp;
    
    if (![self.reachbilityInstance isReachable]) {
        
        [self createOfflineView];
        [self.tableView setTableHeaderView:self.offlineView];
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[[self navigationController]navigationBar] setHidden:FALSE];
    [self.navigationItem.rightBarButtonItem setTitle:[NSString stringWithFormat:NSLocalizedString(@"Next", nil)]];
    UIOffset backButtonTextOffset = UIOffsetMake(0, -60);
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:backButtonTextOffset
                                                         forBarMetrics:UIBarMetricsDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showConnectionDown
{

        UIAlertView *_alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"Connection Down", nil)] delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"ok", nil)]  otherButtonTitles:nil];
        [_alert show];
    
}

-(void) createOfflineView
{
    UIView *transView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, self.tableView.frame.size.width, 30)];
    UILabel *viewLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0.f, self.view.frame.size.width , 30)];
    [viewLbl setTextColor:[UIColor whiteColor]];
    [viewLbl setText:NSLocalizedString(@"No Connection",Nil)];
    [viewLbl setTextAlignment:NSTextAlignmentCenter];
    [transView addSubview:viewLbl];
    
       
//    UIButton *refreshBtn=[[UIButton alloc]initWithFrame:CGRectMake(5, 5, 30, 30)];
//    [refreshBtn setBackgroundImage:[UIImage imageNamed:@"refresh.png"] forState:UIControlStateNormal];
//    [refreshBtn addTarget:self action:@selector(checkConnectivity:) forControlEvents:UIControlEventTouchUpInside];
//    [transView addSubview:refreshBtn];
    transView.backgroundColor = [UIColor grayColor];
    self.offlineView = transView;
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
