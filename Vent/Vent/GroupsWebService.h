//
//  GroupsWebService.h
//  Vent
//
//  Created by Sameh Farouk on 4/20/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "CommonWebService.h"
#import "GroupDM.h"

@interface GroupsWebService : CommonWebService

- (bool)creatGroup:(GroupDM *)group Error:(NSError**)error;

@end
