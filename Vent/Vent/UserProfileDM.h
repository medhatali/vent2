//
//  UserProfile.h
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "DataModelObjectParent.h"

@interface UserProfileDM : DataModelObjectParent

/// properties for data retrieved
@property (nonatomic, strong) NSString *FirstName;
@property (nonatomic, strong) NSString *LastName;
@property (nonatomic, strong) NSString *Status;
@property (nonatomic, strong) NSString *ImagePath;
@property (nonatomic, strong) NSString *DateOfBirth;
@property (nonatomic, strong) NSString *PhoneNumber;



@end
