//
//  GroupDM.m
//  Vent
//
//  Created by Sameh Farouk on 4/20/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupDM.h"

@implementation GroupDM

NSString *const gGroupTitle = @"GroupTitle";
NSString *const gMembersPhoneNumber = @"MembersPhoneNumber";
NSString *const gAdminPhoneNumber = @"AdminPhoneNumber";
NSString *const gGroupIconPath = @"GroupIconPath";
NSString *const gGroupId = @"GroupId";


+ (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    GroupDM *group = [[GroupDM alloc]init];
    
    group.GroupTitle = [dataDictionary objectForKey:gGroupTitle];
    group.MembersPhoneNumber = [dataDictionary objectForKey:gMembersPhoneNumber];
    group.AdminPhoneNumber = [dataDictionary objectForKey:gAdminPhoneNumber];
    group.GroupIconPath = [dataDictionary objectForKey:gGroupIconPath];
    group.GroupId = [dataDictionary objectForKey:gGroupId];
    
    return group;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    
    NSMutableArray *keysArray = [[NSMutableArray alloc]init];
    NSMutableArray *valuesArray = [[NSMutableArray alloc]init];
    
    
    if (self.GroupId) {
        [keysArray addObject:gGroupTitle];
        [valuesArray addObject:self.GroupTitle];
    }
    
    if (self.MembersPhoneNumber) {
        [keysArray addObject:gMembersPhoneNumber];
        [valuesArray addObject:self.MembersPhoneNumber];
    }
    
    if (self.AdminPhoneNumber) {
        [keysArray addObject:gAdminPhoneNumber];
        [valuesArray addObject:self.AdminPhoneNumber];
    }
    
    if (self.GroupIconPath) {
        [keysArray addObject:gGroupIconPath];
        [valuesArray addObject:self.GroupIconPath];
    }
    
    if (self.GroupId) {
        [keysArray addObject:gGroupId];
        [valuesArray addObject:self.GroupId];
    }
    
    NSDictionary *dictionary = [[NSMutableDictionary alloc]initWithObjects:valuesArray forKeys:keysArray];
    
    return dictionary;
}


@end
