//
//  PushNotifiactionHandler.h
//  Vent
//
//  Created by Medhat Ali on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CommonHeader.h"

@interface PushNotifiactionHandler : NSObject

-(void)initPushNotification:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
-(void) handleRemoteNotification:(NSDictionary *)userInfo;
-(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;


@end
