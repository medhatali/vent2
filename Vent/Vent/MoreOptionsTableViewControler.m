//
//  MoreOptionsTableViewControler.m
//  Vent
//
//  Created by Medhat Ali on 6/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "MoreOptionsTableViewControler.h"
#import "THPinViewController.h"
#import "ContactsBussinessService.h"


@interface MoreOptionsTableViewControler () <THPinViewControllerDelegate>
@property (nonatomic, strong) UIImageView *secretContentView;
@property (nonatomic, strong) UIButton *loginLogoutButton;
@property (nonatomic, copy) NSString *correctPin;
@property (nonatomic, assign) NSUInteger remainingPinEntries;
@property (nonatomic, assign) BOOL locked;
@property (strong, nonatomic) IBOutlet UISwitch *checkSecure;
@property (strong,nonatomic)  User* currentUser;
@property (weak, nonatomic) IBOutlet UILabel *secureTimeLimit;


@property BOOL isSecureValidate;

@end

@implementation MoreOptionsTableViewControler

static const NSUInteger THNumberOfPinEntries = 5;


-(void)viewWillAppear:(BOOL)animated
{
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    self.currentUser=[contactBWS getCurrentUserProfile];
    
    if ([self.currentUser.userIsSecureLogin boolValue] == YES) {
        [self.checkSecure setOn:YES];
    }
    else
    {
        [self.checkSecure setOn:NO];
    }
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];

     [self.tabBarController.tabBar setHidden:NO];
    
    
    NSInteger securelimit = [[NSUserDefaults standardUserDefaults] integerForKey:KVentSecureTimeLimit];
    
    self.secureTimeLimit.text = [NSString stringWithFormat:@"%ld Sec" , securelimit];
  
    
    
}

#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    NSLog(@"selected row %ld in section %ld" , (long)indexPath.row , (long)indexPath.section);
    
    if (indexPath.section == 0) {
        
        if (indexPath.row ==0) {
            [self performSegueWithIdentifier:@"AboutControl" sender:self];
        }
        else if (indexPath.row ==1)
        {
            [self performSegueWithIdentifier:@"HelpControl" sender:self];
        }
        
    }
    else if (indexPath.section ==1)
    {
        
        if (indexPath.row ==0) {
           [self performSegueWithIdentifier:@"ProfileControl" sender:self];
        }
        else if (indexPath.row ==1)
        {
            //[self performSegueWithIdentifier:@"PinControl" sender:self];
            
           // [self openSecurityPasswordEntry];
            
            
        }
        else if (indexPath.row ==2)
        {
            // secure time limit
            [self performSegueWithIdentifier:@"SecureTimeLimit" sender:self];
            
            // [self openSecurityPasswordEntry];
            
            
        }
        
    }
    
    
}

#pragma mark - screen navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
//    if([segue.identifier isEqualToString:@"NewBroadcast"])
//    {
//        NewBroadCastViewController *dest=(NewBroadCastViewController *)segue.destinationViewController;
//        dest.inUpdateMode = TRUE;
//        dest.grouContactList =self.selectedModelToUpdate;
//        
//    }

//     [self performSegueWithIdentifier:@"NewBroadcast" sender:self];
    
    
}


#pragma mark - pin delegate
- (NSUInteger)pinLengthForPinViewController:(THPinViewController *)pinViewController
{
    return 4;
}

- (BOOL)pinViewController:(THPinViewController *)pinViewController isPinValid:(NSString *)pin
{
    if (!self.isSecureValidate) {
        self.currentUser.userPin = pin;
        self.currentUser.userIsSecureLogin =[NSNumber numberWithBool:YES];
        
        UserRepository *repo=[[UserRepository alloc]init];
        NSError *error;
        [repo updateUser:self.currentUser Error:&error];
        
        return YES;
        
    }
    else
    {
         if ([pin isEqualToString:self.correctPin]) {
             self.currentUser.userPin = @"";
             self.currentUser.userIsSecureLogin =[NSNumber numberWithBool:NO];
             
             UserRepository *repo=[[UserRepository alloc]init];
             NSError *error;
             [repo updateUser:self.currentUser Error:&error];
        
             return YES;
            } else {
                self.remainingPinEntries--;
                return NO;
            }

        
    }
//     else {
//        self.remainingPinEntries--;
//        return NO;
//    }
}

- (BOOL)userCanRetryInPinViewController:(THPinViewController *)pinViewController
{
    return (self.remainingPinEntries > 0);
}

- (IBAction)checkSecureLogin:(id)sender {

    
    if (![self.checkSecure isOn]) {
        [self validateSecurityPasswordEntry];
        
        return;
    }
    
    if ([self.currentUser.userEmailVerified isEqualToNumber:[NSNumber numberWithBool:FALSE]] || self.currentUser.userEmailVerified == nil) {
        //self.lblEmailVerified.text =@"Not Verify";
        [self.checkSecure setOn:FALSE];
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Email Verification Error", nil)
                                    message:NSLocalizedString(@"Email must be verified before open secure login , you should open the mail address that entered to verify the mail sent to you from Vent", nil)
                                   delegate:self
                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                          otherButtonTitles:NSLocalizedString(@"Profile", nil),nil] show];
        
    }
    else
    {
        //self.lblEmailVerified.text =@"Verify";
        
        if ([self.checkSecure isOn]) {
            [self openSecurityPasswordEntry];
        }
        else
        {
            
            
            [self validateSecurityPasswordEntry];
            
        }

        
        
    }
    

    
    
}


-(void)openSecurityPasswordEntry
{
    
    self.correctPin = self.currentUser.userPin;
    self.isSecureValidate = FALSE;
    
    self.remainingPinEntries = THNumberOfPinEntries;
    THPinViewController *pinViewController = [[THPinViewController alloc] init];
    pinViewController.promptTitle = [NSString stringWithFormat:NSLocalizedString(@"Enter New PIN", nil)];
    UIColor *darkBlueColor = [UIColor colorWithRed:0.012f green:0.071f blue:0.365f alpha:1.0f];
    pinViewController.promptColor = darkBlueColor;
    pinViewController.view.tintColor = darkBlueColor;
    
    // for a solid background color, use this:
    pinViewController.backgroundColor = [UIColor grayColor];
    
    //    // for a translucent background, use this:
    //    self.view.tag = THPinViewControllerContentViewTag;
    //    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    //    pinViewController.translucentBackground = YES;
    
    //pinViewController.delegate = self;
    
    //[self.view addSubview:pinViewController.view];
    
    [self presentViewController:pinViewController animated:YES completion:NULL];
    
}

-(void)validateSecurityPasswordEntry
{
   self.correctPin = self.currentUser.userPin;
    
     self.isSecureValidate = TRUE;
    
    self.remainingPinEntries = THNumberOfPinEntries;
    THPinViewController *pinViewController = [[THPinViewController alloc] initWithDelegate:self];
    pinViewController.promptTitle = [NSString stringWithFormat:NSLocalizedString(@"Enter Pin", nil)];
    ;
    UIColor *darkBlueColor = [UIColor colorWithRed:0.012f green:0.071f blue:0.365f alpha:1.0f];
    pinViewController.promptColor = darkBlueColor;
    pinViewController.view.tintColor = darkBlueColor;
    
    // for a solid background color, use this:
    pinViewController.backgroundColor = [UIColor grayColor];
    
    //    // for a translucent background, use this:
    //    self.view.tag = THPinViewControllerContentViewTag;
    //    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    //    pinViewController.translucentBackground = YES;
    
    pinViewController.delegate = self;
    
    //[self.view addSubview:pinViewController.view];
    
    [self presentViewController:pinViewController animated:YES completion:NULL];
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"Cancel" pressed
            
            
            
            break;
            
        case 1: //"profile" pressed
            
             [self performSegueWithIdentifier:@"ProfileControl" sender:self];
            break;
    }
}










#pragma mark - THPinViewControllerDelegate




- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    if (! self.locked) {
        [self showPinViewAnimated:NO];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
}

#pragma mark - Properties

//- (void)setLocked:(BOOL)locked
//{
//    _locked = locked;
//
//    if (self.locked) {
//        self.remainingPinEntries = THNumberOfPinEntries;
//        //[self.loginLogoutButton removeTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
//        //[self.loginLogoutButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
//        //self.secretContentView.hidden = YES;
//    } else {
//        [self.loginLogoutButton removeTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
//        [self.loginLogoutButton addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
//        self.secretContentView.hidden = NO;
//    }
//}

#pragma mark - UI

- (void)showPinViewAnimated:(BOOL)animated
{
    THPinViewController *pinViewController = [[THPinViewController alloc] initWithDelegate:self];
    pinViewController.promptTitle = [NSString stringWithFormat:NSLocalizedString(@"Enter Pin", nil)];
    UIColor *darkBlueColor = [UIColor colorWithRed:0.012f green:0.071f blue:0.365f alpha:1.0f];
    pinViewController.promptColor = darkBlueColor;
    pinViewController.view.tintColor = darkBlueColor;
    
    // for a solid background color, use this:
    pinViewController.backgroundColor = [UIColor whiteColor];
    
    // for a translucent background, use this:
    self.view.tag = THPinViewControllerContentViewTag;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    pinViewController.translucentBackground = YES;
    
    [self presentViewController:pinViewController animated:animated completion:nil];
}

#pragma mark - User Interaction

- (void)login:(id)sender
{
    [self showPinViewAnimated:YES];
}

- (void)logout:(id)sender
{
    self.locked = YES;
    [self.loginLogoutButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"Enter Pin", nil)] forState:UIControlStateNormal];
}
/*
 #pragma mark - THPinViewControllerDelegate
 
 - (NSUInteger)pinLengthForPinViewController:(THPinViewController *)pinViewController
 {
 return 4;
 }
 
 - (BOOL)pinViewController:(THPinViewController *)pinViewController isPinValid:(NSString *)pin
 {
 if ([pin isEqualToString:self.correctPin]) {
 return YES;
 } else {
 self.remainingPinEntries--;
 return NO;
 }
 }
 
 - (BOOL)userCanRetryInPinViewController:(THPinViewController *)pinViewController
 {
 return (self.remainingPinEntries > 0);
 }
 
 - (void)incorrectPinEnteredInPinViewController:(THPinViewController *)pinViewController
 {
 if (self.remainingPinEntries > THNumberOfPinEntries / 2) {
 return;
 }
 
 UIAlertView *alert =
 [[UIAlertView alloc] initWithTitle:@"Incorrect PIN"
 message:(self.remainingPinEntries == 1 ?
 @"You can try again once." :
 [NSString stringWithFormat:@"You can try again %lu times.",
 (unsigned long)self.remainingPinEntries])
 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [alert show];
 
 }
 
 - (void)pinViewControllerWillDismissAfterPinEntryWasSuccessful:(THPinViewController *)pinViewController
 {
 self.locked = NO;
 [self.loginLogoutButton setTitle:@"Logout" forState:UIControlStateNormal];
 }
 
 - (void)pinViewControllerWillDismissAfterPinEntryWasUnsuccessful:(THPinViewController *)pinViewController
 {
 self.locked = YES;
 [self.loginLogoutButton setTitle:@"Access Denied / Enter PIN" forState:UIControlStateNormal];
 }
 
 - (void)pinViewControllerWillDismissAfterPinEntryWasCancelled:(THPinViewController *)pinViewController
 {
 if (! self.locked) {
 [self logout:self];
 }
 }
 */


- (void)pinViewControllerWillDismissAfterPinEntryWasUnsuccessful:(THPinViewController *)pinViewController
{
    //  self.locked = YES;
    //  [self.loginLogoutButton setTitle:@"Access Denied / Enter PIN" forState:UIControlStateNormal];
    
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Incorrect PIN", nil)]
                               message:[NSString stringWithFormat:NSLocalizedString(@"Pin you entered is inccorect", nil)]
                              delegate:nil cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)] otherButtonTitles:nil];
    [alert show];
    
    exit(0);
    
}

- (void)pinViewControllerWillDismissAfterPinEntryWasCancelled:(THPinViewController *)pinViewController
{
    exit(0);
}


- (void)pinViewControllerWillDismissAfterPinEntryWasForgetPin:(THPinViewController *)pinViewController{
    
}

- (void)pinViewControllerDidDismissAfterPinEntryWasForgetPin:(THPinViewController *)pinViewController
{
    
}



- (void)incorrectPinEnteredInPinViewController:(THPinViewController *)pinViewController
{
    if (self.remainingPinEntries > THNumberOfPinEntries / 2) {
        return;
    }
    
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Incorrect PIN", nil)]
                               message:(self.remainingPinEntries == 1 ?
                                        [NSString stringWithFormat:NSLocalizedString(@"You can try again once.", nil)] :
                                        [NSString stringWithFormat:@"%@ %lu %@",[NSString stringWithFormat:NSLocalizedString(@"You can try again", nil)],
                                         (unsigned long)self.remainingPinEntries , [NSString stringWithFormat:NSLocalizedString(@"times", nil)]])
                              delegate:nil cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)] otherButtonTitles:nil];
    [alert show];
    
}


@end
