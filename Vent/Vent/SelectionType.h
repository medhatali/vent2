//
//  SelectionType.h
//  Vent
//
//  Created by Sameh Farouk on 3/29/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#ifndef Vent_SelectionType_h
#define Vent_SelectionType_h

typedef enum {
    kSingleSelect = 0,
    kMultipleSelect,
} SelectionType;

#endif
