//
//  ChatSessionRepository.h
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseRepository.h"
#import "ChatSession.h"

@interface ChatSessionRepository : BaseRepository

-(ChatSession *) addChatSession:(NSString*)chatSessionID
                chatSessionFrom:(NSString*)chatSessionFrom
                  chatSessionTo:(NSString*)chatSessionTo
             chatSessionMessage:(NSString*)chatSessionMessage
                chatSessionDate:(NSString*)chatSessionDate
                chatSessionRead:(NSNumber*)chatSessionRead
           chatSessionDelivered:(NSNumber*)chatSessionDelivered
             chatSessionSecured:(NSNumber*)chatSessionSecured
           chatSessionMessageID:(NSString*)chatSessionMessageID
             chatSessionGroupID:(NSString*)chatSessionGroupID
                chatSessionType:(NSString*)chatSessionType
       chatSessionDeliveredDate:(NSString*)chatSessionDeliveredDate
            chatSessionReadDate:(NSString*)chatSessionReadDate
              chatSessionIsSent:(NSNumber*)chatSessionIsSent
               chatSessionFaild:(NSNumber*)chatSessionFaild
               chatSessionError:(NSString*)chatSessionError
               chatSessionTimer:(NSString*)chatSessionTimer
            chatSessionMediaUrl:(NSString*)chatSessionMediaUrl
               chatSessionSmily:(NSString*)chatSessionSmily
            chatSessionLocation:(NSString*)chatSessionLocation
               chatSessionVCard:(NSString*)chatSessionVCard
           chatSessionMyMessage:(NSNumber*)chatSessionMyMessage
            chatSessionChatList:(ChatList*)chatSessionChatList
           chatSessionGroupList:(GroupList*)chatSessionGroupList
  chatSessionBroadcastList:(BroadcastList*)chatSessionBroadcastList
                          Error:(NSError **)error;


-(void)deleteChatSession:(ChatSession *)ChatSession Error:(NSError **)error;

-(void)updateChatSession:(ChatSession *)ChatSession Error:(NSError **)error;

-(NSArray*)getChatSessionsByChatList:(ChatList*)ChatList Error:(NSError **)error;

-(NSArray*)getChatSessionsByMessageId:(NSString*)messageId Error:(NSError **)error;


@end
