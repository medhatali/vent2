//
//  Contacts.m
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "Contacts.h"
#import "ContactPhones.h"
#import "User.h"


@implementation Contacts

@dynamic contactFirstName;
@dynamic contactFullName;
@dynamic contactID;
@dynamic contactImage;
@dynamic contactImageUrl;
@dynamic contactIsVentUser;
@dynamic contactLastName;
@dynamic contactMiddleName;
@dynamic contactIsOnline;
@dynamic contactIsBlocked;
@dynamic contactPhones;
@dynamic contactUser;

@end
