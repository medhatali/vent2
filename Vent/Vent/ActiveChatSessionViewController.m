//
//  ActiveChatSessionViewController.m
//  Vent
//
//  Created by Medhat Ali on 5/17/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ActiveChatSessionViewController.h"
#import "HandlerProtocol.h"
#import "GlobalHandler.h"
#import "GroupProfileViewControler.h"
#import "UploadDownLoadBussinessService.h"
#import "ContactsBussinessService.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "MessageNotificationBussiness.h"
#import "AppUtility.h"
#import "ChatListRepository.h"
#import "MainContactsTableView.h"
#import <Foundation/Foundation.h>
#import "ContactsRepository.h"
#import "SmilesCollectionViewDataSource.h"
@import AudioToolbox;
@import AVFoundation;
@import Photos;

@interface ActiveChatSessionViewController ()<UINavigationControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>



@end

@implementation ActiveChatSessionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.textViewDelegate = self;
    self.otherToolbarViewDelegate = self;
    self.delegate =self;
    
    /// initilaize the handler
    self.GlobalHandlerInstance =[GlobalHandler sharedInstance];
    
    
   // [self toggleSendBUttonStatus];
    [self.inputToolbar toggleSendButtonEnabled];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

    self.GlobalHandlerInstance.delegate=self;
    
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.GlobalHandlerInstance.delegate=self;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.tabBarController.tabBar setHidden:NO];
    self.GlobalHandlerInstance.delegate=nil;
}



#pragma mark - Actions

- (void)handleTitleSingleTap:(UIButton *)titleButton {
    //Do stuff here...
    //NSLog(@"Open Profile");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    GroupProfileViewControler * groupProfileVC=(GroupProfileViewControler*)[storyboard instantiateViewControllerWithIdentifier:@"GroupInfo"];
    
    
    [self.navigationController pushViewController:groupProfileVC animated:YES];
    
}



#pragma mark - Text view delegate

- (void)textInputToolBarViewDidBeginEditing:(UITextView *)textView
{
    //onUserStartTyping
//     NSString * ventphonenumber =[contactRepo getVentPhoneNumberForContact:self.currentUser.userPhoneNumber Error:nil];
  
    
  //  NSLog(@"textview delegate from UI begin edit");
}

- (void)textInputToolBarViewDidChange:(UITextView *)textView
{
  //  NSLog(@"textview delegate from UI did change %@" ,self.inputToolbar.contentView.textView.detailText);
    
   // self.inputToolbar.contentView.textView.detailText = [NSString stringWithFormat:@"%@",self.inputToolbar.contentView.textView.text];
    
   // self.inputToolbar.contentView.textView.text = [NSString stringWithFormat:@"%@",self.inputToolbar.contentView.textView.text ];
    
//    self.inputToolbar.contentView.textView.text =[NSString stringWithFormat:@"%@%@",self.inputToolbar.contentView.textView.text,textView.text ];
    
    self.inputToolbar.contentView.textView.attributedText=[self.inputToolbar.contentView.textView parseStringtoAttributeString:self.inputToolbar.contentView.textView.detailText];
    
   self.inputToolbar.contentView.textView.attributedText =[[SmilesCollectionViewDataSource sharedInstance] parseStringtoAttributeString:self.inputToolbar.contentView.textView.detailText FontSize:30];
    
    NSLog(@"Add: Text Detail = %@ , text View = %@" ,self.inputToolbar.contentView.textView.detailText ,self.inputToolbar.contentView.textView.text );
    
    ContactsRepository * contactRepo=[[ContactsRepository alloc]init];
    NSString * ventphonenumber =[contactRepo getVentPhoneNumberForContact:self.reciverContact Error:nil];
    [GlobalHandler userStartTyping:ventphonenumber];

}

- (void)shouldChangeTextInRange:(NSString *)replacementText
{
    
 //   [self deleteTextFromUITextView];
    
    if ( self.inputToolbar.contentView.textView.detailText ) {
     self.inputToolbar.contentView.textView.detailText = [NSString stringWithFormat:@"%@%@",self.inputToolbar.contentView.textView.detailText ,replacementText];
    }
    else
    {
       self.inputToolbar.contentView.textView.detailText = [NSString stringWithFormat:@"%@",replacementText];
    }
}

- (void)deleteTextInputToolBar:(UITextView *)textView
{
    
  
    [self deleteTextFromUITextView];
    
    [self.inputToolbar toggleSendButtonEnabled];
    
    [self textInputToolBarViewDidChange:textView];

    
    
}

- (void)textInputToolBarViewDidEndEditing:(UITextView *)textView
{
   // NSLog(@"textview delegate from UI end edit");
}

#pragma -mark other toolbar delegate

- (void)toolBarButtonPressed:(id)sender ButtonIndex:(NSInteger)buttonIndex
{
   // NSLog(@"other toolbar clicked %ld",(long)buttonIndex);
    
    if (buttonIndex == 1) {
        //show keyboard
        [self.keyboardController.keyboardView setHidden:NO];
        [self.keyboardController.keyboardView setUserInteractionEnabled:YES];
        //[self hideSmilyControl];
        [self.inputToolbar.contentView.textView becomeFirstResponder];
        
        
    }
    else if (buttonIndex == 2)
    {
        //show smily
  
        
        
        //[self.inputToolbar.contentView.textView becomeFirstResponder];
        
        [self.keyboardController.keyboardView setHidden:YES];
        [self.keyboardController.keyboardView setUserInteractionEnabled:NO];
        [self.inputToolbar.contentView.textView endEditing:YES];

        //[self.keyboardController.keyboardView resignFirstResponder];
        
       // [self.keyboardController jsq_setKeyboardViewHidden:YES];
       // [self.keyboardController endListeningForKeyboard];
       // [self.keyboardController jsq_removeKeyboardFrameObserver];
        

        // [self showSmilyControl];
        
        //[self.smiliesView becomeFirstResponder];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self showSmilyControl];
        });
        
        
       
       
        
        
    }
    else if (buttonIndex == 3)
    {
        [self.keyboardController.keyboardView setHidden:YES];
        [self.keyboardController.keyboardView setUserInteractionEnabled:NO];
        [self.inputToolbar.contentView.textView endEditing:YES];
        
        UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)],[NSString stringWithFormat:NSLocalizedString(@"Choose Existing Photo", nil)] ,[NSString stringWithFormat:NSLocalizedString(@"Take Video", nil)],[NSString stringWithFormat:NSLocalizedString(@"Choose Existing Video", nil)], nil];
        
        [_actionSheet showInView:[self view]];
        
//        [self hideSmilyControl];
//        [self.keyboardController.keyboardView setHidden:YES];
        [self.keyboardController.keyboardView setUserInteractionEnabled:NO];
        
        
    }
    else if (buttonIndex == 4)
    {
        //show current location
        
        if([CLLocationManager locationServicesEnabled] &&
           [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
        {
            
            MessageDM *newMessage=[[MessageDM alloc]init];
            newMessage.messageId=[newMessage uniqueUUID];
            newMessage.content =[NSString stringWithFormat:@"%@,%@",self.ventChatDM.latitude,self.ventChatDM.longitude ];
            newMessage.sendingDateTime=[NSDate date];
            newMessage.deviceToken = self.currentUser.userDeviceUUID;
            newMessage.fromPhoneNumber= self.currentUser.userPhoneNumber;
            newMessage.toPhoneNumbers=self.reciverContactPhones;
            newMessage.messageTimer =0;
            newMessage.contentType = location;
            newMessage.groupId = nil;
            
            [self sendMessageToServer:newMessage ImageDate:nil Contacts:nil];
            
        }
        else
        {
          
//           
//                [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Location services are disabled."
//                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
           
            
            UIAlertView *locationDisabled = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Warning", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"Location services are disabled.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
            
            [locationDisabled show];
            
            
           
        }

       


        
    }
    else if (buttonIndex == 5)
    {
        //show contact
        

        
        ContactsBussinessService *contactBWS=[[ContactsBussinessService alloc]init];
        if (![contactBWS isContatcsAccessGrantInDispath]) {
            
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               
                               
                               UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                               
                               [cantLoadContactsAlert show];
                               
                               
                               
                           });
            
            return;
        }
        
        [self.keyboardController.keyboardView setHidden:YES];
        [self.keyboardController.keyboardView setUserInteractionEnabled:NO];
        [self.inputToolbar.contentView.textView endEditing:YES];
        
        MainContactsTableView *contactView =[[MainContactsTableView alloc]initWithNibName:@"ContactsTableView" delegate:self];
        contactView.allContacts=YES;
        self.isVcardSelection = YES;
        [[contactView navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Contacts", nil)]];
        //   [[self navigationController] pushViewController:contactView animated:YES];
        // [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactView];
        
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self showDetailViewController:navController sender:self];
        
        
    }
    
    
    //    if ([self.keyboardController.keyboardView isHidden]) {
    //
    //
    //    }
    //    else
    //    {
    //
    //    }
    
}


#pragma -mark events handler protocols

- (void)onSendMessage:(id)message{
    
    
    // recieve message from server
    
   // NSLog(@"chat active session: OnSendMessage %@",message);
    
    MessageDM *currentModel=(MessageDM *)message;
    
    
    if ([[GlobalHandler sharedInstance]isAPNRecieved] == FALSE) {
        if (currentModel.chatlistId != NULL) {
            
            if (self.selectedChatList != NULL) {
                if (![currentModel.chatlistId isEqualToString:self.selectedChatList.chatID] )
                {
                    
                    [JSQSystemSoundPlayer jsq_playMessageReceivedSound:self.selectedChatList.chatIsBlocked IsMute:self.selectedChatList.chatMute];
                    

                    [[GlobalHandler sharedInstance] displayLocalNotificationGlobal:[NSString stringWithFormat:NSLocalizedString(@"New Message recieved", nil)] Body:message];
                    
                }
            }
            else  if (self.selectedGroupList != NULL) {
                if (! [currentModel.chatlistId isEqualToString:self.selectedGroupList.groupID]) {
                    [JSQSystemSoundPlayer jsq_playMessageReceivedSound:self.selectedGroupList.groupIsBlocked IsMute:self.selectedGroupList.groupMuted];
                    [[GlobalHandler sharedInstance] displayLocalNotificationGlobal:[NSString stringWithFormat:NSLocalizedString(@"New Message recieved", nil)] Body:message];
                }
                
            }
            else
            {
                 [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
                
                [[GlobalHandler sharedInstance] displayLocalNotificationGlobal:[NSString stringWithFormat:NSLocalizedString(@"New Message recieved", nil)] Body:message];
   
            }
        }
        else
        {
            [JSQSystemSoundPlayer jsq_playMessageReceivedSound:[NSNumber numberWithBool:NO] IsMute:[NSNumber numberWithBool:NO]];
            
            [[GlobalHandler sharedInstance] displayLocalNotificationGlobal:[NSString stringWithFormat:NSLocalizedString(@"New Message recieved", nil)] Body:message];
        }
        
        
        
        
    }
    
    [self recieveMessageFromServer:currentModel];
    
//    NSMutableArray *userIds = [[self.demoData.users allKeys] mutableCopy];
//    [userIds removeObject:self.senderId];
////    NSString *randomUserId = userIds[arc4random_uniform((int)[userIds count])];
//    JSQMessage *newMessage = nil;
//    
//    
//    
//    newMessage = [JSQMessage messageWithSenderId:self.reciverContact.contactID
//                                     displayName:self.reciverContact.contactFullName
//                                            text:currentModel.content];
//    
//    
//    
//    [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
//    
//    
//    if (currentModel.contentType == text) {
//        
//     //   [self.demoData.messages addObject:newMessage];
//        
//    }else if (currentModel.contentType == hyperLink){
//        
//    }else if (currentModel.contentType == image){
//        
//        
//        
//        
//        
//        UIImage __block *thumbnailImage = nil;
//        dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        
//        
//        
//        dispatch_async(taskQ,
//                       ^{
//                           UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
//                           NSError *error;
//                           
//                           NSData *thumbnailData =  [fileServiceObject downLoadThumbnail:currentModel.content Error:&error];
//                           thumbnailImage = [[UIImage alloc] initWithData:thumbnailData];
//                           
//                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                           NSString *documentsDirectory = [paths objectAtIndex:0];
//                           
//                           
//                           ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
//                           
//                           User* currentUser=[contactBWS getCurrentUserProfile];
//                           
//                           MessageDM *newMessage=[[MessageDM alloc]init];
//                           newMessage.messageId=[newMessage uniqueUUID];
//                           
//                           NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",[[AppUtility sharedUtilityInstance]getMediaDirectory], currentModel.content]];
//                           [thumbnailData writeToFile:localFilePath atomically:YES];
//                           NSLog(@"localFilePath.%@",localFilePath);
//                           
//                           //newMessage.content =localFilePath;
//                           newMessage.content =currentModel.content;
//                           newMessage.sendingDateTime=[NSDate date];
//                           newMessage.deviceToken = currentUser.userDeviceUUID;
//                           newMessage.fromPhoneNumber= currentUser.userPhoneNumber;
//                           
//                           newMessage.toPhoneNumbers=self.reciverContactPhones;
//                           newMessage.messageTimer =0;
//                           newMessage.contentType = image;
//                           newMessage.groupId = nil;
//                           
//                           
//                           MessageNotificationBussiness * messageBussiness=[[MessageNotificationBussiness alloc]init];
//                           [messageBussiness didSendSignalRMessage:newMessage currentChatList:self.selectedChatList IsSent:TRUE  Error:&error];
//
//                           [JSQSystemSoundPlayer jsq_playMessageSentSound];
// 
//                           
//                           [self.demoData addPhotoMediaMessage:thumbnailImage];
//                           [[self collectionView] reloadData];
//                           [self finishReceivingMessageAnimated:YES];
//                          
//                           
//                       });
//        
//        [[self collectionView] reloadData];
//        [self finishReceivingMessageAnimated:YES];
//
//        
//    }else if (currentModel.contentType == video){
//        
//        
//        UIImage __block *thumbnailImage = nil;
//        dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        
//        
//        
//        dispatch_async(taskQ,
//                       ^{
//                           UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
//                           NSError *error;
//                           
//                           
//                           NSData *thumbnailData =  [fileServiceObject downLoadThumbnail:currentModel.content Error:&error];
//                           thumbnailImage = [[UIImage alloc] initWithData:thumbnailData];
//                           
//                           
//                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                           NSString *documentsDirectory = [paths objectAtIndex:0];
//                           
//                           
//                           ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
//                           
//                           User* currentUser=[contactBWS getCurrentUserProfile];
//                           
//                           MessageDM *newMessage=[[MessageDM alloc]init];
//                           newMessage.messageId=[newMessage uniqueUUID];
//                           
//                           NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",[[AppUtility sharedUtilityInstance]getMediaDirectory], currentModel.content]];
//                           [thumbnailData writeToFile:localFilePath atomically:YES];
//                           NSLog(@"localFilePath.%@",localFilePath);
//                           
//                           //newMessage.content =localFilePath;
//                           newMessage.content =currentModel.content;
//                           newMessage.sendingDateTime=[NSDate date];
//                           newMessage.deviceToken = currentUser.userDeviceUUID;
//                           newMessage.fromPhoneNumber= currentUser.userPhoneNumber;
//                           
//                           newMessage.toPhoneNumbers=self.reciverContactPhones;
//                           newMessage.messageTimer =0;
//                           newMessage.contentType = image;
//                           newMessage.groupId = nil;
//                           
//                           
//                           MessageNotificationBussiness * messageBussiness=[[MessageNotificationBussiness alloc]init];
//                           [messageBussiness didSendSignalRMessage:newMessage currentChatList:self.selectedChatList IsSent:TRUE  Error:&error];
//                           
//                           [JSQSystemSoundPlayer jsq_playMessageSentSound];
//                           
//                           
//                           //[self.demoData addPhotoMediaMessage:thumbnailImage];
//                           [self.demoData addVideoMediaMessage:localFilePath];
//                           [[self collectionView] reloadData];
//                           [self finishReceivingMessageAnimated:YES];
//                           
//                           
//                       });
//        
//        [[self collectionView] reloadData];
//        [self finishReceivingMessageAnimated:YES];
//        
//
//        
//        
//        
//    }else if (currentModel.contentType == location){
//        
//    }else if (currentModel.contentType == vCard){
//        
//
//        
//    }else if (currentModel.contentType == audio){
//        
//    }
//    
//    [self finishReceivingMessageAnimated:YES];
//    
//    [GlobalHandler markMessageAsRead:[currentModel messageId]];
//    
//    ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
//    NSError *error;
//    self.chatsession =[chatListRepo getChatListSessions:self.selectedChatList Error:&error];
//    
}

-(void) onMarkMessageAsDelivered: messageId{
    [[self collectionView] reloadData];
    
}

-(void) onMarkMessageAsSent: messageId{
    [[self collectionView] reloadData];
}

-(void) onMarkMessageAsRead: messageId{
    [[self collectionView] reloadData];
}


-(void) onUserStartTyping: userIdentifier
{
    self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"Typing..", nil)] ;
}

#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    switch (buttonIndex) {
        case 0:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Device has no camera", nil)]
                                                                     delegate:nil
                                                            cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            else{
                
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                if ([self checkCameraAvailability]) {
                    [self presentViewController:picker animated:YES completion:NULL];
                }else{
                    [self ShowCameraPermissionAlert];
                }
            }
            break;
        case 1:
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
           // NSLog(@"authorizationStatus is %d",[PHPhotoLibrary authorizationStatus]);
            if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
                [self presentViewController:picker animated:YES completion:NULL];
            }else{
                [self ShowPhotosPermissionAlert];
            }
            
            break;
        case 2:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Device has no camera", nil)]
                                                                     delegate:nil
                                                            cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            else{
                picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                if ([self checkCameraAvailability]) {
                    [self presentViewController:picker animated:YES completion:NULL];
                }else{
                    [self ShowCameraPermissionAlert];
                }
            }
            break;
        case 3:
            picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
           // NSLog(@"authorizationStatus is %d",[PHPhotoLibrary authorizationStatus]);
            if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
                [self presentViewController:picker animated:YES completion:NULL];
            }else{
                [self ShowPhotosPermissionAlert];
            }
            break;
        default:
            break;
    }
}

#pragma mark - Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    NSData *webData = nil;
    NSString *extention = nil;
    UploadFileType fileType = KImage;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    NSInteger type=3;
    
    if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
//        NSString *moviePath = [videoUrl path];
        extention = @"MOV";
//        [[self demoData] addVideoMediaMessage:moviePath];
//        [[self collectionView] reloadData];
        webData = [NSData dataWithContentsOfURL:videoUrl];
        fileType = KVideo;
        type=4;
        //[self reloadAllData];
//        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
//            UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
//        }
    }else{
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        extention = @"PNG";
        type=3;
        if (chosenImage) {
            [self.demoData addPhotoMediaMessage:chosenImage];
           // [self.collectionView reloadData];
           // [self reloadAllData];
            webData = UIImagePNGRepresentation(chosenImage);
            fileType = KImage;
        }
    }
    
    
    
    if (webData) {
        

        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
       

        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
        
        User* currentUser=[contactBWS getCurrentUserProfile];
        MessageDM *newMessage=[[MessageDM alloc]init];
        newMessage.messageId=[newMessage uniqueUUID];
        
        NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.%@",[[AppUtility sharedUtilityInstance]getMediaDirectory], newMessage.messageId,extention]];
        [webData writeToFile:localFilePath atomically:YES];
        //NSLog(@"localFilePath.%@",localFilePath);
        
        newMessage.content =localFilePath;
        
        newMessage.content =[NSString stringWithFormat:@"%@.%@",newMessage.messageId ,extention ];
        //newMessage.contentType = type;
        newMessage.sendingDateTime=[NSDate date];
        newMessage.deviceToken = currentUser.userDeviceUUID;
        newMessage.fromPhoneNumber= currentUser.userPhoneNumber;
        
        newMessage.toPhoneNumbers=self.reciverContactPhones;
        newMessage.messageTimer =0;
        newMessage.contentType = (int)type;
        newMessage.groupId = nil;
        
        /// ********
//        if (type == 4) {
////            NSString *moviePath = [NSString stringWithFormat:@"File://%@",localFilePath];
//            [[self demoData] addVideoMediaMessage:newMessage.content];
//        }
    
        /// ********
        
        [self sendMessageToServer:newMessage ImageDate:webData Contacts:nil];
//         [self reloadAllData];
//        
//        
//        [self finishSendingMessageAnimated:YES];
//        [JSQSystemSoundPlayer jsq_playMessageSentSound];

    }
        [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
      [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark smily control protocol

-(void)smilySelected:(NSDictionary*) emoji
{
//    self.inputToolbar.contentView.textView.text =[NSString stringWithFormat:@"%@%@",self.inputToolbar.contentView.textView.text ,emoji];
//    [self.inputToolbar toggleSendButtonEnabled];
    
    
    //UIImage * pic = [[UIImage alloc]init];
    UIImage * pic= [UIImage imageNamed:[emoji objectForKey:@"Name"]];
    //NSTextAttachmentCell *attachmentCell = [[NSTextAttachmentCell alloc] initImageCell:pic];
    
    
    UIGraphicsBeginImageContext(CGSizeMake(40, 40));
    [pic drawInRect:CGRectMake(0, 0, 40, 40)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    [attachment setImage:newImage];

    
    //NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.inputToolbar.contentView.textView.attributedText];
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:attachment];
    //[attributedString replaceCharactersInRange:NSMakeRange(6, 1) withAttributedString:attrStringWithImage];

    NSRange range = NSMakeRange(0, 1);
    range = NSMakeRange(0, [self.inputToolbar.contentView.textView.attributedText  length]);
    
    NSMutableAttributedString *baseString=[[NSMutableAttributedString alloc]initWithAttributedString:self.inputToolbar.contentView.textView.attributedText];
    
    [baseString appendAttributedString:attrStringWithImage];
  

    
    // self.inputToolbar.contentView.textView.attributedText = baseString;
    
    if (self.inputToolbar.contentView.textView.detailText == nil) {
        self.inputToolbar.contentView.textView.detailText = [NSString stringWithFormat:@"%@ %@ ",@"" ,[emoji objectForKey:@"Code"]];
    }
    else
    {
    
    self.inputToolbar.contentView.textView.detailText = [NSString stringWithFormat:@"%@ %@ ",self.inputToolbar.contentView.textView.detailText ,[emoji objectForKey:@"Code"]];
    }
    
    self.inputToolbar.contentView.textView.text = [NSString stringWithFormat:@"%@ %@ ",self.inputToolbar.contentView.textView.text ,[emoji objectForKey:@"Code"]];
    
    [ self.inputToolbar.contentView.textView.delegate textViewDidChange:self.inputToolbar.contentView.textView];
    
//    self.inputToolbar.contentView.textView.attributedText=[self.inputToolbar.contentView.textView parseStringtoAttributeString:self.inputToolbar.contentView.textView.detailText];
  
    //self.inputToolbar.contentView.textView.attributedText = baseString;
    
//    NSAttributedString *attributedString = [NSAttributedString  attributedStringWithAttachment: attachment];
//    [[self.inputToolbar.contentView.textView textStorage] appendAttributedString:attributedString];
    
  //  self.inputToolbar.contentView.textView.text =[NSString stringWithFormat:@"%@%@",self.inputToolbar.contentView.textView.text ,attributedString];

    
    [self.inputToolbar toggleSendButtonEnabled];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       AudioServicesPlaySystemSound(1104);
                   });
    
}

-(void)removeLastEmoji
{

    [self deleteTextFromUITextView];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       AudioServicesPlaySystemSound(1104);
                   });
    
    
    [self.inputToolbar toggleSendButtonEnabled];
    
    [self textInputToolBarViewDidChange:self.inputToolbar.contentView.textView];
    
}



-(void)deleteTextFromUITextView
{
    
    if ([self.inputToolbar.contentView.textView.detailText length] > 0 && [self.inputToolbar.contentView.textView.text length] > 0)
    {
        if ([self.inputToolbar.contentView.textView.detailText length] > 8) {
            NSString *lastString=[self.inputToolbar.contentView.textView.detailText substringFromIndex:[self.inputToolbar.contentView.textView.detailText length]- 9];
            
            if ([lastString containsString:@":0x"]) {
                self.inputToolbar.contentView.textView.detailText= [self.inputToolbar.contentView.textView.detailText substringToIndex:[self.inputToolbar.contentView.textView.detailText length]- 9];
            }
            else if ([self.inputToolbar.contentView.textView.detailText length] > 2)
            {
                NSString *lastStringEmoji=[self.inputToolbar.contentView.textView.detailText substringFromIndex:[self.inputToolbar.contentView.textView.detailText length]- 1];
                
                BOOL cancontainUni= [lastStringEmoji canBeConvertedToEncoding:NSASCIIStringEncoding];
                if (!cancontainUni ) {
                    // detect that contains emoji
                    self.inputToolbar.contentView.textView.text=[self.inputToolbar.contentView.textView.text substringToIndex:[self.inputToolbar.contentView.textView.text length]-2];
                    self.inputToolbar.contentView.textView.detailText= [self.inputToolbar.contentView.textView.detailText substringToIndex:[self.inputToolbar.contentView.textView.detailText length]- 2];
                }
                else
                {
                    self.inputToolbar.contentView.textView.text=[self.inputToolbar.contentView.textView.text substringToIndex:[self.inputToolbar.contentView.textView.text length]-1];
                    self.inputToolbar.contentView.textView.detailText= [self.inputToolbar.contentView.textView.detailText substringToIndex:[self.inputToolbar.contentView.textView.detailText length]- 1];
                }
            }
            else
            {
                self.inputToolbar.contentView.textView.text=[self.inputToolbar.contentView.textView.text substringToIndex:[self.inputToolbar.contentView.textView.text length]-1];
                self.inputToolbar.contentView.textView.detailText= [self.inputToolbar.contentView.textView.detailText substringToIndex:[self.inputToolbar.contentView.textView.detailText length]- 1];
            }
            
        }
        else if ([self.inputToolbar.contentView.textView.detailText length] >= 2)
        {
            NSString *lastStringEmoji=[self.inputToolbar.contentView.textView.detailText substringFromIndex:[self.inputToolbar.contentView.textView.detailText length]- 1];
            
            BOOL cancontainUni= [lastStringEmoji canBeConvertedToEncoding:NSASCIIStringEncoding];
            if (!cancontainUni) {
                // detect that contains emoji
                self.inputToolbar.contentView.textView.text=[self.inputToolbar.contentView.textView.text substringToIndex:[self.inputToolbar.contentView.textView.text length]-2];
                self.inputToolbar.contentView.textView.detailText= [self.inputToolbar.contentView.textView.detailText substringToIndex:[self.inputToolbar.contentView.textView.detailText length]- 2];
            }
            else
            {
                self.inputToolbar.contentView.textView.text=[self.inputToolbar.contentView.textView.text substringToIndex:[self.inputToolbar.contentView.textView.text length]-1];
                self.inputToolbar.contentView.textView.detailText= [self.inputToolbar.contentView.textView.detailText substringToIndex:[self.inputToolbar.contentView.textView.detailText length]- 1];
            }
        }
        else
        {
            self.inputToolbar.contentView.textView.text=[self.inputToolbar.contentView.textView.text substringToIndex:[self.inputToolbar.contentView.textView.text length]-1];
            self.inputToolbar.contentView.textView.detailText= [self.inputToolbar.contentView.textView.detailText substringToIndex:[self.inputToolbar.contentView.textView.detailText length]- 1];
        }
        
        
           NSLog(@"Delete: Text Detail = %@ , text View = %@" ,self.inputToolbar.contentView.textView.detailText ,self.inputToolbar.contentView.textView.text );
    }

}

#pragma mark change send button enable/disable

-(void)toggleSendBUttonStatus
{
    NSString* signalRStatus=[self.GlobalHandlerInstance getSignalRStatus];
    
    if ([signalRStatus isEqualToString:@"connected"]) {
        [self.inputToolbar.contentView.rightBarButtonItem setEnabled:YES];

    }
    else
    {
        [self.inputToolbar.contentView.rightBarButtonItem setEnabled:NO];

    }

    
}

#pragma camera and photo permissions

- (BOOL)checkCameraAvailability{
    BOOL __block result = NO;
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusAuthorized) {
       // NSLog(@"AVAuthorizationStatusAuthorized");
        result = YES;
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
        //NSLog(@"AVAuthorizationStatusDenied");
        result = NO;
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won&#39;t happen
        //NSLog(@"AVAuthorizationStatusRestricted");
        result = NO;
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if(granted){
                //NSLog(@"Granted access to %@", mediaType);
                result = YES;
            } else {
                //NSLog(@"Not granted access to %@", mediaType);
                result = NO;
            }
        }];
    } else {
        // impossible, unknown authorization status
        result = NO;
    }
    return result;
}


//status
//typedef enum : NSInteger  {
//    PHAuthorizationStatusNotDetermined  = 0,
//    PHAuthorizationStatusRestricted ,
//    PHAuthorizationStatusDenied ,
//    PHAuthorizationStatusAuthorized
//} PHAuthorizationStatus;


#pragma redirect to system settings page within application

-(void)ShowCameraPermissionAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NULL message:[NSString stringWithFormat:NSLocalizedString(@"Vent dose not have \naccess to your camera. To \nenable access, tap Settings \nand turn on Camera.", nil)] delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
    [alert show];
}

-(void)ShowPhotosPermissionAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NULL message:[NSString stringWithFormat:NSLocalizedString(@"Vent dose not have \naccess to your photos or \nvideos. To enable access, tap \nSettings and turn on Photos.", nil)] delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
    [alert show];
}

- (void)openSettings
{
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self openSettings];
    }
}



@end
