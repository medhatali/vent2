//
//  TermsOfServiceViewController.m
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "TermsOfServiceViewController.h"
#import "LoadingView.h"

@interface TermsOfServiceViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblTermsAndService;
@property (weak, nonatomic) IBOutlet UITextView *lblTermsServiceTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;

@end

@implementation TermsOfServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblTermsAndService.text = [NSString stringWithFormat:NSLocalizedString(@"Terms of Service", nil)];
    
    self.lblTermsServiceTxt.text = [NSString stringWithFormat:NSLocalizedString(@"Terms of Service Text", nil)];
    
    [self.btnAgree setTitle:[NSString stringWithFormat:NSLocalizedString(@"Agree", nil)] forState:UIControlStateNormal];
    
    

    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [[[self navigationController]navigationBar] setHidden:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
