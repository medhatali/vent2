//
//  FirstLoadContactsViewController.m
//  Vent
//
//  Created by Medhat Ali on 3/14/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "FirstLoadContactsViewController.h"
#import "ContactsBussinessService.h"
#import "RegisterBussinessService.h"
#import "CommonHeader.h"

@interface FirstLoadContactsViewController ()
@property (strong, nonatomic) LoadingView *loadingView;
@end

@implementation FirstLoadContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadFirstTimeContacts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[[self navigationController]navigationBar] setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
                          dispatch_async(dispatch_get_main_queue(),
                                         ^{
                                              [self.loadingView hideLoadingViewAnimated:NO];
    
                                          });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)loadFirstTimeContacts
{

    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Load Contacts", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
   dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                      dispatch_async(dispatch_get_main_queue(),
                                     ^{
                                          [self.loadingView showLoadingViewAnimated:NO];
                                      });
    
                       ContactsBussinessService *contactsServiceObject =[[ContactsBussinessService alloc]init];
                       RegisterBussinessService *profileServiceObject=[[RegisterBussinessService alloc]init];
                       User * currentUser=[profileServiceObject getCurrentUserProfile];
                       [contactsServiceObject loadContacts:currentUser.userCountry ParentView:self];
                       

                       
                  });
    
    

}

@end
