//
//  ChatListUsersPhones.m
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatListUsersPhones.h"
#import "ChatList.h"


@implementation ChatListUsersPhones

@dynamic chatUserEnabled;
@dynamic chatUserJoinDate;
@dynamic chatUserPhone;
@dynamic phoneChatList;

@end
