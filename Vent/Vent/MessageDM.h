//
//  MessageDM.h
//  Vent
//
//  Created by Sameh Farouk on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "DataModelObjectParent.h"


@interface MessageDM : DataModelObjectParent

typedef enum
{
    text=1,
    hyperLink=2,
    image=3,
    video=4,
    location=5,
    vCard=6,
    audio=7
} ContentType;

@property (strong,nonatomic) NSString *messageId;
@property (strong,nonatomic) NSString *groupId;
@property (strong,nonatomic) NSString *content;
@property (strong,nonatomic) NSString *fromPhoneNumber;
@property (strong,nonatomic) NSArray *toPhoneNumbers;
@property (strong,nonatomic) NSString *deviceToken;
@property (nonatomic) ContentType contentType;
@property (strong,nonatomic) NSDate *sendingDateTime;
@property (nonatomic) NSInteger messageTimer;
@property (strong,nonatomic) NSString *chatlistId;

- (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary;

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields;


@end

