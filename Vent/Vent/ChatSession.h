//
//  ChatSession.h
//  Vent
//
//  Created by Medhat Ali on 7/27/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BroadcastList, ChatList, GroupList;

@interface ChatSession : NSManagedObject

@property (nonatomic, retain) NSString * chatSessionDate;
@property (nonatomic, retain) NSNumber * chatSessionDelivered;
@property (nonatomic, retain) NSString * chatSessionDeliveredDate;
@property (nonatomic, retain) NSString * chatSessionFrom;
@property (nonatomic, retain) NSString * chatSessionGroupID;
@property (nonatomic, retain) NSString * chatSessionID;
@property (nonatomic, retain) NSNumber * chatSessionIsSent;
@property (nonatomic, retain) NSString * chatSessionMessage;
@property (nonatomic, retain) NSString * chatSessionMessageID;
@property (nonatomic, retain) NSNumber * chatSessionRead;
@property (nonatomic, retain) NSString * chatSessionReadDate;
@property (nonatomic, retain) NSNumber * chatSessionSecured;
@property (nonatomic, retain) NSString * chatSessionTo;
@property (nonatomic, retain) NSString * chatSessionType;
@property (nonatomic, retain) NSNumber * chatSessionFaild;
@property (nonatomic, retain) NSString * chatSessionError;
@property (nonatomic, retain) NSString * chatSessionTimer;
@property (nonatomic, retain) NSString * chatSessionMediaUrl;
@property (nonatomic, retain) NSString * chatSessionSmily;
@property (nonatomic, retain) NSString * chatSessionLocation;
@property (nonatomic, retain) NSString * chatSessionVCard;
@property (nonatomic, retain) NSNumber * chatSessionMyMessage;
@property (nonatomic, retain) BroadcastList *chatSessionBroadcastList;
@property (nonatomic, retain) ChatList *chatSessionChatList;
@property (nonatomic, retain) GroupList *chatSessionGroupList;

@end
