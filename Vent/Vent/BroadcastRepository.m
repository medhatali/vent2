//
//  BroadcastRepository.m
//  Vent
//
//  Created by Medhat Ali on 6/11/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BroadcastRepository.h"

@implementation BroadcastRepository

- (id)init
{
    self = [super init];
    if (self) {
        self.repositoryEntityName=@"BroadcastList";
        
    }
    return self;
}



-(BroadcastList *) addNewBroadcastListWithPhoneNumber:(NSString*)broadcastDate
                                   broadcastID:(NSString*)broadcastID
                        broadcastName:(NSString*)broadcastName
                            broadcastListOwner:(User*)broadcastListOwner
                            broadcastChatSession:(ChatSession*)broadcastChatSession
                              broadcastListUsers:(NSArray*)broadcastListUsers
                                      Error:(NSError **)error
{
    
    NSManagedObject *newManagedObject = [self createModelForCurrentEntity:error];
    
        
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:broadcastDate forKey:@"broadcastDate"];
    [newManagedObject setValue:broadcastName forKey:@"broadcastName"];
    [newManagedObject setValue:broadcastListOwner forKey:@"broadcastListOwner"];
    

    if (broadcastChatSession != Nil) {
        [newManagedObject setValue:broadcastChatSession forKey:@"broadcastChatSession"];
    }
    
    if(broadcastID == nil || [broadcastID isEqualToString:@"" ])
    {
        [newManagedObject setValue:[[self uniqueUUID]lowercaseString] forKey:@"broadcastID"];
    }
    
    [self insertModel:newManagedObject EntityName:self.repositoryEntityName Error:error];
    
    // add list of phone numbers to chat list
    
    for (NSString *phoneNumber in broadcastListUsers) {
        [self addUserPhoneToBroadcastList:phoneNumber broadcastUserList:(BroadcastList*)newManagedObject Error:error];
    }
    
    
    
    return (BroadcastList *)newManagedObject;
    
    
}



-(BroadcastListUserPhones *) addUserPhoneToBroadcastList:(NSString*)broadcastUserPhone
                                  broadcastUserList:(BroadcastList*)broadcastUserList
                                          Error:(NSError **)error
{
    
    
    NSManagedObject *newManagedObject = [self createModelByEntityName:@"BroadcastListUserPhones" Error:error];
    
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:broadcastUserPhone forKey:@"broadcastUserPhone"];
    [newManagedObject setValue:broadcastUserList forKey:@"broadcastUserList"];
    
    [self insertModel:newManagedObject EntityName:@"BroadcastListUserPhones" Error:error];
    
    return (BroadcastListUserPhones *)newManagedObject;
    
    
    
    
}

-(void)deleteBroadcastList:(NSManagedObject *)broadcastList Error:(NSError **)error
{
    
    [self deleteModel:broadcastList EntityName:self.repositoryEntityName Error:error];
    
}

-(void)updateBroadcastList:(NSManagedObject *)broadcastList Error:(NSError **)error
{
    
    [self updateModel:broadcastList EntityName:self.repositoryEntityName Error:error];
    
}

-(void)deleteBroadcastListUsersPhones:(BroadcastListUserPhones *)BroadcastListUsersPhones Error:(NSError **)error
{
    
    [self deleteModel:BroadcastListUsersPhones EntityName:@"BroadcastListUserPhones" Error:error];
    
}

-(void)updateBroadcastListUsersPhones:(BroadcastListUserPhones *)BroadcastListUsersPhones Error:(NSError **)error
{
    
    [self updateModel:BroadcastListUsersPhones EntityName:@"BroadcastListUserPhones" Error:error];
    
}


-(NSArray*)getUserBroadcastList:(User*)User Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(broadcastListOwner == %@) ",User];
    // userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    NSArray *sortList=[[NSArray alloc]initWithObjects:@"broadcastDate", nil];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
    
    
    return userdata;
}


-(NSArray*)getUserBroadcastListSorted:(User*)User SortOption:(NSString*)sortOption Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(broadcastListOwner == %@) ",User];
    // userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    NSArray *sortList;
    
    if ([sortOption isEqualToString:sortDefault]) {
        sortList=[[NSArray alloc]initWithObjects:@"broadcastDate", nil];
        userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:sortList descSortStringOrNil:Nil Error:error];
    }
    else if ([sortOption isEqualToString:sortAtoZ]) {
        sortList=[[NSArray alloc]initWithObjects:@"broadcastDate", nil];
        userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
        
    }
    else if ([sortOption isEqualToString:sorttype]) {
        sortList=[[NSArray alloc]initWithObjects:@"broadcastDate", nil];
        userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate ascSortStringOrNil:Nil descSortStringOrNil:sortList Error:error];
    }
    
    
    
    
    
    
    return userdata;
}



-(BroadcastList*)getBroadcastListByStartDate:(User*)User StartDate:(NSString*)startDate Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(broadcastListOwner == %@) AND (broadcastDate == %@) ",User , startDate];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return (BroadcastList*)userdata;
    }
    
    return nil;
}


-(NSArray*)getBroadcastListPhones:(BroadcastList*)BroadcastList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(phoneBroadcastList == %@) ",BroadcastList];
    //   NSArray *sortlist=[NSArray alloc]initWithObjects:@"", nil];
    userdata= [self getResultsFromEntity:@"BroadcastListUsersPhones" predicateOrNil:userpredicate Error:error];
    //    userdata= [self getResultsFromEntity:@"BroadcastListUsersPhones" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    
    return userdata;
}


-(BroadcastListUserPhones*)getBroadcastListUserbyPhone:(NSString*)phoneNumber Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(broadcastUserPhone == %@) ",phoneNumber];
    userdata= [self getResultsFromEntity:@"BroadcastListUserPhones" predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return (BroadcastListUserPhones*)[userdata objectAtIndex:0];
    }
    return nil;
    
}

-(BroadcastList*)getBroadcastListByPhoneNumber:(NSString*)phone Error:(NSError **)error
{
    
    BroadcastListUserPhones *userPhone=[self getBroadcastListUserbyPhone:phone Error:error];
    
    if (userPhone != nil) {
        return (BroadcastList*)userPhone.broadcastUserList;
    }
    
    return nil;
}



-(NSArray*)getBroadcastListUserbyPhones:(NSArray*)phoneNumber Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(broadcastUserPhone in %@) ",phoneNumber];
    userdata= [self getResultsFromEntity:@"BroadcastListUserPhones" predicateOrNil:userpredicate Error:error];
    
    return userdata;
    
}

-(BroadcastList*)getBroadcastListByPhoneNumbers:(NSArray*)phone Error:(NSError **)error
{
    
    NSArray *userPhone=[self getBroadcastListUserbyPhones:phone Error:error];
    
    if (userPhone != nil && userPhone.count >0) {
        return (BroadcastList*)((BroadcastListUserPhones*)[userPhone objectAtIndex:0]).broadcastUserList;
    }
    
    return nil;
}

#pragma -mark chat session

-(NSArray*)getBroadcastListSessions:(BroadcastList*)BroadcastList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionBroadcastList == %@) ",BroadcastList];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    
    return userdata;
}


-(NSArray*)getBroadcastListSessionsWithRecordsNumber:(BroadcastList*)BroadcastList RecordsLimit:(NSInteger)recordsLimit Error:(NSError **)error
{
    NSArray *userdata=[[NSArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionBroadcastList == %@) ",BroadcastList];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist NumberOfRecords:recordsLimit   Error:error];
    
    
    return userdata;
}

-(NSArray*)getUnreadBroadcastListSessions:(BroadcastList*)BroadcastList Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(chatSessionBroadcastList == %@) AND (chatSessionRead == 0) ",BroadcastList];
    NSArray *sortlist=[[NSArray alloc]initWithObjects:@"chatSessionDeliveredDate", nil];
    userdata= [self getResultsFromEntity:@"ChatSession" predicateOrNil:userpredicate ascSortStringOrNil:nil descSortStringOrNil:sortlist Error:error];
    
    
    return userdata;
}


@end
