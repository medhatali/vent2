//
//  GroupProfileViewControler.m
//  Vent
//
//  Created by Medhat Ali on 6/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "GroupProfileViewControler.h"
#import "ContactsBussinessService.h"
#import "CommonHeader.h"
#import "RegisterBussinessService.h"
#import "UploadDownLoadBussinessService.h"
#import "ContactsBussinessService.h"
#import "User.h"
#import "GlobalHandler.h"

@implementation GroupProfileViewControler



-(void)viewDidLoad
{
    
    //self.lblGroupName.text =@"My First Group";
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];

    self.currentUser=[contactBWS getCurrentUserProfile];
    
    
    if ([self.currentUser.userEmailVerified isEqualToNumber:[NSNumber numberWithBool:FALSE]] || self.currentUser.userEmailVerified == nil) {
        self.lblEmailVerified.text =[NSString stringWithFormat:NSLocalizedString(@"Not Verify", nil)];
    }
         else
         {
             self.lblEmailVerified.text =[NSString stringWithFormat:NSLocalizedString(@"Verify", nil)];
         }
         
         
    [[[self profileImageBtn] layer] setCornerRadius: self.profileImageBtn.frame.size.width / 2 ];
    [[[self profileImageBtn] layer] setBorderWidth: 3.0f];
    [[[self profileImageBtn] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [[self profileImageBtn] setClipsToBounds: YES];
    
    
    self.lblDisplayName.text=self.currentUser.userFirstName;
    self.lblFirstName.text=self.currentUser.userLastName;
    self.lblLastName.text=self.currentUser.userPhoneNumber;
    self.lblEmail.text=self.currentUser.userEmail;
    self.lblStatus.text=self.currentUser.userStatus;
    
    
    UIImage *userProfile=[UIImage imageWithData:self.currentUser.userProfileImage];
    if (userProfile != nil) {
        [[self profileImageBtn]  setBackgroundImage:userProfile forState:UIControlStateNormal];
    }

    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.lblDisplayName.text=self.currentUser.userFirstName;
    self.lblFirstName.text=self.currentUser.userLastName;
    self.lblLastName.text=self.currentUser.userPhoneNumber;
    self.lblStatus.text=self.currentUser.userStatus;
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];

    [self.tabBarController.tabBar setHidden:YES];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    
//    self.currentUser.userDisplayName=self.lblDisplayName.text;
//    self.currentUser.userFirstName=self.lblFirstName.text;
//    self.currentUser.userLastName=self.lblLastName.text;

//    UserRepository *repo=[[UserRepository alloc]init];
//    NSError *error;
//    [repo updateUser:self.currentUser Error:&error];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];

    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected row %ld in section %ld" , (long)indexPath.row , (long)indexPath.section);
    
    if (indexPath.section == 2) {
        
        if (indexPath.row ==0) {
            [self performSegueWithIdentifier:@"StatusControl" sender:self];
        }

        
    }
    
    if (indexPath.section == 0) {
        
        UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)], [NSString stringWithFormat:NSLocalizedString(@"Choose Existing", nil)], nil];
        
        [_actionSheet showInView:[self view]];
        
    }

}

#pragma mark - screen navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //    if([segue.identifier isEqualToString:@"NewBroadcast"])
    //    {
    //        NewBroadCastViewController *dest=(NewBroadCastViewController *)segue.destinationViewController;
    //        dest.inUpdateMode = TRUE;
    //        dest.grouContactList =self.selectedModelToUpdate;
    //
    //    }
    
    //     [self performSegueWithIdentifier:@"NewBroadcast" sender:self];
    
    
}


- (IBAction)changeProfilePicture:(id)sender {
    UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"Cancel", nil)] destructiveButtonTitle:nil otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Take Photo", nil)],[NSString stringWithFormat:NSLocalizedString(@"Choose Existing", nil)] , nil];
    
    [_actionSheet showInView:[self view]];
}

#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    UIImagePickerController *_picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = YES;
    
    switch (buttonIndex) {
        case 0:
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Device has no camera", nil)]
                                                                     delegate:nil
                                                            cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                            otherButtonTitles: nil];
                
                [myAlertView show];
            }
            else{
                
                _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:_picker animated:YES completion:NULL];
                
            }
            break;
        case 1:
            _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:_picker animated:YES completion:NULL];
            break;
        default:
            break;
    }
}

#pragma mark - Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *_chosenImage = info[UIImagePickerControllerEditedImage];
    [[self profileImageBtn] setBackgroundImage:_chosenImage forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)doneAction:(id)sender {
    
     if (![self.lblEmail.text isEqualToString:@""]) {
    if (![self isValidEmail:self.lblEmail.text]) {
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Email validation Error", nil)
                                    message:NSLocalizedString(@"Email not in a correct format", nil)
                                   delegate:self
                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                          otherButtonTitles:nil] show];
        
        return;
    }
     }

////        UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
//    
//        self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Update User Profile Image", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
//        
//        dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        
//        dispatch_async(taskQ,
//                       ^{
//                           dispatch_async(dispatch_get_main_queue(),
//                                          ^{
//                                              [self.loadingView showLoadingViewAnimated:YES];
//                                          });
//                           
//                           NSError *error;
//                           
//                           // upload profile image to server
//                           UIImage *profileimage=[[self profileImageBtn]backgroundImageForState:UIControlStateNormal];
//                           NSData *imageData = UIImagePNGRepresentation(profileimage);
//                           
////                           NSString *imagePath = [fileServiceObject uploadImage:imageData ImageName:@"file" ImageExt:@"png" Error:&error];
//                           
//                           self.currentUser.userProfileImage=imageData;
//                           
//                           UserRepository *repo=[[UserRepository alloc]init];
//                           [repo updateUser:self.currentUser Error:&error];
//                           
////                           // update user profile online and local DB
////                           UserProfileDM *profileCurrentUser = [[UserProfileDM alloc]init];
////                           profileCurrentUser.FirstName =self.enterName.text;
////                           profileCurrentUser.LastName =@"";
////                           profileCurrentUser.Status = @"";
////                           profileCurrentUser.ImagePath =imagePath;
////                           profileCurrentUser.DateOfBirth =Nil;
////                           profileCurrentUser.PhoneNumber =self.registrationData.PhoneNumber;
////                           
////                           
////                           bool success=[registerServiceObject updateUserProfile:profileCurrentUser UserModel:self.registrationData Error:&error];
//                           
//                           
//                           if (![self.enterMail.text isEqualToString:@""])
//                           {
//                               ProfileWebService *profileService=[[ProfileWebService alloc]init];
//                               [profileService addEmailToCurrentUser:self.enterMail.text Error:nil];
//                           }
//                           
//                           dispatch_async(dispatch_get_main_queue(),
//                                          ^{
//                                              [self.loadingView hideLoadingViewAnimated:YES];
//                                              
//                                              if (imageData) {
//                                                  
//                                                  // go to contacts
//                                                  
//                                                 // [self performSegueWithIdentifier:@"LoadContacts" sender:self];
//                                                  // [self performSegueWithIdentifier:@"GoProfile" sender:self];
//                                                  
//                                              }
//                                              else
//                                              {
//                                                  [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update Error", nil)
//                                                                              message:NSLocalizedString(@"error in Update Profile", nil)
//                                                                             delegate:nil
//                                                                    cancelButtonTitle:NSLocalizedString(@"OK", nil)
//                                                                    otherButtonTitles:nil] show];
//                                                  
//                                                  // [self performSegueWithIdentifier:@"GoProfile" sender:self];
//                                                  
//                                              }
//                                              
//                                              
//                                          });
//                           
//                       });
//        
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.reachbilityInstance =appDelegate.reachabilityApp;
    
    if ([self.reachbilityInstance isReachable]) {
        
    
    RegisterBussinessService *registerServiceObject =[[RegisterBussinessService alloc]init];
    UploadDownLoadBussinessService *fileServiceObject =[[UploadDownLoadBussinessService alloc]init];
    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Update Profile", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       NSError *error;
                       
                       // upload profile image to server
                       UIImage *profileimage=[[self profileImageBtn]backgroundImageForState:UIControlStateNormal];
                       NSData *imageData = UIImagePNGRepresentation(profileimage);
                       
                       NSString *imagePath = [fileServiceObject uploadImage:imageData ImageName:@"file" ImageExt:@"png" Error:&error];
                       
                       // update user profile online and local DB
                       UserProfileDM *profileCurrentUser = [[UserProfileDM alloc]init];
                       profileCurrentUser.FirstName =self.lblDisplayName.text;
                       profileCurrentUser.LastName =self.lblFirstName.text;
                       profileCurrentUser.Status = self.lblStatus.text;
                       profileCurrentUser.ImagePath =imagePath;
                       profileCurrentUser.DateOfBirth =Nil;
                       
                       ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
                       
                       User* currentUser=[contactBWS getCurrentUserProfile];
                       
                       profileCurrentUser.PhoneNumber =currentUser.userPhoneNumber;
                       
                       
                       bool success=[registerServiceObject updateUserProfile:profileCurrentUser UserModel:nil Error:&error];
                       
                       if (![self.lblEmail.text isEqualToString:@""])
                       {
                           ProfileWebService *profileService=[[ProfileWebService alloc]init];
                           [profileService addEmailToCurrentUser:self.lblEmail.text Error:nil];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                          
                                          if (success) {
                                              
                                              currentUser.userFirstName =profileCurrentUser.FirstName;
                                              currentUser.userLastName =profileCurrentUser.LastName;
                                              currentUser.userStatus =profileCurrentUser.Status;
                                              currentUser.userProfileImageName =profileCurrentUser.ImagePath;
                                              currentUser.userProfileImage =imageData;
                                              currentUser.userEmail = self.lblEmail.text;
                                              currentUser.userDisplayName = self.lblDisplayName.text;
                                              
                                              UserRepository *userRepo=[[UserRepository alloc]init];
                                              [userRepo updateUser:currentUser Error:nil];
                                              
                                              
                                              ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
                                              
                                              self.currentUser=[contactBWS getCurrentUserProfile];
                                              
                                              self.lblDisplayName.text=currentUser.userFirstName;
                                              self.lblFirstName.text=currentUser.userLastName;
                                              self.lblLastName.text=currentUser.userPhoneNumber;
                                              self.lblEmail.text=currentUser.userEmail;
                                              self.lblStatus.text=currentUser.userStatus;
                                              
                                              // update done successfully
                                              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update Done", nil)
                                                                          message:NSLocalizedString(@"Profile updated", nil)
                                                                         delegate:nil
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                otherButtonTitles:nil] show];
                                              
                                          }
                                          else
                                          {
                                              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update Error", nil)
                                                                          message:NSLocalizedString(@"error in Update Profile", nil)
                                                                         delegate:nil
                                                                cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                otherButtonTitles:nil] show];
                                              
                                              // [self performSegueWithIdentifier:@"GoProfile" sender:self];
                                              
                                          }
                                          
                                          
                                      });
                       
                   });
    

    }
    else
    {
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connection Error", nil)
                                    message:NSLocalizedString(@"Please check you internet Connection", nil)
                                   delegate:self
                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                          otherButtonTitles:nil] show];
        
    }




}



- (BOOL)isValidEmail:(NSString *)email
{
    NSString *regex1 = @"\\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z";
    NSString *regex2 = @"^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*";
    NSPredicate *test1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    NSPredicate *test2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex2];
    return [test1 evaluateWithObject:email] && [test2 evaluateWithObject:email];
}

@end
