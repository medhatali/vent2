//
//  BussinessSevericeController.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "BussinessSevericeController.h"



@interface BussinessSevericeController ()


//@property (retain,nonatomic) NSString *loggedUserName;
//@property (retain,nonatomic) NSString *loggedUserPassword;

@end


@implementation BussinessSevericeController

#pragma mark - init

-(id)init
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.reachbilityInstance =appDelegate.reachabilityApp;
//    self.loggedUserName = [[NSUserDefaults standardUserDefaults] valueForKey:kLastLoggedUser];
//    self.loggedUserPassword = [[NSUserDefaults standardUserDefaults] valueForKey:kLastLoggedUserPass];
//    
//    self.currentLoginDM = [[AuthenticationDM alloc]init];
//    self.currentLoginDM .loginName = self.loggedUserName ;
//    self.currentLoginDM .password =self.loggedUserPassword;
    
    return self;
}

-(User *)getCurrentUserProfile
{
    UserRepository *userRepo=[[UserRepository alloc]init];
    
    NSError *error;
    NSString *deviceToken= [[NSUserDefaults standardUserDefaults] valueForKey:KVentDeviceToken];
    User *currentUser=(User *)[userRepo getUserbyUserDeviceUUID:deviceToken Error:&error];
    
    return currentUser;
    
}

//#pragma mark - Login Business
//
//- (StatusDM*)login:(NSString*)UserName Password:(NSString*)password
//{
//    
//    NSError *error;
//    StatusDM * status=[[StatusDM alloc]init];
//    status.statCode =[NSNumber numberWithInteger:1];
//    status.statDescription = @"المستخدم غير موجود";
//    
//    //check user exist in DB
//    UserRepository * userCoreData=[[UserRepository alloc]init];
//    NSMutableArray *userFound = [userCoreData getUserbyName:UserName Error:&error];
//    
//    if ([self.reachbilityInstance isReachable]) {
//        // get from Webservice
//        AuthenticationDM *loginDM = [[AuthenticationDM alloc]init];
//        loginDM.loginName =UserName;
//        loginDM.password =password;
//        UserWebService * loginServiceInstance=[[UserWebService alloc]init];
//        NSDictionary *loginReturn = [loginServiceInstance login:loginDM];
//        
//        status = (StatusDM *)[StatusDM getDataModelObjectFromAPIDictionary:[loginReturn objectForKey:StatusString]];
//        //UserDM *userReturn=(UserDM *)[UserDM getDataModelObjectFromAPIDictionary:[[loginReturn objectForKey:StatusString] objectForKey:userString]];
//        
//        if (status.statCode == [NSNumber numberWithInteger:6]) {
//            
//            [[NSUserDefaults standardUserDefaults] setValue:UserName forKey:kLastLoggedUser];
//            [[NSUserDefaults standardUserDefaults] setValue:password forKey:kLastLoggedUserPass];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            
//            // save User To database
//            if (userFound.count > 0) {
//                //update existing
//                User *founduser =(User*)[userFound objectAtIndex:0];
//                founduser.userPassword = password;
//                [userCoreData updateUser:founduser Error:&error];
//            }
//            else
//            {
//                //insert new one
//                
//                [userCoreData insertNewUser:UserName UserPassword:password Error:&error];
//                
//                
//            }
//            //                 [userCoreData deleteAllUsers:UserName Error:&error];
//            //                 [userCoreData insertNewUser:UserName UserPassword:password Error:&error];
//            
//            
//            
//            //return [NSNumber numberWithInteger:0];
//            return  status;
//            
//        }
//        else
//        {
//            //                #warning Medhat: to be removed when web service work
//            //                return [NSNumber numberWithInteger:0];
//            
//            return  status;
//        }
//        
//        
//        
//    }
//    else
//    {
//        // get from Data base by user name and password
//        if (userFound.count > 0) {
//            
//            User *founduser= (User *)[userFound objectAtIndex:0];
//            if ([founduser.userPassword isEqualToString:password]) {
//                status.statCode =[NSNumber numberWithInteger:6];
//                return status;
//            }
//            
//        }
//        
//        
//        
//    }
//    
//    return status;
//    
//}
//
//#pragma mark - Load all data orders , items , projects
//
//- (BOOL)checkOfflineUpdates
//{
//    NSError *error;
//    StatusDM * status=[[StatusDM alloc]init];
//    status.statCode =[NSNumber numberWithInteger:1];
//    status.statDescription = @"المستخدم غير موجود";
//    
//    UserRepository * userCoreData=[[UserRepository alloc]init];
//    NSMutableArray *userFound = [userCoreData getUserbyName:self.loggedUserName Error:&error];
//    
//    OrderListRepository * ordersCoreData=[[OrderListRepository alloc]init];
//    NSMutableArray *offlineUpdatedOrders=[ordersCoreData getOfflineOrdersForUser:[userFound objectAtIndex:0] Error:&error];
//    
//    if (offlineUpdatedOrders.count > 0) {
//        return TRUE;
//    }
//    
//    return FALSE;
//}



    
@end
