//
//  UserProfile.m
//  Vent
//
//  Created by Medhat Ali on 3/4/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "UserProfileDM.h"

@implementation UserProfileDM


NSString *const iFirstNameP = @"FirstName";
NSString *const iLastNameP = @"LastName";
NSString *const iStatusP = @"Status";
NSString *const iImagePathP = @"ImagePath";
NSString *const iDateOfBirthP = @"DateOfBirth";
NSString *const iPhoneNumberP = @"PhoneNumber";

+ (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    UserProfileDM *userDataInfo = [[UserProfileDM alloc]init];
    
    userDataInfo.FirstName = [dataDictionary objectForKey:iFirstNameP];
    userDataInfo.LastName = [dataDictionary objectForKey:iLastNameP];
    userDataInfo.Status = [dataDictionary objectForKey:iStatusP];
    userDataInfo.ImagePath = [dataDictionary objectForKey:iImagePathP];
    userDataInfo.DateOfBirth = [dataDictionary objectForKey:iDateOfBirthP];
     userDataInfo.PhoneNumber = [dataDictionary objectForKey:iPhoneNumberP];
    
    return userDataInfo;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    
    NSMutableArray *keysArray = [[NSMutableArray alloc]init];
    NSMutableArray *valuesArray = [[NSMutableArray alloc]init];
    
    
    if (self.FirstName) {
        [keysArray addObject:iFirstNameP];
        [valuesArray addObject:self.FirstName];
    }
    
    if (self.LastName) {
        [keysArray addObject:iLastNameP];
        [valuesArray addObject:self.LastName];
    }
    
    if (self.DateOfBirth) {
        [keysArray addObject:iDateOfBirthP];
        [valuesArray addObject:self.DateOfBirth];
    }
    
    if (self.Status) {
        [keysArray addObject:iStatusP];
        [valuesArray addObject:self.Status];
    }
    
    if (self.ImagePath) {
        [keysArray addObject:iImagePathP];
        [valuesArray addObject:self.ImagePath];
    }
    
    if (self.PhoneNumber) {
        [keysArray addObject:iPhoneNumberP];
        [valuesArray addObject:self.PhoneNumber];
    }
    
    
    NSDictionary *dictionary = [[NSMutableDictionary alloc]initWithObjects:valuesArray forKeys:keysArray];
    
    return dictionary;
}



@end
