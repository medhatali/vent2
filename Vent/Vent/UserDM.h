//
//  UserDM.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "DataModelObjectParent.h"

@interface UserDM : DataModelObjectParent

/// properties for data retrieved
@property (nonatomic, strong) NSString *DeviceToken;
@property (nonatomic, strong) NSString *DisplayName;
@property (nonatomic, strong) NSString *PhoneNumber;
@property (nonatomic, strong) NSString *DeviceName;
@property (nonatomic, strong) NSNumber *OSType;

@end
