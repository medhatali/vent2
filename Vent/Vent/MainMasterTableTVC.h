//
//  MainMasterTableTVC.h
//  Vent
//
//  Created by Medhat Ali on 3/17/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"
#import "FCVerticalMenuItem.h"
#import "FCVerticalMenu.h"
#import "BaseSearchTableViewController.h"
#import "SWTableViewCell.h"
#import "ChatListHeaderTableViewCell.h"
#import "ChatListTableViewCell.h"
#import "ContactListDelegate.h"
#import "GlobalHandler.h"
#import "ChatTopBarView.h"
#import "PushScreenDM.h"
#import "CommonHeader.h"
#import "ChatListRepository.h"
#import "GroupListRepository.h"
#import "AppUtility.h"

@interface MainMasterTableTVC : BaseSearchTableViewController<BaseSearchTableViewControllerDelegate,UIAlertViewDelegate>

@property (retain,nonatomic) UIView *offlineView;
@property (retain,nonatomic) Reachability *reachbilityInstance;
@property (strong, nonatomic) LoadingView *loadingView;
@property (strong, nonatomic) ChatTopBarView *topView;



@property (nonatomic,strong) ChatListRepository * chatListRepo;
@property (nonatomic,strong) GroupListRepository *GroupListRepo;
@property NSInteger totalUnread;
@property NSInteger totalUnreadGroup;

@property (strong, nonatomic) NSString *cellIdentifier;
@property (strong, readonly, nonatomic) FCVerticalMenu *verticalMenu;
@property (strong, nonatomic) NSString *selectedTopMenu;
@property NSInteger selectedTopMenuIndex;
@property BOOL enableTableEdit;

@property BOOL enableDeepLinking;
@property (strong, nonatomic) id selectedModelForDeepLinking;

@property (weak, nonatomic) GlobalHandler *GlobalHandlerInstance;
@property (strong, nonatomic) SignalRHandler *signalRHandlerInstance;

-(void) showConnectionDown;
-(void) createOfflineView;
-(void)configureVerticalMenu;


- (NSArray *)rightSwipButtons:(NSInteger)numberofButtons IsBlocked:(NSNumber*)isBlocked IsMute:(NSNumber*)isMute;
- (NSArray *)rightSwipButtonsFavourite:(NSInteger)numberofButtons;
- (NSArray *)rightSwipButtonsGroup:(NSInteger)numberofButtons  IsBlocked:(NSNumber*)isBlocked IsMute:(NSNumber*)isMute
;

- (SelectionType)selectionType;
- (NSMutableArray*) searchResultsForSearchText:(NSString *)searchText;
- (UITableViewCell*) resultsCellForRowAtIndexPath:(NSIndexPath *)indexPath;

-(void)configNavigationBarItems:(BOOL)enableEdit;
-(void)configNavigationBarItemsForGroup:(BOOL)enableEdit;
- (IBAction)selectChatsession:(id)sender;
- (IBAction)selectGroupsession:(id)sender;
-(void)getUnreadMessageForTabBar;

@end
