//
//  HandlerProtocol.m
//  Vent
//
//  Created by Medhat Ali on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HandlerProtocol <NSObject>

@optional
-(void) onSendMessage: message;
-(void) onGetAllOnlineFriends: onlineFriendsIdentifiers;
-(void) onNotifyFriendsUserJoined: userIdentifier;
-(void) onNotifyFriendsUserLeft: userIdentifier;
-(void) onMarkMessageAsDelivered: messageId;
-(void) onMarkMessageAsSent: messageId;
-(void) onMarkMessageAsRead: messageId;
-(void) onGetNewMessages: messages;
-(void) onUserStartTyping: userIdentifier;
-(void) onCanResend: canResend : messageId;
-(void) onLoadAllGroups: group;
-(void) onNotifyUserJoinApp: userIdentifier;
-(void) onStopClient;
-(void) onGetNotifications: notifications;
-(void) onEmailVerified: email;
-(void) onCreatedGroup: groupId;
-(void) onAddedToGroup: group;
-(void) onAddUserToGroup: userIdentifier : groupId;
-(void) onNotifyMembersUserJoinedGroup: userIdentifier : groupId;
-(void) onNotifyMembersUserLeftGroup: userIdentifier : groupId;
-(void) onLeavedGroup : groupId;
-(void) onChangeGroupAdmin: userIdentifier : groupId;
-(void) onRemovedUserFromGroup: userIdentifier : groupId;
-(void) onUpdatedGroupTitle: groupId;
-(void) onNotifyMembersTitleChanged: title : groupId;
-(void) onNotifyMembersIconChanged: path : groupId;
-(void) onUpdatedGroupIcon: groupId;
-(void) onGetAllGroups : groupIds;
-(void) onGetGroup : group;

-(void)started;
-(void)received:(NSDictionary *) data;
-(void)error:(NSError *)error;
-(void)closed;
-(void)reconnecting;
-(void)reconnected;
-(void)stateChanged:(connectionState) connectionState;
-(void)connectionSlow;

@end
