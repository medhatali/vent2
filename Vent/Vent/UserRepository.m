//
//  UserRepository.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "UserRepository.h"


@implementation UserRepository

static dispatch_once_t pred;
static UserRepository *sharedProfile;

+ (id) sharedProfile {
    
    dispatch_once(&pred, ^{
        sharedProfile = [[UserRepository alloc] init];
    });
    
    return sharedProfile;
    
}

- (id)init
{
    self = [super init];
    if (self) {
        self.repositoryEntityName=@"User";
        
    }
    return self;
}

-(User *) addUser:(NSString*)userDeviceUUID
            userDeviceName:(NSString*)userDeviceName
            userDisplayName:(NSString*)userDisplayName
            userEmail:(NSString*)userEmail
            userPhoneNumber:(NSString*)userPhoneNumber
            userCountry:(NSString*)userCountry
            userIsRegistered:(NSNumber*)userIsRegistered
            userIsSecureLogin:(NSNumber*)userIsSecureLogin
            userFirstName:(NSString*)userFirstName
            userLastName:(NSString*)userLastName
            userStatus:(NSString*)userStatus
            userProfileImageName:(NSString*)userProfileImageName
            userProfileImagePath:(NSString*)userProfileImagePath
            userDate:(NSString*)userDate
            userActive:(NSNumber*)userActive
            userProfileImage:(NSData*)userProfileImage
            userPin:(NSString*)userPin
            userCountryCode:(NSString*)userCountryCode
            Error:(NSError **)error
{

    NSManagedObject *newManagedObject = [self createModelForCurrentEntity:error];
    
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:userDeviceUUID forKey:@"userDeviceUUID"];
    [newManagedObject setValue:userDeviceName forKey:@"userDeviceName"];
    [newManagedObject setValue:userDisplayName forKey:@"userDisplayName"];
    [newManagedObject setValue:userEmail forKey:@"userEmail"];
    [newManagedObject setValue:userPhoneNumber forKey:@"userPhoneNumber"];
    [newManagedObject setValue:userCountry forKey:@"userCountry"];
    [newManagedObject setValue:userIsRegistered forKey:@"userIsRegistered"];
    [newManagedObject setValue:userIsSecureLogin forKey:@"userIsSecureLogin"];
    
    [newManagedObject setValue:userFirstName forKey:@"userFirstName"];
    [newManagedObject setValue:userLastName forKey:@"userLastName"];
    [newManagedObject setValue:userStatus forKey:@"userStatus"];
    [newManagedObject setValue:userProfileImageName forKey:@"userProfileImageName"];
    [newManagedObject setValue:userProfileImagePath forKey:@"userProfileImagePath"];
    
    [newManagedObject setValue:userDate forKey:@"userDate"];
    [newManagedObject setValue:userActive forKey:@"userActive"];
    
    if (userProfileImage != Nil) {
    [newManagedObject setValue:userProfileImage forKey:@"userProfileImage"];
    }
    
    [newManagedObject setValue:userPin forKey:@"userPin"];
    [newManagedObject setValue:userCountryCode forKey:@"userCountryCode"];
    
//    if (credentials != NULL) {
//        [newManagedObject setValue:credentials forKey:@"credentials"];
//    }
//    if (sites != NULL) {
//        [newManagedObject setValue:sites forKey:@"sites"];
//        
//    }
    
    
    [self insertModel:newManagedObject EntityName:self.repositoryEntityName Error:error];
    
    [[UserRepository sharedProfile] setPublicUserProfile:(User*)newManagedObject];
    
    return (User *)newManagedObject;
    
    
}



-(void)deleteUser:(User *)user Error:(NSError **)error
{
    
    [self deleteModel:user EntityName:self.repositoryEntityName Error:error];
    
}

-(void)updateUser:(User *)user Error:(NSError **)error
{
    
    [self updateModel:user EntityName:self.repositoryEntityName Error:error];
    
    
}

-(NSManagedObject*)getUserbyDeviceName:(NSString*)userDeviceName Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(userDeviceName == %@) ",userDeviceName];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count > 0) {
        return [userdata objectAtIndex:0];
    }
    return nil;
}

-(User*)getUserbyUserDeviceUUID:(NSString*)userDeviceUUID Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(userDeviceUUID == %@) ",userDeviceUUID];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        [[UserRepository sharedProfile] setPublicUserProfile:(User*)[userdata objectAtIndex:0]];
        return [userdata objectAtIndex:0];
    }
    
    return nil;
}


-(NSMutableArray*)getAllUser:(NSString*)username Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    userdata= [self getAllRows:self.repositoryEntityName predicateOrNil:nil ascSortStringOrNil:nil descSortStringOrNil:nil Error:error];
    
    return userdata;
}


@end
