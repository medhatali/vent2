//
//  UserDM.m
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "UserDM.h"

@implementation UserDM

/// properties for data send


NSString *const iDeviceToken = @"DeviceToken";
NSString *const iDisplayName = @"DisplayName";
NSString *const iPhoneNumber = @"PhoneNumber";
NSString *const iDeviceName = @"DeviceName";
NSString *const iOSType = @"OperatingSystem";


+ (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    UserDM *userDataInfo = [[UserDM alloc]init];
    
    userDataInfo.DeviceToken = [dataDictionary objectForKey:iDeviceToken];
    userDataInfo.DisplayName = [dataDictionary objectForKey:iDisplayName];
    userDataInfo.PhoneNumber = [dataDictionary objectForKey:iPhoneNumber];
    userDataInfo.DeviceName = [dataDictionary objectForKey:iDeviceName];
    userDataInfo.OSType = [dataDictionary objectForKey:iOSType];

    
    return userDataInfo;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    
    NSMutableArray *keysArray = [[NSMutableArray alloc]init];
    NSMutableArray *valuesArray = [[NSMutableArray alloc]init];
    
    
    if (self.DeviceToken) {
        [keysArray addObject:iDeviceToken];
        [valuesArray addObject:self.DeviceToken];
    }
    
    if (self.DisplayName) {
        [keysArray addObject:iDisplayName];
        [valuesArray addObject:self.DisplayName];
    }
    
    if (self.PhoneNumber) {
        [keysArray addObject:iPhoneNumber];
        [valuesArray addObject:self.PhoneNumber];
    }
    
    if (self.DeviceName) {
        [keysArray addObject:iDeviceName];
        [valuesArray addObject:self.DeviceName];
    }
    
    if (self.OSType) {
        [keysArray addObject:iOSType];
        [valuesArray addObject:self.OSType];
    }
    
    
    NSDictionary *dictionary = [[NSMutableDictionary alloc]initWithObjects:valuesArray forKeys:keysArray];
    
    return dictionary;
}



@end
