//
//  ChatSessionViewController.h
//  Vent
//
//  Created by Medhat Ali on 4/18/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "JSQMessages.h"

#import "DemoModelData.h"
#import "NSUserDefaults+DemoSettings.h"
#import "GlobalHandler.h"
#import "ChatList.h"
#import "Contacts.h"
#import "GroupList.h"
#import "BroadcastList.h"
#import "ContactListDelegate.h"
#import "TopChatDelegate.h"
#import "Contacts.h"
#import "VentChatDataModel.h"
#import "ContactsBussinessService.h"
#import "MainMasterTableTVC.h"

typedef NS_ENUM(NSInteger, ChatType) {
    SingleChat          = 0,
    ConversationChat     = 1,
    BroacastChat = 2,
    GroupChat = 3,
};

@class DemoMessagesViewController;

@protocol JSQDemoViewControllerDelegate <NSObject>

- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc;

@end



@interface ChatSessionViewController :JSQMessagesViewController <UIActionSheetDelegate,ContactListDelegate,TopChatDelegate,UpdateChatCellProtocol>

@property (weak, nonatomic) id<JSQDemoViewControllerDelegate> delegateModal;
@property (weak, nonatomic) GlobalHandler *GlobalHandlerInstance;
@property (strong, nonatomic) VentChatDataModel *ventChatDM;
@property (strong, nonatomic) DemoModelData *demoData;
@property (retain, nonatomic) NSArray *chatsession;

@property (weak, nonatomic) ChatList *selectedChatList;
@property (weak, nonatomic) GroupList *selectedGroupList;
@property (weak, nonatomic) BroadcastList *selectedBroadcastList;
@property (weak, nonatomic) MainMasterTableTVC *parentMainVC;
@property NSInteger totalUnread;

@property (strong, nonatomic) User *senderContact;
@property (strong, nonatomic) Contacts *reciverContact;
@property (strong, nonatomic) NSMutableArray *reciverContactPhones;
@property (weak, nonatomic) ChatTopBarView *topView;

@property BOOL isVcardSelection;

@property (strong, nonatomic)NSTimer* timerSecure;

@property ( nonatomic, assign) ChatType chatTypeSession;

@property (strong, nonatomic) ContactsBussinessService * contactBWS;
@property (strong, nonatomic)User* currentUser;

@property NSInteger numberOflimitedRecords;


- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate;

-(void)reloadAllData;

- (void)receiveMessagePressed:(UIBarButtonItem *)sender;

- (void)closePressed:(UIBarButtonItem *)sender;

-(void)sendMessageToServer:(MessageDM *)messageModel ImageDate:(NSData*)imageData Contacts:(Contacts*)contact;
-(void)recieveMessageFromServer:(MessageDM *)messageModel;

@end
