//
//  NSDate+DateFormater.m
//  Vent
//
//  Created by Medhat Ali on 9/10/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//


#import "NSDate+DateFormater.h"

@implementation NSDate (DateFormater)

#pragma -mark Date Handling
-(NSString *)dateFormatedWithStyle:(NSDateFormatterStyle)style{
    NSDateFormatter *formater= [[NSDateFormatter alloc] init];
    [formater setLocale:[NSLocale currentLocale]];
    [formater setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [formater setDateStyle:style];
    [formater setTimeStyle:NSDateFormatterNoStyle];
    return [formater stringFromDate:self];
}

- (NSString*)getDateString:(NSDate*)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:DefaultFormatDateTime];
    return [formatter stringFromDate:date];
}

- (NSString*)getShortDateString:(NSDate*)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:DefaultShortFormatDateTime];
    return [formatter stringFromDate:date];
}

- (NSDate*)getDateFromString:(NSString*)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:DefaultFormatDateTime];
    return [formatter dateFromString:date];
}

- (NSDate*)getShortDateFromServerString:(NSString*)date
{
    NSDateFormatter *formatterFrom = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterFrom setLocale:locale];
    [formatterFrom setDateFormat:ServerDateTimeFormat];
    NSDate *source= [formatterFrom dateFromString:date];
    
    NSDateFormatter *formatterTo= [NSDateFormatter new];
    [formatterTo setDateFormat:DefaultShortFormatDateTime];
    
    return [formatterTo dateFromString:[formatterTo stringFromDate:source]];
}

-(NSDate*) getDatefromServerDateString:(NSString*)serverDateString
{
    NSDateFormatter *formatterFrom = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterFrom setLocale:locale];
    [formatterFrom setDateFormat:ServerDateTimeFormat];
    return [formatterFrom dateFromString:serverDateString];
}

- (NSString*)getServerDateString:(NSDate*)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:ServerDateTimeFormat];
    return [formatter stringFromDate:date];
}

- (NSDate*)getServerDateFromDate:(NSDate*)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:ServerDateTimeFormat];
    NSString *source= [formatter stringFromDate:date];
    return [formatter dateFromString:source];
}


- (NSDate*)getDateFromServerString:(NSString*)date
{
    NSDateFormatter *formatterFrom = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterFrom setLocale:locale];
    [formatterFrom setDateFormat:ServerDateTimeFormat];
    NSDate *source= [formatterFrom dateFromString:date];
    
    NSDateFormatter *formatterTo= [NSDateFormatter new];
    [formatterTo setDateFormat:DefaultFormatDateTime];
    
    return [formatterTo dateFromString:[formatterTo stringFromDate:source]];
}

- (NSDate*)getFormatDateFromDate:(NSDate*)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:DefaultFormatDateTime];
    return [formatter dateFromString:[formatter stringFromDate:date]];
}

#pragma -mark Time Handling

-(NSString *)timeFormatedWithStyle:(NSDateFormatterStyle)style{
    return [self timeFormatedWithStyle:style withTimeZoneInHours:0];
}

-(NSString *)timeFormatedWithStyle:(NSDateFormatterStyle)style withTimeZone:(NSTimeZone *)timezone{
    return [self timeFormatedWithStyle:style withTimeZoneInHours:[timezone secondsFromGMT]];
}

-(NSString *)timeFormatedWithStyle:(NSDateFormatterStyle)style withTimeZoneInHours:(NSInteger)timeZoneInHours{
    
    NSInteger timeZoneInSeconds=timeZoneInHours*60*60;
    
    NSDateFormatter *formater= [[NSDateFormatter alloc] init];
    [formater setLocale:[NSLocale currentLocale]];
    [formater setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:timeZoneInSeconds]];
    [formater setDateStyle:NSDateFormatterNoStyle];
    [formater setTimeStyle:style];
    return [formater stringFromDate:self];
}

#pragma -mark Date And Time Handling
-(NSString *)dateFormatedWithFormat:(NSString*)format{
    return [self dateFormatedWithFormat:format withTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
}

-(NSString *)dateFormatedWithFormat:(NSString *)format withTimeZone:(NSTimeZone *)timezone{
    NSString *output;
    
    NSDateFormatter *formater= [[NSDateFormatter alloc] init];
    [formater setLocale:[NSLocale currentLocale]];
    [formater setTimeZone:timezone];
    //this condition only to solve 24 hours format bug
    if (([format rangeOfString:@"HH"].location != NSNotFound) && ![self isTimeIs24]) {
        
        
        [formater setDateFormat:[format stringByReplacingOccurrencesOfString:@"HH" withString:[self to24Hours]]];
        output= [formater stringFromDate:self];
        
    }else{
        [formater setDateFormat:format];
        output= [formater stringFromDate:self];
    }
    return output;
}

-(NSString *)dateFormatedWithStyle:(NSDateFormatterStyle)dateStyle timeFormatedWithStyle:(NSDateFormatterStyle)timeStyle{
    return [self dateFormatedWithStyle:dateStyle timeFormatedWithStyle:timeStyle withTimeZoneInHours:0];
}

-(NSString *)dateFormatedWithStyle:(NSDateFormatterStyle)dateStyle timeFormatedWithStyle:(NSDateFormatterStyle)timeStyle withTimeZone:(NSTimeZone *)timezone{
    return [self dateFormatedWithStyle:dateStyle timeFormatedWithStyle:timeStyle withTimeZoneInHours:[timezone secondsFromGMT]];
}

-(NSString *)dateFormatedWithStyle:(NSDateFormatterStyle)dateStyle timeFormatedWithStyle:(NSDateFormatterStyle)timeStyle withTimeZoneInHours:(NSInteger)timeZoneInHours{
    
    NSInteger timeZoneInSeconds=timeZoneInHours*60*60;
    
    NSDateFormatter *formater= [[NSDateFormatter alloc] init];
    [formater setLocale:[NSLocale currentLocale]];
    [formater setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:timeZoneInSeconds]];
    [formater setDateStyle:dateStyle];
    [formater setTimeStyle:timeStyle];
    return [formater stringFromDate:self];
}

#pragma -mark- 24 Hours Workaround
-(BOOL)isTimeIs24{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:self];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    return (amRange.location == NSNotFound && pmRange.location == NSNotFound);
}

-(NSString *)to24Hours{
    NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
   	[dateFormatter setDateFormat:@"hha"];
    
    NSString* fullTime = [dateFormatter stringFromDate:self] ;
    
    NSString* meridian=[fullTime substringFromIndex:2];
    NSString* time = [fullTime substringToIndex:2];
    
    
    int hour = [time intValue];
    
    if([meridian isEqualToString:@"AM"]) {
        if (hour==12) {
            hour=0;
        }
    }else if([meridian isEqualToString:@"PM"]){
        if (hour!=12) {
            hour += 12;
        }
    }
    
    time = [NSString stringWithFormat:@"%02d", hour];
    
    return time;
}

@end

