//
//  FileGetDM.m
//  Vent
//
//  Created by Sameh Farouk on 5/19/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "FileGetDM.h"

@implementation FileGetDM

NSString *const fType = @"type";
NSString *const fId = @"id";


+ (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    FileGetDM *file = [[FileGetDM alloc]init];
    
    file.type = (StoreType)[dataDictionary objectForKey:fType];
    file.FileId = [dataDictionary objectForKey:fId];
    
    
    return file;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    
    NSMutableArray *keysArray = [[NSMutableArray alloc]init];
    NSMutableArray *valuesArray = [[NSMutableArray alloc]init];
    
    
    if (self.type) {
        [keysArray addObject:fType];
        [valuesArray addObject:[NSNumber numberWithInt:self.type]];
    }
    
    if (self.FileId) {
        [keysArray addObject:fId];
        [valuesArray addObject:self.FileId];
    }
    
    NSDictionary *dictionary = [[NSMutableDictionary alloc]initWithObjects:valuesArray forKeys:keysArray];
    
    return dictionary;
}


@end
