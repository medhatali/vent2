//
//  NewBroadCastViewController.h
//  Vent
//
//  Created by Medhat Ali on 6/11/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"
#import "BroadcastList.h"

@interface NewBroadCastViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *lblContatcs;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnCreateUpdate;

@property (retain,nonatomic) Reachability *reachbilityInstance;
@property (strong, nonatomic) LoadingView *loadingView;
@property (weak, nonatomic) IBOutlet UITextField *txtListName;

@property (strong, nonatomic)  NSMutableArray *grouContactList;
@property (strong, nonatomic)  NSMutableDictionary *grouContactListNew;
@property (weak, nonatomic) BroadcastList *selectedBroadcastListTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblContacts;

@property BOOL inUpdateMode;
@end
