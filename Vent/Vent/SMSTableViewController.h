//
//  SMSTableViewController.h
//  Smiley
//
//  Created by Sameh Farouk on 6/13/14.
//  Copyright (c) 2014 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterationTVC.h"
#import "UserDM.h"
#import "Country.h"

@interface SMSTableViewController : RegisterationTVC


@property (strong, nonatomic) UserDM* registrationData;
@property (strong, nonatomic) NSString* coountryCode;
@property (strong, nonatomic) Country* country;

@end
