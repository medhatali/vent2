 //
//  MessageNotificationBussiness.m
//  Vent
//
//  Created by Medhat Ali on 5/17/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "MessageNotificationBussiness.h"
#import "MessageDM.h"
#import "ChatListRepository.h"
#import "ChatSessionRepository.h"
#import "GroupListRepository.h"
#import "ContactsBussinessService.h"
#import "ContactsRepository.h"
#import "NSDate+DateFormater.h"
#import "MessageTimeDM.h"
#import "BroadcastRepository.h"
#import "BroadcastList.h"
#import "JSQSystemSoundPlayer.h"


@implementation MessageNotificationBussiness

- (id)didRecieveSignalRMessage:(DataModelObjectParent*)messageModel IsSent:(Boolean)isSent Error:(NSError**)error
{
    MessageDM* currentMessage=(MessageDM*)messageModel;
    ChatSessionRepository * chatSessionRepo =[[ChatSessionRepository alloc]init];
    
 
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    NSString *currentStringDate=[[NSDate alloc]getServerDateString:[NSDate date]];
    
    NSString *currentdate=[NSString stringWithFormat:@"%@",currentStringDate];
    
    
    if ((currentMessage.groupId == (id)[NSNull null] || currentMessage.groupId == nil)&&currentMessage.messageId != nil) {
        // chat list not group
        ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
        
        if (currentMessage.toPhoneNumbers.count > 1) {
            //conversation chat
            
            
          //  ChatList* currentChatList=[chatListRepo getChatListByPhoneNumbers:currentMessage.toPhoneNumbers Error:error];
            ChatList* currentChatList=[chatListRepo getChatListByID:currentMessage.messageId Error:error];

            
            NSString *messageType =@"";
            if (currentMessage.contentType != 0) {
                messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
            }
            
            if (currentChatList == nil)
            {
                
                
                
                currentChatList = [chatListRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Conversation" chatLastUpdatedDate:currentdate chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:FALSE] chatID:@"" chatName:@"Conversation" chatLastMessage:@"" chatUserImage:nil chatUserImageUrl:@"" chatFromPhone:currentMessage.fromPhoneNumber chatToPhone:@"" chatIsFavourite:[NSNumber numberWithBool:FALSE] chatListOwner: currentUser chatListSession:nil chatListUsers:currentMessage.toPhoneNumbers chatIsSecure:[NSNumber numberWithBool:NO] Error:error];
            }
            
            if ([currentChatList.chatIsBlocked isEqualToNumber:[NSNumber numberWithBool:YES]]) {
                return currentChatList;
            }
            
            [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentChatList.chatName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:TRUE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                         chatSessionGroupID:currentMessage.groupId
                            chatSessionType:messageType
                   chatSessionDeliveredDate:currentdate
                        chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                           chatSessionFaild:[NSNumber numberWithBool:FALSE]
                           chatSessionError:@""
                           chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                        chatSessionMediaUrl:@""
                           chatSessionSmily:@""
                        chatSessionLocation:@""
                           chatSessionVCard:@""
                       chatSessionMyMessage:[NSNumber numberWithBool:FALSE]
                        chatSessionChatList:currentChatList
                       chatSessionGroupList:nil chatSessionBroadcastList:nil Error:error];
            
            
            currentChatList.chatLastUpdatedDate =currentdate;
            [chatListRepo updateChatList:currentChatList Error:error];

            
            
            
            return currentChatList;
            
        }
        else
        {
            //one to one chat
            
            ChatList* currentChatList=[chatListRepo getChatListByPhoneNumber:currentMessage.fromPhoneNumber Error:error];
            
            
            NSString* messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
            
            NSString* groupID=[NSString stringWithFormat:@"%@", currentMessage.groupId];
            
            if (currentChatList.fault || currentChatList == nil)
            {
                
                ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
                
                NSString *fromDetail=@"";
                Contacts* fromContact= [contactsRepo getUserContactByPhoneNumber:currentMessage.fromPhoneNumber contactUser:currentUser Error:error];
                
                if(fromContact == nil)
                {
                    fromDetail =currentMessage.fromPhoneNumber;
                }
                else
                {
                    fromDetail =fromContact.contactFullName;
                }
                
                NSArray * toPhoneUsers=[[NSArray alloc]initWithObjects:currentMessage.fromPhoneNumber, nil];
                
                currentChatList = [chatListRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Chat" chatLastUpdatedDate:currentdate chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:NO] chatID:@"" chatName:fromDetail chatLastMessage:@"" chatUserImage:nil chatUserImageUrl:@"" chatFromPhone:currentUser.userPhoneNumber  chatToPhone:currentMessage.fromPhoneNumber chatIsFavourite:[NSNumber numberWithBool:FALSE] chatListOwner: currentUser chatListSession:nil chatListUsers:toPhoneUsers chatIsSecure:[NSNumber numberWithBool:NO] Error:error];
            }
            
            if ([currentChatList.chatIsBlocked isEqualToNumber:[NSNumber numberWithBool:YES]]) {
                return currentChatList;
            }
            
            [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentChatList.chatName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:TRUE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                         chatSessionGroupID:groupID
                            chatSessionType:messageType
                   chatSessionDeliveredDate:currentdate
                        chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                           chatSessionFaild:[NSNumber numberWithBool:FALSE]
                           chatSessionError:@""
                           chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                        chatSessionMediaUrl:@""
                           chatSessionSmily:@""
                        chatSessionLocation:@""
                           chatSessionVCard:@""
                       chatSessionMyMessage:[NSNumber numberWithBool:FALSE]
                        chatSessionChatList:currentChatList chatSessionGroupList:nil chatSessionBroadcastList:nil Error:error];
            
            //[chatListRepo updateChatList:currentChatList Error:error];
            
            
            currentChatList.chatLastUpdatedDate =currentdate;
            [chatListRepo updateChatList:currentChatList Error:error];
            
            
            
            
            return currentChatList;
            
        }
    }
    else
    {
        // group message not
        
        GroupListRepository *groupListRepo=[[GroupListRepository alloc]init];
        
        
        
        //GroupList* currentGroupList=[groupListRepo getGrouptListByPhoneNumbers:@[currentMessage.fromPhoneNumber] Error:error];
        GroupList* currentGroupList=[groupListRepo getUserGroupListbyGroupID:currentMessage.groupId Error:error];
        
        NSString *messageType =@"";
        if (currentMessage.contentType != 0) {
            messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
        }
        
        if (currentGroupList != nil)
        {
            
            // group must be exist before recieving any messages
            //currentGroupList.groupName
            
            if ([currentGroupList.groupIsBlocked isEqualToNumber:[NSNumber numberWithBool:YES]]) {
                return currentGroupList;
            }
            
            [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentMessage.fromPhoneNumber chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:TRUE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                         chatSessionGroupID:currentMessage.groupId
                            chatSessionType:messageType
                   chatSessionDeliveredDate:currentdate
                        chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                           chatSessionFaild:[NSNumber numberWithBool:FALSE]
                           chatSessionError:@""
                           chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                        chatSessionMediaUrl:@""
                           chatSessionSmily:@""
                        chatSessionLocation:@""
                           chatSessionVCard:@""
                       chatSessionMyMessage:[NSNumber numberWithBool:FALSE]
                        chatSessionChatList:nil
                       chatSessionGroupList:currentGroupList chatSessionBroadcastList:nil Error:error];
            
            
            currentGroupList.groupLastUpdatedDate =currentdate;
            [groupListRepo updateGroupList:currentGroupList Error:error];
            
            return currentGroupList;
            
        }
        
        
        
        
    }
 
    return nil;
}

- (void)didSendSignalRMessage:(DataModelObjectParent*)messageModel currentGroupList:(GroupList*)currentGroupList IsSent:(Boolean)isSent Error:(NSError**)error
{
    MessageDM* currentMessage=(MessageDM*)messageModel;
    ChatSessionRepository * chatSessionRepo =[[ChatSessionRepository alloc]init];
    
    
    NSString *currentdate=[[NSDate alloc] getDateString:[NSDate date]];
    
    NSString *messageType =@"";
    if (currentMessage.contentType != 0) {
        messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
    }
    
    if (currentGroupList != nil)
    {
        
        
        GroupListRepository *groupListRepo=[[GroupListRepository alloc]init];
        
        NSString *messageType =@"";
        if (currentMessage.contentType != 0) {
            messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
        }

        
        // group must be exist before recieving any messages
        
        [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentGroupList.groupName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:FALSE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                     chatSessionGroupID:currentMessage.groupId
                        chatSessionType:messageType
               chatSessionDeliveredDate:currentdate
                    chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                       chatSessionFaild:[NSNumber numberWithBool:FALSE]
                       chatSessionError:@""
                       chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                    chatSessionMediaUrl:@""
                       chatSessionSmily:@""
                    chatSessionLocation:@""
                       chatSessionVCard:@""
                   chatSessionMyMessage:[NSNumber numberWithBool:TRUE]
                    chatSessionChatList:nil
                   chatSessionGroupList:currentGroupList chatSessionBroadcastList:nil Error:error];
        
        
        currentGroupList.groupLastUpdatedDate =currentdate;
        [groupListRepo updateGroupList:currentGroupList Error:error];
        
    }
    
}


- (void)didSendSignalRMessage:(DataModelObjectParent*)messageModel currentBroadcastList:(BroadcastList*)currentBroadcastList IsSent:(Boolean)isSent Error:(NSError**)error
{
    MessageDM* currentMessage=(MessageDM*)messageModel;
    ChatSessionRepository * chatSessionRepo =[[ChatSessionRepository alloc]init];
    
    
    NSString *currentdate=[[NSDate alloc] getDateString:[NSDate date]];
    
    NSString *messageType =@"";
    if (currentMessage.contentType != 0) {
        messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
    }
    
    if (currentBroadcastList != nil)
    {
        BroadcastRepository *broadcastListRepo=[[BroadcastRepository alloc]init];
        
        NSString *messageType =@"";
        if (currentMessage.contentType != 0) {
            messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
        }
        
        
        // group must be exist before recieving any messages
        
        [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentBroadcastList.broadcastName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:FALSE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                     chatSessionGroupID:currentMessage.groupId
                        chatSessionType:messageType
               chatSessionDeliveredDate:currentdate
                    chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                       chatSessionFaild:[NSNumber numberWithBool:FALSE]
                       chatSessionError:@""
                       chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                    chatSessionMediaUrl:@""
                       chatSessionSmily:@""
                    chatSessionLocation:@""
                       chatSessionVCard:@""
                   chatSessionMyMessage:[NSNumber numberWithBool:TRUE]
                    chatSessionChatList:nil
                   chatSessionGroupList:nil chatSessionBroadcastList:currentBroadcastList Error:error];
        
        
        currentBroadcastList.broadcastDate =currentdate;
        [broadcastListRepo updateBroadcastList:currentBroadcastList Error:error];
        
        
        ChatListRepository *chatListRepo = [[ChatListRepository alloc]init];
        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
        User* currentUser=[contactBWS getCurrentUserProfile];
        ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
        for (NSString *toPhoneNumber in currentMessage.toPhoneNumbers) {
            
            ChatList* currentChatList=[chatListRepo getChatListByPhoneNumber:toPhoneNumber Error:error];
            
            if (!currentChatList) {
                
                NSString *fromDetail=@"";
                Contacts* fromContact= [contactsRepo getUserContactByPhoneNumber:toPhoneNumber contactUser:currentUser Error:error];
                
                if(fromContact == nil)
                {
                    fromDetail =currentMessage.fromPhoneNumber;
                }
                else
                {
                    fromDetail =fromContact.contactFullName;
                }
                
                NSArray * toPhoneUsers=[[NSArray alloc]initWithObjects:toPhoneNumber, nil];
                
                
                currentChatList = [chatListRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Chat" chatLastUpdatedDate:currentdate chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:NO] chatID:@"" chatName:fromDetail chatLastMessage:@"" chatUserImage:nil chatUserImageUrl:@"" chatFromPhone:currentUser.userPhoneNumber  chatToPhone:toPhoneNumber chatIsFavourite:[NSNumber numberWithBool:FALSE] chatListOwner: currentUser chatListSession:nil chatListUsers:toPhoneUsers chatIsSecure:[NSNumber numberWithBool:NO] Error:error];
            }
            
            NSString* messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
            
            NSString* groupID=[NSString stringWithFormat:@"%@", currentMessage.groupId];
            
            [chatSessionRepo addChatSession:nil chatSessionFrom:currentChatList.chatName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:FALSE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                         chatSessionGroupID:groupID
                            chatSessionType:messageType
                   chatSessionDeliveredDate:currentdate
                        chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                           chatSessionFaild:[NSNumber numberWithBool:FALSE]
                           chatSessionError:@""
                           chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                        chatSessionMediaUrl:@""
                           chatSessionSmily:@""
                        chatSessionLocation:@""
                           chatSessionVCard:@""
                       chatSessionMyMessage:[NSNumber numberWithBool:TRUE]
                        chatSessionChatList:currentChatList chatSessionGroupList:nil chatSessionBroadcastList:nil Error:error];
            
        }
    
    }

}

- (void)didSendSignalRMessage:(DataModelObjectParent*)messageModel currentChatList:(ChatList*)currentChatList IsSent:(Boolean)isSent Error:(NSError**)error
{
     // message send by me
    MessageDM* currentMessage=(MessageDM*)messageModel;
    ChatSessionRepository * chatSessionRepo =[[ChatSessionRepository alloc]init];
    
    NSString *currentdate=[[NSDate alloc] getDateString:[NSDate date]];
    
    // if ([self.reachbilityInstance isReachable]) {
    
    if ((currentMessage.groupId == (id)[NSNull null] || currentMessage.groupId == nil)&& currentMessage.messageId != nil) {
        // chat list not group
        // ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
        
        if (currentMessage.toPhoneNumbers.count > 1) {
            //conversation chat
            
            
            
            
            NSString *messageType =@"";
            if (currentMessage.contentType != 0) {
                messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
            }
            
            [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentChatList.chatName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:FALSE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                         chatSessionGroupID:currentMessage.groupId
                            chatSessionType:messageType
                   chatSessionDeliveredDate:currentdate
                        chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                           chatSessionFaild:[NSNumber numberWithBool:FALSE]
                           chatSessionError:@""
                           chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                        chatSessionMediaUrl:@""
                           chatSessionSmily:@""
                        chatSessionLocation:@""
                           chatSessionVCard:@""
                       chatSessionMyMessage:[NSNumber numberWithBool:TRUE]
                        chatSessionChatList:currentChatList chatSessionGroupList:nil chatSessionBroadcastList:nil Error:error];
            
            
            
            
        }
        else
        {
            //one to one chat
            
            
            NSString* messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
            
            NSString* groupID=[NSString stringWithFormat:@"%@", currentMessage.groupId];
            
            [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentChatList.chatName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:FALSE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                         chatSessionGroupID:groupID
                            chatSessionType:messageType
                   chatSessionDeliveredDate:currentdate
                        chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                           chatSessionFaild:[NSNumber numberWithBool:FALSE]
                           chatSessionError:@""
                           chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                        chatSessionMediaUrl:@""
                           chatSessionSmily:@""
                        chatSessionLocation:@""
                           chatSessionVCard:@""
                       chatSessionMyMessage:[NSNumber numberWithBool:TRUE]
                        chatSessionChatList:currentChatList chatSessionGroupList:nil chatSessionBroadcastList:nil Error:error];
            
            
            
            
        }
        
    }
    else
    {
        // group message not
        
        GroupListRepository *groupListRepo=[[GroupListRepository alloc]init];
        
        
        
        GroupList* currentGroupList=[groupListRepo getUserGroupListbyGroupID:currentMessage.groupId Error:error];
        
        NSString *messageType =@"";
        if (currentMessage.contentType != 0) {
            messageType=[NSString stringWithFormat:@"%u", currentMessage.contentType];
        }
        
        if (currentGroupList != nil)
        {
            
            // group must be exist before recieving any messages
            
            [chatSessionRepo addChatSession:currentMessage.messageId chatSessionFrom:currentGroupList.groupName chatSessionTo:@"" chatSessionMessage:currentMessage.content chatSessionDate:[NSString stringWithFormat:@"%@", currentMessage.sendingDateTime] chatSessionRead:[NSNumber numberWithBool:FALSE] chatSessionDelivered:[NSNumber numberWithBool:FALSE] chatSessionSecured:[NSNumber numberWithBool:FALSE] chatSessionMessageID:currentMessage.messageId
                         chatSessionGroupID:currentMessage.groupId
                            chatSessionType:messageType
                   chatSessionDeliveredDate:currentdate
                        chatSessionReadDate:nil chatSessionIsSent:[NSNumber numberWithBool:isSent]
                           chatSessionFaild:[NSNumber numberWithBool:FALSE]
                           chatSessionError:@""
                           chatSessionTimer:[NSString stringWithFormat:@"%li", (long)currentMessage.messageTimer]
                        chatSessionMediaUrl:@""
                           chatSessionSmily:@""
                        chatSessionLocation:@""
                           chatSessionVCard:@""
                       chatSessionMyMessage:[NSNumber numberWithBool:TRUE]
                        chatSessionChatList:nil
                       chatSessionGroupList:currentGroupList chatSessionBroadcastList:nil Error:error];
            
            
            currentGroupList.groupLastUpdatedDate =currentdate;
            [groupListRepo updateGroupList:currentGroupList Error:error];
            
        }
        
        
        
        
        
        
    }
    
    
    
    //  }
    

}


- (void)didRecieveSignalRSignalRMarkAsDelivered:(NSString*)messageId OnMessageTime:(NSString*) messageTime IsSent:(Boolean)isSent Error:(NSError**)error{
    ChatSessionRepository * chatSessionRepo =[[ChatSessionRepository alloc]init];
    NSArray * chatSessionAr = [chatSessionRepo getChatSessionsByMessageId:messageId Error:error];
    if (chatSessionAr.count > 0) {
        ChatSession *chatSession = [chatSessionAr objectAtIndex:0];
        [chatSession setChatSessionDelivered:[NSNumber numberWithBool:YES]];
        [chatSession setChatSessionDeliveredDate:messageTime];
        [chatSessionRepo updateChatSession:chatSession Error:error];
    }
}

- (void)didRecieveSignalRSignalRMarkAsSent:(MessageTimeDM*)messageTM   IsSent:(Boolean)isSent Error:(NSError**)error{
    ChatSessionRepository * chatSessionRepo =[[ChatSessionRepository alloc]init];
    NSArray * chatSessionAr = [chatSessionRepo getChatSessionsByMessageId:messageTM.messageId Error:error];
    if (chatSessionAr.count > 0) {
        ChatSession *chatSession = [chatSessionAr objectAtIndex:0];
        [chatSession setChatSessionIsSent:[NSNumber numberWithBool:YES]];
        [chatSession setChatSessionDate:[[NSDate alloc]getServerDateString:messageTM.messageTime] ];
        [chatSessionRepo updateChatSession:chatSession Error:error];
    }
}

- (void)didRecieveSignalRSignalRMarkAsRead:(NSString*)messageId OnMessageTime:(NSString*) messageTime IsSent:(Boolean)isSent Error:(NSError**)error{
    ChatSessionRepository * chatSessionRepo =[[ChatSessionRepository alloc]init];
    NSArray * chatSessionAr = [chatSessionRepo getChatSessionsByMessageId:messageId Error:error];
    if (chatSessionAr.count > 0) {
        ChatSession *chatSession = [chatSessionAr objectAtIndex:0];
        [chatSession setChatSessionRead:[NSNumber numberWithBool:YES]];
        [chatSession setChatSessionReadDate:messageTime];
        [chatSessionRepo updateChatSession:chatSession Error:error];
    }
    
}

@end
