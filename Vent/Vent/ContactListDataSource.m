//
//  ContactListDataSource.m
//  Vent
//
//  Created by Sameh Farouk on 3/12/15.
//  Copyright (c) ITSC. All rights reserved.
//

#import "ContactListDataSource.h"
#import "Contacts.h"
#import "ContactsRepository.h"
#import "ContactsBussinessService.h"

@implementation ContactListDataSource

- (NSArray *)contacts{
    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    User* currentUser=[contactBWS getCurrentUserProfile];
    NSError *error;
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       [contactBWS loadPhoneContactWithOutLoading:currentUser.userCountryCode];

//                     [contactBWS loadPhoneContactWithOutLoading:@"+2"];
                       
                       
                   });
    
    NSArray *contacts;
    if (self.allContacts) {
        contacts=[contactsRepo getAllUserContact:currentUser Error:&error];
    }
    else
    {
    contacts=[contactsRepo getAllVentUserContact:currentUser Error:&error];
    }
//    NSArray *contacts =[contactsRepo getAllUserContact:currentUser Error:&error];
    return contacts;
}

@end
