//
//  MainMasterTableTVC.m
//  Vent
//
//  Created by Medhat Ali on 3/17/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "MainMasterTableTVC.h"
#import "SWTableViewCell.h"
#import "AppUtility.h"
#import "MainContactsTableView.h"
#import "ChatSessionViewController.h"
#import "ActiveChatSessionViewController.h"
#import "ChatList.h"
#import "SignalRContctivtyProtocol.h"
#import "GGFullscreenImageViewController.h"



@interface MainMasterTableTVC () <SignalRContctivtyProtocol>



@end

@implementation MainMasterTableTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    self.tableView.tableHeaderView =nil;
    //reachability handler
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.reachbilityInstance =appDelegate.reachabilityApp;
    
//    if (![self.reachbilityInstance isReachable]) {
//        
//        [self createOfflineView];
//        [self.tableView setTableHeaderView:self.offlineView];
//    }

    NSArray* nibViews= [[NSBundle mainBundle] loadNibNamed:@"TopChatView" owner:self options:nil];
    self.topView = [ nibViews objectAtIndex: 0];
    
    self.topView.contentMode = UIViewContentModeScaleAspectFit;
    self.topView.center = CGPointMake(self.navigationController.navigationBar.frame.size.width / 2.0, self.navigationController.navigationBar.frame.size.height / 2.0);
    self.topView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.topView.lblSubTitle.text=@"";
    self.navigationItem.titleView =self.topView;
    
    
    [self setDelegate:self];
    
    //regist common cell
    [self.tableView registerNib:[UINib nibWithNibName:@"ChatListTableViewCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"ChatListTVCell"];
    
    
    // update search bar orientation
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
    
    
    /// initilaize the handler
    self.GlobalHandlerInstance =[GlobalHandler sharedInstance];
     self.signalRHandlerInstance =[SignalRHandler sharedInstance];
    
    // initilaize theme color
    [self.tabBarController.tabBar setTintColor:[[AppUtility sharedUtilityInstance] colorBaseTheme]];
    
   [self.navigationController.navigationBar setTintColor:[[AppUtility sharedUtilityInstance] colorBaseTheme]];
    
    

    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.topView.lblSubTitle.text= [self.GlobalHandlerInstance getSignalRStatus];
    [[[self navigationController]navigationBar] setHidden:FALSE];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [[[GlobalHandler sharedInstance] SignalRInstance] setDelegate:self];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showConnectionDown
{
    
    UIAlertView *_alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:NSLocalizedString(@"Connection Down", nil)] delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"ok", nil)]  otherButtonTitles:nil];
    [_alert show];
    
}

-(void) createOfflineView
{
    UIView *transView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, self.tableView.frame.size.width, 30)];
    UILabel *viewLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0.f, self.view.frame.size.width , 30)];
    [viewLbl setTextColor:[UIColor whiteColor]];
    [viewLbl setText:NSLocalizedString(@"No Connection",Nil)];
    [viewLbl setTextAlignment:NSTextAlignmentCenter];
    [transView addSubview:viewLbl];
    
    
    //    UIButton *refreshBtn=[[UIButton alloc]initWithFrame:CGRectMake(5, 5, 30, 30)];
    //    [refreshBtn setBackgroundImage:[UIImage imageNamed:@"refresh.png"] forState:UIControlStateNormal];
    //    [refreshBtn addTarget:self action:@selector(checkConnectivity:) forControlEvents:UIControlEventTouchUpInside];
    //    [transView addSubview:refreshBtn];
    transView.backgroundColor = [UIColor grayColor];
    self.offlineView = transView;
}

- (void)configureVerticalMenu
{
    
    NSArray *topMenuList= [[AppUtility sharedUtilityInstance] getTopMenuList];
    
    NSMutableArray *menuItems=[[NSMutableArray alloc]init];
    
    for (NSDictionary *menitem in topMenuList) {
        FCVerticalMenuItem *item = [[FCVerticalMenuItem alloc] initWithTitle:[menitem objectForKey:@"Title"] andIconImage:[UIImage imageNamed:@"settings-icon"]];
        item.actionBlock = ^{
            self.selectedTopMenu =[menitem objectForKey:@"Title"];
            self.selectedTopMenuIndex =[ topMenuList indexOfObject:menitem]+1;
        };
        [menuItems addObject:item];
    }
    
//    FCVerticalMenuItem *item1 = [[FCVerticalMenuItem alloc] initWithTitle:@"First Menu"
//                                                             andIconImage:[UIImage imageNamed:@"settings-icon"]];
//    
//    FCVerticalMenuItem *item2 = [[FCVerticalMenuItem alloc] initWithTitle:@"Second Menu"
//                                                             andIconImage:nil];
//    
//    FCVerticalMenuItem *item3 = [[FCVerticalMenuItem alloc] initWithTitle:@"Third Menu"
//                                                             andIconImage:nil];
//    
//    FCVerticalMenuItem *item4 = [[FCVerticalMenuItem alloc] initWithTitle:@"Fourth Menu"
//                                                             andIconImage:nil];
//    
//    FCVerticalMenuItem *item5 = [[FCVerticalMenuItem alloc] initWithTitle:@"Fifth Menu"
//                                                             andIconImage:nil];
//    
//    FCVerticalMenuItem *item6 = [[FCVerticalMenuItem alloc] initWithTitle:@"Sixth Menu"
//                                                             andIconImage:nil];
//    
//    item1.actionBlock = ^{
//        NSLog(@"test element 1");
//        //        FCFirstViewController *vc = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"FirstViewController"];
//        //        if ([self.viewControllers[0] isEqual:vc])
//        //            return;
//        //
//        //        [self setViewControllers:@[vc] animated:NO];
//    };
//    item2.actionBlock = ^{
//        NSLog(@"test element 2");
//        
//        
//    };
//    item3.actionBlock = ^{
//        NSLog(@"test element 3");
//        
//        
//    };
//    item4.actionBlock = ^{
//        NSLog(@"test element 4");
//        
//        
//    };
//    item5.actionBlock = ^{
//        NSLog(@"test element 5");
//    };
//    item6.actionBlock = ^{
//        NSLog(@"test element 6");
//    };
    
    
 //   _verticalMenu = [[FCVerticalMenu alloc] initWithItems:@[item1, item2, item3, item4, item5, item6]];
    
     _verticalMenu = [[FCVerticalMenu alloc] initWithItems:menuItems];
    _verticalMenu.appearsBehindNavigationBar = YES;
    
    //self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    //self.verticalMenu.liveBlurBackgroundStyle = UIBarStyleBlack;
    _verticalMenu.liveBlurTintColor =[UIColor grayColor];
    
    
    
}


- (NSArray *)rightSwipButtons:(NSInteger)numberofButtons IsBlocked:(NSNumber*)isBlocked IsMute:(NSNumber*)isMute
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:
//     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
//                                                title:@"+Fav"];
    if (numberofButtons == 1) {
        
        if ( [isMute isEqual:[NSNumber numberWithBool:YES]]) {
            [rightUtilityButtons sw_addUtilityButtonWithColor:
             [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                        title:[NSString stringWithFormat:NSLocalizedString(@"unMute", nil)]];
        }
        else
        {
            [rightUtilityButtons sw_addUtilityButtonWithColor:
             [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                        title:[NSString stringWithFormat:NSLocalizedString(@"Mute", nil)]];
        }
        
        if ( [isBlocked isEqual:[NSNumber numberWithBool:YES]]) {
            [rightUtilityButtons sw_addUtilityButtonWithColor:
             [UIColor colorWithRed:0.78f green:0.78f blue:0.9f alpha:1.0]
                                                        title:[NSString stringWithFormat:NSLocalizedString(@"unBlock", nil)]];
        }
        else
        {
            [rightUtilityButtons sw_addUtilityButtonWithColor:
             [UIColor colorWithRed:0.78f green:0.78f blue:0.9f alpha:1.0]
                                                        title:[NSString stringWithFormat:NSLocalizedString(@"Block", nil)]];
        }
        
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"Delete", nil)]];
        
        return rightUtilityButtons;
    }
    
    if ( [isMute isEqual:[NSNumber numberWithBool:YES]]) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"unMute", nil)]];
    }
    else
    {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"Mute", nil)]];
    }
    
    if ( [isBlocked isEqual:[NSNumber numberWithBool:YES]]) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.9f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"unBlock", nil)]];
    }
    else
    {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.9f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"Block", nil)]];
    }
  
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:[NSString stringWithFormat:NSLocalizedString(@"Fav", nil)]];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:[NSString stringWithFormat:NSLocalizedString(@"Delete", nil)]];
    
    return rightUtilityButtons;
}

- (NSArray *)rightSwipButtonsFavourite:(NSInteger)numberofButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    //    [rightUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
    //                                                title:@"+Fav"];

    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:[NSString stringWithFormat:NSLocalizedString(@"-Fav", nil)]];

    
    return rightUtilityButtons;
}

- (NSArray *)rightSwipButtonsGroup:(NSInteger)numberofButtons  IsBlocked:(NSNumber*)isBlocked IsMute:(NSNumber*)isMute

{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    //    [rightUtilityButtons sw_addUtilityButtonWithColor:
    //     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
    //                                                title:@"+Fav"];
    
    
    if ( [isMute isEqual:[NSNumber numberWithBool:YES]]) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"unMute", nil)]];
    }
    else
    {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"Mute", nil)]];
    }
    
    if ( [isBlocked isEqual:[NSNumber numberWithBool:YES]]) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.9f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"unBlock", nil)]];
    }
    else
    {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.78f green:0.78f blue:0.9f alpha:1.0]
                                                    title:[NSString stringWithFormat:NSLocalizedString(@"Block", nil)]];
    }

    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:[NSString stringWithFormat:NSLocalizedString(@"Delete", nil)]];
    
    
    return rightUtilityButtons;
}


#pragma mark - Notifications for Orientation Changes

- (void)deviceOrientationDidChange:(NSNotification *)notification {
    [self.searchController.searchBar sizeToFit];
}


#pragma mark - BaseSearchSelectionDelegate

- (SelectionType)selectionType{
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                  reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                userInfo:nil];
}
- (NSMutableArray*) searchResultsForSearchText:(NSString *)searchText{
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                  reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                userInfo:nil];
}

-(UITableViewCell*)resultsCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

//- (CGFloat)resultsHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    ChatListTableViewCell *cell = [ChatListTableViewCell new];
//    return cell.frame.size.height;
//    
//}

#pragma -mark config navigation bar

-(void)configNavigationBarItems:(BOOL)enableEdit
{
    
    self.enableTableEdit = enableEdit;
    
    if (enableEdit) {
        [self.tableView setEditing:YES animated:YES];
        
       UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(cancelEditClick:)];
        
        self.navigationItem.rightBarButtonItems = @[cancelButton];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Done", nil)] style:UIBarButtonItemStylePlain target:self action:@selector(doneEditClick:)];
        
        self.navigationItem.leftBarButtonItems = @[doneButton];
        
    }
    else
    {
        UIBarButtonItem *addContactButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(selectContact:)];

        self.navigationItem.rightBarButtonItems = @[addContactButton];
        
      //  UIBarButtonItem *optionButton = [[UIBarButtonItem alloc] initWithTitle:@"Option" style:UIBarButtonItemStylePlain target:self action:@selector(moreOPtionsClick:)];
        UIBarButtonItem *optionButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MoreOptions"] style:UIBarButtonItemStylePlain target:self action:@selector(moreOPtionsClick:)];


        self.navigationItem.leftBarButtonItems = @[optionButton];
    
        
    }
    
}

-(void)configNavigationBarItemsForGroup:(BOOL)enableEdit
{
    if (enableEdit) {
        [self.tableView setEditing:YES animated:YES];
        
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(cancelEditClick:)];
        
        self.navigationItem.rightBarButtonItems = @[cancelButton];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Done", nil)] style:UIBarButtonItemStylePlain target:self action:@selector(doneEditClick:)];
        
        self.navigationItem.leftBarButtonItems = @[doneButton];
        
    }
    else
    {
        UIBarButtonItem *addGroupButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(selectNewGroup:)];
        
        self.navigationItem.rightBarButtonItems = @[addGroupButton];
        
        //  UIBarButtonItem *optionButton = [[UIBarButtonItem alloc] initWithTitle:@"Option" style:UIBarButtonItemStylePlain target:self action:@selector(moreOPtionsClick:)];
        UIBarButtonItem *optionButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MoreOptions"] style:UIBarButtonItemStylePlain target:self action:@selector(moreOPtionsClick:)];
        
        
        self.navigationItem.leftBarButtonItems = @[optionButton];
        
        
    }
    
}


#pragma mark - toolbar actions
- (IBAction)selectContact:(id)sender {
    NSLog(@"select Contacts");
    
    ContactsBussinessService *contactBWS=[[ContactsBussinessService alloc]init];
    if (![contactBWS isContatcsAccessGrantInDispath]) {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           
                           
                           UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                           
                           [cantLoadContactsAlert show];
                           
                           
                           
                       });
        
        return;
    }
    
    MainContactsTableView *contactView =[[MainContactsTableView alloc]initWithNibName:@"ContactsTableView" delegate:self];
    
    [[contactView navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Contacts", nil)]];
    //   [[self navigationController] pushViewController:contactView animated:YES];
    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactView];
    
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self showDetailViewController:navController sender:self];
    
    
}



- (IBAction)selectNewGroup:(id)sender {
    NSLog(@"select new group");
    
    [self performSegueWithIdentifier:@"CreateGroup" sender:self];
    
//    MainContactsTableView *contactView =[[MainContactsTableView alloc]initWithNibName:@"ContactsTableView" delegate:self];
//    
//    [[contactView navigationItem] setTitle:@"Contacts"];
//    //   [[self navigationController] pushViewController:contactView animated:YES];
//    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    
//    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactView];
//    
//    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//    [self showDetailViewController:navController sender:self];
    
    
}

- (IBAction)moreOPtionsClick:(id)sender {
    NSLog(@"more OPtions click");
    
    if (self.verticalMenu.isOpen)
        return [self.verticalMenu dismissWithCompletionBlock:nil];
    
    [self.verticalMenu showFromNavigationBar:self.navigationController.navigationBar inView:self.parentViewController.view];
    
    // [_verticalMenu showFromNavigationBar:self.navigationController.navigationBar inView:self.view];
}



-(IBAction)openVerticalMenu:(id)sender
{
    if (self.verticalMenu.isOpen)
        return [self.verticalMenu dismissWithCompletionBlock:nil];
    
    [self.verticalMenu showFromNavigationBar:self.navigationController.navigationBar inView:self.view];
}


-(IBAction)doneEditClick:(id)sender
{
 [self.tableView setEditing:NO animated:YES];
     [self configNavigationBarItems:NO];
}

-(IBAction)cancelEditClick:(id)sender
{
//     [self.tableView setEditing:NO animated:YES];
//    [self configNavigationBarItems:NO];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    if([segue.identifier isEqualToString:@"CreateGroup"]){
        
//        ProfileTableViewController *dest =(ProfileTableViewController *) segue.destinationViewController;
//        dest.registrationData =self.registrationData;
    }
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    
    return height;
}


- (IBAction)selectChatsession:(id)sender
{
//   if (self.totalUnread != 0) {
//       //self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:@"%@(%ld)",self.navigationController.navigationBar.topItem.title , (long)self.totalUnread];
//        
////         self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:@"%@ (%ld)",[NSString stringWithFormat:NSLocalizedString(@"Chats", nil)], (long)self.totalUnread];
//        
//        NSString *title=[NSString stringWithFormat:@"%@ (%ld)",NSLocalizedString(@"Chats", nil), (long)self.totalUnread];
//        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:nil action:nil];
//        self.navigationItem.leftBarButtonItem = backButtonItem;
//        
//        
//    }
//    else
//    {
//         self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:NSLocalizedString(@"Chats", nil)];
//    }
    
    
    ChatList *selectModel = (ChatList *)sender;
    
   ActiveChatSessionViewController * chatsessionVC=[[ActiveChatSessionViewController alloc]initWithNibName:@"ChatSessionViewController" bundle:[NSBundle mainBundle]];
    
    if([selectModel.chatType isEqualToString:@"Conversation"] )
    {
       chatsessionVC.chatTypeSession = ConversationChat ;
    }
    else
    {
      chatsessionVC.chatTypeSession = SingleChat ;
    }
    
    chatsessionVC.selectedChatList =selectModel;
    chatsessionVC.totalUnread =self.totalUnread;
    chatsessionVC.parentMainVC =self;
    [self.navigationController pushViewController:chatsessionVC animated:YES];
    
}


- (IBAction)selectGroupsession:(id)sender
{
//     if (self.totalUnreadGroup != 0) {
////    self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:@"%@(%ld)",self.navigationController.navigationBar.topItem.title , (long)self.totalUnreadGroup];
//         
//           self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:@"%@ (%ld)", [NSString stringWithFormat:NSLocalizedString(@"Groups", nil)],(long)self.totalUnreadGroup];
//         
//     }
//    else
//    {
//        self.navigationController.navigationBar.topItem.title =[NSString stringWithFormat:NSLocalizedString(@"Groups", nil)];
//    }
    
    GroupList *selectModel = (GroupList *)sender;
    
    ActiveChatSessionViewController * chatsessionVC=[[ActiveChatSessionViewController alloc]initWithNibName:@"ChatSessionViewController" bundle:[NSBundle mainBundle]];
    chatsessionVC.chatTypeSession = GroupChat ;
    chatsessionVC.selectedGroupList =selectModel;
    chatsessionVC.totalUnread =self.totalUnread;
    [self.navigationController pushViewController:chatsessionVC animated:YES];
    
}

-(void)getUnreadMessageForTabBar
{
    self.chatListRepo=[[ChatListRepository alloc]init];
    self.totalUnread=[self.chatListRepo getAllUnreadChatListSessions:nil Error:nil];
    if (self.totalUnread != 0) {
        [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%li",(long)self.totalUnread]];
    }
    else
    {
        [[[[[self tabBarController] tabBar] items] objectAtIndex:0] setBadgeValue:nil];
    }
    
    
    self.GroupListRepo=[[GroupListRepository alloc]init];
    self.totalUnreadGroup=[self.GroupListRepo getAllUnreadGroupListSessions:nil Error:nil];
    
    if (self.totalUnreadGroup != 0) {
        [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:[NSString stringWithFormat:@"%li",(long)self.totalUnreadGroup]];
    }
    else
    {
        [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:nil];
    }
}

#pragma mark signalR connection status protocol
-(void)started
{
    //NSLog(@"ChatListTableViewController => GlobalHandler =>  SignalR: started");
    self.topView.lblSubTitle.text = [NSString stringWithFormat:NSLocalizedString(@"connected", nil)];
}

-(void)received:(NSDictionary *) data
{
    self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"connected", nil)] ;
}

-(void)error:(NSError *)error
{
    
}

-(void)closed
{
    
}

-(void)reconnecting
{
    self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"re-connecting", nil)] ;
}

-(void)reconnected
{
    self.topView.lblSubTitle.text =[NSString stringWithFormat:NSLocalizedString(@"re-connected", nil)] ;
}

-(void)stateChanged:(connectionState) connectionState
{
    if (connectionState == connected) {
        self.topView.lblSubTitle.text = [NSString stringWithFormat:NSLocalizedString(@"connected", nil)];
    }
    
}

-(void)connectionSlow
{
    
}

#pragma UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self openSettings];
    }
}

- (void)openSettings
{
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}






@end
