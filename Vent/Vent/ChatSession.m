//
//  ChatSession.m
//  Vent
//
//  Created by Medhat Ali on 7/27/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatSession.h"
#import "BroadcastList.h"
#import "ChatList.h"
#import "GroupList.h"


@implementation ChatSession

@dynamic chatSessionDate;
@dynamic chatSessionDelivered;
@dynamic chatSessionDeliveredDate;
@dynamic chatSessionFrom;
@dynamic chatSessionGroupID;
@dynamic chatSessionID;
@dynamic chatSessionIsSent;
@dynamic chatSessionMessage;
@dynamic chatSessionMessageID;
@dynamic chatSessionRead;
@dynamic chatSessionReadDate;
@dynamic chatSessionSecured;
@dynamic chatSessionTo;
@dynamic chatSessionType;
@dynamic chatSessionFaild;
@dynamic chatSessionError;
@dynamic chatSessionTimer;
@dynamic chatSessionMediaUrl;
@dynamic chatSessionSmily;
@dynamic chatSessionLocation;
@dynamic chatSessionVCard;
@dynamic chatSessionMyMessage;
@dynamic chatSessionBroadcastList;
@dynamic chatSessionChatList;
@dynamic chatSessionGroupList;

@end
