//
//  ChatListTableViewCell.m
//  Vent
//
//  Created by Medhat Ali on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ChatListTableViewCell.h"
#import "ChatList.h"
#import "GroupList.h"
#import "Favourite.h"
#import "ChatSession.h"
#import "ChatListRepository.h"
#import "NSDate+Utilities.h"
#import "GroupListRepository.h"
#import "NSDate+DateFormater.h"
#import "ContactsRepository.h"
#import "ContactsBussinessService.h"
#import "SmilesCollectionViewDataSource.h"

@implementation ChatListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self initializer1];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    

    return self;
}

- (void)awakeFromNib {
    // Initialization code
    
 [self initializer1];

    
}

- (void)initializer1
{
    [[self.profileImage layer] setCornerRadius: self.profileImage.frame.size.width / 2 ];
    [[[self profileImage] layer] setBorderWidth: 2.0f];
    [[[self profileImage] layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self profileImage] setClipsToBounds: YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void)updateCellDataFromModel:(id)model {
   
     self.lblFirstLastName.text=@"";
    
    if ([model isKindOfClass:[ChatList class]]) {
        
        ChatList* currentmodel=(ChatList*)model;
        self.lblFirstName.text = currentmodel.chatName;
        
        if (currentmodel.chatUserImage == Nil) {
            self.lblFirstLastName.text = [self getFirstLastCharInName:currentmodel.chatName];
        }
        else
        {
        self.profileImage.image =[UIImage imageWithData:currentmodel.chatUserImage];
            self.lblFirstLastName.text=@"";
        }
        
        if ( [currentmodel.chatIsBlocked isEqual:[NSNumber numberWithBool:YES]]) {
            //bloque
            self.statusImage.image =[UIImage imageNamed:@"bloque"];
        }
        else if ( [currentmodel.chatMute isEqual:[NSNumber numberWithBool:YES]]) {
            //bloque
            self.statusImage.image =[UIImage imageNamed:@"SoundMute"];
        }
        else
        {
            self.statusImage.image =[UIImage imageNamed:@""];
        }
        
        
        
        ChatListRepository *chatListRepo=[[ChatListRepository alloc]init];
        NSError *error;
        NSArray *chatsession =[chatListRepo getChatListSessions:currentmodel Error:&error];
        
        NSInteger lastMessageIndex=chatsession.count;
        self.lblLastMessage.text=@"";
        self.lblLastMessageDate.text =@"";
        
        if (lastMessageIndex > 0) {
 
            ChatSession *lastchatsession =(ChatSession *)[chatsession objectAtIndex:lastMessageIndex-1];
            if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"1"]) {
                // self.lblLastMessage.text = ((ChatSession*)lastchatsession).chatSessionMessage;
                self.lblLastMessage.attributedText =[[SmilesCollectionViewDataSource sharedInstance] parseStringtoAttributeString:((ChatSession*)lastchatsession).chatSessionMessage FontSize:30];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"3"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Image", nil)];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"4"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Video", nil)];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"5"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Location", nil)];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"6"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Contact", nil)];
            }
           
            //TODO: NSDate+Utilities.h
            
            NSDate * deliveredDate=[[NSDate alloc]getDateFromServerString:((ChatSession*)lastchatsession).chatSessionDeliveredDate];
            
            if (deliveredDate == nil) {
                deliveredDate=[[NSDate alloc]getDateFromString:((ChatSession*)lastchatsession).chatSessionDeliveredDate];
            }
            self.lblLastMessageDate.text = [[NSDate alloc] getShortDateString:deliveredDate];
            
             NSArray *unreadchatsession =[chatListRepo getUnreadChatListSessions:currentmodel Error:&error];
            [self updateNumberOfUnreadMessage:unreadchatsession.count];
            
            
        }
        else
        {
             [self updateNumberOfUnreadMessage:0];
            
            
            NSDate * deliveredDate=[[NSDate alloc]getDateFromServerString:currentmodel.chatLastUpdatedDate];
            self.lblLastMessageDate.text = [[NSDate alloc] getShortDateString:deliveredDate];
            
            
            
        }
        

        
        ContactsRepository *repo=[[ContactsRepository alloc]init];
        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
        
        User* currentUser=[contactBWS getCurrentUserProfile];
        Contacts *currentOne=[repo getUserContactByPhoneNumber:currentmodel.chatToPhone contactUser:currentUser Error:nil];

        if (currentOne) {
            if ([currentOne.contactIsOnline isEqualToNumber:[NSNumber numberWithBool:TRUE]]) {
                [[[self profileImage] layer] setBorderColor:[[UIColor greenColor] CGColor]];
                
            }
            else
            {
                [[[self profileImage] layer] setBorderColor:[[UIColor grayColor] CGColor]];
                
            }
        }
       
        
        
    }
    else if ([model isKindOfClass:[GroupList class]])
    {
        
        GroupList* currentmodel=(GroupList*)model;
        self.lblFirstName.text = currentmodel.groupName;
        
        if (currentmodel.groupImage == Nil) {
            self.lblFirstLastName.text = [self getFirstLastCharInName:currentmodel.groupName];
        }
        else
        {
            self.profileImage.image =[UIImage imageWithData:currentmodel.groupImage];
        }

//        self.lblLastMessage.text = currentmodel.groupLastMessage;
//        self.lblLastMessageDate.text = currentmodel.groupLastUpdatedDate;
//        self.lblFirstLastName.text=@"";
        
        if ([currentmodel.groupIsBlocked isEqual: [NSNumber numberWithBool:YES]]) {
            //bloque
            self.statusImage.image =[UIImage imageNamed:@"bloque"];
        }
        else if ( [currentmodel.groupMuted isEqual:[NSNumber numberWithBool:YES]]) {
            //bloque
            self.statusImage.image =[UIImage imageNamed:@"SoundMute"];
        }
        else
        {
            self.statusImage.image =[UIImage imageNamed:@""];
        }

        
       
        
        GroupListRepository *groupListRepo=[[GroupListRepository alloc]init];
        NSError *error;
        NSArray *groupSession =[groupListRepo getGroupListSessions:currentmodel Error:&error];
        
        NSInteger lastMessageIndex=groupSession.count;
        self.lblLastMessage.text=@"";
        self.lblLastMessageDate.text =@"";
        
        if (lastMessageIndex > 0) {
            
            
            ChatSession *lastchatsession =(ChatSession *)[groupSession objectAtIndex:lastMessageIndex-1];
            if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"1"]) {
                // self.lblLastMessage.text = ((ChatSession*)lastchatsession).chatSessionMessage;
                self.lblLastMessage.attributedText =[[SmilesCollectionViewDataSource sharedInstance] parseStringtoAttributeString:((ChatSession*)lastchatsession).chatSessionMessage FontSize:30];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"3"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Image", nil)];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"4"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Video", nil)];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"5"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Location", nil)];
            }
            else if ([((ChatSession*)lastchatsession).chatSessionType isEqualToString:@"6"]) {
                self.lblLastMessage.text =[NSString stringWithFormat:NSLocalizedString(@"Contact", nil)];
            }
            
            //TODO: NSDate+Utilities.h
            
            NSDate * deliveredDate=[[NSDate alloc]getDateFromServerString:((ChatSession*)lastchatsession).chatSessionDeliveredDate];
            
            
            if (deliveredDate == nil) {
                deliveredDate=[[NSDate alloc]getDateFromString:((ChatSession*)lastchatsession).chatSessionDeliveredDate];
            }
            
            self.lblLastMessageDate.text = [[NSDate alloc] getShortDateString:deliveredDate];
            
            
            
            NSArray *unreadchatsession =[groupListRepo getUnreadGroupListSessions:currentmodel Error:&error];
            [self updateNumberOfUnreadMessage:unreadchatsession.count];
            
            
            
        }
        else
        {
            [self updateNumberOfUnreadMessage:0];
        }
        
   
        
        
    }
     else if ([model isKindOfClass:[Favourite class]])
     {
         
     }
    
    
    
}

-(NSString *)getFirstLastCharInName:(NSString *)fullname
{
    NSString *shortString =@"";
    
    if (![fullname isEqualToString:@""])
    {

        
    NSArray *array = [fullname componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    if (array) {
        if (array.count >1)
        {
         
#warning Sameh first chatacter Sapce(s)
        shortString=[NSString stringWithFormat:@"%@%@",[[array[0] substringToIndex:1]uppercaseString],[[array[1] substringToIndex:1] uppercaseString]];
        }
        else
        {
           shortString=[NSString stringWithFormat:@"%@",[[array[0] substringToIndex:1]uppercaseString]];
        }
    }
   
    }
    return shortString;
}


-(void)updateNumberOfUnreadMessage:(NSInteger)numOfUnreadMesage
{

    if(numOfUnreadMesage > 0)
    {
        [self.lblNumberOfUnreadMessages setHidden:FALSE];
        [self.lblNumberOfUnreadMessages setTitle:[NSString stringWithFormat:@"%li",(long)numOfUnreadMesage] forState:UIControlStateNormal];
        
        [[[self lblNumberOfUnreadMessages] layer] setCornerRadius: self.lblNumberOfUnreadMessages.frame.size.width / 2 ];
        [[[self lblNumberOfUnreadMessages] layer] setBorderWidth: 3.0f];
        [[[self lblNumberOfUnreadMessages] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
        [[self lblNumberOfUnreadMessages] setClipsToBounds: YES];
        
    }
    else
    {
       
        [self.lblNumberOfUnreadMessages setTitle:[NSString stringWithFormat:@"0"] forState:UIControlStateNormal];
         [self.lblNumberOfUnreadMessages setHidden:TRUE];
    }
    
    
 
    
}

@end
