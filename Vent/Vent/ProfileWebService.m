//
//  ProfileWebService.m
//  Vent
//
//  Created by Medhat Ali on 3/8/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "ProfileWebService.h"
#import "ContactsBussinessService.h"


@implementation ProfileWebService

- (id)init
{
    self = [super init];
    if (self) {
        self.serviceName = @"/api/EditProfile";
        self.odataserviceName = @"";
        self.odataserviceNameWithFilter = @"";
    }
    return self;
}

- (NSDictionary *)updateUserProfile:(UserProfileDM *)userProfileDM Error:(NSError**)error
{
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",self.serviceName];
    
    NSMutableDictionary *sentDict = [[NSMutableDictionary alloc]initWithDictionary:[userProfileDM createApiDictionaryFromData:YES]];

    NSDictionary *result = [self.caller postData:sentDict ToWebserviceForResource:serviceURL Error:error];
    
    
    return result;
}


- (bool)addEmailToCurrentUser:(NSString*)email Error:(NSError**)error
{
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",@"/api/AddEmail"];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    NSString * phoneNumber = currentUser.userPhoneNumber;
    
    NSDictionary *sentDict = @{
                             @"PhoneNumber" : phoneNumber,
                             @"EmailAddress" : email,
                             };
    
    
    
    NSInteger result = [self.caller postDataAndGetStatusCode:sentDict ToWebserviceForResource:serviceURL WithHTTPHeader:nil Error:error];
    
    
    return (result == 200);
}


@end
