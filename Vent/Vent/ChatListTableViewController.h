//
//  ChatListTableViewController.h
//  Vent
//
//  Created by Medhat Ali on 3/17/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMasterTableTVC.h"


@interface ChatListTableViewController : MainMasterTableTVC <FCVerticalMenuDelegate,SWTableViewCellDelegate,ContactListDelegate>




@property (nonatomic, retain) NSArray *chatList;

@end
