//
//  BaseSearchTableViewControllerDelegate.h
//  Vent
///Users/sameh/Work/Vent/itsc-repo/Vent/Vent/BaseSearchTableViewControllerDelegate.h
//  Created by Sameh Farouk on 3/24/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#ifndef Vent_BaseSearchTableViewControllerDelegate_h
#define Vent_BaseSearchTableViewControllerDelegate_h
#import "SelectionType.h"

@protocol BaseSearchTableViewControllerDelegate <UITableViewDelegate>
- (SelectionType)selectionType;
- (NSMutableArray*) searchResultsForSearchText:(NSString *)searchText;
- (UITableViewCell*)resultsCellForRowAtIndexPath:(NSIndexPath *)indexPath;

@optional

- (void)didSelectEntry:(NSObject *)entry;
- (void)didSelectEntries:(NSArray *)entries;
- (CGFloat)resultsHeightForRowAtIndexPath:(NSIndexPath *)indexPath;
@end

#endif
