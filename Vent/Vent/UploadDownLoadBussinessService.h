//
//  UploadDownLoadBussinessService.h
//  Vent
//
//  Created by Medhat Ali on 3/9/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "BussinessSevericeController.h"

typedef enum uploadFileTypes
{
    KImage,
    KVideo,
    KAudio
} UploadFileType;


@interface UploadDownLoadBussinessService : BussinessSevericeController

- (NSString *)uploadFileWithType:(UploadFileType)uploadFileType FileData:(NSData *)file fileName:(NSString *)fileName FileExt:(NSString *)fileExt Error:(NSError**)error;
- (NSString *)uploadImage:(NSData *)image ImageName:(NSString *)imageName ImageExt:(NSString *)imageExt Error:(NSError**)error;

- (NSString *)uploadVideo:(NSData *)video VideoName:(NSString *)videoName VideoExt:(NSString *)videoExt Error:(NSError**)error;

- (NSData *)downLoadFile:(NSString *)fileName Error:(NSError**)error;

- (NSData *)downLoadThumbnail:(NSString *)fileName Error:(NSError**)error;

@end
