//
//  WebServiceConnection.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceConnection : NSObject

@property (strong,nonatomic) NSString * cWebServiceURL;

+ (WebServiceConnection *)sharedInstance;

- (NSArray *)getFromWebserviceForResource:(NSString *)resourceName Error:(NSError**)error;
- (NSDictionary *)postData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error;
- (NSDictionary *)postData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName WithHTTPHeader: (NSDictionary *)header Error:(NSError**)error;
-(NSInteger)postDataAndGetStatusCode:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName WithHTTPHeader: (NSDictionary *)header Error:(NSError**)error;
- (NSData *)postDataFile:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error;
- (NSDictionary *)getData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error;
- (NSDictionary *)editData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error;
- (NSDictionary *)editOData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error;
- (NSDictionary *)deleteData:(NSDictionary *)body ToWebserviceForResource:(NSString *)resourceName Error:(NSError**)error;

-(NSDictionary*)postUploadWithError:(NSString*)filename
                           FileType:(NSString*)filetype
                      FileExtension:(NSString*)fileextension
                           FileData:(NSData*)FileData
                  ForWebServiceName:resourceName
                              Error:(NSError**)error;

- (NSDictionary *)postDataForContacts:(NSMutableArray *)body ToWebserviceForResource:(NSString *)resourceName phoneNumber:(NSString *)phoneNumber deviceToken:(NSString *)deviceToken Error:(NSError**)error;

@end
