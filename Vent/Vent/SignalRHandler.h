//
//  SignalRHandler.h
//  Vent
//
//  Created by Medhat Ali on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SignalR.h"
#import "NSString+URLEncoding.h"
#import "SignalRContctivtyProtocol.h"
#import "ChatTopBarView.h"

@interface SignalRHandler : NSObject

+ (SignalRHandler *)sharedInstance;

@property (strong,nonatomic) SRHubConnection *hubConnection;
@property (strong,nonatomic) SRHubProxy *proxy;
@property (nonatomic, strong) id <SignalRContctivtyProtocol> delegate;

@property (strong, nonatomic) ChatTopBarView *topView;

-(SignalRHandler*)initSignalR;

-(void)start;
-(void)stop;

@end
