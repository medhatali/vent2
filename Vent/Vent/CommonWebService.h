//
//  CommonWebService.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceConnection.h"

#define NoNameString @""
#define AuthenticationString @"authentication"

@interface CommonWebService : NSObject

@property (retain, nonatomic) WebServiceConnection *caller;
@property (retain, nonatomic)  NSString *serviceName;
@property (retain, nonatomic)  NSString *odataserviceName;
@property (retain, nonatomic)  NSString *odataserviceNameWithFilter;
@property (retain, nonatomic)  NSMutableArray *modelObjects;


@end
