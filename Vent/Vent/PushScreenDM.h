//
//  PushScreenDM.h
//  Vent
//
//  Created by Medhat Ali on 8/20/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushScreenDM : NSObject <NSCoding>

@property (nonatomic, retain) NSString *screenType; //1 chat , 2 group , 3 broadcast ,
@property (nonatomic, retain) NSString *listId;


@end
