//
//  GroupListUsersPhones.h
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GroupList;

@interface GroupListUsersPhones : NSManagedObject

@property (nonatomic, retain) NSNumber * groupUserEnabled;
@property (nonatomic, retain) NSString * groupUserJoinDate;
@property (nonatomic, retain) NSString * groupUserPhone;
@property (nonatomic, retain) NSNumber * groupUserIsAdmin;
@property (nonatomic, retain) GroupList *phoneGroupList;

@end
