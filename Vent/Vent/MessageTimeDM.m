//
//  MessageTimeDM.m
//  Vent
//
//  Created by Sameh Farouk on 5/26/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "MessageTimeDM.h"
#import "NSDate+DateFormater.h"

@implementation MessageTimeDM

NSString *const tMessageId = @"MessageId";
NSString *const tMessageTime = @"MessageTime";


- (DataModelObjectParent *)getDataModelObjectFromAPIDictionary:(NSDictionary *)dataDictionary
{
    MessageTimeDM *messageTime = [[MessageTimeDM alloc]init];
    
    messageTime.messageId = [dataDictionary objectForKey:tMessageId];

    NSDate *date=[[NSDate alloc] getDatefromServerDateString:[dataDictionary objectForKey:tMessageTime]];
    messageTime.messageTime = date;
    
    return messageTime;
}

- (NSDictionary *)createApiDictionaryFromData:(BOOL)identityFields
{
    
    NSMutableArray *keysArray = [[NSMutableArray alloc]init];
    NSMutableArray *valuesArray = [[NSMutableArray alloc]init];
    
    
    if (self.messageId) {
        [keysArray addObject:tMessageId];
        [valuesArray addObject:self.messageId];
    }
    
    if (self.messageTime) {
        [keysArray addObject:tMessageTime];
        
        NSString *date =[[NSDate alloc] getServerDateString:[self messageTime]];

        
        [valuesArray addObject:date];
    }
    
    NSDictionary *dictionary = [[NSMutableDictionary alloc]initWithObjects:valuesArray forKeys:keysArray];
    
    return dictionary;
}


@end
