//
//  SignalRHandler.m
//  Vent
//
//  Created by Medhat Ali on 5/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "SignalRHandler.h"
#import "MessageDM.h"
#import "ContactsBussinessService.h"
#import "User.h"
#import "AppUtility.h"
#import "SRKeepAliveData.h"



@interface SignalRHandler()

@end


@implementation SignalRHandler

static SignalRHandler *myInstane = nil;


static dispatch_once_t pred;

+ (SignalRHandler *)sharedInstance
{
     dispatch_once(&pred, ^{
        myInstane = [[[self class]allocWithZone:nil]init];

    });
    return myInstane;
}

- (id)initSignalR
{
    self = [super init];
    if (self) {
        /// signal R initilization
        
        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
        
        User* currentUser=[contactBWS getCurrentUserProfile];
        
        if (currentUser != nil) {
            
        
        NSString * phoneNumber = currentUser.userPhoneNumber;
        
        NSDictionary *queryString = @{
                                      @"PhoneNumber" : [phoneNumber urlEncodeUsingEncoding: NSUTF8StringEncoding],
                                      @"DeviceToken" : [currentUser.userDeviceUUID urlEncodeUsingEncoding: NSUTF8StringEncoding],
                                      };
        
        
        /*
         Development 		http://104.40.182.7:54486
         Test		 		http://104.40.182.7:54487
         Stress Test 		http://104.40.154.251:63256
         Production         http://104.40.215.190:63256
         Local              http://10.211.55.3:54486
         */
        
        self.hubConnection = [SRHubConnection connectionWithURL:[[AppUtility sharedUtilityInstance]getServiceUrl ] query:queryString];
        
//        SRKeepAliveData *keepAliveData = [[SRKeepAliveData alloc] initWithLastKeepAlive:[NSDate date] timeout:[NSNumber numberWithInt:5000] timeoutWarning:[NSNumber numberWithInt:5000] checkInterval:[NSNumber numberWithInt:5000]];
//        
//        [self.hubConnection setKeepAliveData:keepAliveData];
        
        
        
        self.proxy = [self.hubConnection createHubProxy:@"ChattingHub"];
        
      __strong typeof(self) weakSelf = self;
        
        self.hubConnection.started = ^{
            NSLog(@"SignalR: Started");
            [weakSelf.delegate started];
        };
        self.hubConnection.received = ^(NSDictionary * data){
            NSLog(@"SignalR: Recived");
            [weakSelf.delegate received:data];
            
        };
        self.hubConnection.error = ^(NSError *error){
            NSLog(@"SignalR: Error %@",error);
            [weakSelf.delegate error:error];
        };
        self.hubConnection.closed = ^{
            NSLog(@"SignalR: Closed");
            
            [weakSelf.delegate closed];
            //[NSThread sleepForTimeInterval:5.0f];
            [weakSelf start];
        };
        self.hubConnection.reconnecting = ^{
            NSLog(@"SignalR: Reconnecting");
            weakSelf.topView.lblSubTitle.text =@"reconnecting";
            [weakSelf.delegate reconnecting];
            
            
        };
        self.hubConnection.reconnected = ^{
            NSLog(@"SignalR: Reconnected");
            weakSelf.topView.lblSubTitle.text =@"reconnected";
            [weakSelf.delegate reconnected];
        };
        self.hubConnection.stateChanged = ^(connectionState connectionState){
            NSLog(@"SignalR: StateChanged %u",connectionState);
            [weakSelf.delegate stateChanged:connectionState];
        };
        self.hubConnection.connectionSlow = ^{
            NSLog(@"SignalR: ConnectionSlow");
            [weakSelf.delegate connectionSlow];
        };
    }
        
    }
    
    return self;
}


-(void)start{
    
    [[self hubConnection] start];

}

-(void) stop{
      [[self hubConnection] stop];
}

@end
