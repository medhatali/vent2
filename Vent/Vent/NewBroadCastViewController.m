//
//  NewBroadCastViewController.m
//  Vent
//
//  Created by Medhat Ali on 6/11/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "NewBroadCastViewController.h"
#import "MainContactsTableView.h"
#import "Contacts.h"
#import "ContactsRepository.h"
#import "GroupListRepository.h"
#import "ContactsBussinessService.h"
#import "BroadcastRepository.h"

@interface NewBroadCastViewController () <UIGestureRecognizerDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ContactListDelegate>


@property BOOL updateUserData;


@end

@implementation NewBroadCastViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.updateUserData = FALSE;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    tapGestureRecognizer.delegate  = self;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    
    
   
    
    
    if(!self.inUpdateMode)
    {
       self.grouContactList =[[NSMutableArray alloc]init];
        //[self.btnCreateUpdate setTitle:@"Create"];
        self.txtListName.text = [NSString stringWithFormat:NSLocalizedString(@"Broadcast Name", nil)];
    }
    else
    {
        self.txtListName.text =self.selectedBroadcastListTemp.broadcastName;
      // [self.btnCreateUpdate setTitle:@"Update"];
    }
    
    self.lblContacts.text =[NSString stringWithFormat:@"%@ (%lu) ",[NSString stringWithFormat:NSLocalizedString(@"Contacts", nil)],(unsigned long)self.grouContactList.count];

    [self.tableView setEditing:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return self.grouContactList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"groupContactCell2" forIndexPath:indexPath];
    
//    if (! cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"groupContactCell2"] ;
//    }
    
    Contacts *selected=(Contacts *)[self.grouContactList objectAtIndex:indexPath.row];
    cell.textLabel.text =selected.contactFullName;
    
    if ([[self.grouContactListNew valueForKey:selected.contactID] isEqualToString:@"Yes"]) {
        [cell.detailTextLabel setText:@"New"];
        [cell.detailTextLabel setTextColor:[UIColor grayColor]];
    }
    else
    {
        cell.detailTextLabel.text = @"";
        [cell.detailTextLabel setTextColor:[UIColor whiteColor]];
    }
    

    cell.imageView.image =[UIImage imageNamed:@"Profile.png"];
    //cell.accessoryType =UITableViewCellAccessoryCheckmark ;
    
    
    [[[cell imageView] layer] setCornerRadius: 20];
    [[[cell imageView] layer] setBorderWidth: 3.0f];
    [[[cell imageView] layer] setBorderColor:[[UIColor whiteColor] CGColor]];
    [[cell imageView] setClipsToBounds: YES];
    
    
    
    return cell;
    
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.grouContactList removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}




- (IBAction)doneClick:(id)sender {
    
    
    if (self.grouContactList.count < 2 || self.grouContactList == nil) {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Min number of Contacts", nil)]
                                                              message:[NSString stringWithFormat:NSLocalizedString(@"Recipients list should contain at least one friend", nil)]
                                                             delegate:nil
                                                    cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        return;
    }
    
    if(!self.inUpdateMode)
    {
    
    NSError *error;
    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
    NSMutableArray *VentPhoneList=[[NSMutableArray alloc]init];
    BroadcastRepository *broadcastListRepo = [[BroadcastRepository alloc]init];
    
    NSString *currentdate=[NSString stringWithFormat:@"%@" ,[NSDate date]];
    
    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    User* currentUser=[contactBWS getCurrentUserProfile];
    
    
    for (Contacts *selectedCont in self.grouContactList) {
        NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:selectedCont Error:&error];
        
        NSLog(@"contact selection %@",ventphonenumber);
        [VentPhoneList addObject:ventphonenumber];
    }
    
    NSArray *phonelist=[[NSArray alloc]initWithArray:VentPhoneList];
    
//    NSString *groupID = [[NSUUID UUID] UUIDString];
    // create group online and local DB
    
    self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Creating Broadcast List", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
    
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(taskQ,
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView showLoadingViewAnimated:YES];
                                      });
                       
                       NSError *error;
                       
                                             
                       bool success = TRUE;
                       
                      
                       
                       if (success) {
                           
                           [broadcastListRepo addNewBroadcastListWithPhoneNumber:currentdate broadcastID:nil broadcastName: self.txtListName.text broadcastListOwner:currentUser broadcastChatSession:nil broadcastListUsers:phonelist Error:&error];
                           
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              [self.navigationController popViewControllerAnimated:YES];
                                          });
                       }
                       else
                       {
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Create Broadcast Error", nil)message:NSLocalizedString(@"error in Create broadcast list", nil)delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
                                              
                                          });
                           
                       }
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.loadingView hideLoadingViewAnimated:YES];
                                      });
                       
                       
                   });

    }
    else
    {
        
        
        NSError *error;
        ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
        NSMutableArray *VentPhoneList=[[NSMutableArray alloc]init];
        BroadcastRepository *broadcastListRepo = [[BroadcastRepository alloc]init];
        
        NSString *currentdate=[NSString stringWithFormat:@"%@" ,[NSDate date]];
        
        ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
        User* currentUser=[contactBWS getCurrentUserProfile];
        
        
        for (Contacts *selectedCont in self.grouContactList) {
            NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:selectedCont Error:&error];
            
            NSLog(@"contact selection %@",ventphonenumber);
            [VentPhoneList addObject:ventphonenumber];
        }
        
        NSArray *phonelist=[[NSArray alloc]initWithArray:VentPhoneList];
        
        //    NSString *groupID = [[NSUUID UUID] UUIDString];
        // create group online and local DB
        
        self.loadingView =[[LoadingView alloc]initWithLabel:[NSString stringWithFormat:NSLocalizedString(@"Update Broadcast List", nil)] withProgressBar:FALSE colorOFActivityIndicator:nil];
        
        dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        dispatch_async(taskQ,
                       ^{
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              [self.loadingView showLoadingViewAnimated:YES];
                                          });
                           
                           NSError *error;
                           
                           
                           
                           bool success = TRUE;
                           
                           
                           
                           if (success) {
                               
                               [broadcastListRepo addNewBroadcastListWithPhoneNumber:currentdate broadcastID:nil broadcastName: self.txtListName.text broadcastListOwner:currentUser broadcastChatSession:nil broadcastListUsers:phonelist Error:&error];
                               
                               [broadcastListRepo deleteBroadcastList:self.selectedBroadcastListTemp Error:nil];
                               
                               dispatch_async(dispatch_get_main_queue(),
                                              ^{
                                                  [self.navigationController popViewControllerAnimated:YES];
                                              });
                           }
                           else
                           {
                               dispatch_async(dispatch_get_main_queue(),
                                              ^{
                                                  [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update Broadcast Error", nil)message:NSLocalizedString(@"error in Create broadcast list", nil)delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
                                                  
                                              });
                               
                           }
                           
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              [self.loadingView hideLoadingViewAnimated:YES];
                                          });
                           
                           
                       });
        
        
    }
    
}


#pragma mark - Contact Selection

- (SelectionType)selectionType{
    
    return kMultipleSelect; //kSingleSelect;
}

- (IBAction)addContactClick:(id)sender {
    
    
    
    ContactsBussinessService *contactBWS=[[ContactsBussinessService alloc]init];
    if (![contactBWS isContatcsAccessGrantInDispath]) {
        
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           
                           
                           UIAlertView *cantLoadContactsAlert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Cannot Load Contact", nil)]  message:[NSString stringWithFormat:NSLocalizedString(@"You must give the app permission to load contacts first.", nil)]  delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]  otherButtonTitles:[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)], nil];
                           
                           [cantLoadContactsAlert show];
                           
                           
                           
                       });
        
        return;
    }
    
    MainContactsTableView *contactView =[[MainContactsTableView alloc]initWithNibName:@"ContactsTableView" delegate:self];
    [contactView setSelectedContacts:_grouContactList];
    [[contactView navigationItem] setTitle:[NSString stringWithFormat:NSLocalizedString(@"Contacts", nil)]];
    //   [[self navigationController] pushViewController:contactView animated:YES];
    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactView];
    
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self showDetailViewController:navController sender:self];
    
}

-(void)didSelectContacts:(Contacts *)contacts
{
    //    NSError *error;
    //    ContactsRepository * contactsRepo=[[ContactsRepository alloc]init];
    //
    //    NSString * ventphonenumber =[contactsRepo getVentPhoneNumberForContact:contacts Error:&error];
    //
    //    NSLog(@"contact selection %@",ventphonenumber);
    //
    //
    //    ChatListRepository * chatlistRepo=[[ChatListRepository alloc]init];
    //
    //    NSArray *phonelist=[[NSArray alloc]initWithObjects:ventphonenumber, nil];
    //    NSString *currentdate=[NSString stringWithFormat:@"%@" ,[NSDate date]];
    //
    //    ContactsBussinessService * contactBWS=[[ContactsBussinessService alloc]init];
    //    User* currentUser=[contactBWS getCurrentUserProfile];
    //
    //
    //    [chatlistRepo addNewChatListWithPhoneNumber:currentdate chatEnabled:[NSNumber numberWithBool:YES] chatType:@"Chat" chatLastUpdatedDate:currentdate  chatSequence:[NSNumber numberWithInt:0] chatMute:[NSNumber numberWithBool:NO] chatID:@"" chatName:contacts.contactFullName chatLastMessage:@"" chatUserImage:contacts.contactImage chatUserImageUrl:@"" chatIsFavourite:[NSNumber numberWithBool:NO]  chatListOwner:currentUser chatListSession:Nil chatListUsers:phonelist Error:&error];
    //
    //    [self reloadData];
}

-(void)didSelectContactsList:(NSArray *)contactsList
{
    NSLog(@"contacts list selection for conversation ");
    self.grouContactListNew =[[ NSMutableDictionary alloc]init];
    

    
    for (Contacts *selectedCont in contactsList) {
        if (self.grouContactList == nil) {
            self.grouContactList =[[NSMutableArray alloc]init];
        }
        
        BOOL contExist=FALSE;
        
        for (Contacts *selectedExistCont in self.grouContactList) {
            if (selectedExistCont == selectedCont) {
                contExist = TRUE;
            }
        }
        
        if (contExist == FALSE) {
            [self.grouContactListNew setObject:@"Yes" forKey:selectedCont.contactID];
            [self.grouContactList addObject:selectedCont];
        }
        
    }
    
    self.lblContacts.text =[NSString stringWithFormat:@"%@ (%lu) ",[NSString stringWithFormat:NSLocalizedString(@"Contacts", nil)],(unsigned long)self.grouContactList.count];
    [self.tableView reloadData];
}


#pragma UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self openSettings];
    }
}

- (void)openSettings
{
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}


@end
