//
//  BaseCountryListViewController.h
//  Vent
//
//  Created by Sameh Farouk on 3/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryListViewDelegate.h"

@class Country;
@class CountryCell;

extern NSString *const kCellIdentifier;

@interface BaseCountryListViewController : UITableViewController

@property (nonatomic, assign) id<CountryListViewDelegate>delegate;

- (void)configureCell:(CountryCell *)cell forCountry:(Country *)country;

@end
