//
//  ResultsCountryListViewController.m
//  Vent
//
//  Created by Sameh Farouk on 3/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "ResultsCountryListViewController.h"
#import "Country.h"
#import "CountryCell.h"

@interface ResultsCountryListViewController ()

@end

@implementation ResultsCountryListViewController

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self filteredCountries] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryCell *cell = (CountryCell *)[self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if(!cell) {
        cell = [[CountryCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellIdentifier];
    }
    
    Country *country =
    [[self filteredCountries] objectAtIndex:[indexPath row]];
    
    [self configureCell:cell forCountry:country];
    
    return cell;
}

@end
