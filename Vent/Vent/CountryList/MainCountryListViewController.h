//
//  MainCountryListViewController.h
//  Vent
//
//  Created by Sameh Farouk on 3/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseCountryListViewController.h"
#import "Country.h"


@interface MainCountryListViewController : BaseCountryListViewController

@property (nonatomic, copy) NSArray *countries;
@property (strong, nonatomic) Country *country;

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate country:(Country*) country;

@end
