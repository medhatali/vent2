//
//  BaseCountryListViewController.m
//  Vent
//
//  Created by Sameh Farouk on 3/9/15.
//  Copyright (c) ITSC. All rights reserved.
//

#import "BaseCountryListViewController.h"
#import "Country.h"
#import "CountryCell.h"

NSString *const kCellIdentifier = @"contactCell";

@interface BaseCountryListViewController ()

@end

@implementation BaseCountryListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)configureCell:(CountryCell *)cell forCountry:(Country *)country {
    
    if (cell) {
        [[cell textLabel] setText:[country name]];
        [[cell detailTextLabel] setText:[country dial_code]];
        
        NSString *countryImage=[NSString stringWithFormat:@"%@.png",[[country code] lowercaseString]];
        [[cell imageView] setImage:[UIImage imageNamed:countryImage]];
    }
}

@end
