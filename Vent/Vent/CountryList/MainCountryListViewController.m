//
//  MainCountryListViewController.m
//  Vent
//
//  Created by Sameh Farouk on 3/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "MainCountryListViewController.h"
#import "ResultsCountryListViewController.h"
#import "Country.h"
#import "CountryCell.h"
#import "CountryListDataSource.h"

@interface MainCountryListViewController () <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController *searchController;

// our secondary search results table view
@property (nonatomic, strong) ResultsCountryListViewController *resultsTableController;

// for state restoration
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@property (nonatomic, strong) CountryListDataSource *dataSource;

@end

@implementation MainCountryListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate country:(Country*) country{
    self = [self initWithNibName:nibNameOrNil delegate:delegate];
    _country = country;
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil delegate:(id)delegate
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        [self setDelegate: delegate];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _dataSource = [[CountryListDataSource alloc] init];
    _countries = [self partitionObjects:[_dataSource countries] collationStringSelector:@selector(name)];
    [self.tableView reloadData];
    
    _resultsTableController = [[ResultsCountryListViewController alloc] init];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.barTintColor = self.navigationController.navigationBar.barTintColor;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    self.resultsTableController.tableView.delegate = self;
    self.searchController.delegate = self;
    //self.searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    
    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    //
    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // restore the searchController's active state
    if (self.searchControllerWasActive) {
        self.searchController.active = self.searchControllerWasActive;
        _searchControllerWasActive = NO;
        
        if (self.searchControllerSearchFieldWasFirstResponder) {
            [self.searchController.searchBar becomeFirstResponder];
            _searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return (tableView == self.tableView) ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count] : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return (tableView == self.tableView) ? [[[self countries] objectAtIndex:section] count]:[[[self resultsTableController] filteredCountries] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CountryCell *cell = (CountryCell *)[self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if(cell == nil) {
        cell = [[CountryCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellIdentifier];
    }
    
    Country *country =
    [[[self countries] objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
    
    [self configureCell:cell forCountry:country];
    
    if ([[[cell textLabel] text] isEqual:[_country name]]) {
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    
    return cell;
}

// here we are the table view delegate for both our main table and filtered table, so we can
// push from the current navigation controller (resultsTableController's parent view controller
// is not this UINavigationController)
//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self delegate] respondsToSelector:@selector(didSelectCountry:)]) {
    Country *selectedCountry = (tableView == self.tableView) ?
    [[[self countries] objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]] :[[[self resultsTableController]filteredCountries]objectAtIndex:[indexPath row]];
        
    
    [self.delegate didSelectCountry:selectedCountry];
    } else {
        NSLog(@"CountryListView Delegate : didSelectCountry is not implemented");
    }
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:[[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    BOOL showSection = [[_countries objectAtIndex:section] count] != 0;
    //only show the section title if there are rows in the section
    return (showSection) ? [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section] : nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    if (title == UITableViewIndexSearch) {
        [tableView scrollRectToVisible:self.searchController.searchBar.frame animated:NO];
        return -1;
    } else {
        return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index-1];
    }
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    NSMutableArray *searchResults = [NSMutableArray arrayWithCapacity:[[[self dataSource] countries] count]];
    
    /*
     Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
     */
    for (NSArray *section in self.countries) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@ OR code contains[c] %@",searchText,searchText];
        [searchResults addObjectsFromArray: [section filteredArrayUsingPredicate:predicate]];
    }
    
    // hand over the filtered results to our search results table
    ResultsCountryListViewController *tableController = (ResultsCountryListViewController *)self.searchController.searchResultsController;
    tableController.filteredCountries = searchResults;
    [tableController.tableView reloadData];
}


#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const CountryViewControllerTitleKey = @"ViewControllerTitleKey";
NSString *const CountrySearchControllerIsActiveKey = @"SearchControllerIsActiveKey";
NSString *const CountrySearchBarTextKey = @"SearchBarTextKey";
NSString *const CountrySearchBarIsFirstResponderKey = @"SearchBarIsFirstResponderKey";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:CountryViewControllerTitleKey];
    
    UISearchController *searchController = self.searchController;
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:CountrySearchControllerIsActiveKey];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:CountrySearchBarIsFirstResponderKey];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:CountrySearchBarTextKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:CountryViewControllerTitleKey];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:CountrySearchControllerIsActiveKey];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:CountrySearchBarIsFirstResponderKey];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:CountrySearchBarTextKey];
}

#pragma mark -

-(NSArray *)partitionObjects:(NSArray *)array collationStringSelector:(SEL)selector{
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    NSInteger sectionCount = [[collation sectionTitles] count]; //section count is take from sectionTitles and not sectionIndexTitles
    NSMutableArray *unsortedSections = [NSMutableArray arrayWithCapacity:sectionCount];
    
    //create an array to hold the data for each section
    for(int i = 0; i < sectionCount; i++)
    {
        [unsortedSections addObject:[NSMutableArray array]];
    }
    
    //put each object into a section
    for (id object in array)
    {
        Country *_countryObj = [[Country alloc] initWithJSONNSData:object];
        NSInteger index = [collation sectionForObject:_countryObj
                              collationStringSelector:selector];
        [[unsortedSections objectAtIndex:index] addObject:_countryObj];
    }
    
    NSMutableArray *sections = [NSMutableArray arrayWithCapacity:sectionCount];
    
    //sort each section
    for (NSMutableArray *section in unsortedSections)
    {
        [sections addObject:[collation sortedArrayFromArray:section collationStringSelector:selector]];
    }
    
    return sections;
}


@end
