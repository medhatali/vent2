//
//  ResultsCountryListViewController.h
//  Vent
//
//  Created by Sameh Farouk on 3/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseCountryListViewController.h"

@interface ResultsCountryListViewController : BaseCountryListViewController

@property (nonatomic, strong) NSMutableArray *filteredCountries;

@end
