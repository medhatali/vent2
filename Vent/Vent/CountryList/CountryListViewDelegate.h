//
//  CountryListViewDelegate.h
//  Vent
//
//  Created by Sameh Farouk on 3/9/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#ifndef Vent_CountryListViewDelegate_h
#define Vent_CountryListViewDelegate_h

@class Country;

@protocol CountryListViewDelegate <NSObject>
- (void)didSelectCountry:(Country *)country;
@end

#endif
