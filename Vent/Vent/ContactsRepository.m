//
//  ContactsRepository.m
//  Vent
//
//  Created by Medhat Ali on 3/10/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "ContactsRepository.h"

@implementation ContactsRepository

- (id)init
{
    self = [super init];
    if (self) {
        self.repositoryEntityName=@"Contacts";
        
    }
    return self;
}

-(Contacts *) addContact:(NSString*)contactFirstName
   contactMiddleName:(NSString*)contactMiddleName
  contactLastName:(NSString*)contactLastName
        contactFullName:(NSString*)contactFullName
 contactIsVentUser:(NSNumber*)contactIsVentUser
contactUser:(User*)contactUser
contactImage:(NSData*)contactImage
contactImageUrl:(NSString*)contactImageUrl
contactID:(NSString*)contactID
Error:(NSError **)error
{
    
    NSManagedObject *newManagedObject = [self createModelForCurrentEntity:error];
    
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:contactFirstName forKey:@"contactFirstName"];
    [newManagedObject setValue:contactMiddleName forKey:@"contactMiddleName"];
    [newManagedObject setValue:contactLastName forKey:@"contactLastName"];
    [newManagedObject setValue:contactFullName forKey:@"contactFullName"];
    [newManagedObject setValue:contactIsVentUser forKey:@"contactIsVentUser"];
   
    [newManagedObject setValue:contactUser forKey:@"contactUser"];
    
    if (contactImage != Nil) {
         [newManagedObject setValue:contactImage forKey:@"contactImage"];
    }
    
     [newManagedObject setValue:contactImageUrl forKey:@"contactImageUrl"];
     [newManagedObject setValue:contactID forKey:@"contactID"];
    
    [self insertModel:newManagedObject EntityName:self.repositoryEntityName Error:error];
    
    return (Contacts *)newManagedObject;
    
    
}



-(ContactPhones *) addPhonesToContacts:(NSString*)phoneNumber
            phoneIsVentNumber:(NSNumber*)phoneIsVentNumber
       contact:(Contacts*)contact
                   Error:(NSError **)error
{
    
    
    NSManagedObject *newManagedObject = [self createModelByEntityName:@"ContactPhones" Error:error];
    
    // If appropriate, configure the new managed object.
    [newManagedObject setValue:phoneNumber forKey:@"phoneNumber"];
    [newManagedObject setValue:phoneIsVentNumber forKey:@"phoneIsVentNumber"];
    [newManagedObject setValue:contact forKey:@"phoneContact"];
    
    
    [self insertModel:newManagedObject EntityName:@"ContactPhones" Error:error];
    
    return (ContactPhones *)newManagedObject;
    

    
    
}

-(void)deleteContactr:(Contacts *)contact Error:(NSError **)error
{
    
    [self deleteModel:contact EntityName:self.repositoryEntityName Error:error];
    
}

-(void)updateContact:(Contacts *)contact Error:(NSError **)error
{
    
    [self updateModel:contact EntityName:self.repositoryEntityName Error:error];
    
}

-(void)deletePhoneNumber:(ContactPhones *)contactphones Error:(NSError **)error
{
    
    [self deleteModel:contactphones EntityName:@"ContactPhones" Error:error];
    
}

-(void)updatePhoneNumber:(ContactPhones *)contactphones Error:(NSError **)error
{
    
    [self updateModel:contactphones EntityName:@"ContactPhones" Error:error];
    
}


-(Contacts*)getUserContactFullName:(NSString*)contactFullName contactUser:(User*)contactUser Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(contactFullName == %@) AND (contactUser == %@) ",contactFullName,contactUser];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return [userdata objectAtIndex:0];
    }
    
    return nil;
}

-(NSArray*)getAllUserContact:(User*)contactUser Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(contactUser == %@) ",contactUser];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return userdata;
    }
    
    return nil;
}

-(NSArray*)getAllVentUserContact:(User*)contactUser Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(contactUser == %@) AND (contactIsVentUser == 1) ",contactUser];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return userdata;
    }
    
    return nil;
}


-(ContactPhones*)getPhoneNumber:(NSString*)phonenumber Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(phoneNumber == %@) ",phonenumber];
    userdata= [self getResultsFromEntity:@"ContactPhones" predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return [userdata objectAtIndex:0];
    }
    
    return nil;
}

-(Contacts*)getUserContactByPhoneNumber:(NSString*)phoneNumber contactUser:(User*)contactUser Error:(NSError **)error
{
    
    if (phoneNumber == nil) {
        return nil;
    }
    
    ContactPhones *selectedPhone=[self getPhoneNumber:phoneNumber Error:error];
    
    
    //check if this is new user not in my contacts + add hime
    
    if (selectedPhone == nil) {
        
       Contacts *newVentuser= [self addContact:phoneNumber contactMiddleName:@"" contactLastName:@"" contactFullName:phoneNumber contactIsVentUser:[NSNumber numberWithBool:YES] contactUser:contactUser contactImage:nil contactImageUrl:nil contactID:nil Error:error];
        if (newVentuser) {
           ContactPhones *newVentuserPhone= [self addPhonesToContacts:phoneNumber phoneIsVentNumber:[NSNumber numberWithBool:YES] contact:newVentuser Error:error];
            
            if (newVentuserPhone) {
                return newVentuser;
            }
            
        }
        
    }
    return selectedPhone.phoneContact;
    
//    NSMutableArray *userdata=[[NSMutableArray alloc]init];
//    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(contactPhones == %@)",selectedPhone];
//    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
//    
//    if (userdata.count >0) {
//        return [userdata objectAtIndex:0];
//    }
//    
//    return nil;
}

-(Contacts*)getUserContactByID:(NSString*)contactID contactUser:(User*)contactUser Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(contactID == %@)",contactID];
    userdata= [self getResultsFromEntity:self.repositoryEntityName predicateOrNil:userpredicate Error:error];
    
    if (userdata.count >0) {
        return [userdata objectAtIndex:0];
    }
    
    return nil;
}

-(NSString*)getVentPhoneNumberForContact:(Contacts*)Contact Error:(NSError **)error
{
    NSMutableArray *userdata=[[NSMutableArray alloc]init];
    NSPredicate *userpredicate = [NSPredicate predicateWithFormat:@"(phoneContact == %@) ",Contact];
    userdata= [self getResultsFromEntity:@"ContactPhones" predicateOrNil:userpredicate Error:error];
    
    if (userdata.count > 0) {
        return ((ContactPhones*)[userdata objectAtIndex:0]).phoneNumber;
    }
    
    return nil;
}
@end
