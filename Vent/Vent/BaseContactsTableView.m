//
//  BaseContactsTableView.m
//  Vent
//
//  Created by Sameh Farouk on 3/10/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseContactsTableView.h"
#import "ContactsTableViewCell.h"
#import "Contacts.h"


@interface BaseContactsTableView ()

@end

@implementation BaseContactsTableView


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if (!_selectedContacts)
        [self setSelectedContacts:[NSMutableArray new]];
}

@end
