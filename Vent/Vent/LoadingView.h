

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LoadingView : UIView

+ (id) sharedManager;

- (id) initWithLabel:(NSString *)loadingViewLabelTitle
     withProgressBar:(BOOL)progressBarRequired
colorOFActivityIndicator:(UIColor *)activityIndicatorColor;

//Showing and Hiding Loading View
- (void) showLoadingViewAnimated:(BOOL)animated;
- (void) hideLoadingViewAnimated:(BOOL)animated;

//Setting Progress Bar Percentage (Not Implemented Yet!)
- (void) setProgress:(float)progressToSet;
- (void) addLoadingViewLabelWithText:(NSString *)loadingViewLabelTitle;

@end
