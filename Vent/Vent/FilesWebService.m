//
//  FilesWebService.m
//  Vent
//
//  Created by Medhat Ali on 3/8/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "FilesWebService.h"

@implementation FilesWebService

- (id)init
{
    self = [super init];
    if (self) {
        self.serviceName = @"/api/File";
        self.odataserviceName = @"";
        self.odataserviceNameWithFilter = @"";
    }
    return self;
}

- (NSData *)downLoadFile:(NSString *)fileName Type:(StoreType) storeType Error:(NSError**)error
{
    FileGetDM *file = [FileGetDM new];
    [file setFileId:fileName];
    [file setType:storeType];
    
    NSDictionary * sentDict = [file createApiDictionaryFromData:NO];
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@%@",self.serviceName,@"/Download"];
    
    
    NSData *result = [self.caller postDataFile:sentDict ToWebserviceForResource:serviceURL Error:error];
    
    
    return result;
}

- (NSDictionary *)uploadImage:(NSData *)image ImageName:(NSString *)imageName ImageExt:(NSString *)imageExt Error:(NSError**)error{
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@%@",self.serviceName,@"/Upload"];
    
   
    
    NSDictionary *result = [self.caller postUploadWithError:imageName FileType:@"image" FileExtension:imageExt FileData:image ForWebServiceName:serviceURL Error:error];
    
    
    return result;
}

- (NSDictionary *)uploadVideo:(NSData *)video VideoName:(NSString *)videoName VideoExt:(NSString *)videoExt Error:(NSError**)error{
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@%@",self.serviceName,@"/Upload"];
    
    
    NSDictionary *result = [self.caller postUploadWithError:videoName FileType:@"video" FileExtension:videoExt FileData:video ForWebServiceName:serviceURL Error:error];
    
    
    return result;
}

@end
