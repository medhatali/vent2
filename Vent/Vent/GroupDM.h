//
//  GroupDM.h
//  Vent
//
//  Created by Sameh Farouk on 4/20/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "DataModelObjectParent.h"

@interface GroupDM : DataModelObjectParent

/// properties for data retrieved
@property (nonatomic, strong) NSString *GroupTitle;
@property (nonatomic, strong) NSMutableArray *MembersPhoneNumber;
@property (nonatomic, strong) NSString *AdminPhoneNumber;
@property (nonatomic, strong) NSString *GroupIconPath;
@property (nonatomic, strong) NSString *GroupId;


@end
