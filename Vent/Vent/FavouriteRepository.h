//
//  FavouriteRepository.h
//  Vent
//
//  Created by Medhat Ali on 4/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BaseRepository.h"
#import "Favourite.h"

@interface FavouriteRepository : BaseRepository


-(Favourite *) addNewFavourite:(NSString*)favouriteID
                 favouriteType:(NSString*)favouriteType
           favouriteChatListID:(NSString*)favouriteChatListID
          favouriteGroupListID:(NSString*)favouriteGroupListID
            favouriteContactID:(NSString*)favouriteContactID
                favouriteOwner:(User*)favouriteOwner
                         Error:(NSError **)error;

-(void)deleteFavourite:(Favourite *)favourite Error:(NSError **)error;

-(void)updateGroupList:(Favourite *)favourite Error:(NSError **)error;

@end
