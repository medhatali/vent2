//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "JSQMessages.h"
#import "Contacts.h"
#import "User.h"

/**
 *  This is for demo/testing purposes only. 
 *  This object sets up some fake model data.
 *  Do not actually do anything like this.
 */

static NSString * const kJSQDemoAvatarDisplayNameSquires = @"Jesse Squires";
static NSString * const kJSQDemoAvatarDisplayNameCook = @"Tim Cook";
static NSString * const kJSQDemoAvatarDisplayNameJobs = @"Jobs";
static NSString * const kJSQDemoAvatarDisplayNameWoz = @"Steve Wozniak";

static NSString * const kJSQDemoAvatarIdSquires = @"053496-4509-289";
static NSString * const kJSQDemoAvatarIdCook = @"468-768355-23123";
static NSString * const kJSQDemoAvatarIdJobs = @"707-8956784-57";
static NSString * const kJSQDemoAvatarIdWoz = @"309-41802-93823";



@interface DemoModelData : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) NSMutableArray *messages;

@property (strong, nonatomic) NSDictionary *avatars;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (strong, nonatomic) NSDictionary *users;

//
//@property (strong, nonatomic) User *senderAvatar;
//@property (strong, nonatomic) Contacts *recieverAvatar;



@property (strong, nonatomic) NSString *senderAvatarID;
@property (strong, nonatomic) NSString *senderAvatarName;
@property (strong, nonatomic) NSString *recieverAvatarID;
@property (strong, nonatomic) NSString *recieverAvatarName;

@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic)  CLLocationManager *locationManager;

@property (strong, nonatomic) id parentVC;

//- (instancetype)initWithSender:(User*)sender Reciever:(Contacts*)reciever;
-(void)stopUpdatingLocation;

- (instancetype)initWithSenderAndReciever:(NSString*)senderId SenderName:(NSString*)senderName SenderImage:(NSData*)senderImage
                                ReciverId:(NSString*)reciverId ReciverName:(NSString*)reciverName ReciverImage:(NSData*)reciverImage;

- (void)loadOldChatSessions:(NSArray*)oldData;

- (void)addPhotoMediaMessage;
- (void)addPhotoMediaMessage:(UIImage*)image;

- (void)addVcardMediaMessage:(UIImage*)image FullName:(NSString*)fullName VCard:(NSString*)vcard;

- (void)addLocationMediaMessageCompletion:(JSQLocationMediaItemCompletionBlock)completion;



- (void)addVideoMediaMessage;
- (void)addVideoMediaMessage:(NSString*)image;

@end
