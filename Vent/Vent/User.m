//
//  User.m
//  Vent
//
//  Created by Medhat Ali on 9/14/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "User.h"
#import "BroadcastList.h"
#import "ChatList.h"
#import "Contacts.h"
#import "Favourite.h"
#import "GroupList.h"


@implementation User

@dynamic userActive;
@dynamic userCountry;
@dynamic userCountryCode;
@dynamic userDate;
@dynamic userDeviceName;
@dynamic userDeviceUUID;
@dynamic userDisplayName;
@dynamic userEmail;
@dynamic userFirstName;
@dynamic userIsRegistered;
@dynamic userIsSecureLogin;
@dynamic userLastName;
@dynamic userPhoneNumber;
@dynamic userPin;
@dynamic userProfileImage;
@dynamic userProfileImageName;
@dynamic userProfileImagePath;
@dynamic userStatus;
@dynamic userIsOnline;
@dynamic userPattern;
@dynamic userEmailVerified;
@dynamic userBroadcast;
@dynamic userChatList;
@dynamic userContatcs;
@dynamic userFavourite;
@dynamic userGroupList;

@end
