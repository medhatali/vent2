//
//  BroadcastList.m
//  Vent
//
//  Created by Medhat Ali on 6/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import "BroadcastList.h"
#import "BroadcastListUserPhones.h"
#import "ChatSession.h"
#import "User.h"


@implementation BroadcastList

@dynamic broadcastID;
@dynamic broadcastName;
@dynamic broadcastDate;
@dynamic broadcastListOwner;
@dynamic broadcastListUsers;
@dynamic broadcastChatSession;

@end
