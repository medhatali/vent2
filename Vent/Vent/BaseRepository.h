//
//  BaseRepository.h
//  Vent
//
//  Created by Medhat Ali on 3/2/15.
//  Copyright (c) 2015 Medhat.Ali@icloud.com. All rights reserved.
//

#import "CoreDataManager.h"
#import "User.h"

#define sortDefault @"sortdefault"
#define sortAtoZ @"sortAtoZ"
#define sorttype @"sorttype"



@interface BaseRepository : CoreDataManager


@property (nonatomic, retain) NSString *repositoryEntityName;


-(NSString *)uniqueUUID;

#pragma -mark Basic Functions

-(void)insertModel:(NSManagedObject*)momodel EntityName:(NSString *)entityName Error:(NSError **)error;

-(void)deleteModel:(NSManagedObject *)momodel EntityName:(NSString *)entityName Error:(NSError **)error;

-(void)updateModel:(NSManagedObject *)momodel EntityName:(NSString *)entityName Error:(NSError **)error;

#pragma -mark methods for all rows

-(NSMutableArray*)getAllRows:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError **)error;

-(void)deleteAllRows:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError **)error;

#pragma -mark common methods

-(NSManagedObject*)createModelByEntityName:(NSString *)entityName Error:(NSError **)error;
-(NSManagedObject*)createModelForCurrentEntity:(NSError **)error;
- (BOOL) saveContext:(NSManagedObjectContext *)context Error:(NSError **)error;


#pragma -mark helper methods

- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName Error:(NSError **)error;
- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil Error:(NSError **)error;

- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil Error:(NSError **)error;

- (NSMutableArray *) getResultsFromEntity:(NSString *)entityName predicateOrNil:(NSPredicate *)predicateOrNil ascSortStringOrNil:(NSArray *)ascSortStringOrNil descSortStringOrNil:(NSArray *)descSortStringOrNil NumberOfRecords:(NSInteger)numberOfRecords Error:(NSError **)error;


@end
