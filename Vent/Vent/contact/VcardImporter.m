//
//  VcardImporter.m
//  AddressBookVcardImport
//
//  Created by Alan Harper on 20/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "VcardImporter.h"
#import "Base64.h"
#import <UIKit/UIKit.h>

@implementation VcardImporter

- (id) init {
    if (self = [super init]) {
       // addressBook = ABAddressBookCreate();
        
         addressBook  = ABAddressBookCreateWithOptions(NULL, nil);

        
    }

    return self;
}

- (void) dealloc {
    CFRelease(addressBook);
}

- (void)parse:(NSString*)vcard
{
   // [self emptyAddressBook];
    
//    NSString *filename = [[NSBundle mainBundle] pathForResource:@"vCards" ofType:@"vcf"];
//    NSLog(@"openning file %@", filename);
//    NSData *stringData = [NSData dataWithContentsOfFile:filename];
//    NSString *vcardString = [[NSString alloc] initWithData:stringData encoding:NSUTF8StringEncoding];
    
    
    NSArray *lines = [vcard componentsSeparatedByString:@"\n"];
    
    for(NSString* line in lines) {
        [self parseLine:line];
    }
    
    //ABAddressBookSave(addressBook, NULL);
    
    
}

-(void)saveVcardToAddress
{
    CFErrorRef error = NULL;
    
    addressBook  = ABAddressBookCreateWithOptions(NULL, nil);

   ABAddressBookAddRecord(addressBook, self.personRecord, &error);
    
    ABAddressBookSave(addressBook, &error);
    
    if (error == nil) {
        UIAlertView *myAlertViewSuccess = [[UIAlertView alloc] initWithTitle:@""
                                                              message:[NSString stringWithFormat:NSLocalizedString(@"Contact saved to Device contacts", nil)]
                                                             delegate:nil
                                                    cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                    otherButtonTitles: nil];
        
        [myAlertViewSuccess show];
    }
    else
    {
        UIAlertView *myAlertViewFail = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Error", nil)]
                                                              message:[NSString stringWithFormat:NSLocalizedString(@"Contact Save Error", nil)]
                                                             delegate:nil
                                                    cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)]
                                                    otherButtonTitles: nil];
        
        [myAlertViewFail show];
    }
    
}

- (void) parseLine:(NSString *)line {
    if (base64image && [line hasPrefix:@"  "]) {
        NSString *trimmedLine = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        base64image = [base64image stringByAppendingString:trimmedLine];
    } else if (base64image) {
        // finished contatenating image string
        [self parseImage];
    } else if ([line hasPrefix:@"BEGIN"]) {
        self.personRecord = ABPersonCreate();
    } else if ([line hasPrefix:@"END"]) {
        ABAddressBookAddRecord(addressBook,self.personRecord, NULL);
    } else if ([line hasPrefix:@"N:"]) {
        [self parseName:line];
    } else if ([line hasPrefix:@"EMAIL;"]) {
        [self parseEmail:line];
    } else if ([line hasPrefix:@"TEL;"]) {
        [self parsePhone:line];
    } else if ([line hasPrefix:@"PHOTO;BASE64"]) {
        base64image = [NSString string];
    }
}

- (void) parseName:(NSString *)line {
    NSArray *upperComponents = [line componentsSeparatedByString:@":"];
    NSArray *components = [[upperComponents objectAtIndex:1] componentsSeparatedByString:@";"];
    ABRecordSetValue (self.personRecord, kABPersonLastNameProperty,(__bridge CFTypeRef)([components objectAtIndex:0]), NULL);
    ABRecordSetValue (self.personRecord, kABPersonFirstNameProperty,(__bridge CFTypeRef)([components objectAtIndex:1]), NULL);
    ABRecordSetValue (self.personRecord, kABPersonPrefixProperty,(__bridge CFTypeRef)([components objectAtIndex:3]), NULL);
}

- (void) parsePhone:(NSString *)line {
    NSArray *upperComponents = [line componentsSeparatedByString:@":"];
    NSRange range=NSMakeRange(0, [[upperComponents objectAtIndex:1] length]-1);
    NSString * telephoney=[[upperComponents objectAtIndex:1] substringWithRange:range];
   // ABRecordSetValue (self.personRecord, kABPersonPhoneProperty,(__bridge CFTypeRef)(telephoney), NULL);
    
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);

    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) telephoney, kABPersonPhoneMainLabel, NULL);

    ABRecordSetValue(self.personRecord, kABPersonPhoneProperty, phoneNumberMultiValue, NULL);

    
    //NSArray *components = [[upperComponents objectAtIndex:1] componentsSeparatedByString:@";"];

}

- (void) parseEmail:(NSString *)line {
    NSArray *mainComponents = [line componentsSeparatedByString:@":"];
    NSString *emailAddress = [mainComponents objectAtIndex:1];
    CFStringRef label;
    ABMutableMultiValueRef multiEmail;
    
    if ([line rangeOfString:@"WORK"].location != NSNotFound) {
        label = kABWorkLabel;
    } else if ([line rangeOfString:@"HOME"].location != NSNotFound) {
        label = kABHomeLabel;
    } else {
        label = kABOtherLabel;
    }

    ABMultiValueRef immutableMultiEmail = ABRecordCopyValue(self.personRecord, kABPersonEmailProperty);
    if (immutableMultiEmail) {
        multiEmail = ABMultiValueCreateMutableCopy(immutableMultiEmail);
    } else {
        multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType); //kABMultiDictionaryPropertyType
    }
    ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(emailAddress), label, NULL);
    ABRecordSetValue(self.personRecord, kABPersonEmailProperty, multiEmail,nil);
    
    CFRelease(multiEmail);
    if (immutableMultiEmail) {
        CFRelease(immutableMultiEmail);
    }
}

- (void) parseImage {
//    NSData *imageData = [Base64 decode:base64image];
//    base64image = nil;
//    ABPersonSetImageData(personRecord, (__bridge CFDataRef)imageData, NULL);
    
}
- (void) emptyAddressBook {
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
    int arrayCount =(int) CFArrayGetCount(people);
    ABRecordRef abrecord;
    
    for (int i = 0; i < arrayCount; i++) {
        abrecord = CFArrayGetValueAtIndex(people, i);
        ABAddressBookRemoveRecord(addressBook,abrecord, NULL);
    }
    CFRelease(people);
}


- (NSString *)vCardRepresentation:(int)ID
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    ABRecordRef contactperson = ABAddressBookGetPersonWithRecordID(addressBookRef,ID);
    
    ABRecordRef person = contactperson;
    ABRecordRef people[1];
    people[0] = person;
    CFArrayRef peopleArray = CFArrayCreate(NULL, (void *)people, 1, &kCFTypeArrayCallBacks);
    NSData *vCardData = CFBridgingRelease(ABPersonCreateVCardRepresentationWithPeople(peopleArray));
    NSString *vCard = [[NSString alloc] initWithData:vCardData encoding:NSUTF8StringEncoding];
    NSLog(@"vCard > %@", vCard);
    
    return vCard;
}


@end
