//
//  ContactsTableViewCell.h
//  Vent
//
//  Created by Sameh Farouk on 3/23/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Contacts;

@interface ContactsTableViewCell : UITableViewCell
@property (weak, nonatomic) UIImageView *profileImage;
@property (weak, nonatomic) NSString *name;
@property (nonatomic) BOOL isVentUser;
@property (strong , nonatomic) Contacts *contacts;

@end
