//
//  SecureTimeLimitTableViewController.m
//  Vent
//
//  Created by Medhat Ali on 12/15/15.
//  Copyright © 2015 ITSC. All rights reserved.
//

#import "SecureTimeLimitTableViewController.h"
#import "CommonHeader.h"

@interface SecureTimeLimitTableViewController ()

@end

@implementation SecureTimeLimitTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//    NSInteger securelimit=    [[NSUserDefaults standardUserDefaults] integerForKey:KVentSecureTimeLimit];
//    NSIndexPath * path=[[NSIndexPath alloc]init];
//    path.row=((securelimit/10)-1);
//    path.section =0;
//    
//    UITableViewCell *cell= [self.tableView cellForRowAtIndexPath:path];
//    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
//
    
    NSIndexPath *indexPath;
    UITableViewCell *cell;
    
    NSInteger sectionCount = [self.tableView numberOfSections];
    for (NSInteger section = 0; section < sectionCount; section++) {
        NSInteger rowCount = [self.tableView numberOfRowsInSection:section];
        for (NSInteger row = 0; row < rowCount; row++) {
            indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            cell = [self.tableView cellForRowAtIndexPath:indexPath];
            //NSLog(@"Section %@ row %@: %@", @(section), @(row), cell.textField.text);
             NSInteger securelimit=    [[NSUserDefaults standardUserDefaults] integerForKey:KVentSecureTimeLimit];
            if (indexPath.row == ((securelimit/10)-1)) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        
            
        }
    }

    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tabBarController.tabBar setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 6;
//}


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//   
////    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
//    
//    // Configure the cell...
//    UITableViewCell *Cell = [tableView cellForRowAtIndexPath:indexPath];
//
//     Cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    
//    
//    return Cell;
//}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected time limit = %ld" , (indexPath.row +1)*10);
    
//    UITableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    
 
    [[NSUserDefaults standardUserDefaults] setInteger: (indexPath.row +1)*10 forKey:KVentSecureTimeLimit];
    
    
//    NSIndexPath *indexPath2;
//    UITableViewCell *cell;
//    
//    NSInteger sectionCount = [self.tableView numberOfSections];
//    for (NSInteger section = 0; section < sectionCount; section++) {
//        NSInteger rowCount = [self.tableView numberOfRowsInSection:section];
//        for (NSInteger row = 0; row < rowCount; row++) {
//            indexPath2 = [NSIndexPath indexPathForRow:row inSection:section];
//            cell = [self.tableView cellForRowAtIndexPath:indexPath];
//            //NSLog(@"Section %@ row %@: %@", @(section), @(row), cell.textField.text);
//            NSInteger securelimit=    [[NSUserDefaults standardUserDefaults] integerForKey:KVentSecureTimeLimit];
//            if (indexPath2.row == ((securelimit/10)-1)) {
//                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
//            }
//            else
//            {
//                [cell setAccessoryType:UITableViewCellAccessoryNone];
//            }
//            
//            
//        }
//    }

    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
       [self.navigationController popToRootViewControllerAnimated:YES];
    });
    
    
}


@end
