//
//  BroadcastListUserPhones.h
//  Vent
//
//  Created by Medhat Ali on 6/12/15.
//  Copyright (c) 2015 ITSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BroadcastList;

@interface BroadcastListUserPhones : NSManagedObject

@property (nonatomic, retain) NSString * broadcastUserPhone;
@property (nonatomic, retain) BroadcastList *broadcastUserList;

@end
